$(function(){

	var x, i, j, selElmnt, a, b, c;
		x = document.getElementsByClassName("custom-select");
	for (i = 0; i < x.length; i++) {
	  selElmnt = x[i].getElementsByTagName("select")[0];
	  selectName = x[i].getAttribute('data-name');
	  a = document.createElement("DIV");
	  a.setAttribute("class", "select-selected");
	  a.innerHTML = '<span class="animationSelect">' + selectName + '</span>';

	  x[i].appendChild(a);
	  b = document.createElement("DIV");
	  b.setAttribute("class", "select-items select-hide");

	  for (j = 1; j < selElmnt.length; j++) {
	    c = document.createElement("DIV");
	    c.innerHTML = selElmnt.options[j].innerHTML;
	    c.setAttribute("rel", selElmnt.options[j].value);
	    c.addEventListener("click", function(e) {

	    	
	    	// Evento click option
	        var y, i, k, s, h;
		        s = this.parentNode.parentNode.getElementsByTagName("select")[0];
		        h = this.parentNode.previousSibling;

		    h.setAttribute("class", "select-selected optionSelected");

	        var selectName = this.parentNode.parentNode.getAttribute('data-name');

	        for (i = 0; i < s.length; i++) {
	          if (s.options[i].innerHTML == this.innerHTML) {
	            s.selectedIndex = i;
	            h.innerHTML = '<span class="animationSelect">' + selectName + '</span>' + this.innerHTML;
	            y = this.parentNode.getElementsByClassName("same-as-selected");
	            for (k = 0; k < y.length; k++) {
	              y[k].removeAttribute("class");
	            }
	            this.setAttribute("class", "same-as-selected");
	            break;
	          }
	        }
	        h.click();
	    });
	    b.appendChild(c);
	  }

	  x[i].appendChild(b);
	  a.addEventListener("click", function(e) {
	    e.stopPropagation();
	    closeAllSelect(this);

	    // ABRIR
	    this.parentNode.getElementsByClassName("animationSelect")[0].classList.add("selected");
	    this.nextSibling.classList.toggle("select-hide");
	    this.classList.toggle("select-arrow-active");
	  });
	}

	function closeAllSelect(elmnt) {
	  var x, y, i, arrNo = [];
	  var hvClas = false;
	  x = document.getElementsByClassName("select-items");
	  y = document.getElementsByClassName("select-selected");
	  for (i = 0; i < y.length; i++) {
	    if (elmnt == y[i]) {
	      arrNo.push(i);
	    } else {
	      y[i].classList.remove("select-arrow-active");
	    }
	  }
	  for (i = 0; i < x.length; i++) {
	    if (arrNo.indexOf(i)) {
	    	z = x[i].getElementsByTagName("DIV");
	    	for (var o = 0; z.length > o; o++) {
	    		if(z[o].classList.contains('same-as-selected')) hvClas = true;
	    	}
	    	
	      x[i].classList.add("select-hide");
	    }
	    if(!hvClas){
	    	x[i].parentNode.getElementsByClassName("animationSelect")[0].classList.remove("selected");
	    }
	  }
	}

	document.addEventListener("click", closeAllSelect);

})