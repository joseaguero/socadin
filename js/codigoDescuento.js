$(function(){
	$(".descuento").on('keyup', function(){
        var codigo = $(this).val();
        var comuna_id;
        var radio = $('input[name="envio"]:checked').val();
        var retiro;

        if (radio == 'direccionCliente') {
        	retiro = 0;
        	comuna_id = $('#comuna').val();
        	if (comuna_id == 'Seleccione Comuna') {
        		comuna_id = 0;
        	}
        }else{
        	retiro = 1;
        	comuna_id = 0;
        }

		$("#loadingCheckDescuento").fadeIn(100);
		$.ajax({
			url         : "tienda/ajax_codigo_descuento.php",
			type        : "post",
			async       : true,
			data		: {codigo:codigo},
			cache: false,
			success     : function(resp){
				if(resp == 1){
					$(".codigoOK").addClass("aceptado");
					$(".codigoOK").html('<i class="fas fa-check fa-lg"></i>');
					$("#loadingCheckDescuento").fadeOut(100);
					$(".descuento").attr('disabled', "disabled");
					/*$(".contCartCompraRapida").load("tienda/carroConDescuento.php");*/
                    $("#contDatosCartVariables").load("tienda/carroConDescuento.php?comuna_id="+comuna_id+"&retiro="+retiro);
                    
				} else {
					$(".codigoOK").removeClass("aceptado");
					$(".codigoOK").html('<i class="fas fa-ban fa-lg"></i>');
					$("#loadingCheckDescuento").fadeOut(100);
					$("#contDatosCartVariables").load("tienda/carroConDescuento.php?comuna_id="+comuna_id+"&retiro="+retiro);
				};
			}
		});
    });
});