$(function(){

	$('.slider-ficha-for').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		fade: true,
		asNavFor: '.slider-ficha-nav',
		infinite: true,
		prevArrow: $('.prevFicha'),
		nextArrow: $('.nextFicha')
	});

	$('.slider-ficha-nav').slick({
		slidesToShow: 4,
		slidesToScroll: 4,
		asNavFor: '.slider-ficha-for',
		dots: false,
		focusOnSelect: true,
		vertical: true,
		arrows: false,
		infinite: false,
	});
	
	 /*Menu responsive */
	 $('.toggle-btn').click(function(){
		$('.filter-btn').toggleClass('open');
	
	 });
	 $('.filter-btn a').click(function(){
		$('.filter-btn').removeClass('open');
	 });
	 
	$('.hamburger').click(function () {
		$('.hamburger').toggleClass('open');
		$('.contMenu').slideToggle(100);
	});
	
	$(".movil").click(function(){
		$("ul.sub").hide();
		console.log("sdfsdfsfd");
		$(this).parent().find("ul.sub").show(100);
		});
		
	$(".abrirFooter").click(function(){
		$(".contrespFooter").hide(100);
		$(this).parent().parent().find(".contrespFooter").show(1000);
		});
	 /*Menu responsive */
	
	/*Buscador responsive*/ 
	$('.iconoBuscador').click(function () {
		$('.buscador').slideToggle();
	});
	
    /*Botonera responsive de identificacion*/
    $(".contBotonesIdentificacionResponsive a").click(function(){
			$(".contBotonesIdentificacionResponsive a").removeClass("active");
			$(this).addClass("active");
			$(".cont50Identificacion").fadeOut(100);
			var tabs = $(this).attr("rel");
			$("#" + tabs).fadeIn(100);
		});
    
    
    /*BOTONERAS ENVIO Y PAGO RESPONSIVE*/
	$(".contBotoneraEnvioYPago a").click(function(){
		$(".contBotoneraEnvioYPago a").removeClass("activo");
		$(this).addClass("activo");
		var tab = $(this).attr("rel");
		$(".contPaso3Movil").fadeOut(100);
		$("#"+tab).fadeIn(100);
		if(tab == "misDatos"){
			$("#facturaMovil").fadeIn(100);
			}
		
		});
	/*BOTONERAS ENVIO Y PAGO RESPONSIVE*/

	$('.SliderProduct').slick({
		infinite: true,
		slidesToShow: 3,
		slidesToScroll: 1,
		prevArrow: $('.prevMove'),
		nextArrow: $('.nextMove')
	});

	$('.SliderProduct-2').slick({
		infinite: true,
		slidesToShow: 3,
		slidesToScroll: 1,
		prevArrow: $('.prevMove2'),
		nextArrow: $('.nextMove2')
	});
    
	$('.btnNewsletter').click(function(){
		var correo = $('.emailNewsletter').val();

		if (correo.trim() != '') {
			if (validarEmail(correo.trim())) {
				$.ajax({
					url: 'ajax/newsletter.php',
					type: 'post',
					dataType: 'json',
					data: { correo: correo },
					beforeSend: function(){
						$('.cont_loading').fadeIn(200);
					}
				}).done(function(res){
					console.log(res);
					if (res['status'] == 'success') {
						$('.content_success_news .correo_news').html(res['message']);
						$('.content_success_news').fadeIn(300);
						$('.emailNewsletter').val('');
					}else{
						var status = res['status'],
							message = res['message'];

						swal('', message, status);

					}
					$('.cont_loading').fadeOut(200);
				}).fail(function(res){
					console.log(res);
					$('.cont_loading').fadeOut(200);
				})
			}else{
				swal('', 'Formato del correo es incorrecto', 'error');
			}
		}else{
			swal('', 'Ingresa un correo', 'warning');
		}
		
	});

	$('.btn_news').on('click', function(){
		$('.content_success_news').fadeOut(300);
	})
/*
Agregar correo al newsletter -------------------------	----------------------	----------------------	
*/
	

/*
GUARDAR NUEVA DIRECCION
*/
$("#guardarDireccion").click(function(){
	var nombre = $("#nombre").val();
	var region = $("#region").val();
	var comuna = $("#comuna").val();
	var localidad = $("#localidad").val();
	var calle = $("#calle").val();
	var numero = $("#numero").val();
	
	console.log(region);
	console.log(comuna);
	console.log(localidad);
	if(nombre != "" && region != "" && comuna != "" && localidad != "" && calle != "" && numero != ""){
		$("#formNuevaDireccion").submit();
		} else {
			console.log("Faltan campos por completar.");
			swal({
			  type: 'error',
			  text: 'Faltan campos por completar.'
			});
		}
	
});


/*Eliminar direcciones guardadas*/	
$(".deleteDireccion").click(function(){
	var id = $(this).attr("rel");
	console.log(id);
	$.ajax({
		url      : "ajax/eliminaDireccion.php",
		type     : "post",
		async    : true,
		data 	 : {id : id},
		success  : function(resp){
			console.log(resp);
			if(resp == 1){
				console.log("dirección eliminada");
				swal({
					  type: 'success',
					  text: 'Dirección eliminada.'
					});
					$("#dir_"+id).hide(300);
				
				} else {
					console.log("No fue posible eliminar dirección.");
						swal({
						  type: 'error',
						  text: '"No fue posible eliminar dirección.'
						});
					}
		}
	});

});
	
	
	

	
	

	
	
	/*
Registro
*/
	$("#btnRegistro").click(function(){
		var nombre = $("#nombre").val();
		var apellido = $('#apellido').val();
		var rut = $("#Rut").val();
		var email = $("#email").val();
		var clave = $("#clave").val();
		var clave2 = $("#clave2").val();

		
		
		if(nombre != '' && email != '' && clave != '' && clave2 !='' && apellido != ''){
			if(email.indexOf('@', 0) == -1 || email.indexOf('.', 0) == -1) {
				/* Correo invalido */
				$("#email").addClass("campo_vacio");
				//alertify.error("Email invalido");
				swal('','Email invalido','error'); 
			} else {
				/* Correo valido */
				if(clave === clave2){
					/* Envio el formulario */
					$("#formularioRegistro").submit();
				} else {
					/* Claves no coinciden */
					//alertify.error("Claves no coinciden");
					swal('','Claves no coinciden','error'); 
					$("#clave").addClass("campo_vacio");
					$("#clave2").addClass("campo_vacio");
				};
			}; /*Fin validacion email */
		} else {
		/* indico que todos los campos deben estar llenos */
			//alertify.error("Faltan campos por completar");
			swal('','Faltan campos por completar','error'); 
			if(nombre == '' ){
				$("#nombre").addClass("campo_vacio");
			} else {
				$("#nombre").removeClass("campo_vacio");
			};

			if(apellido == '' ){
				$("#apellido").addClass("campo_vacio");
			} else {
				$("#apellido").removeClass("campo_vacio");
			};
			
			if(email == '' ){
				$("#email").addClass("campo_vacio");
			} else {
				$("#email").removeClass("campo_vacio");
			};
			
			if(rut == '' ){
				$("#Rut").addClass("campo_vacio");
			} else {
				$("#Rut").removeClass("campo_vacio");
			};
			
			if(clave == '' ){
				$("#clave").addClass("campo_vacio");
			} else {
				$("#clave").removeClass("campo_vacio");
			};
			
			if(clave2 == '' ){
				$("#clave2").addClass("campo_vacio");
			} else {
				$("#clave2").removeClass("campo_vacio");
			};

			
			
		};
		
	});/*fin registro paso 1*/
	
	
	
	
	/*Actualizar */
	$("#btnActualizar").click(function(){
		var nombre = $("#nombre").val();
		var apellido = $('#apellido').val();
		var Rut = $("#Rut").val();
		var email = $("#email").val();
		var telefono = $("#telefono").val();
		var clave = $("#clave").val();
		var clave2 = $("#clave2").val();

		
		if(clave == clave2){
			if(email.indexOf('@', 0) == -1 || email.indexOf('.', 0) == -1) {
				swal('','Email incorrecto','error'); 
				} else {
					$("#formularioActualizar").submit();
					}
			
			} else if(clave == "" && clave2 == ""){
				if(email.indexOf('@', 0) == -1 || email.indexOf('.', 0) == -1) {
					swal('','Email incorrecto','error'); 
					} else {
						$("#formularioActualizar").submit();
						}
			} else {
					swal('','Claves no coinciden','error'); 
				}
				
	});/*fin actualizar1*/
	
	
	
	
	
	
	
	
	/*Inicio de session*/
	$("#inicioSesion").click(function(){
		console.log("entra");
		var email = $("#email").val();
		var password = $("#clave").val();
		
		
		if(email != '' && password != ''){
			if(email.indexOf('@', 0) == -1 || email.indexOf('.', 0) == -1) {
				/* Correo invalido */
				$("#email").addClass("campo_vacio");
				//alertify.error("Email invalido");
				swal('','Email invalido','error'); 
			} else {
				/*envio formulario para log*/
				$("#formularioLogin").submit();
			}; /*Fin validacion email */
		} else {
		/* indico que todos los campos deben estar llenos */
			//alertify.error("Faltan campos por completar");
			swal('','Faltan campos por completar','error'); 
			if(email == '' ){
				$("#email").addClass("campo_vacio");
			} else {
				$("#email").removeClass("campo_vacio");
			};
			
			if(password == '' ){
				$("#clave").addClass("campo_vacio");
			} else {
				$("#clave").removeClass("campo_vacio");
			};
		};
	});/*fin login*/
	
	
	
	
	/*Inicio de session carro de compras*/
	$("#inicioSesionIdentificacion").click(function(){
		
		var email = $("#email").val();
		var password = $("#clave").val();
		
		console.log(password);
		if(email != '' && password != ''){
			if(email.indexOf('@', 0) == -1 || email.indexOf('.', 0) == -1) {
				/* Correo invalido */
				$("#email").addClass("campo_vacio");
				swal('','Email invalido','error'); 
			} else {
				/*envio formulario para log*/
				$(".formInicioSesion").submit();
			}; /*Fin validacion email */
		} else {
		/* indico que todos los campos deben estar llenos */
			swal('','Faltan campos por completar','error'); 
			if(email == '' ){
				$("#email").addClass("campo_vacio");
			} else {
				$("#email").removeClass("campo_vacio");
			};
			
			if(password == '' ){
				$("#clave").addClass("campo_vacio");
			} else {
				$("#clave").removeClass("campo_vacio");
			};
		};
	});/*fin login*/
	
	
	
	
/*Recuperar clave ----------------------------------------------------------------------------------------------------*/
	$('#recuperarClave').click(function(){
		//alert("sdfdsf");
		var correo = $('#email').val();
		if(correo.indexOf('@', 0) == -1 || correo.indexOf('.', 0) == -1) {
            swal('','El correo electrónico introducido no es correcto.','error'); 
			return false;
        } else {
			console.log(correo);
			$("#formRecuperarClave").submit();
		};
	});/*fin funcion*/
/*Recuperar clave ----------------------------------------------------------------------------------------------------*/
	
	
	/* Encvio formulario para crear cuenta a partir de los datos obtenidos de una compra exitosa*/
	$(".btnCuentaExito").click(function(){
		$("#crearCuentaDesdeExito").submit();
		});
	
});







	
	
	/*
Validar rut
*/
	
$("#Rut").Rut({
   on_error: function(){ 
   				//swal(type:'error', text:'');
				swal('Error','Rut incorrecto','error'); 
				$("#Rut").addClass("campo_vacio");
				$("#Rut").val("");
			},
   format_on: 'keyup',
   on_success: function(){ $("#Rut").removeClass("campo_vacio");} 
});

$(".rut-input").Rut({
   format_on: 'keyup',
});

$("#rut_retiro").Rut({
   on_error: function(){ 
   				//swal(type:'error', text:'');
				swal('Error','Rut incorrecto','error'); 
				$("#Rut").addClass("campo_vacio");
				$("#Rut").val("");
			},
   format_on: 'keyup',
   on_success: function(){ $("#Rut").removeClass("campo_vacio");} 
});
$("#RutFactura").Rut({
   on_error: function(){ 
   				alertify.error('El rut ingresado es incorrecto'); 
				$("#RutFactura").addClass("incompleto");
				$("#RutFactura").val("");
			},
   format_on: 'keyup',
   on_success: function(){ $("#RutFactura").removeClass("incompleto");} 
   
});
	
$('.countdown').each(function(){
	var $this = $(this);
	var date = $this.attr('rel');
	var countDownDate = new Date(date).getTime();
	
	var x = setInterval(function() {
		var now = new Date().getTime();
		var distance = countDownDate - now;
		
		var days = Math.floor(distance / (1000 * 60 * 60 * 24));
		var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
		var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
		var seconds = Math.floor((distance % (1000 * 60)) / 1000);
		$this.empty();
		
		if(days == 1){
			$this.append(days + " día " + hours + " Horas ");
		}else if(days > 0){
			$this.append(days + " días " + hours + " Horas ");
		}else if(hours == 1){
			$this.append(hours + " Hora " + minutes + " minutos");
		}else if(hours > 0){
			$this.append(hours + " Horas " + minutes + " minutos");
		}else{
			$this.append(minutes + " Minutos " + seconds + "s");	
		}
		
		if (distance < 0) {
			$this.empty();
			$this.append("TERMINADA");
		}
	}, 1000);
});

$('#spinner').on( "spinstop", function(e){
	var cant 	= $(this).val();
	var id 	= $(this).parent().parent().attr('rel');

	$('.precio-ficha').load('ajax/precio_ficha.php?cant='+cant+'&id='+id);

	//console.log('cant: ' + cant + ' id: ' + id);
});

var popup = setInterval(IsVisible, 3000);

function IsVisible(){
	if ($('.popup-news').is(':visible')){
		$(".bg-popup").click(function() {
			var nombre_popup = $('.bg-popup').attr('data-cliente');
			$('.bg-popup').fadeOut();
		    $('.popup-news').fadeOut();
		    Cookies.set(nombre_popup,'Newsletter (No Suscrito)',{expires: 14});
		});
	}
	clearInterval(popup);
}

$('.popup-news').click(function (e) {
    e.stopPropagation();
});
$(".popup-news .contenido > .close").on('click', function(e){
	var nombre_popup = $('.bg-popup').attr('data-cliente');
	Cookies.set(nombre_popup,'Newsletter (No Suscrito)',{expires: 14});
	$('.bg-popup').fadeOut();
    $('.popup-news').fadeOut();
})

$('#suscribe-newsletter').on('click', function(e){

	e.preventDefault();
	var nombre = $('#nombre-suscribe').val();
	var email = $('#email-suscribe').val();
	var nombre_popup = $('.bg-popup').attr('data-cliente');

	if (nombre.trim() != '' && email.trim() != '') {
		$.ajax({
			url: 'ajax/popup.php',
			type: 'post',
			data: {nombre: nombre, email: email},
			beforeSend: function(){
				$('#suscribe-newsletter').html('ESPERE POR FAVOR...');
			},
			success: function(res){
				Cookies.set(nombre_popup,'Newsletter Suscrito',{expires: 90});
				$('#suscribe-newsletter').html('SUSCRIBIRME');
				$('.bg-popup').fadeOut();
	    		$('.popup-news').fadeOut();
	    		swal('','Gracias por suscribirte.','success');
			}
		})
	}else{
		swal('','Debe completar todos los campos', 'error');
	}
})

$('.head-filtro').on('click', function(){
	if (!$(this).parent().find('.lista-filtro').hasClass('onCatBox')) {
		$(this).parent().find('.lista-filtro').slideToggle(300);
	}else{
		$(this).parent().find('.lista-filtro').slideUp(300, function(){
			$(this).parent().find('.lista-filtro').removeClass('onCatBox');
		});
	}
	
	$(this).toggleClass('head-active');
})

$('.btnSubir').on('click', function(){
	$('html, body').animate( { scrollTop : 0 }, 600 );
})

$('.linkTerminos').on('click', function(){
	var dataScroll = $(this).attr('data-scroll')
		altura = $(dataScroll).offset().top;

	$('html, body').animate( { scrollTop : altura }, 600 );

})


$('.input-form').on('click', function(e){
	e.stopPropagation();
	var label = $(this).find('.label_form');
	var focusObject = ($(this).find('.input_text').length) ? $(this).find('.input_text') : $(this).find('textarea');

	if (!label.hasClass('active')) {
		label.addClass('active');
		focusObject.focus();
	}
})

$('.input-form input[class*="input_text"], .input-form textarea').on('focus', function(e){
	e.stopPropagation();
	var label = $(this).parent().find('.label_form');

	if (!label.hasClass('active')) {
		label.addClass('active');
	}
})

$(document).on('click', function(){
	closeAllInputs();
})

function closeAllInputs(){
	$('.input-form').each(function(i, x){
		var label = $(this).find('.label_form');

		var focusObject = ($(this).find('.input_text').length) ? $(this).find('.input_text') : $(this).find('textarea');

		if (label.hasClass('active')) {
			if (focusObject.val() == '') {
				focusObject.focusout();
				label.removeClass('active');
				focusObject.removeClass('input-active');
			}else{
				focusObject.addClass('input-active');
			}
		}
	})
}

function justNumbers(e){
	var keynum = window.event ? window.event.keyCode : e.which;
    if ((keynum == 8) || (keynum == 46))
    return true;
     
    return /\d/.test(String.fromCharCode(keynum));
}

$('.box_main_qty .box_qty').on('click', function(e){
	e.stopPropagation();

	$(this).parent().find('.box_select_stock').toggle();

})

function openSelectCot(ts){
	$(ts).parent().find('.box_select_stock_cot').toggle();
}

$('.box_main_qty .row_stock_ficha').on('click', function(){

	var data_value  = $(this).attr('data-val'),
		id 			= $('.box_qty').attr('data-id');

	if (data_value != 'add') {
		$('.cantidad_ficha').val(data_value);
		var text_unidad = (parseInt(data_value) > 1) ? 'Unidades' : 'Unidad';
		$('.text_unidad').html(text_unidad);
		// $('.box_price_ajx').load('ajax/precio_ficha.php?cant='+data_value+'&id='+id);
	}else{
		// Más de 10 productos
		$('.cantidad_ficha').val('');
		$('.cantidad_ficha').removeAttr('readonly');
		$('.cantidad_ficha').focus();
		$('.cantidad_ficha').attr('onblur', 'blurSelectFicha(this)');
		$('.text_unidad').html('Unidades');
	}

	$('.box_select_stock').toggle();

})

$('#formCotizacion').on('submit', function(){
	$('.cont_loading').fadeIn(200);

	var nombre = $(this).find('input[name="nombre"]'),
		email = $(this).find('input[name="email"]'),
		telefono = $(this).find('input[name="telefono"]'),
		mensaje = $(this).find('textarea[name="mensaje"]');

	var error = 0;

	if (nombre.val().trim() == '') {
		$(nombre).parent().find('label').addClass('error');
		$(nombre).addClass('error-input');

		formError(nombre, 'Llenar campo obligatorio', 'in');
		$(nombre).focus();
		error = 1;
	}else{
		$(nombre).parent().find('label').removeClass('error');
		$(nombre).removeClass('error-input');

		formError(nombre, '', 'out');
	}

	if (email.val().trim() == '') {
		$(email).parent().find('label').addClass('error');
		$(email).addClass('error-input');

		formError(email, 'Llenar campo obligatorio', 'in');
		$(email).focus();
		error = 1;
	}else{
		if (validarEmail(email.val().trim())) {
			$(email).parent().find('label').removeClass('error');
			$(email).removeClass('error-input');

			formError(email, '', 'out');
		}else{
			$(email).parent().find('label').addClass('error');
			$(email).addClass('error-input');

			formError(email, 'Formato incorrecto', 'in');
			$(email).focus();
			error = 1;
		}
	}

	if (telefono.val().trim() == '') {
		$(telefono).parent().find('label').addClass('error');
		$(telefono).addClass('error-input');

		formError(telefono, 'Llenar campo obligatorio', 'in');
		$(telefono).focus();
		error = 1;
	}else{
		var re = /^([0-9]){8,12}$/;

		if (re.test(telefono.val().trim())) {
			$(telefono).parent().find('label').removeClass('error');
			$(telefono).removeClass('error-input');

			formError(telefono, '', 'out');
		}else{
			$(telefono).parent().find('label').addClass('error');
			$(telefono).addClass('error-input');

			formError(telefono, 'Formato incorrecto', 'in');
			$(telefono).focus();
			error = 1;
		}
		
	}

	if (mensaje.val().trim() == '') {
		$(mensaje).parent().find('label').addClass('error');
		$(mensaje).addClass('error-input');

		formError(mensaje, 'Llenar campo obligatorio', 'in');
		$(mensaje).focus();
		error = 1;
	}else{
		$(mensaje).parent().find('label').removeClass('error');
		$(mensaje).removeClass('error-input');

		formError(mensaje, '', 'out');
	}

	if (error == 1) {
		$('.cont_loading').fadeOut(200);
		return false;
	}else{
		return true;
	}

})

$('.cancelarDir, .bg-addDir').on('click', function(){
	$('.bg-addDir').fadeOut();
	$('.content_new_dir').fadeOut();
})

function rowStockCot(ts){

	var data_value  = $(ts).attr('data-val'),
		id 			= $(ts).parent().parent().find('.box_qty').attr('data-id');

	if (data_value != 'add') {
		$.ajax({
			url: 'ajax/cotizacion/actions.php',
			type: 'post',
			dataType: 'json',
			data: { id: id, qty: data_value, action: 'update' }
		}).done(function(res){
			$('.box_action .qty_cot').load('ajax/cotizacion/qty.php');

			var text_unidad = (parseInt(data_value) > 1) ? 'Unidades' : 'Unidad';
			$(ts).parent().parent().find('.text_unidad').html(text_unidad);
			$(ts).parent().parent().find('.cantidad_ficha').val(data_value);

		}).fail(function(res){
			console.log(res);
		})

		// $('.box_price_ajx').load('ajax/precio_ficha.php?cant='+data_value+'&id='+id);
	}else{
		// Más de 10 productos
		$(ts).parent().parent().find('.cantidad_ficha').val('');
		$(ts).parent().parent().find('.cantidad_ficha').removeAttr('readonly');
		$(ts).parent().parent().find('.cantidad_ficha').focus();
		$(ts).parent().parent().find('.cantidad_ficha').attr('onblur', 'blurSelectCot(this)');
		$(ts).parent().parent().find('.text_unidad').html('Unidades');
	}

	$(ts).parent().parent().find('.box_select_stock_cot').toggle();

}

function cantidadCarro(ts){
	$(ts).parent().find('.box_select_stock_carro').toggle();
}

function rowStockCarro(ts){

	var data_value  = $(ts).attr('data-val'),
		id 			= $(ts).parent().parent().find('.box_qty').attr('data-id');

	if (data_value != 'add') {

		$.ajax({
			type: 'POST',
			url: 'ajax/ajax_validar_stock.php',
			data: {id:id, qty:data_value, change: true},
			cache: false,
			beforeSend: function(){
				$('.cont_loading').fadeIn(100);
			}
		}).done(function(res){
			if(res >= 0){
				$.ajax({
					type: 'GET',
					url: 'tienda/addCarro.php',
					data: {id:id, action:"update", qty:data_value},
					cache: false,
				}).done(function(res){
					$(".cart-resumen .cart").load("tienda/showCart.php");
					$(".cart-resumen .resumenCart").load("tienda/resumenValoresShowCart.php");
					$('.cont_loading').fadeOut(100);
				}).fail(function(res){
					console.log(res);
					$('.cont_loading').fadeOut(100);
				});
			}else{
				$('.cont_loading').fadeOut(100);

				swal('', 'Stock insuficiente','error');
			};

		}).fail(function(res){
			console.log(res);
			$('.cont_loading').fadeOut(100);
		});
		
	}else{
		// Más de 10 productos
		$(ts).parent().parent().find('.cantidad_carro').val('');
		$(ts).parent().parent().find('.cantidad_carro').removeAttr('readonly');
		$(ts).parent().parent().find('.cantidad_carro').focus();
		$(ts).parent().parent().find('.cantidad_carro').attr('onblur', 'blurSelectCarro(this)');
	}

	$('.box_select_stock_carro').toggle();

}

function blurSelectCarro(obj){
	var valor  = $(obj).val(),
		id 	= $(obj).parent().attr('data-id');

	if (valor.trim() != '') {
		$.ajax({
			type: 'POST',
			url: 'ajax/ajax_validar_stock.php',
			data: {id:id, qty:valor, change: true},
			cache: false,
			beforeSend: function(){
				$('.cont_loading').fadeIn(100);
			}
		}).done(function(res){

			if(res >= 0){
				$.ajax({
					type: 'GET',
					url: 'tienda/addCarro.php',
					data: {id:id, action:"update", qty:valor},
					cache: false,
				}).done(function(res){
					$(".cart-resumen .cart").load("tienda/showCart.php");
					$(".cart-resumen .resumenCart").load("tienda/resumenValoresShowCart.php");
					$('.cont_loading').fadeOut(100);
				}).fail(function(res){
					console.log(res);
					$('.cont_loading').fadeOut(100);
				});
			}else{
				$('.cont_loading').fadeOut(100);

				swal('', 'Stock insuficiente','error');
			};

		}).fail(function(res){
			console.log(res);
			$('.cont_loading').fadeOut(100);
		});

		$(obj).removeAttr('onblur');
	}
}

function blurSelectFicha(obj){
	var valor  = $(obj).val(),
		id 	= $(obj).parent().attr('data-id');

	if (valor.trim() != '') {
		$.ajax({
			url: 'ajax/ajax_validar_stock.php',
			type: 'post',
			data: {id: id, qty: valor, change: false}
		}).done(function(res){
			if (res > 0) {
				$('.cantidad_ficha').val(valor);
				$('.box_price_ajx').load('ajax/precio_ficha.php?cant='+valor+'&id='+id);
			}else{
				swal('', 'Stock insuficiente', 'error');
			}
		}).fail(function(res){
			console.log(res);
		})

		$(obj).removeAttr('onblur');
	}

}

function blurSelectCot(obj){
	var valor  = $(obj).val(),
		id 	= $(obj).parent().attr('data-id');

	if (valor.trim() != '') {
		
		$.ajax({
			url: 'ajax/cotizacion/actions.php',
			type: 'post',
			dataType: 'json',
			data: { id: id, qty: valor, action: 'update' }
		}).done(function(res){
			// console.log(res);
			$('.box_action .qty_cot').load('ajax/cotizacion/qty.php');

		}).fail(function(res){
			console.log(res);
		})

		$(obj).removeAttr('onblur');
	}

}

$('.content_tabs_ficha .headTabs .tab').on('click', function(){
	var dataTab = $(this).attr('data-tab'),
		ts = $(this);

	$(this).parent().find('.tab').removeClass('active');

	$('.bodyTabs[class*="showBody"]').fadeOut(300, function(){
		$(this).removeClass('showBody');
		$('#'+dataTab).fadeIn(300);
		$('#'+dataTab).addClass('showBody');
		$(ts).addClass('active');
	})

})

$('#formPagoLogin').on('submit', function(){
	
	var tipoDespacho 		= $('input[name="tipoDespacho"]').val(), // Despacho o retiro
		factura 			= $('input[name="tipoPago"]').is(':checked'),
		razon_social 		= $('input[name="razon_social"]'),
		rut_empresa 		= $('input[name="rut_empresa"]'),
		giro 				= $('input[name="giro"]'),
		direccion_empresa 	= $('input[name="direccion_empresa"]'),
		mis_direcciones 	= $('select[name="mis_direcciones"]'),
		nombre_retiro 		= $('input[name="nombre_retiro"]'),
		rut_retiro 			= $('input[name="rut_retiro"]'),
		terminos 			= $('#terminos').is(':checked'),
		tiro_retiro 		= $('input[name="tiro_retiro"]').val();

	var error = 0;

	if (factura) {
		if (razon_social.val().trim() == '') {
			$(razon_social).parent().find('label').addClass('error');
			$(razon_social).addClass('error-input');

			formError(razon_social, 'Llenar campo obligatorio', 'in');
			$(razon_social).focus();
			error = 1;
		}else{
			$(razon_social).parent().find('label').removeClass('error');
			$(razon_social).removeClass('error-input');

			formError(razon_social, '', 'out');
		}

		if (rut_empresa.val().trim() == '') {
			$(rut_empresa).parent().find('label').addClass('error');
			$(rut_empresa).addClass('error-input');

			formError(rut_empresa, 'Llenar campo obligatorio', 'in');
			$(rut_empresa).focus();
			error = 1;
		}else{
			if ($.rut.validar(rut_empresa.val().trim())) {
				$(rut_empresa).parent().find('label').removeClass('error');
				$(rut_empresa).removeClass('error-input');

				formError(rut_empresa, '', 'out');
			}else{
				$(rut_empresa).parent().find('label').addClass('error');
				$(rut_empresa).addClass('error-input');

				formError(rut_empresa, 'Formato incorrecto', 'in');
				$(rut_empresa).focus();
				$error = 1;
			}
		}

		if (giro.val().trim() == '') {
			$(giro).parent().find('label').addClass('error');
			$(giro).addClass('error-input');

			formError(giro, 'Llenar campo obligatorio', 'in');
			$(giro).focus();
			error = 1;
		}else{
			$(giro).parent().find('label').removeClass('error');
			$(giro).removeClass('error-input');

			formError(giro, '', 'out');
		}

		if (direccion_empresa.val().trim() == '') {
			$(direccion_empresa).parent().find('label').addClass('error');
			$(direccion_empresa).addClass('error-input');

			formError(direccion_empresa, 'Llenar campo obligatorio', 'in');
			$(direccion_empresa).focus();
			error = 1;
		}else{
			$(direccion_empresa).parent().find('label').removeClass('error');
			$(direccion_empresa).removeClass('error-input');

			formError(direccion_empresa, '', 'out');
		}
	}

	if (tipoDespacho == 'retiro') {
		if (tiro_retiro == 'otraPersona') {
			if (nombre_retiro.val().trim() == '') {
				$(nombre_retiro).parent().find('label').addClass('error');
				$(nombre_retiro).addClass('error-input');

				formError(nombre_retiro, 'Llenar campo obligatorio', 'in');
				$(rut_retiro).focus();
				error = 1;
			}else{
				$(nombre_retiro).parent().find('label').removeClass('error');
				$(nombre_retiro).removeClass('error-input');

				formError(nombre_retiro, '', 'out');
			}

			if (rut_retiro.val().trim() == '') {
				$(rut_retiro).parent().find('label').addClass('error');
				$(rut_retiro).addClass('error-input');

				formError(rut_retiro, 'Llenar campo obligatorio', 'in');
				$(rut_retiro).focus();
				error = 1;
			}else{
				$(rut_retiro).parent().find('label').removeClass('error');
				$(rut_retiro).removeClass('error-input');

				formError(rut_retiro, '', 'out');
			}
		}
	}else{
		if (mis_direcciones.val() == 0) {
			$(mis_direcciones).parent().parent().find('.error-description').html('Debes seleccionar una dirección');
			$(mis_direcciones).parent().parent().find('.error-description').fadeIn();
			$(mis_direcciones).parent().find('.select-selected').addClass('error-select');
			$(mis_direcciones).parent().find('.select-selected .animationSelect').addClass('span-select-error');

			swal('', 'Debes seleccionar una dirección', 'warning');

			$(mis_direcciones).focus();
			error = 1;
		}else{
			$(mis_direcciones).parent().parent().find('.error-description').html('');
			$(mis_direcciones).parent().parent().find('.error-description').fadeOut();
			$(mis_direcciones).parent().find('.select-selected').removeClass('error-select');
			$(mis_direcciones).parent().find('.select-selected .animationSelect').removeClass('span-select-error');
		}
	}

	if (error == 1) {
		return false;
	}else{
		console.log('aa');
		if (!terminos) {
			swal('', 'Debes aceptar los términos y condiciones', 'warning');
			return false;
		}
	}

})

// Alertas carro
function closeCart(){
	$('.popupCarro').fadeOut(300, function(){
		$('.popupCarro .head h3').html('Carro de compras <img class="closeCart" onclick="closeCart()" src="img/icons/closeNegro.svg">');
	});
}

$('.open-cart').on('click', function(){
	$('.popupCarro').fadeIn();
})

$('.closeCot').on('click', function(){
	$('.popupCot').fadeOut();
})

// Fin alertas carro

$('.openLogin').on('click', function(){
	$('.bg-pop').fadeIn();
	$('.popLogin').fadeIn();
})

$('.open-login-xs').on('click', function(){
	$('.nav_resp').fadeOut();
	$('.bg-pop').fadeIn();
	$('.popLogin').fadeIn();
})

$('.open-registro-xs').on('click', function(){
	$('.nav_resp').fadeOut();
	$('.bg-pop').fadeIn();
	$('.popRegistro').fadeIn();
})

$('.bg-pop').on('click', function(){
	$('.bg-pop').fadeOut();
	$('.popLogin').fadeOut();
	$('.popRegistro').fadeOut();
	$('.popRecuperar').fadeOut();
})

$('.btn-registro-open').on('click', function(){
	$('.popLogin').fadeOut(300, function(){
		$('.popRegistro').fadeIn();
	});
})

$('.loginOpenReg').on('click', function(){
	$('.popRegistro').fadeOut(300, function(){
		$('.popLogin').fadeIn();
	});
})

$('.login-rec').on('click', function(){
	$('.popRecuperar').fadeOut(300, function(){
		$('.popLogin').fadeIn();
	})
})

$('.reg-rec').on('click', function(){
	$('.popRecuperar').fadeOut(300, function(){
		$('.popLogin').fadeOut();
		$('.popRegistro').fadeIn();
	})
})

$('.recPasswordOpen').on('click', function(){
	$('.popLogin').fadeOut(300, function(){
		$('.popRecuperar').fadeIn();
	});
})

$('.btn-registro').on('click', function(){
	var nombre 		= $('#nombreRegistro').val(),
		email 		= $('#emailRegistro').val(),
		password 	= $('#passRegistro').val(),
		repassword 	= $('#rePassRegistro').val();

	if (nombre.trim() != '' && email.trim() != '' && password.trim() != '' && repassword.trim() != '') {
		if (validarEmail(email.trim())) {
			if (password.trim() == repassword.trim()) {
				$.ajax({
					url: 'ajax/registro.php',
					type: 'POST',
					dataType: 'JSON',
					data: { nombre: nombre.trim(), email: email.trim(), password: password.trim(), repassword: repassword.trim() },
					beforeSend: function(){
						$('.cont_loading').fadeIn(100);
					}
				}).done(function(res){
					var status = res['status'],
						message = res['message'];

					swal('', message, status);

					if (status == 'success') {

						$('.popRegistro').fadeOut(300, function(){
							$('.popLogin').fadeIn();

							$('#nombreRegistro').val(''),
							$('#emailRegistro').val(''),
							$('#passRegistro').val(''),
							$('#rePassRegistro').val('');
						});

					}

					$('.cont_loading').fadeOut(100);

				}).fail(function(res){
					$('.cont_loading').fadeOut(100);
					console.log(res);
				})
			}else{
				swal('', 'Las contraseñas no coinciden', 'error');
			}
		}else{
			swal('', 'Email ingresado incorrecto', 'error');
		}
	}else{
		swal('', 'Debes completar todos los campos', 'error');
	}
})

$('.btn-recuperar').on('click', function(){
	var email 		= $('#emailRecuperar').val().trim();
	if (email != '') {
		if (validarEmail(email)) {
				$.ajax({
					url: 'ajax/recuperar_clave.php',
					type: 'POST',
					dataType: 'JSON',
					data: { email: email.trim() },
					beforeSend: function(){
						$('.cont_loading').fadeIn(100);
					}
				}).done(function(res){
					if (res == 1) {
						swal('Éxito', "Te enviamos una nueva clave a tu email", "success");
						$('.popRegistro').fadeOut(300, function(){
							$('.popLogin').fadeIn();
							$('#emailRecuperar').val('');
						});
					}
					if (res == 2) {
						swal('', "No fue posible enviar una nueva clave.. intente mas tarde", "error");
					}
					if (res == 3) {
						swal('', "Su correo no se encuentra registrado", "error");
					}

					$('.cont_loading').fadeOut(100);

				}).fail(function(res){
					$('.cont_loading').fadeOut(100);
					console.log(res);
				})

		}else{
			swal('', 'Email ingresado incorrecto', 'error');
		}
	}else{
		swal('', 'Debes escribir un email', 'error');
	}
})

$('.btn-login').on('click', function(){
	var email = $('#emailLogin').val(),
		password = $('#passLogin').val();

	if (email.trim() != '' && password.trim() != '') {
		$.ajax({
			url: 'ajax/login.php',
			type: 'POST',
			dataType: 'JSON',
			data: { email: email.trim(), password: password.trim() },
			beforeSend: function(){
				$('.cont_loading').fadeIn(100);
			}
		}).done(function(res){
			var status = res['status'],
				message = res['message'];

			if (status == 'success') {
				location.href = "cuenta";
			}else{
				swal('', message, status);
			}
			$('.cont_loading').fadeOut(100);
		}).fail(function(res){
			console.log(res);
			$('.cont_loading').fadeOut(100);
		})
	}else{
		swal('', 'Debes completar todos los campos', 'error');
	}

})

$('.openDash').on('click', function(){
	$(this).find('.menu-dash-header').fadeToggle();
})

$('.bg-fil-res').on('click', function(){
	$('.bg-fil-res').fadeOut();
	$('.contenedor-filtros').fadeOut();
})

$('.open-fil-res').on('click', function(){
	$('.bg-fil-res').fadeIn();
	$('.contenedor-filtros').fadeIn();
	$('html, body').animate({scrollTop:0}, 300);
})

$(function(){
	$(".select-dim .select-items").on('click', function(){
		var requestType 	= $(this).parent().attr('data-request'),
			valueSelected	= $(this).parent().find('select').val(),
			productId		= $('.gr-dimensiones').attr('data-id');

		var cadenaAncho = $('.select-dim[data-request="ancho"]').find('select').val(),
			cadenaPerfil = $('.select-dim[data-request="perfil"]').find('select').val(),
			cadenaAro = $('.select-dim[data-request="aro"]').find('select').val();

		$.ajax({
			url: 'ajax/selectFicha.php',
			type: 'post',
			dataType: 'json',
			data: { requestType: requestType, valueSelected: valueSelected, cadenaAncho: cadenaAncho, cadenaPerfil: cadenaPerfil, cadenaAro: cadenaAro, producto_id: productId },
			beforeSend: function(){
				$('.cont_loading').fadeIn(100);
			}
		}).done(function(res){
			$('.cont_loading').fadeOut(100);
			// console.log(res);

			if (res['final']) {
				$('.box_main_qty .box_qty').attr('data-id', res['id_hijo']);

				if (res['solo_cotizar'] == 1) {
					$('.precio_ficha').html('');
					$('.gr-accionesCarro .col-cotizar').html('<a href="javascript:void(0)" data-id="'+res['id_hijo']+'" class="btnCotizar" onclick="cotizarProducto(this, '+res['id_hijo']+')">Cotizar</a>');

				}else{
					if (res['descuento'] > 0) {
						$('.precio_ficha').html('\
							<span class="descuento">Antes $'+number_format(res['descuento'], 0)+' + IVA</span>\
							<span class="precio">$'+number_format(res['precio'], 0)+' + IVA</span>');

						$('.ajx_descuento').html('<div class="circulo_descuento">-'+Math.round(100 - (res['descuento'] * 100) / res['precio'])+'%</div>');

					}else{
						$('.precio_ficha').html('<span class="precio">$'+number_format(res['precio'], 0)+' + IVA</span>');
						$('.ajx_descuento').html('');
					}

					$('.gr-accionesCarro').removeClass('gr-accionesCarro-dim');
					$('.gr-accionesCarro .col-remove').remove();
					$('.gr-accionesCarro').append('\
						<div class="col col-cotizar col-remove"><a href="javascript:void(0)" class="btnCotizar" onclick="cotizarProducto(this, '+res['id_hijo']+')">Cotizar</a></div>\
						<div class="col col-remove"><a href="javascript:void(0)" class="btnComprar" onclick="comprarProducto(this, '+res['id_hijo']+')">Agregar al carro</a></div>\
					');
				}

			}else{

				$('.precio_ficha').html('<span class="texto-precio">Selecciona las medidas para ver el precio</span>');
				$('.gr-accionesCarro .col-remove').remove();
				$('.gr-accionesCarro').addClass('gr-accionesCarro-dim');
				$('.gr-accionesCarro').append('\
					<div class="col col-cotizar col-remove">\
                        <a href="javascript:void(0)" class="buttonDisabledFicha">Selecciona las medidas para comprar</a>\
                </div>')
                $('.ajx_descuento').html('');

				if (res['selectAncho'] != "no_action") {
					$('.select-dim[data-request="ancho"]').find('select').html('');
					for (var i = 0; i < res['selectAncho'].length; i++) {
						var valorAncho = res['selectAncho'][i];
						$('.select-dim[data-request="ancho"]').find('select').append('<option value="'+valorAncho+'">'+valorAncho+'</option>');
					}
				}

				if (res['selectPerfil'] != "no_action") {

					var divAro 			= $('.select-dim[data-request="aro"]'),
						divPerfil 		= $('.select-dim[data-request="perfil"]');

					divPerfil.find('select').html('');
					divPerfil.find('.select-items').html('');
					divPerfil.find('.select-selected').removeClass('optionSelected');
					divPerfil.find('.select-selected').html('<span class="animationSelect">Perfil</span>');
					
					divAro.find('.select-selected').removeClass('optionSelected');
					divAro.find('.select-selected').html('<span class="animationSelect">Aro</span>');
					divAro.find('select').html('');
					divAro.find('.select-items').html('');

					for (var i = 0; i < res['selectPerfil'].length; i++) {
						var valorPerfil = res['selectPerfil'][i];

						divPerfil.find('select').append('<option value="'+valorPerfil+'">'+valorPerfil+'</option>');

						divPerfil.find('.select-items').append('<div onclick="rowSelectDim(this)" rel="'+valorPerfil+'">'+valorPerfil+'</div>');

					}
				}

				if (res['selectAro'] != "no_action") {

					var divPerfil 		= $('.select-dim[data-request="perfil"]'),
						divAro			= $('.select-dim[data-request="aro"]');

					divAro.find('.select-selected').removeClass('optionSelected');
					divAro.find('.select-selected').html('<span class="animationSelect">Aro</span>');

					divAro.find('select').html('');
					divAro.find('.select-items').html('');

					for (var i = 0; i < res['selectAro'].length; i++) {
						var valorPerfil = res['selectAro'][i];
						divAro.find('select').append('<option value="'+valorPerfil+'">'+valorPerfil+'</option>');

						divAro.find('.select-items').append('<div onclick="rowSelectDim(this)" rel="'+valorPerfil+'">'+valorPerfil+'</div>');
					}
				}

			}


		}).fail(function(res){
			$('.cont_loading').fadeOut(100);
			console.log(res);
		})

	})

	$(".select-home .select-items").on('click', function(){
		var requestType 	= $(this).parent().attr('data-request'),
			valueSelected	= $(this).parent().find('select').val();

		var cadenaAncho = $('.select-home[data-request="ancho"]').find('select').val(),
			cadenaPerfil = $('.select-home[data-request="perfil"]').find('select').val(),
			cadenaAro = $('.select-home[data-request="aro"]').find('select').val();

		$.ajax({
			url: 'ajax/selecBuscadorHome.php',
			type: 'post',
			dataType: 'json',
			data: { requestType: requestType, valueSelected: valueSelected, cadenaAncho: cadenaAncho, cadenaPerfil: cadenaPerfil, cadenaAro: cadenaAro },
			beforeSend: function(){
				$('.cont_loading').fadeIn(100);
			}
		}).done(function(res){
			$('.cont_loading').fadeOut(100);
			// console.log(res);

			if (res['selectAncho'] != "no_action") {
				$('.select-home[data-request="ancho"]').find('select').html('');
				for (var i = 0; i < res['selectAncho'].length; i++) {
					var valorAncho = res['selectAncho'][i];
					$('.select-home[data-request="ancho"]').find('select').append('<option value="'+valorAncho+'">'+valorAncho+'</option>');
				}
			}

			if (res['selectPerfil'] != "no_action") {

				var divAro 			= $('.select-home[data-request="aro"]'),
					divPerfil 		= $('.select-home[data-request="perfil"]');

				divPerfil.find('select').html('<option value="0">&nbsp;</option>');
				divPerfil.find('.select-items').html('');
				divPerfil.find('.select-selected').removeClass('optionSelected');
				divPerfil.find('.select-selected').html('<span class="animationSelect">Perfil</span>');
				
				divAro.find('.select-selected').removeClass('optionSelected');
				divAro.find('.select-selected').html('<span class="animationSelect">Aro</span>');
				divAro.find('select').html('<option value="0">&nbsp;</option>');
				divAro.find('.select-items').html('');

				for (var i = 0; i < res['selectPerfil'].length; i++) {
					var valorPerfil = res['selectPerfil'][i];

					divPerfil.find('select').append('<option value="'+valorPerfil+'">'+valorPerfil+'</option>');

					divPerfil.find('.select-items').append('<div onclick="rowSelectDim(this)" rel="'+valorPerfil+'">'+valorPerfil+'</div>');

				}
			}

			if (res['selectAro'] != "no_action") {

				var divPerfil 		= $('.select-home[data-request="perfil"]'),
					divAro			= $('.select-home[data-request="aro"]');

				divAro.find('.select-selected').removeClass('optionSelected');
				divAro.find('.select-selected').html('<span class="animationSelect">Aro</span>');

				divAro.find('select').html('<option value="0">&nbsp;</option>');
				divAro.find('.select-items').html('');

				for (var i = 0; i < res['selectAro'].length; i++) {
					var valorPerfil = res['selectAro'][i];
					divAro.find('select').append('<option value="'+valorPerfil+'">'+valorPerfil+'</option>');

					divAro.find('.select-items').append('<div onclick="rowSelectDim(this)" rel="'+valorPerfil+'">'+valorPerfil+'</div>');
				}
			}


		}).fail(function(res){
			$('.cont_loading').fadeOut(100);
			console.log(res);
		})

	})

	$('.select-region .select-items').on('click', function(){
		var valueSelected	= $(this).parent().find('select').val(),
			requestType 	= $(this).parent().attr('data-request'),
			ts				= $(this),
			email 			= $('#emailCompra').val();

		$.ajax({
			url: 'ajax/getComunas.php',
			type: 'post',
			dataType: 'json',
			data: { region_id: valueSelected },
			beforeSend: function(){
				$('.cont_loading').fadeIn(100);
			}
		}).done(function(res){

			$('.cartResumen').load('ajax/cart/showCartResumen.php?comuna=0&email='+email+'&e=1&tp=1');
			$('.content_despachos').fadeOut();

			$('.cont_loading').fadeOut(100);
			$('select[name="comuna_despacho"]').html('<option value="0">&nbsp;</option>')
			$('select[name="comuna_despacho"]').parent().find('.select-items').html('');

			$('select[name="comuna_despacho"]').parent().find('.select-selected').removeClass('optionSelected');
			$('select[name="comuna_despacho"]').parent().find('.select-selected').html('<span class="animationSelect">Comuna</span>');

			$('select[name="comuna_despacho"]').val(0);

			for (var i = 0; i < res.length; i++) {
				$('select[name="comuna_despacho"]').append('<option value="'+res[i]['id']+'">'+res[i]['nombre']+'</option>');

				$('select[name="comuna_despacho"]').parent().find('.select-items').append('<div onclick="rowSelectDim(this)" rel="'+res[i]['id']+'">'+res[i]['nombre']+'</div>');
			}

		}).fail(function(res){
			$('.cont_loading').fadeOut(100);
			console.log(res);
		})

	})

	$('.select-comuna .select-items').on('click', function(){
		var valueSelected	= $(this).parent().find('select').val(),
			requestType 	= $(this).parent().attr('data-request'),
			ts				= $(this),
			email 			= $('#emailCompra').val();

		$.ajax({
			url: 'ajax/cart/valorDespacho.php',
			type: 'post',
			dataType: 'json',
			data: { comuna: valueSelected },
			beforeSend: function(){
				$('.cont_loading').fadeIn(100);
			}
		}).done(function(res){

			$('.content_despachos').fadeIn();

			if (res['estandar'] == 'x') {
				$('.content_despachos .rb-despachos[data-type="estandar"] .precio').html('Por pagar');
			}else{
				$('.content_despachos .rb-despachos[data-type="estandar"] .precio').html('$'+number_format(res['estandar'], 0));
			}

			if (res['rapido'] > 0) {
				$('.content_despachos .rb-despachos[data-type="rapido"]').removeClass('rb-rapido');
			}else{
				$('.content_despachos .rb-despachos[data-type="rapido"]').addClass('rb-rapido');
				$('.content_despachos .rb-despachos[data-type="estandar"] input').prop('checked', true);
			}

			$('.content_despachos .rb-despachos[data-type="rapido"] .precio').html('$'+number_format(res['rapido'], 0));

			$('.cartResumen').load('ajax/cart/showCartResumen.php?comuna='+valueSelected+'&email='+email+'&e=1&tp=1');
			$('.cont_loading').fadeOut(100);

		}).fail(function(res){
			$('.cont_loading').fadeOut(100);
			console.log(res);
		})

	})

	$('.select-direcciones .select-items').on('click', function(){
		var valueSelected	= $(this).parent().find('select').val(),
			requestType 	= $(this).parent().attr('data-request'),
			ts				= $(this),
			email 			= $('input[name="email"]').val();

		$.ajax({
			url: 'ajax/cart/getComunaWithDireccion.php',
			type: 'post',
			data: { dir_id: valueSelected },
			beforeSend: function(){
				$('.cont_loading').fadeIn(100);
			}
		}).done(function(res){

			var comuna_id = res;

			$.ajax({
				url: 'ajax/cart/valorDespacho.php',
				type: 'post',
				dataType: 'json',
				data: { comuna: comuna_id },
			}).done(function(res){

				$('.content_despachos').fadeIn();

				if (res['estandar'] == 'x') {
					$('.content_despachos .rb-despachos[data-type="estandar"] .precio').html('Por pagar');
				}else{
					$('.content_despachos .rb-despachos[data-type="estandar"] .precio').html('$'+number_format(res['estandar'], 0));
				}

				$('.select-direcciones').attr('data-comuna', comuna_id);

				if (res['rapido'] > 0) {
					$('.content_despachos .rb-despachos[data-type="rapido"]').removeClass('rb-rapido');
				}else{
					$('.content_despachos .rb-despachos[data-type="rapido"]').addClass('rb-rapido');
					$('.content_despachos .rb-despachos[data-type="estandar"] input').prop('checked', true);
				}

				$('.content_despachos .rb-despachos[data-type="rapido"] .precio').html('$'+number_format(res['rapido'], 0));	


				$('.cartResumen').load('ajax/cart/showCartResumen.php?comuna='+comuna_id+'&email='+email+'&e=1&tp=1');
				$('.cont_loading').fadeOut(100);

			}).fail(function(res){
				$('.cont_loading').fadeOut(100);
				console.log(res);
			})

		}).fail(function(res){
			$('.cont_loading').fadeOut(100);
			console.log(res);
		})

		

	})

})

$('input[name="metodoDespacho"]').on('change', function(){

	var tipoDespacho = ($(this).val() == 'estandar') ? 1 : 2;

	if ($('.select-direcciones').length == 0) {
		var valueSelected = $('select[name="comuna_despacho"]').val(),
			email = $('#emailCompra').val();
	}else{
		var valueSelected = $('.select-direcciones').attr('data-comuna'),
			email = $('.emailUsuario').val();
	}

	$('.cartResumen').load('ajax/cart/showCartResumen.php?comuna='+valueSelected+'&email='+email+'&e=1&tp='+tipoDespacho);

})

$('#btnEmailCompra').on('click', function(e){
	e.preventDefault();
	var obligatorios 	= true;
	var email = $('#emailCompra').val();

	if(!email){ $('#emailCompra').css("border","1px solid #044fe6"); $('#emailCompra').focus(); obligatorios = false; }
	else{ $('#emailCompra').css("border","1px solid #dfe1e6"); }

	if(!validarEmail(email)){ $('#emailCompra').css("border","1px solid #044fe6"); $('#emailCompra').focus(); obligatorios = false; }
	else{ $('#emailCompra').css("border","1px solid #dfe1e6"); }

	if(obligatorios){

        $.ajax({
            type    : 'POST',
            url     : 'ajax/cart/emailCompra.php',
            data    : { email:email },
            cache   : false,
            beforeSend: function(){
            	$(".cont_loading").fadeIn(200);
            },
        }).done(function(rsp){
        	if(rsp.trim() == 'ERROR'){
            	location.href = "envio-y-pago?email=" + encodeURI(email) + '&n=true';
            }else{
            	location.href = "envio-y-pago?email=" + encodeURI(email);
            }
        }).fail(function(res){
        	$(".cont_loading").fadeOut(200);
        	console.log(res);
        });

	}else{
		return false;
	}
});

$('#boleta').on('click', function(){
	$('.datos-factura').slideUp(500);
});

$('#factura').on('click', function(){
	$('.datos-factura').slideToggle(500);
});

$('#domicilio').on('click', function(){
	var email 	= $('#emailCompra').val(),
		comuna  = $('select[name="comuna_despacho"]').val();
	$('.datos-retiro').slideUp(500, function(){
		$('.datos-envio').slideDown(500);	
		$('#retiro').removeClass('active');
		$('#domicilio').addClass('active');
		$('input[name="tipoDespacho"]').val('domicilio');

		$('.cartResumen').load('ajax/cart/showCartResumen.php?comuna='+comuna+'&email='+email+'&e=1&tp=1');
	});
	
});

$('#domicilioLogin').on('click', function(){
	var email 	= $('input[name="email"]').val(),
		comuna  = $('.select-direcciones').attr('data-comuna');
	$('.datos-retiro').slideUp(500, function(){
		$('.datos-envio').slideDown(500);	
		$('#retiroLogin').removeClass('active');
		$('#domicilioLogin').addClass('active');
		$('input[name="tipoDespacho"]').val('domicilio');

		$('.cartResumen').load('ajax/cart/showCartResumen.php?comuna='+comuna+'&email='+email+'&e=1&tp=1');
	});
	
});

$('#retiro').on('click', function(){
	var email 	= $('#emailCompra').val();
	$('.datos-envio').slideUp(500, function(){
		$('.datos-retiro').slideDown(500);	
		$('#domicilio').removeClass('active');
		$('#retiro').addClass('active');
		$('input[name="tipoDespacho"]').val('retiro');

		$('.cartResumen').load('ajax/cart/showCartResumen.php?comuna=0&email='+email+'&e=1&tp=1');

	});
});

$('#retiroLogin').on('click', function(){
	var email 	= $('input[name="email"]').val();
	$('.datos-envio').slideUp(500, function(){
		$('.datos-retiro').slideDown(500);	
		$('#domicilioLogin').removeClass('active');
		$('#retiroLogin').addClass('active');
		$('input[name="tipoDespacho"]').val('retiro');

		$('.cartResumen').load('ajax/cart/showCartResumen.php?comuna=0&email='+email+'&e=1&tp=1');

	});
});

$('#yoRetiro').on('click', function(){
	var ts = $(this);
	if (!$(this).hasClass('chked')) {
		ts.addClass('chked');
		$('#otraPersona').removeClass('chked');
		$('.content_quien_retira').fadeOut(300, function(){
			$('input[name="nombre_retiro"]').val('');
			$('input[name="rut_retiro"]').val('');
			$('input[name="tiro_retiro"]').val('yoRetiro');
		});
	}
})


$('#otraPersona').on('click', function(){
	var ts = $(this);
	if (!$(this).hasClass('chked')) {
		ts.addClass('chked');
		$('#yoRetiro').removeClass('chked');
		$('.content_quien_retira').fadeIn(300, function(){
			$('input[name="tiro_retiro"]').val('otraPersona');
		});
	}
})

$('.agregarDirPop').on('click', function(){
	var region = parseInt($('select[name="region_despacho"]').val()),
		comuna = parseInt($('select[name="comuna_despacho"]').val()),
		direccion = $('input[name="direccion"]').val(),
		numero = $('input[name="numero"]').val(),
		numero_extra = $('input[name="numero_extra"]').val();

	if (region > 0 && comuna > 0 && direccion.trim() != '' && numero.trim() != '') {
		$.ajax({
			url: 'ajax/cart/agregarDireccion.php',
			type: 'post',
			data: { region: region, comuna: comuna, direccion: direccion, numero: numero, numero_extra: numero_extra },
			beforeSend: function(){
				$(".cont_loading").fadeIn(100);
			}
		}).done(function(res){
			if (res == 1) {
				location.reload();
			}else{
				swal('', 'Error al agregar dirección', 'error');
			}
		}).fail(function(res){
			console.log(res);
			$(".cont_loading").fadeOut(100);
		})
	}else{
		swal('', 'Debes completar todos los campos', 'error');
	}

})

function eliminarCot(ts){
	var id = $(ts).attr('data-id');

	$.ajax({
		url: 'ajax/cotizacion/actions.php',
		type: 'post',
		dataType: 'json',
		data: { id: id, qty: 0, action: 'delete' },
		beforeSend: function(){
			$('.cont_loading').fadeIn(100);
		}
	}).done(function(res){
		$('.cont_loading').fadeOut(100);
		$('.ajx_cot').load(" .content_carro_cot");
	}).fail(function(res){
		console.log(res);
		$('.cont_loading').fadeOut(100);
	})

}

function comprarProducto(ts, id){
	
	var cantidad = parseInt($(ts).parent().parent().find('.box_qty .cantidad_ficha').val());
	var cantidadActual = parseInt($('.box_action small.qty_cart').html());

	$.ajax({ // Ajax consulta stock
		type: 'POST',
		url: 'ajax/ajax_validar_stock.php',
		data: { id: id, qty: cantidad, change: false},
		cache: false,
		beforeSend: function(){
			$(".cont_loading").fadeIn(200);
		}
	}).done(function(resp){
		if(resp >= 0){
			$.ajax({ // Ajax agregar al carro
				type: 'GET',
				url: 'tienda/addCarro.php',
				data: {id: id, action:"add", qty:cantidad},
				cache: false
			}).done(function(resp){

				console.log(resp);
				$(".cont_loading").fadeOut(200);
				$('.ajxLoad-carro').load(' .content-popcarro');
				$('.popupCarro').fadeIn(300);

				var cantidadTotal = cantidad+cantidadActual;
				$('.box_action small.qty_cart').html(cantidadTotal);

				$('.popupCarro .head h3').html('Agregado al carro de compras <img class="closeCart" onclick="closeCart()" src="img/icons/closeNegro.svg">');

			}).fail(function(res){
				$(".cont_loading").fadeOut(200);
				console.log(res);
			});
		}else{
			$(".cont_loading").fadeOut(200);
			swal({
				  type: 'error',
				  title: 'Stock insuficiente',
				  text: 'No fue posible agregar tu producto por falta de stock'
				})
		};
	}).fail(function(res){
		$(".cont_loading").fadeOut(200);
		console.log(res);
	});

}

$('.btnAgregarDireccion').on('click', function(){
	$('.bg-addDir').fadeIn();
	$('.content_new_dir').fadeIn();
})

function ir_al_pago(){
	var formInfo 		= document.forms['formInfo'];
	var focus 			= "";
	var obligatorios 	= true;
	var datos 			= {
		factura 			: $('#factura').is(':checked'),
		nombre 				: formInfo.nombre.value,
		telefono 			: formInfo.telefono.value,
		tipo_despacho 		: formInfo.tipoDespacho.value,
		region 				: parseInt(formInfo.region_despacho.value),
		comuna 				: parseInt(formInfo.comuna_despacho.value),
		direccion 			: formInfo.direccion.value,
		tienda 				: formInfo.retiroTienda.value,
		nombre_retiro 		: formInfo.nombre_retiro.value,
		rut_retiro 			: formInfo.rut_retiro.value,
		razon_social 		: formInfo.razon_social.value,
		rut_empresa 		: formInfo.rut_empresa.value,
		giro 				: formInfo.giro.value,
		direccion_empresa 	: formInfo.direccion_empresa.value,
		guardar_info 		: $('#fcompra').is(':checked'),
		mis_datos 			: $('#mis_datos').val(),
		numero_direccion	: formInfo.numero.value,
		numero_extra		: formInfo.numero_extra.value,
		quien_retira 		: $('#yoRetiro').hasClass('chked'),
		email 				: $('#emailCompra').val(),
		metodoDespacho      : $('input[name="metodoDespacho"]:checked').val()
	};


	if($('.mis_direcciones').val()){
		datos.mis_direcciones = $('.mis_direcciones').val();
	}

	console.log(datos);

	if(datos.factura){

		if (!datos.razon_social) {
			$(formInfo.razon_social).parent().find('label').addClass('error');
			$(formInfo.razon_social).addClass('error-input');

			formError(formInfo.razon_social, 'Llenar campo obligatorio', 'in');

			if(!focus) focus = formInfo.razon_social; obligatorios = false; 
		}else{
			$(formInfo.razon_social).parent().find('label').removeClass('error');
			$(formInfo.razon_social).removeClass('error-input');

			formError(formInfo.razon_social, '', 'out');
		}

		if (!datos.rut_empresa) {
			$(formInfo.rut_empresa).parent().find('label').addClass('error');
			$(formInfo.rut_empresa).addClass('error-input');

			formError(formInfo.rut_empresa, 'Llenar campo obligatorio', 'in');

			if(!focus) focus = formInfo.rut_empresa; obligatorios = false; 
		}else{
			if ($.rut.validar(datos.rut_empresa)) {
				$(formInfo.rut_empresa).parent().find('label').removeClass('error');
				$(formInfo.rut_empresa).removeClass('error-input');

				formError(formInfo.rut_empresa, '', 'out');
			}else{
				$(formInfo.rut_empresa).parent().find('label').addClass('error');
				$(formInfo.rut_empresa).addClass('error-input');

				formError(formInfo.rut_empresa, 'Formato de rut incorrecto', 'in');

				if(!focus) focus = formInfo.rut_empresa; obligatorios = false; 
			}			
		}

		if (!datos.giro) {
			$(formInfo.giro).parent().find('label').addClass('error');
			$(formInfo.giro).addClass('error-input');

			formError(formInfo.giro, 'Llenar campo obligatorio', 'in');

			if(!focus) focus = formInfo.giro; obligatorios = false;
		}else{
			$(formInfo.giro).parent().find('label').removeClass('error');
			$(formInfo.giro).removeClass('error-input');

			formError(formInfo.giro, '', 'out');
		}

		if (!datos.direccion_empresa) {
			$(formInfo.direccion_empresa).parent().find('label').addClass('error');
			$(formInfo.direccion_empresa).addClass('error-input');

			formError(formInfo.direccion_empresa, 'Llenar campo obligatorio', 'in');

			if(!focus) focus = formInfo.direccion_empresa; obligatorios = false;
		}else{
			$(formInfo.direccion_empresa).parent().find('label').removeClass('error');
			$(formInfo.direccion_empresa).removeClass('error-input');

			formError(formInfo.direccion_empresa, '', 'out');
		}
	}

	if (!datos.nombre) {
		$(formInfo.nombre).parent().find('label').addClass('error');
		$(formInfo.nombre).addClass('error-input');

		formError(formInfo.nombre, 'Llenar campo obligatorio', 'in');
		if(!focus) focus = formInfo.nombre; obligatorios = false;
	}else{
		$(formInfo.nombre).parent().find('label').removeClass('error');
		$(formInfo.nombre).removeClass('error-input');

		formError(formInfo.nombre, '', 'out');
	}

	if (!datos.telefono) {
		$(formInfo.telefono).parent().find('label').addClass('error');
		$(formInfo.telefono).addClass('error-input');

		formError(formInfo.telefono, 'Llenar campo obligatorio', 'in');
		if(!focus) focus = formInfo.telefono; obligatorios = false;
	}else{
		var re = /^([0-9]){8,12}$/;

		if (re.test(datos.telefono)) {
			$(formInfo.telefono).parent().find('label').removeClass('error');
			$(formInfo.telefono).removeClass('error-input');
			formError(formInfo.telefono, '', 'out');
		}else{
			$(formInfo.telefono).parent().find('label').addClass('error');
			$(formInfo.telefono).addClass('error-input');

			formError(formInfo.telefono, 'Error de formato', 'in');
		}
	
	}

	if(datos.tipo_despacho == 'domicilio'){

		if (datos.region == 0) {
			$(formInfo.region_despacho).parent().parent().find('.error-description').html('Debes seleccionar una region');
			$(formInfo.region_despacho).parent().parent().find('.error-description').fadeIn();
			$(formInfo.region_despacho).parent().find('.select-selected').addClass('error-select');
			$(formInfo.region_despacho).parent().find('.select-selected .animationSelect').addClass('span-select-error');
		}else{
			$(formInfo.region_despacho).parent().parent().find('.error-description').html('');
			$(formInfo.region_despacho).parent().parent().find('.error-description').fadeOut();
			$(formInfo.region_despacho).parent().find('.select-selected').removeClass('error-select');
			$(formInfo.region_despacho).parent().find('.select-selected .animationSelect').removeClass('span-select-error');
		}

		if (datos.comuna == 0) {
			$(formInfo.comuna_despacho).parent().parent().find('.error-description').html('Debes seleccionar una comuna');
			$(formInfo.comuna_despacho).parent().parent().find('.error-description').fadeIn();
			$(formInfo.comuna_despacho).parent().find('.select-selected').addClass('error-select');
			$(formInfo.comuna_despacho).parent().find('.select-selected .animationSelect').addClass('span-select-error');
		}else{
			$(formInfo.comuna_despacho).parent().parent().find('.error-description').html('');
			$(formInfo.comuna_despacho).parent().parent().find('.error-description').fadeOut();
			$(formInfo.comuna_despacho).parent().find('.select-selected').removeClass('error-select');
			$(formInfo.comuna_despacho).parent().find('.select-selected .animationSelect').removeClass('span-select-error');
		}

		if (!datos.direccion) {
			$(formInfo.direccion).parent().find('label').addClass('error');
			$(formInfo.direccion).addClass('error-input');

			formError(formInfo.direccion, 'Llenar campo obligatorio', 'in');
			if(!focus) focus = formInfo.direccion; obligatorios = false;
		}else{
			$(formInfo.direccion).parent().find('label').removeClass('error');
			$(formInfo.direccion).removeClass('error-input');

			formError(formInfo.direccion, '', 'out');
		}

		if (!datos.numero_direccion) {
			$(formInfo.numero).parent().find('label').addClass('error');
			$(formInfo.numero).addClass('error-input');

			formError(formInfo.numero, '', 'in');
			if(!focus) focus = formInfo.numero; obligatorios = false;
		}else{
			$(formInfo.numero).parent().find('label').removeClass('error');
			$(formInfo.numero).removeClass('error-input');

			formError(formInfo.numero, '', 'out');
		}

	}else{

		if (!datos.quien_retira) {

			if (!datos.nombre_retiro) {
				$(formInfo.nombre_retiro).parent().find('label').addClass('error');
				$(formInfo.nombre_retiro).addClass('error-input');

				formError(formInfo.nombre_retiro, 'Llenar campo obligatorio', 'in');
				if(!focus) focus = formInfo.nombre_retiro; obligatorios = false;
			}else{
				$(formInfo.nombre_retiro).parent().find('label').removeClass('error');
				$(formInfo.nombre_retiro).removeClass('error-input');

				formError(formInfo.nombre_retiro, '', 'out');
			}

			if (!datos.rut_retiro) {
				$(formInfo.rut_retiro).parent().find('label').addClass('error');
				$(formInfo.rut_retiro).addClass('error-input');

				formError(formInfo.rut_retiro, 'Llenar campo obligatorio', 'in');
				if(!focus) focus = formInfo.rut_retiro; obligatorios = false;
			}else{
				$(formInfo.rut_retiro).parent().find('label').removeClass('error');
				$(formInfo.rut_retiro).removeClass('error-input');

				formError(formInfo.rut_retiro, '', 'out');
			}

		}
	}

	
	if(focus){
		focus.focus();
	}

	if(!obligatorios){
		return false;
	}else{
        $.ajax({
            type    : 'POST',
            url     : 'ajax/cart/info-contacto.php',
            data    : { datos: datos },
            cache   : false,
            beforeSend: function(){
            	$('.cont_loading').fadeIn(100);
            }
        }).done(function(rsp){
        	$('.cont_loading').fadeOut(100);

        	var url = $(location).attr('href');
			var url_ = url.replace("envio-y-pago","resumen-compra");
			$(location).attr('href', url_);

        }).fail(function(res){
        	$('.cont_loading').fadeOut(100);
        	console.log(res);
        });

	}

}

$('#formDatos').on('submit', function(){
	var nombre 			= $(this).find('input[name="nombre"]'),
		dia 			= $(this).find('input[name="dia"]'),
		mes 			= $(this).find('input[name="mes"]'),
		anio 			= $(this).find('input[name="anio"]'),
		email 			= $(this).find('input[name="email"]'),
		pass_actual 	= $(this).find('input[name="actual"]'),
		pass_nueva 		= $(this).find('input[name="nueva"]'),
		pass_nueva_re 	= $(this).find('input[name="renueva"]');

	var error = 0;

	if (nombre.val().trim() == '') {
		$(nombre).parent().find('label').addClass('error');
		$(nombre).addClass('error-input');

		formError(nombre, 'Llenar campo obligatorio', 'in');
		$(nombre).focus();
		error = 1;
	}else{
		$(nombre).parent().find('label').removeClass('error');
		$(nombre).removeClass('error-input');

		formError(nombre, '', 'out');
	}

	if (email.val().trim() == '') {
		$(email).parent().find('label').addClass('error');
		$(email).addClass('error-input');

		formError(email, 'Llenar campo obligatorio', 'in');
		$(email).focus();
		error = 1;
	}else{
		if (validarEmail(email.val().trim())) {
			$(email).parent().find('label').removeClass('error');
			$(email).removeClass('error-input');

			formError(email, '', 'out');
		}else{
			$(email).parent().find('label').addClass('error');
			$(email).addClass('error-input');

			formError(email, 'Formato incorrecto', 'in');
			$(email).focus();
			error = 1;
		}
		
	}

	if (pass_actual.val().trim() != '' || pass_nueva.val().trim() != '' || pass_nueva_re.val().trim() != '') {
		if (pass_actual.val().trim() == '') {
			$(pass_actual).parent().find('label').addClass('error');
			$(pass_actual).addClass('error-input');

			formError(pass_actual, 'Llenar campo obligatorio', 'in');
			$(pass_actual).focus();
			error = 1;
		}else{
			$(pass_actual).parent().find('label').removeClass('error');
			$(pass_actual).removeClass('error-input');

			formError(pass_actual, '', 'out');
		}

		if (pass_nueva.val().trim() == '') {
			$(pass_nueva).parent().find('label').addClass('error');
			$(pass_nueva).addClass('error-input');

			formError(pass_nueva, 'Llenar campo obligatorio', 'in');
			$(pass_nueva).focus();
			error = 1;
		}else{
			$(pass_nueva).parent().find('label').removeClass('error');
			$(pass_nueva).removeClass('error-input');

			formError(pass_nueva, '', 'out');
		}

		if (pass_nueva_re.val().trim() == '') {
			$(pass_nueva_re).parent().find('label').addClass('error');
			$(pass_nueva_re).addClass('error-input');

			formError(pass_nueva_re, 'Llenar campo obligatorio', 'in');
			$(pass_nueva_re).focus();
			error = 1;
		}else{
			$(pass_nueva_re).parent().find('label').removeClass('error');
			$(pass_nueva_re).removeClass('error-input');

			formError(pass_nueva_re, '', 'out');
		}
	}

	if (error == 1) {
		return false;
	}

})

$('.eliminarDireccion').on('click', function(){
	var id = $(this).attr('data-id');

	$.ajax({
		url: 'ajax/user/eliminar_direccion.php',
		type: 'post',
		data: { id: id },
		beforeSend: function(){
			$('.cont_loading').fadeIn(100);
		}
	}).done(function(res){
		if (res == 1) {
			location.reload();
		}else{
			swal('', 'Error al eliminar la dirección', 'error');
		}
	}).fail(function(res){
		console.log(res);
		$('.cont_loading').fadeOut(100);
	})
})

$('#formDireccionDash').on('submit', function(){
	var region = $(this).find('select[name="region_despacho"]').val(),
		comuna = $(this).find('select[name="comuna_despacho"]').val(),
		direccion = $(this).find('input[name="direccion"]').val(),
		numero = $(this).find('input[name="numero"]').val();

	if (region > 0 && comuna > 0 && direccion.trim() != '' && numero.trim() != '') {
		
	}else{
		swal('', 'Completa los campos obligatorios (*)', 'error');
		return false;
	}

})

$('.btn-ver-detalle').on('click', function(){
	$(this).parent().parent().parent().find('.open-body').slideToggle();
	$(this).toggleClass('btn-ver-detalle-open');
})

function formError(obj, message, action){
	$(obj).parent().parent().find('.error-description').html(message);
	if (action == 'in') {
		$(obj).parent().parent().find('.error-description').fadeIn();
	}else{
		$(obj).parent().parent().find('.error-description').fadeOut();
	}
	
}

function cotizarProducto(obj, id){
	var qty = $('.cantidad_ficha').val();


	$.ajax({
		url: 'ajax/cotizacion/actions.php',
		type: 'post',
		dataType: 'json',
		data: { id: id, qty: qty, action: 'add' }
	}).done(function(res){
		// console.log(res);
		$('.box_action .qty_cot').load('ajax/cotizacion/qty.php');

		$('.ajxLoad-cot').load(' .content-popcot');

		$('.popupCot').fadeIn();

	}).fail(function(res){
		console.log(res);
	})

}

$('#enviarFormularioRecaptcha').on('click', function(){
	nombre = $("#nombre").val().trim();
	email = $("#email").val().trim();
	telefono = $("#telefono").val().trim();
	mensaje = $("#mensaje").val().trim();

	$('.cont_loading').fadeIn(100);

	if (nombre != '' && email != '' && telefono != '' && mensaje != '') {
		if (validarEmail(email)) {
			grecaptcha.ready(function() {
				grecaptcha.execute('6LdgYlYcAAAAAED7ARuvOg0qEHQRpA-65TL7ZjvT', {action: 'formContacto'}).then(function(token) {
				$('#formContacto').prepend('<input type="hidden" name="token" value="'+ token +'">');
				$('#formContacto').prepend('<input type="hidden" name="action" value="formContacto">');
				$('#formContacto').submit();
			})
		})
		}else{
			swal('', 'Formato de correo incorrecto', 'error');
			$('.cont_loading').fadeOut(100);
		}
	}else{
		swal('', 'Debes completar todos los campos', 'error');
		$('.cont_loading').fadeOut(100);
	}

})

$('#enviarFormularioRecaptcha2').on('click', function(){
	nombre = $("#nombre_empresa").val().trim();
	email = $("#email").val().trim();
	rut = $("#rut_empresa").val().trim();
	telefono = $("#telefono").val().trim();
	mensaje = $("#mensaje").val().trim();
	$('.cont_loading').fadeIn(100);

	if (nombre != '' && email != '' && telefono != '' && mensaje != '' && rut != '') {
		if (validarEmail(email)) {
			if ($.rut.validar(rut)) {
				var re = /^([0-9]){8,12}$/;
				if (re.test(telefono)) {
					$("#telefono").removeClass('error-input');
					// return true;
					// alert("LO logramos");
					grecaptcha.ready(function() {
						grecaptcha.execute('6LdgYlYcAAAAAED7ARuvOg0qEHQRpA-65TL7ZjvT', {action: 'formDistribuidor'}).then(function(token) {
							$('#formDistribuidor').prepend('<input type="hidden" name="token" value="'+ token +'">');
							$('#formDistribuidor').prepend('<input type="hidden" name="action" value="formDistribuidor">');
							$('#formDistribuidor').submit();
						})
					})
				}else{
					$("#telefono").addClass('error-input');
					swal('', 'Formato del teléfono es incorrecto', 'error');
				}
			}else{
				swal('', 'Formato de rut incorrecto', 'error');
				$('.cont_loading').fadeOut(100);
			}
		}else{
			swal('', 'Formato de correo incorrecto', 'error');
			$('.cont_loading').fadeOut(100);
		}
	}else{
		swal('', 'Debes completar todos los campos', 'error');
		$('.cont_loading').fadeOut(100);

		if (nombre == '') {
			$("#nombre_empresa").addClass('error-input');
		}else{
			$("#nombre_empresa").removeClass('error-input');
		}

		if (email == '') {
			$("#email").addClass('error-input');
		}else{
			$("#email").removeClass('error-input');
		}

		if (telefono == '') {
			$("#telefono").addClass('error-input');
		}else{
			$("#telefono").removeClass('error-input');
		}

		if (mensaje == '') {
			$("#mensaje").addClass('error-input');
		}else{
			$("#mensaje").removeClass('error-input');
		}

		if (rut == '') {
			$("#rut_empresa").addClass('error-input');
		}else{
			$("#rut_empresa").removeClass('error-input');
		}
	}

	// return false;

})

$('.btnBuscarCadenas').on('click', function(){
	var ancho = $('select[name="ancho"]').val(),
		perfil = $('select[name="perfil"]').val(),
		aro = $('select[name="aro"]').val();

	// console.log(ancho);

	if (ancho.trim() == '0' || perfil.trim() == '0' || aro.trim() == '0') {
		swal('', 'Debes seleccionar todas las opciones', 'warning');
	}else{
		location.href = 'buscar-cadenas?ancho=' + ancho + '&perfil=' + perfil + '&aro=' + aro;
	}
	

})

$('.categories').on('click', function(){
	$('.menu_categorias').slideToggle();
})

$('.opensub').on('click', function(){
	var ts = $(this);
	if (ts.find('.content_submenu').is(':visible')) {
			ts.find('.content_submenu').slideToggle(300);
			ts.find('.amenu').toggleClass('subopen_arrow');
	}else{
		if ($('.content_submenu').is(':visible')) {
			$('.content_submenu').slideUp(200, function(){
				$('.opensub >').removeClass('subopen_arrow');
				ts.find('.content_submenu').slideDown(300);
				ts.find('.amenu').toggleClass('subopen_arrow');
			})
		}else{
			ts.find('.content_submenu').slideDown(200);
			ts.find('.amenu').toggleClass('subopen_arrow');
		}
	}
	
	
})

$(function(){
	var min_valor_filtros = parseInt($('.min-filtro').attr('data-info'));
	var max_valor_filtros = $('.precio_filtro').attr('data-max');
	var max_filtro_actual = $('.precio_filtro').attr('data-max-actual');
	var max_value = $('.max-filtro').attr('data-info');

	var max_valor = (max_filtro_actual > 0) ? max_filtro_actual : max_valor_filtros;

	$('.min-filtro').html('$' + number_format(min_valor_filtros, 0));
    $('.max-filtro').html('$' + number_format(max_valor, 0));

	$( "#slider-range" ).slider({
	  range: true,
	  min: 0,
	  max: max_valor_filtros,
	  step: 5000,
	  values: [ min_valor_filtros, max_valor ],
	  slide: function( event, ui ) {
	  	$('.min-filtro').html('$' + number_format(ui.values[0], 0));
	  	$('.max-filtro').html('$' + number_format(ui.values[1], 0));

	  	$('.min-filtro').attr('data-valor', ui.values[0]);
	  	$('.max-filtro').attr('data-valor', ui.values[1]);
	  },
	  change: function( event, ui ) {
	  	// lo mismo que en los chk
	  	var link = '',
			ref = $('.tituloFiltros').attr('data-ref'),
			min_price = $('.min-filtro').attr('data-valor'),
			max_price = $('.max-filtro').attr('data-valor'),
			orden = $('.selected-order').attr('data-order');

		var str_subcat = '?subc=',
			str_min = '&desde=',
			str_max = '&hasta=';

		if (ref == 'ofertas') {
			var str_min = '?desde=';
		}

		if (ref == 'lineas') {
			// Recorremos las subcategorias
			$('.filtroChk[name="chk_cat"]:checked').each(function(i,x){
				if (str_subcat == '?subc=') {
					str_subcat += $(this).val();
				}else{
					str_subcat += '-' + $(this).val();
				}
			})
		}else{
			str_subcat = '?filtros=';
			$('.filtroChk[name="chk_cat"]:checked').each(function(i,x){
				if (str_subcat == '?filtros=') {
					str_subcat += $(this).val();
				}else{
					str_subcat += '-' + $(this).val();
				}
			})
		}

		var check_desde = getParameterByName('desde'),
			check_hasta = getParameterByName('hasta');

		str_min += min_price;
		str_max += max_price;

		if (ref == 'lineas') {
			link = str_subcat + str_min + str_max + '&orden=' + orden + '&page=1';
		}else if(ref == 'categorias' || ref == 'subcategorias'){
			link = str_subcat + str_min + str_max + '&orden=' + orden + '&page=1';
		}else if(ref == 'ofertas'){
			link = str_min + str_max + '&orden=' + orden + '&page=1';
		}else if(ref == 'busqueda'){
			var busqueda = getParameterByName('buscar');

			link = '?buscar=' + busqueda + str_min + str_max + '&orden=' + orden + '&page=1';
		}

		var	url_actual = window.location.pathname;

		location.href = url_actual + link;

	  }
	});
})

$('.filtroChk').on('change', function(){

	var link = '',
		ref = $('.tituloFiltros').attr('data-ref'),
		min_price = $('.min-filtro').attr('data-info'),
		max_price = $('.max-filtro').attr('data-info'),
		orden = $('.selected-order').attr('data-order');

	var str_subcat = '?subc=',
		str_min = '&desde=',
		str_max = '&hasta=';

	if (ref == 'lineas') {
		// Recorremos las subcategorias
		$('.filtroChk[name="chk_cat"]:checked').each(function(i,x){
			if (str_subcat == '?subc=') {
				str_subcat += $(this).val();
			}else{
				str_subcat += '-' + $(this).val();
			}
		})
	}else{
		str_subcat = '?filtros=';
		$('.filtroChk[name="chk_cat"]:checked').each(function(i,x){
			if (str_subcat == '?filtros=') {
				str_subcat += $(this).val();
			}else{
				str_subcat += '-' + $(this).val();
			}
		})
	}

	var check_desde = getParameterByName('desde'),
		check_hasta = getParameterByName('hasta');
	
	if (check_desde) {
		if (check_desde != '') {
			if (parseInt(check_hasta) > 0) {

				str_min += min_price;
				str_max += max_price;

			}else{
				str_min = '&desde=0';
				str_max = '&hasta=0';
			}
		}else{
			str_min = '&desde=0';
			str_max = '&hasta=0';
		}
	}else{
		str_min = '&desde=0';
		str_max = '&hasta=0';
	}

	if (ref == 'lineas') {
		link = str_subcat + str_min + str_max + '&orden=' + orden + '&page=1';
	}else if(ref == 'categorias' || ref == 'subcategorias'){
		link = str_subcat + str_min + str_max + '&orden=' + orden + '&page=1';
	}else if(ref == 'ofertas'){
		link = str_min + str_max + '&orden=' + orden + '&page=1';
	}else if(ref == 'busqueda'){
		var busqueda = getParameterByName('buscar');

		link = '?buscar=' + busqueda + str_min + str_max + '&orden=' + orden + '&page=1';
	}

	var	url_actual = window.location.pathname;

	location.href = url_actual + link;

})

$('.selected-order').on('click', function(){
	$('.box_hidden_order').fadeToggle(300);
})

$('.aplicado').on('click', function(){
	var action = $(this).attr('data-action'),
		ref = $('.filtros_col').attr('data-ref');

	var link = '',
		ref = $('.tituloFiltros').attr('data-ref'),
		min_price = $('.min-filtro').attr('data-info'),
		max_price = $('.max-filtro').attr('data-info'),
		orden = $('.selected-order').attr('data-order');

	var str_subcat = '',
		str_filtros = '',
		str_min = '&desde=',
		str_max = '&hasta=';

	$('.filtroChk[name="chk_cat"]:checked').each(function(i,x){
		if (str_subcat == '') {
			str_subcat += $(this).val();
		}else{
			str_subcat += '-' + $(this).val();
		}
	})

	$('.filtroChk[name="chk_cat"]:checked').each(function(i,x){
		if (str_filtros == '') {
			str_filtros += $(this).val();
		}else{
			str_filtros += '-' + $(this).val();
		}
	})

	var check_desde = getParameterByName('desde'),
		check_hasta = getParameterByName('hasta');
	
	if (check_desde) {
		if (check_desde != '') {
			if (parseInt(check_hasta) > 0) {

				str_min += min_price;
				str_max += max_price;

			}else{
				str_min = '&desde=0';
				str_max = '&hasta=0';
			}
		}else{
			str_min = '&desde=0';
			str_max = '&hasta=0';
		}
	}else{
		str_min = '&desde=0';
		str_max = '&hasta=0';
	}

	if (action != 'precio') {
		var id = $(this).attr('data-id');

		var params = getParameterByName(action),
			split = params.split('-');

		for (var i = 0; i < split.length; i++) {
			if (id == split[i]) {
				split.splice(i, 1);
			}
		}

		if (action == 'subc') {
			str_subcat = split.join('-');
		}else if(action == 'filtros'){
			str_filtros = split.join('-');
		}
	}else{

		str_min = '&desde=0';
		str_max = '&hasta=0';

	}

	if (ref == 'lineas') {
		link = '?subc=' + str_subcat + str_min + str_max + '&orden=' + orden + '&page=1';
	}else if(ref == 'categorias' || ref == 'subcategorias'){
		link = '?filtros=' + str_filtros + str_min + str_max + '&orden=' + orden + '&page=1';
	}else if(ref == 'ofertas'){
		link = str_min + str_max + '&orden=' + orden + '&page=1';
	}else if(ref == 'busqueda'){
		var busqueda = getParameterByName('buscar');

		link = '?buscar=' + busqueda + str_min + str_max + '&orden=' + orden + '&page=1';
	}
	
	var	url_actual = window.location.pathname;

	location.href = url_actual + link;

})

// $('.btnSucc').on('click', function(){
// 	$('.content_success').fadeOut(200);
// })

$('.search_xs').on('click', function(){
	$('.responsive_search').fadeIn();
	$('.bg_search').fadeIn();
})

$('.btnCloseSearch').on('click', function(){
	$('.responsive_search').fadeOut();
	$('.bg_search').fadeOut();
})

$('.btn-open-cats').on('click', function(e){
	e.preventDefault();
	$(this).parent().parent().find('.subres').addClass('subres_active');
})

$('.btn-open-sub').on('click', function(e){
	e.preventDefault();
	$(this).parent().parent().find('.subsubres').addClass('subres_active');
})

$('.subres .volver').on('click', function(e){
	e.preventDefault();
	$(this).parent().parent().removeClass('subres_active');
})

$('.nav_resp').on('click', function(e){
	e.stopPropagation();
})

$('.closenavres').on('click', function(){
	$('.nav_resp').fadeOut();
})

$('.btnRes').on('click', function(e){
	e.stopPropagation();
	$('.nav_resp').fadeToggle();
})

$('html, body').on('click', function(){
	if ($('.nav_resp').is(':visible')) {
		$('.nav_resp').fadeOut();
	}
})

$('.open-footer').on('click', function(){
	var ts = $(this);
	ts.parent().find('.listFooter').slideToggle(300);
})

$('.btnMostrarResumen').on('click', function(){
	$('.resumen-desk').slideToggle(450);
})

$('.a-example').on('click', function(){
	$('.content-example').fadeIn();
})

$('.close-example').on('click', function(){
	$('.content-example').fadeOut();
})

function codigoDescuento(){
	var codigo = $('.codigo_descuento').val();
	console.log(codigo);

	if (codigo.trim() != '') {
		$.ajax({
			url: 'ajax/cart/codigo-descuento.php',
			type: 'post',
			data: { codigo: codigo },
			beforeSend: function(){
				$('.cont_loading').fadeIn();
			}
		}).done(function(res){

			$('.cont_loading').fadeOut(100);

			if (res == 1) {
				var tipoDespacho = ($('input[name="metodoDespacho"]:checked').val() == 'estandar') ? 1 : 2;

				console.log(tipoDespacho);

				if ($('.select-direcciones').length == 0) {
					var valueSelected = $('select[name="comuna_despacho"]').val(),
						email = $('#emailCompra').val();
				}else{
					var valueSelected = $('.select-direcciones').attr('data-comuna'),
						email = $('.emailUsuario').val();
				}

				$('.cartResumen').load('ajax/cart/showCartResumen.php?comuna='+valueSelected+'&email='+email+'&e=1&tp='+tipoDespacho);
			}else{
				swal('', 'Código de descuento no disponible', 'error');
			}

			// console.log(res);
			
		}).fail(function(res){
			console.log(res);
			$('.cont_loading').fadeOut();
		})
	}else{
		swal('', 'Debes ingresar un codigo de descuento', 'warning');
	}

}

function rowSelectDim(ts){

    var y, i, k, s, h;
        s = ts.parentNode.parentNode.getElementsByTagName("select")[0];
        h = ts.parentNode.previousSibling;

    h.setAttribute("class", "select-selected optionSelected");

    var selectName = ts.parentNode.parentNode.getAttribute('data-name');

    for (i = 0; i < s.length; i++) {
      if (s.options[i].innerHTML == ts.innerHTML) {
        s.selectedIndex = i;
        h.innerHTML = '<span class="animationSelect">' + selectName + '</span>' + ts.innerHTML;
        y = ts.parentNode.getElementsByClassName("same-as-selected");
        for (k = 0; k < y.length; k++) {
          y[k].removeAttribute("class");
        }
        ts.setAttribute("class", "same-as-selected");
        break;
      }
    }
    h.click();

}

function eliminaItemCarroOpen(id){
	var id_pro = id;

	$.ajax({
		url: 'tienda/addCarro.php',
		type: 'get',
		data: { id: id_pro, action: 'delete' },
		beforeSend: function(){
			$(".cont_loading").fadeIn(200);
		}
	}).done(function(res){
		$('.ajxLoad-carro').load(' .content-popcarro');
		$(".cont_loading").fadeOut(200);
	}).fail(function(res){
		console.log(res);
		$(".cont_loading").fadeOut(200);
	})
}

function eliminarItemCot(id){
	$.ajax({
		url: 'ajax/cotizacion/actions.php',
		type: 'post',
		dataType: 'json',
		data: { id: id, qty: 0, action: 'delete' },
		beforeSend: function(){
			$('.cont_loading').fadeIn(100);
		}
	}).done(function(res){
		$('.box_action .qty_cot').load('ajax/cotizacion/qty.php');

		$('.ajxLoad-cot').load(' .content-popcot');
		$('.cont_loading').fadeOut(100);
	}).fail(function(res){
		console.log(res);
		$('.cont_loading').fadeOut(100);
	})
}

function number_format(amount, decimals) {

    amount += ''; // por si pasan un numero en vez de un string
    amount = parseFloat(amount.replace(/[^0-9\.]/g, '')); // elimino cualquier cosa que no sea numero o punto

    decimals = decimals || 0; // por si la variable no fue fue pasada

    // si no es un numero o es igual a cero retorno el mismo cero
    if (isNaN(amount) || amount === 0) 
        return parseFloat(0).toFixed(decimals);

    // si es mayor o menor que cero retorno el valor formateado como numero
    amount = '' + amount.toFixed(decimals);

    var amount_parts = amount.split('.'),
        regexp = /(\d+)(\d{3})/;

    while (regexp.test(amount_parts[0]))
        amount_parts[0] = amount_parts[0].replace(regexp, '$1' + '.' + '$2');

    return amount_parts.join('.');
}

function validarEmail(valor) {
	emailRegex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
	if (emailRegex.test(valor)){
		return true;
	}else {
		return false;
	}
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}