$(function(){

	var activeAncho = parseInt($('select[name="ancho"]').attr('data-active')),
		activePerfil = parseInt($('select[name="perfil"]').attr('data-active'))
		activeAro = parseInt($('select[name="aro"]').attr('data-active'));

	if (activeAncho == 1) {
		$('.select-ancho').find('.select-selected').addClass('optionSelected');
	    $('.select-ancho').find('.select-selected .animationSelect').addClass('selected');

	    $('.select-ancho').find('.select-selected').append($('.select-ancho select option:selected').text());
	}

	if (activePerfil == 1) {
		$('.select-perfil').find('.select-selected').addClass('optionSelected');
	    $('.select-perfil').find('.select-selected .animationSelect').addClass('selected');

	    $('.select-perfil').find('.select-selected').append($('.select-perfil select option:selected').text());
	}

	if (activeAro == 1) {
		$('.select-aro').find('.select-selected').addClass('optionSelected');
	    $('.select-aro').find('.select-selected .animationSelect').addClass('selected');

	    $('.select-aro').find('.select-selected').append($('.select-aro select option:selected').text());
	}

})