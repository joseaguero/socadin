<?php
include("../../admin/conf.php");
require_once('../../admin/includes/tienda/cart/inc/functions.inc.php');

$oc = (isset($_GET["oc"])) ? mysqli_real_escape_string($conexion, 'OC_'.$_GET["oc"]) : 0;

$eoc = consulta_bd("oc, medio_de_pago","pedidos","oc='$oc'","");
$existeOC = mysqli_affected_rows($conexion);
	
if($existeOC == 0){
	    echo '<script>parent.location = "'.$url_base.'404";</script>';
 }else{

    $method = ($eoc[0][1] == 'webpay') ? 'tbk' : 'mpago';

	$camposMetodo = ''; // 20 - 25

    if ($method == 'tbk') {
    $camposMetodo = 'transaction_date, amount, card_number,shares_number,authorization_code,payment_type_code';
    }else{
    $camposMetodo = "mp_transaction_date, mp_paid_amount, mp_card_number, mp_cuotas, mp_auth_code, mp_payment_type";
    }

    $pedido = consulta_bd("
    id,
    cliente_id,
    nombre,
    email,
    telefono,
    region_id,
    comuna_id,
    direccion,
    retiro_en_tienda,
    sucursal_id,
    factura,
    razon_social,
    direccion_factura,
    rut_factura,
    email_factura,
    giro,
    total, 
    valor_despacho,
    total_pagado,
    visto,
    $camposMetodo,
    fecha_creacion,
    descuento
    ", 'pedidos', "oc= '$oc'", '');

 	// Variables condiciones
  $retiro_en_tienda = $pedido[0][8];
  $sucursal_id = $pedido[0][9];
  $factura = $pedido[0][10];

  $razon_social = $pedido[0][11];
  $direccion_factura = $pedido[0][12];
  $rut_factura = $pedido[0][13];
  $email_factura = $pedido[0][14];
  $giro = $pedido[0][15];

  $descuento = $pedido[0][27];

    // Detalle comprador
  $nombre = $pedido[0][2];
  $email = $pedido[0][3];
  $telefono = $pedido[0][4];

  // Detalle despacho - retiro
  if ($retiro_en_tienda == 1) {
    $sucursal = consulta_bd('s.direccion, r.nombre, c.nombre', 'sucursales s JOIN comunas c ON c.id = s.comuna_id JOIN regiones r ON r.id = c.region_id', "s.id = {$sucursal_id}", "");

    $direccion = $sucursal[0][0];
    $region = $sucursal[0][1];
    $comuna = $sucursal[0][2];

  }else{
    $direccion_cliente = consulta_bd('r.nombre, c.nombre', 'regiones r JOIN comunas c ON c.region_id = r.id', "r.id = {$pedido[0][5]} and c.id = {$pedido[0][6]}", '');
    $direccion = $pedido[0][7];
    $region = $direccion_cliente[0][0];
    $comuna = $direccion_cliente[0][1];
  }

  //detalle pedido
  $pedido_id       = $pedido[0][0];
  $subtotal        = $pedido[0][16];
  $despacho    = $pedido[0][17];
  $total_pagado = number_format($pedido[0][18],0,",",".");

  if ($eoc[0][1] == 'transferencia') {
    $fecha           = date('d/m/Y', strtotime($pedido[0][26]));
    $hora_pago       = date('H:i:s', strtotime($pedido[0][26]));
  }else{
    $fecha           = date('d/m/Y', strtotime($pedido[0][20]));
    $hora_pago           = date('H:i:s', strtotime($pedido[0][20]));
  }

  //detalle transbank
  $num_tarjeta     = $pedido[0][22];
  $num_cuotas      = $pedido[0][23];
  $cod_aurizacion   = $pedido[0][24];
  $tipo_pago     = $pedido[0][25];

  //funcion tipo pago
  $tipo_pago = tipo_pago($tipo_pago,$num_cuotas, $method);
		
    $tipo_pago_final = ($eoc[0][1] == 'transferencia') ? "Transferencia" : $tipo_pago['tipo_pago']; 
		
		
 }





$html = '<body>
	<div class="contenedor">
    	<div class="header"><img src="'.$url_base.'/img/logo.png" /></div>
        <h2>¡MUCHAS GRACIAS POR COMPRAR CON NOSOTROS!</h2>
        <h3>SU ORDEN DE COMPRA ES:</h3>
        <h4>'.$oc.'</h4>
        
        <div class="titulo"><span>DATOS DE COMPRA</span></div>
        
        <div class="filaDato">
        	<div class="tituloDato">Nombre comprador:</div>
            <div class="valorDato">'.$nombre.'</div>
        </div>
        <div class="filaDato filaGris">
        	<div class="tituloDato">Fecha y hora de Venta:</div>
            <div class="valorDato">'.$fecha.' | '. $hora_pago .'</div>
        </div>
        <div class="filaDato">
        	<div class="tituloDato">Tipo de transacción:</div>
            <div class="valorDato">Venta</div>
        </div>
        
        <div class="filaDato filaGris">
        	<div class="tituloDato">Tipo de pago:</div>
            <div class="valorDato">'.$tipo_pago_final.'</div>
        </div>';
        if ($eoc[0][1] != 'transferencia') {
            $html .= '

            <div class="filaDato">
            <div class="tituloDato">Tarjeta terminada en:</div>
            <div class="valorDato">**** **** **** '.$num_tarjeta.'</div>
        </div>
        <div class="filaDato filaGris">
            <div class="tituloDato">Número de coutas:</div>
            <div class="valorDato">'.$tipo_pago['cuota'].'</div>
        </div>
        <div class="filaDato">
            <div class="tituloDato">Valor pagado:</div>
            <div class="valorDato">$'.$total_pagado.'</div>
        </div>

            <div class="filaDato filaGris">
                <div class="tituloDato">Código de autorización:</div>
                <div class="valorDato">'.$cod_aurizacion.'</div>
            </div>';
        }
        
        $html .= '<div class="totales">
			
		
		 
            <div class="filaDato">
            	<div class="valorDato">$'.number_format($subtotal,0,",",".").'</div>
                <div class="tituloDato">Sub total:</div>
            </div>';
			if($descuento != 0){
				$html .= '<div class="filaDato">
							<div class="valorDato">$'.number_format($descuento,0,",",".").'</div>
							<div class="tituloDato">Descuento:</div>
						</div>';
			}
            
			
			if($giftcard != 0){
				$html .= '<div class="filaDato">
							<div class="valorDato">$'.number_format($giftcard,0,",",".").'</div>
							<div class="tituloDato">Giftcard:</div>
						</div>';
			}
			
			
  $html .= '<div class="filaDato">
            	<div class="valorDato">$'.number_format($despacho,0,",",".").'</div>
                <div class="tituloDato">Valor envío:</div>
            </div>
            <div class="filaDato">
            	<div class="valorDato total">$'.$total_pagado.'</div>
                <div class="tituloDato total">Total</div>
            </div>
        </div><!--fin totales-->
        
        
        <div class="footer">
        	Todos los derechos reservados. Socadin.cl
        </div>
        
    </div>
</body>';
//echo $html;
//==============================================================
//==============================================================
//==============================================================

$path = __DIR__;
//die("$path");
require_once '../../vendor/autoload.php';
$mpdf = new \Mpdf\Mpdf();
$mpdf->SetDisplayMode('fullpage');
// LOAD a stylesheet
$stylesheet = file_get_contents('estilos.css');
$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text
$mpdf->WriteHTML($html);
$mpdf->Output();
exit;
//==============================================================
//==============================================================
//==============================================================

?>