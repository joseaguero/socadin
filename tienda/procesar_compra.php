<?php
include('../admin/conf.php');
require_once('../admin/includes/tienda/cart/inc/functions.inc.php');

require_once "mercadopago/lib/mercadopago.php";
include('mercadopago/claves.php');

//die("asdasdasd");

$datos = $_SESSION['datos'];

if (!isset($_COOKIE["usuario_id"])) {
	$factura 			= mysqli_real_escape_string($conexion, $datos['factura']);
	$email 				= mysqli_real_escape_string($conexion, $datos['email']);
	$newsletter 		= mysqli_real_escape_string($conexion, $datos['newsletter']);
	$nombre 			= mysqli_real_escape_string($conexion, $datos['nombre']);
	$telefono 			= mysqli_real_escape_string($conexion, $datos['telefono']);
	$tipo_despacho 		= mysqli_real_escape_string($conexion, $datos['tipo_despacho']);
	$region 			= mysqli_real_escape_string($conexion, $datos['region']);
	$comuna 			= mysqli_real_escape_string($conexion, $datos['comuna']);
	$direccion 			= mysqli_real_escape_string($conexion, $datos['direccion']);
	$comentarios 		= mysqli_real_escape_string($conexion, $datos['comentarios']);
	$tienda 			= mysqli_real_escape_string($conexion, $datos['tienda']);
	$nombre_retiro 		= mysqli_real_escape_string($conexion, $datos['nombre_retiro']);
	$rut_retiro 		= mysqli_real_escape_string($conexion, $datos['rut_retiro']);
	$razon_social 		= mysqli_real_escape_string($conexion, $datos['razon_social']);
	$rut_empresa 		= mysqli_real_escape_string($conexion, $datos['rut_empresa']);
	$giro 				= mysqli_real_escape_string($conexion, $datos['giro']);
	$direccion_empresa 	= mysqli_real_escape_string($conexion, $datos['direccion_empresa']);
	$guardar_info 		= mysqli_real_escape_string($conexion, $datos['guardar_info']);
	$metodo_pago 		= mysqli_real_escape_string($conexion, $_POST['metodoPago']);
	$mis_datos 			= mysqli_real_escape_string($conexion, $datos['mis_datos']);
	$numero_direccion   = mysqli_real_escape_string($conexion, $datos['numero_direccion']);
	$numero_extra  		= mysqli_real_escape_string($conexion, $datos['numero_extra']);
	$metodoDespacho     = mysqli_real_escape_string($conexion, $datos['metodoDespacho']);

	$email = trim(strtolower($email));
	$clientes = consulta_bd("id, nombre, telefono","clientes","LOWER(email)='$email'","");
	$cantClientes = mysqli_affected_rows($conexion);

	if($cantClientes > 0){
		$current_user = $clientes[0][0];

		if($mis_datos){
			$nombre 	= $clientes[0][1];
			$telefono	= $clientes[0][2];
		}

	}else{
		$inserCliente = insert_bd("clientes","activo, nombre, email, telefono, razon_social, rut_empresa, giro, direccion_empresa","0, '$nombre','$email','$telefono', '$razon_social', '$rut_empresa', '$giro', '$direccion_empresa'");
		$idClienteInsert = mysqli_insert_id($conexion);
		$current_user = $idClienteInsert;
	}
}else{
	$current_user = $_COOKIE["usuario_id"];
	$idClienteInsert = $_COOKIE["usuario_id"];

	$datosCliente = consulta_bd('email, nombre, telefono', 'clientes', "id = $current_user", '');

	$factura 			= mysqli_real_escape_string($conexion, $_POST['tipoPago']);
	$email 				= mysqli_real_escape_string($conexion, $datosCliente[0][0]);
	$newsletter 		= 0;
	$nombre 			= mysqli_real_escape_string($conexion, $datosCliente[0][1]);
	$telefono 			= mysqli_real_escape_string($conexion, $datosCliente[0][2]);
	$tipo_despacho 		= mysqli_real_escape_string($conexion, $_POST['tipoDespacho']);
	
	$tienda 			= mysqli_real_escape_string($conexion, $_POST['retiroTienda']);

	$nombre_retiro 		= mysqli_real_escape_string($conexion, $datos['nombre_retiro']);
	$rut_retiro 		= mysqli_real_escape_string($conexion, $datos['rut_retiro']);
	$razon_social 		= mysqli_real_escape_string($conexion, $_POST['razon_social']);
	$rut_empresa 		= mysqli_real_escape_string($conexion, $_POST['rut_empresa']);
	$giro 				= mysqli_real_escape_string($conexion, $_POST['giro']);
	$direccion_empresa 	= mysqli_real_escape_string($conexion, $_POST['direccion_empresa']);

	$nombre_retiro 		= mysqli_real_escape_string($conexion, $_POST['nombre_retiro']);
	$rut_retiro 		= mysqli_real_escape_string($conexion, $_POST['rut_retiro']);

	$metodo_pago 		= mysqli_real_escape_string($conexion, $_POST['metodoPago']);
	$metodoDespacho     = mysqli_real_escape_string($conexion, $_POST['metodoDespacho']);

	// Dirección
	$mi_direccion 		= mysqli_real_escape_string($conexion, $_POST['mis_direcciones']);

	$queryDir = consulta_bd('region_id, comuna_id, calle, numero, numero_extra', "clientes_direcciones", "id = $mi_direccion", '');

	$region 			= $queryDir[0][0];
	$comuna 			= $queryDir[0][1];
	$direccion 			= $queryDir[0][2];
	$numero_direccion 	= $queryDir[0][3];
	$numero_extra 		= $queryDir[0][4];

}


if($tipo_despacho == 'retiro'){
	
	$sucursales = consulta_bd("id, nombre, direccion, horario","sucursales","id = $tienda","posicion ASC");
	$sucursal 	=  $sucursales[0][1]." ".$sucursales[0][0];
    $region 	= 0;
    $comuna 	= 0;
    $direccion 	= "";
    $retiro_en_tienda = 1;
    $despacho 	= 0;
    $tipo_envio = "Retiro";
    $por_pagar = 0;

    $carrier = '';
} else {

	if (!isset($_COOKIE['usuario_id'])) {
		//ingreso la direccion del cliente para futuras compras
		if($cantClientes == 0){
			if(!isset($_COOKIE[usuario_id])){
	            $inserCliente = insert_bd("clientes_direcciones","cliente_id, region_id, comuna_id, calle, numero, numero_extra","$idClienteInsert, $region, $comuna, '$direccion', '$numero_direccion', '$numero_extra'");
	        }
	    }
	}

	$valorDespacho 		= valorDespacho($comuna);
	$por_pagar 			= (is_numeric($valorDespacho['estandar'])) ? 0 : 1;
	$despacho 			= ($metodoDespacho == 'estandar') ? $valorDespacho['estandar'] : $valorDespacho['rapido'];
    $tipo_envio 		= ($metodoDespacho == 'estandar') ? 'Estandar' : 'Rápido';
	$retiro_en_tienda 	= 0;
	$tienda 			= 0;
	
	if (!is_numeric($despacho)) {
		$despacho = 0;
	}

	$carrier = check_carrier();

}



$subtotal = round(get_total_price());

$cod = 'null';

//descuento
if(isset($_SESSION["codigo_descuento"])){
	$valor_descuento = $_SESSION['codigo_descuento']['valor'];
	$codigo_descuento = $_SESSION['codigo_descuento']['codigo'];
} else {
	$valor_descuento = 0;
	$codigo_descuento = '';
}

$iva = round(( ($subtotal - $valor_descuento) * 1.19) - ($subtotal - $valor_descuento));
$total = ($subtotal - $valor_descuento) + $iva + $despacho;
$totalSinDespacho = ($subtotal);
//$total = $subtotal-$valor_descuento;

//CART EXPLODE AND COUNT ITEMS
if(!isset($_COOKIE[cart_alfa_cm])){
	setcookie("cart_alfa_cm", "", time() + (365 * 24 * 60 * 60), "/");
	}  
$cart = $_COOKIE[cart_alfa_cm];
	

$items = explode(',',$cart);
foreach ($items as $item) {
	$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
}



////////////////// ----------- REVISA STOCK TEMPORAL ----------- //////////////////

	foreach ($contents as $id=>$qty){

		$isPack = consulta_bd("p.pack, p.codigos","productos p, productos_detalles pd","p.id = pd.producto_id AND pd.id = $id","");

		if($isPack[0][0]){

			$codes_pack = explode(',', $isPack[0][1]);
			foreach ($codes_pack as $value){
				$value 			= trim($value);
				$stock 			= consulta_bd("id, stock, stock_reserva, sku","productos_detalles","sku='$value'","");
				$stock_temporal = consulta_bd("stock","stock_temporal","sku='".$stock[0][3]."'","");
				if($qty > ($stock[0][1] - $stock[0][2] - $stock_temporal[0][0])){
					header("location: mi-carro?stock=$id");
					die();
				}
			}

		}else{
			$stock 			= consulta_bd("id, stock, stock_reserva, sku","productos_detalles","id=$id","");
			$stock_temporal = consulta_bd("stock","stock_temporal","sku='".$stock[0][3]."'","");
			if($qty > ($stock[0][1] - $stock[0][2] - $stock_temporal[0][0])){
				header("location: mi-carro?stock=$id");
				die();
			}	
		}

		
	}

////////////////// ----------- ////////////////// ----------- //////////////////

//VEO SI ES FACTURA
if($factura == 'true'){
	$isFactura = 1;
}else{
	$isFactura 			= 0;
	$razon_social 		= "";
	$giro 				= "";
	$rut_empresa 		= "";
	$direccion_empresa 	= "";
}



$fecha 				= date('Y-m-d', time());
/*$codigo_descuento 	= "";
$valor_descuento 	= 0;*/

if ($_COOKIE['cart_alfa_cm']!="") {
	
	//CREO OC CON CORRELATIVO PARA QUE NO SE REPITA
	$insert 	= insert_bd("pedidos","fecha_creacion","NOW()");
	$pedido_id 	= mysqli_insert_id($conexion);

	if(strlen($pedido_id) < 5){
		$j = strlen($pedido_id);
		while ($j < 5){ $agr .= '0'; $j++; }
		$pedido_id = $agr.$pedido_id;
	}

	$date = date('ymdhi');

	$oc = "OC_".$date;
	$new_oc = "OC_".$date.$pedido_id;

	$estado_pendiente 	= 1;
    $generar_pedido 	= update_bd("pedidos","
    	oc 					= '$new_oc',
    	estado_id 			= '$estado_pendiente',
    	medio_de_pago 		= '$metodo_pago',
    	cliente_id 			= '$current_user',
    	nombre 				= '$nombre',
    	email 				= '$email',
    	telefono 			= '$telefono',
    	retiro_en_tienda 	= '$retiro_en_tienda',
    	direccion 			= '$direccion #$numero_direccion, $numero_extra',
    	comuna_id 			= '$comuna',
    	region_id 			= '$region',
    	comentarios_envio 	= '$comentarios',
    	fecha 				= '$fecha',
    	codigo_descuento 	= '$codigo_descuento',
    	descuento 			= '$valor_descuento',
    	total 				= '$totalSinDespacho',
    	valor_despacho 		= '$despacho',
    	tipo_envio			= '$tipo_envio',
    	total_pagado 		= '$total',
    	razon_social 		= '$razon_social',
    	direccion_factura 	= '$direccion_empresa',
		rut_factura 		= '$rut_empresa',
		giro 				= '$giro',
    	factura 			= '$isFactura',
    	sucursal_id 		= $tienda,
    	nombre_retiro 		= '$nombre_retiro',
    	rut_retiro 			= '$rut_retiro',
    	por_pagar 			= $por_pagar,
    	carrier 			= '$carrier'
    	","id = $pedido_id");
			
	

	/* CARRO ABANDONADO */
	if(!isset($_COOKIE[cart_alfa_cm])){
		setcookie("cart_alfa_cm", "", time() + (365 * 24 * 60 * 60), "/");
	}  
	$pdca = $_COOKIE[cart_alfa_cm];
	update_bd("carros_abandonados","oc='$new_oc'","productos = '$pdca' and correo = '$info_email'");
	$notificacion = (isset($_SESSION[notificacion])) ? mysqli_real_escape_string($conexion, $_SESSION[notificacion]) : 0;

	$cantTotal = 0;

		foreach ($contents as $id=>$qty) {
			$camposPrecio = '';
			$fromPrecios = '';
			if (isset($_COOKIE['usuario_id']) and clienteLista($_COOKIE['usuario_id'])) {
				$camposPrecio = 'lpp.precio, lpp.descuento';
				$fromPrecios = ' JOIN lista_de_precio_productos lpp ON lpp.productos_detalle_id = pd.id';
			}else{
				$camposPrecio = 'pd.precio, pd.descuento';
			}
			$prod = consulta_bd("{$camposPrecio}, p.pack, p.codigos, pd.sku","productos p JOIN productos_detalles pd ON p.id = pd.producto_id{$fromPrecios}","pd.id = $id","");
			////Veo si el producto es un pack, lo manejo de cierta forma///////////////////////
			if($prod[0][2] == 1){
				
				$precio_final = $prod[0][0];
				$codigo_pack = $prod[0][4];
				$codigosProductos = trim($prod[0][3]);
				$productosPack = explode(",", $codigosProductos);
				
				
				//saco la suma de los productos reales
				$totalReal = 0;
				foreach($productosPack as $skuPack) {
					$skuPack = trim($skuPack);
					$valorProdDetalle = consulta_bd("precio","productos_detalles","sku='$skuPack'","");
					$totalReal = $totalReal + $valorProdDetalle[0][0];
				}
				//obtengo el porcentaje de descuento
				$descPorcentajeProducto = ($totalReal - $precio_final)/$totalReal;
				//$descPorcentajeProducto2 = (($precio_final - $totalReal)/$totalReal) * -1;
				//die("$descPorcentajeProducto /////-///// $descPorcentajeProducto2");
				
				$retorno = "";
				$idPd = 0;
				foreach($productosPack as $skuPack) {
					$skuPack = trim($skuPack);
					$valorProdDetalle2 = consulta_bd("precio, id","productos_detalles","sku='$skuPack'","");
					
					$precioUnitario = $valorProdDetalle2[0][0];
					$idProductoDetalle = $valorProdDetalle2[0][1];
					
					$precioDescuento = $valorProdDetalle2[0][0] * (1 - $descPorcentajeProducto);
					$precioTotal = round($precioDescuento * $qty);
					
					$desc = 1 - $descPorcentajeProducto;
					//die("$precioUnitario  --  $precioTotal");
					$sku = $skuPack;
					
					
					$campos = "pedido_id, productos_detalle_id, cantidad, precio_unitario, descuento, codigo, precio_total, fecha_creacion, codigo_pack";
					$values = "$pedido_id,$idProductoDetalle,'$qty',$precioUnitario, $precioDescuento, '$sku', $precioTotal, NOW(), '$codigo_pack'";
					$detalle = insert_bd("productos_pedidos","$campos","$values");
					$cantTotal = $cantTotal + $qty;
				}//fin foreach
				
				
				
			} else {
			///////////////////////////
                
                if ($is_cyber and is_cyber_product($prod[0][6])){
				//si es cyber
					if($prod[0][7] > 0){
						//guardo el precio cyber
							$precio_final = $prod[0][7];
							$descuento = round(100 - (($prod[0][7] * 100) / $prod[0][0]));
						} else if($prod[0][1] > 0){
							//guardo el precio con desceunto
							$precio_final = $prod[0][1];
							$descuento = round(100 - (($prod[0][1] * 100) / $prod[0][0]));
						} else{
							//guardo el precio normal
							$precio_final = $prod[0][0];
							$descuento = 0;
						}
				} else {
					//no es cyber
					if($prod[0][1] > 0){
							$precio_final = $prod[0][1];
							$descuento = round(100 - (($prod[0][1] * 100) / $prod[0][0]));
						}else{
							$precio_final = $prod[0][0];
							$descuento = 0;
						}
					}//fin else no es cyber
				
                
				
				$precioUnitario = $precio_final;
				$precioTotal = $precioUnitario;
				$porcentaje_descuento = $descuento;//$prod[0][1];//$valoresFinales[descuento];
				
				$all_sku = consulta_bd("sku","productos_detalles","id=$id","");
				$sku = $all_sku[0][0];
				/* $precio = $all_sku[0][1]; */

			$campos = "pedido_id, productos_detalle_id, cantidad, precio_unitario, codigo, precio_total, fecha_creacion, descuento";
			$values = "$pedido_id, $id, 1, $precioUnitario,'$sku', $precioTotal, NOW(), $porcentaje_descuento";

				for ($i=0; $i < $qty; $i++) { 
					// For para agregar 1 fila en la bd por producto, así poder utilizar los certificados.
					$detalle = insert_bd("productos_pedidos","$campos","$values");
				}
				$cantTotal = $cantTotal + $qty;
			}//fin del ciclo


			////////////////// ----------- INSERTO STOCK TEMPORAL ----------- //////////////////


				$isPack = consulta_bd("p.pack, p.codigos","productos p, productos_detalles pd","p.id = pd.producto_id AND pd.id = $id","");

				if($isPack[0][0]){
					$codes_pack = explode(',', $isPack[0][1]);
					foreach ($codes_pack as $value){
						$value 			= trim($value);
						$stock  = consulta_bd("id, stock, stock_reserva, sku","productos_detalles","sku='$value'","");
						$stock_temporal = insert_bd("stock_temporal","oc, sku, stock, fecha_creacion","'$new_oc','".$stock[0][3]."', $qty, NOW()");
					}

				}else{

		        	$stock  = consulta_bd("id, stock, stock_reserva, sku","productos_detalles","id=$id","");
					$stock_temporal = insert_bd("stock_temporal","oc, sku, stock, fecha_creacion","'$new_oc','".$stock[0][3]."', $qty, NOW()");

				}


			////////////////// ----------- ////////////////// ----------- //////////////////
		}


		$qtyFinal = update_bd("pedidos","cant_productos = $cantTotal, fecha_creacion = NOW()","id=$pedido_id");

        unset($_SESSION["notificacion"]);

        $token = generateToken($new_oc, $total);

    if ($metodo_pago == 'mercadopago') {
    	// mpago 
		$mp = new MP ($keys['client_id'], $keys['client_secret']);

		$preference_data = array(
			"items" => array(
				array(
					"id" => $new_oc,
					"title" => 'Compra de productos '.opciones('nombre_cliente'),
					"currency_id" => "CLP",
					"picture_url" =>"https://www.mercadopago.com/org-img/MP3/home/logomp3.gif",
					// "description" => "Description",
					// "category_id" => "Category",
					"quantity" => 1,
					"unit_price" => $total
				)
			),
			"payment_methods" => array(
				"excluded_payment_methods" => array(
					array(
						"id" => "khipu"
					)
				)
			),
			//"payment_method_id" => array('visa','master','webpay'),
			"payer" => array(
		        "first_name" => $info_email,
		        "email" => $info_name
		    ),
			"back_urls" => array(
				"success" => $url_base . "tienda/mercadopago/casepay.php?oc={$new_oc}&case=success",
				"failure" => $url_base . "tienda/mercadopago/casepay.php?oc={$new_oc}&case=failure",
				"pending" => $url_base . "tienda/mercadopago/casepay.php?oc={$new_oc}&case=pending"
			),	
			"auto_return" => "approved",
		   "notification_url" => $url_base . "tienda/mercadopago/notificacion.php?oc={$new_oc}&cod={$cod}",
		   "expires" => false,
		   "expiration_date_from" => null,
		   "expiration_date_to" => null

		);

		$preference = $mp->create_preference($preference_data);
    }


    if($metodo_pago == 'transferencia'){
    	// include('../PHPMailer/PHPMailerAutoload.php');
    	include '../Mailin.php';

		echo enviarComprobanteAdmin($new_oc, false, false);
		echo enviarComprobanteCliente($new_oc);

		setcookie('cart_alfa_cm', null, -1, '/');

		header("Location: ../exito-transferencia?oc=$new_oc");

    }

} else {
	header("location:404");
}
?>
<div style="position:absolute; top:0; left:0; width:100%; height:100%; z-index:999; background-image: url(<?= $url_base; ?>tienda/trama.png);">
    <div style="margin: 0 auto; width: 100%;text-align:center;font-family:verdana;font-size: 12px;padding-top:200px;">
        <img src="<?php echo $url_base; ?>img/logo.png" border="0" /><br /><br />
            Estamos procesando su compra, espere por favor...<br />
            cant productos = <?php echo $cantTotal; ?><br />
            total = <?php echo $total ?>
    </div>
</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>


<?php if($metodo_pago == 'webpay'){ ?>
<form id="form" action="webpay/tbk-normal.php" method="post" >
    <input type="hidden" name="TBK_MONTO" value="<?php echo $token; ?>" />
    <input type="hidden" name="TBK_ORDEN_COMPRA" value="<?php echo $new_oc;?>" />
    <input type="hidden" name="TBK_ID_SESION" value="<?php echo $new_oc;?>" />
    <input type="hidden" name="URL_RETURN" value="<?= opciones("url_sitio"); ?>/tienda/webpay/tbk-normal.php?action=result" />
    <input type="hidden" name="URL_FINAL" value="<?= opciones("url_sitio"); ?>/tienda/webpay/tbk-normal.php?action=end" />
</form>
<?php } elseif($metodo_pago == 'mercadopago') { ?>
    <script type="text/javascript">
		setTimeout(function(){
		window.location.href = "<?=$preference["response"]["init_point"]?>";
		}, 1000); // 2Segs
	</script>

<?php } ?>

<?php 
mysqli_close($conexion); ?>

<script type="text/javascript">
$(function(){
	$('#form').submit();
});
</script>