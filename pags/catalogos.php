<div class="breadcrumbs">  
    <div class="container">
        <ul itemscope itemtype="http://schema.org/BreadcrumbList">
            <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="home" itemprop="item">
                    <span itemprop="name">Home</span>
                    <meta itemprop="position" content="1" />
                </a>
                <img src="img/icons/sliderArrow.svg">
            </li>

            <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="catalogos" itemprop="item" class="active">
                    <span itemprop="name">Catálogos</span>
                    <meta itemprop="position" content="2" />
                </a>
            </li>
        </ul>
    </div>
</div>

<?php $catalogos = consulta_bd('id, nombre, archivo', 'catalogos', '', 'posicion asc'); ?>

<div class="container mt-20 mb-50">
    <div class="titulo-pag">Catálogos</div>
    <div class="descripcion-pag">Descarga nuestros catálogos de productos, y entérate de novedades.</div>

    <div class="gr-catalogos mt-20">
        <?php if (is_array($catalogos)): ?>
            <?php foreach ($catalogos as $cat): ?>
                <div class="col">
                    <div class="colPdf"><img src="img/pdf.svg"></div>
                    <div class="info">
                        <span class="name"><?= $cat[1] ?></span>
                        <a href="docs/catalogos/<?= $cat[2] ?>" target="_blank" class="link">Descargar catálogo</a>
                    </div>
                </div>
            <?php endforeach ?>
        <?php else: ?>
            <p>Aún no hay catálogos cargados</p>
        <?php endif ?>
    </div>
</div>