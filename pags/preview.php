<?php 

if(!$_SESSION[Blackpix_admin]){
		echo '<script>location.href ="404";</script>';
	}

	$id = (is_numeric($_GET[id])) ? mysqli_real_escape_string($conexion, $_GET[id]) : 0;
	$producto = consulta_bd("distinct(p.id), 
							c.id, 
							c.nombre, 
							c.linea_id,
							l.nombre, 
							sc.nombre, 
							sc.id, 
							p.nombre,
							pd.sku,
							p.descripcion, 
							p.ficha_tecnica, 
							pd.precio,
							pd.descuento,
							pd.id,
							pd.stock, 
							pd.stock_reserva, pd.venta_minima, p.descripcion_seo, m.nombre",
	"categorias c, lineas l, subcategorias sc, lineas_productos lp, productos p, productos_detalles pd, marcas m",
	"l.id = c.linea_id and c.id=sc.categoria_id and sc.id = lp.subcategoria_id and lp.producto_id = p.id and m.id = p.marca_id and p.id = $id and pd.producto_id = p.id",
	"");
    $cantProd = mysqli_affected_rows($conexion);
	
    //cuando no hay poroducto dirijo al 404
    if($cantProd < 1){
		//echo '<script>location.href ="404";</script>';
		}

$galeria = consulta_bd("archivo","img_productos","producto_id = $id","posicion asc");
			 
    //die(print_r($producto));

?>
	<script type="text/javascript">
    $(function() {
        $("#spinner").spinner({
			max: <?php echo $producto[0][14] - $producto[0][15] ?>,
			min: <?= $producto[0][16]; ?>,
			step: <?= $producto[0][16]; ?>
        });
		
    });
    </script>



<div class="cont100">
    <div class="cont100Centro">
        <ul class="breadcrumb">
          <li><a href="home">Home</a></li>
          <li><a href="lineas/<?= $producto[0][3]."/".url_amigables($producto[0][4]); ?>"><?= $producto[0][4]; ?></a></li>
          <li><a href="categorias/<?= $producto[0][1]."/".url_amigables($producto[0][2]); ?>"><?= $producto[0][2]; ?></a></li>
          <li><a href="subcategorias/<?= $producto[0][6]."/".url_amigables($producto[0][5]); ?>"><?= $producto[0][5]; ?></a></li>
          <li class="active"><?= $producto[0][7]; ?></li>
        </ul>
    </div>
</div><!--Fin breadcrumbs -->
    







<div class="contValoresFixed">
	<div class="valoresFixed cont100Centro">
    	<div class="cont50">
        	<div class="contImgFixedFicha">
            	<img src="imagenes/productos/<?php echo $galeria[0][0]; ?>" width="100%" />
            </div>
            <div class="infoFixedFicha">
            	<span><strong><?= $producto[0][7]; ?></strong></span>
                <span class="codigo">SKU:<?php echo $producto[0][8]; ?></span>
            </div>
        </div><!--Fin cont50-->
        
        <div class="cont50 floatRight">
        	<a href="javascript:void(0)" rel="<?php echo $producto[0][13]; ?>" class="btnAgregar btnAgregarFixed" id="btnAgregarFixed">
               <i class="fas fa-cart-plus"></i> AGREGAR AL CARRO
            </a> 
        	<div class="contPrecioFixed">
                <p class="precio-ficha">
                    <span class="precioOfertaFicha"><strong>$<?php echo number_format(getPrecio($producto[0][13]),0,",","."); ?></strong></span>
                    <?php if(tieneDescuento($producto[0][13])){ ?>
                        <span class="precioNormal">Normal: $<?php echo number_format(getPrecioNormal($producto[0][13]),0,",","."); ?></span>
                    <?php } ?>
                </p>
            </div>
        </div><!--Fin cont50-->
    </div><!--Fin valoresFixed -->
</div><!--Fin contValoresFixed -->









<div class="cont100">
    <div class="cont100Centro">
        
        <div class="cont50 galeriaImagenes">
            <?php if(ofertaTiempo($producto[0][13])){ ?>
                <div class="countdown" rel="<?= ofertaTiempoHasta($producto[0][13]); ?>"></div>
            <?php } ?>
        	<div class="galeria cont100">
            	<div class="imgPrincipal cont100">

                    <?php if ($is_cyber and is_cyber_product($id)): ?>
                        <div class="img-cyber"><img src="img/iconcyber.png" alt="Cyber"></div>
                    <?php endif ?>
                    
                    <a href="imagenes/productos/<?php echo $galeria[0][0]; ?>" class="fancybox cont100">
                        <img src="imagenes/productos/<?php echo $galeria[0][0]; ?>" width="100%" id="img-grande">
                    </a>
                </div>
                <div class="thumbsGaleriaFicha cont100">
                	<?php for($i=0; $i<sizeof($galeria); $i++){ ?>
                    <a href="imagenes/productos/<?php echo $galeria[$i][0]; ?>" rel="imagenes/productos/<?php echo $galeria[$i][0]; ?>" class="thumbs-galeria img-thumbnail fancybox">
                        <img src="imagenes/productos/<?php echo $galeria[$i][0]; ?>" width="100%" />
                    </a>
                    <?php } ?>
                </div>
                	
               
            </div><!-- /Fin Galeria-->
        </div><!--Fin galeriaImagenes-->
        
        
        
        <div class="cont50 infoFichaProducto">
        	<div class="infoProducto">
                <?php if(ultimasUnidades($producto[0][13])){ ?><div class="etq-ultimas">ultimas unidades</div><?php } ?>
                <h2><?= $producto[0][7]; ?></h2>
                <span class="codigo">SKU:<?php echo $producto[0][8]; ?></span>
                
                <p class="precio-ficha">
                    <?php if ($is_cyber and is_cyber_product($id)): 
                        $precios = get_cyber_price($producto[0][13]);
                        $precio_final = $precios['precio_cyber']; ?>
                        <span class="precioOfertaFicha">$<?php echo number_format($precios['precio_cyber'],0,",","."); ?></span>
                        <span class="precioNormal">Normal: $<?php echo number_format($precios['precio'],0,",","."); ?></span>
                    <?php else: 
                        $precio_final = getPrecio($producto[0][13]); ?>
                        <span class="precioOfertaFicha">$<?php echo number_format(getPrecio($producto[0][13]),0,",","."); ?></span>
                        <?php if(tieneDescuento($producto[0][13])){ ?>
                            <span class="precioNormal">Normal: $<?php echo number_format(getPrecioNormal($producto[0][13]),0,",","."); ?></span>
                        <?php } ?>
                    <?php endif; ?>
                </p>
        
            </div>
             
            <div class="infoProducto">
             	<div class="filaAcciones cont100">
                	<div class="cantidad" rel="<?= $producto[0][13]; ?>">
                        <input id="spinner" name="cantidad" value="<?= $producto[0][16]; ?>" readonly/>
                    </div>
                    <a href="javascript:void(0)" rel="<?= $producto[0][13]; ?>" class="btnAgregar" id="btn_agregarCarro">
                       <i class="fas fa-cart-plus"></i> AGREGAR AL CARRO
                    </a> 
                </div><!--Fin filaAcciones -->
                
				
                <?php  if(opciones("cotizador") == 1){ 
				//aca debe ir la regla para poder cotizar 
				//Solo en el caso que el cliente lo necesite
				?>	
                <div class="filaAcciones cont100">
                	<a href="javascript:void(0)" rel="<?= $producto[0][13]; ?>" class="btnAgregar" id="btn_agregarCotizacion">
                       <i class="fas fa-plus"></i> AGREGAR A COTIZACIÓN
                    </a> 
                </div>    
                <?php } ?>
                
                
                
                <div class="filaAcciones cont100">
                    <?php 
					$guardado = guardadoParaDespues($producto[0][13]); 
					if($guardado == 1){ ?>
					<a href="javascript:void(0)" onclick="quitarLista(<?= $producto[0][13]; ?>)" class="btn btn-lista-ficha">
                        <i class="fas fa-heart"></i> GUARDAR PARA DESPUES
                    </a>
					<?php } else { ?>
					<a href="javascript:void(0)" onclick="agregarLista(<?= $producto[0][13]; ?>)" class="btn btn-lista-ficha">
                        <i class="far fa-heart"></i> GUARDAR PARA DESPUES
                    </a>
					<?php } ?>
                    
                    
				</div><!--Fin filaAcciones -->

                <?php if(!tieneDescuento($producto[0][13])){ ?>
                    <?php $precios_cantidad = consulta_bd("nombre, rango, descuento","precios_cantidades","productos_detall_id = {$producto[0][13]}",""); ?>

                    <?php if($precios_cantidad){ ?>
                        <div class="filaAcciones cont100">
                            <p>Descuentos por cantidad</p>
                            <?php foreach($precios_cantidad as $pc){ ?>
                                <div class="preciocant"> <?= $pc[0]; ?> <span> - <?= number_format($pc[2],0,',','.'); ?>%</span></div>
                            <?php } ?>
                        </div>
                    <?php } ?>
                <?php } ?>

					
                    
                <?php if($producto[0][9] != ''){ ?>
                    <p class="tit-descripcion"> Descripción</p>
                    <div class="p-descrip" itemprop="description">
                        <?php echo $producto[0][9]; ?>
                    </div>
                <?php } ?>
             </div>
        	
        </div><!--FIn infoProducto -->
        
    </div>
</div><!--Fin contenido documento-->









<?php 
    //si existe un producto por el id muestro relacionados
	//los relacionados se muestran por la relacion en el administrador y se  completa la cantidad indicada con otros relacionados 
	//de la misma subcategoria
if($cantProd > 0){ 
	$cantidadRelacionados = 4;
	$relacionadosAdmin = consulta_bd("p.id, p.nombre, p.thumbs, p.marca_id, pd.precio, pd.descuento, pd.id","productos p, productos_detalles pd, productos_relacionados pr","pr.producto_id = $id and pr.producto_relacionado = p.id and p.id = pd.producto_id and p.publicado = 1 group by(p.id)","p.id desc limit $cantidadRelacionados"));
	$cantRelacionadosAdmin = mysqli_affected_rows($conexion);
	
	$relacionadosAutomaticos = $cantidadRelacionados - $cantRelacionadosAdmin;
	$relacionados = consulta_bd("p.id, p.nombre, p.thumbs, p.marca_id, pd.precio, pd.descuento, pd.id","productos p, lineas l, categorias c, lineas_productos cp, productos_detalles pd, subcategorias sc","l.id=c.linea_id and c.id = sc.categoria_id and sc.id = cp.subcategoria_id and cp.producto_id = p.id and p.id = pd.producto_id and p.publicado = 1 and cp.subcategoria_id = {$producto[0][6]} and p.id <> $id  group by(p.id)","rand() limit $relacionadosAutomaticos");
	$cantRelacionados = mysqli_affected_rows($conexion);	
?>

    <!--Productos relacionados -->
    <div class="cont100">
        <div class="cont100Centro">
        	<h3>Productos relacionados</h3>
            
            <?php for($i=0; $i<sizeof($relacionadosAdmin); $i++){ ?>
                <div class="grilla">
                    <?php if(ultimasUnidades($relacionadosAdmin[$i][0])){ ?><div class="etq-ultimas">ultimas unidades</div><?php } ?>
                	<a href="ficha/<?= $relacionadosAdmin[$i][0]; ?>/<?= url_amigables($relacionadosAdmin[$i][1]); ?>" class="imgGrilla"></a>
                    <a href="ficha/<?= $relacionadosAdmin[$i][0]; ?>/<?= url_amigables($relacionadosAdmin[$i][1]); ?>" class="nombreGrilla"><?= $relacionadosAdmin[$i][1]; ?></a>
                    <a href="ficha/<?= $relacionadosAdmin[$i][0]; ?>/<?= url_amigables($relacionadosAdmin[$i][1]); ?>" class="valorGrilla">
                        <?php if(tieneDescuento($relacionadosAdmin[$i][6])){ ?>
                            <span class="antes">$<?= number_format(getPrecioNormal($relacionadosAdmin[$i][6]),0,",",".") ?></span>
                            <span> - </span>
                            <span class='conDescuento'>$<?= number_format(getPrecio($relacionadosAdmin[$i][6]),0,",",".") ?></span>
                        <?php }else{ ?>
                            $<?= number_format(getPrecio($relacionadosAdmin[$i][6]),0,",",".") ?>
                        <?php } ?>
                    </a>
                </div><!--Fin Grilla -->
            <?php } ?>
            
            
            <?php for($i=0; $i<sizeof($relacionados); $i++){ ?>
            <div class="grilla">
                <?php if(ultimasUnidades($relacionados[$i][0])){ ?><div class="etq-ultimas">ultimas unidades</div><?php } ?>
            	<a href="ficha/<?= $relacionados[$i][0]; ?>/<?= url_amigables($relacionados[$i][1]); ?>" class="imgGrilla"></a>
                <a href="ficha/<?= $relacionados[$i][0]; ?>/<?= url_amigables($relacionados[$i][1]); ?>" class="nombreGrilla"><?= $relacionados[$i][1]; ?></a>
                <a href="ficha/<?= $relacionados[$i][0]; ?>/<?= url_amigables($relacionados[$i][1]); ?>" class="valorGrilla">
                    <?php if(tieneDescuento($relacionados[$i][6])){ ?>
                        <span class="antes">$<?= number_format(getPrecioNormal($relacionados[$i][6]),0,",",".") ?></span>
                        <span> - </span>
                        <span class='conDescuento'>$<?= number_format(getPrecio($relacionados[$i][6]),0,",",".") ?></span>
                    <?php }else{ ?>
                        $<?= number_format(getPrecio($relacionados[$i][6]),0,",",".") ?>
                    <?php } ?>
                </a>
            </div><!--Fin Grilla -->
            <?php } ?>
            
        </div>
    </div><!--Productos relacionados -->

<?php } //fin condicion relacionados ?>
<div>
    <div itemtype="http://schema.org/Product" itemscope>
        <meta itemprop="mpn" content="<?= $producto[0][8] ?>" />
        <meta itemprop="name" content="<?= $producto[0][7] ?>" />
        <?php if (is_array($galeria)): ?>
            <?php for($i=0; $i<sizeof($galeria); $i++){ ?>
                <link itemprop="image" href="imagenes/productos/<?= $galeria[$i][0] ?>" />
            <?php } ?>
        <?php endif ?>
        <meta itemprop="description" content="<?= strip_tags($producto[0][17]) ?>" />
        
        <div itemprop="offers" itemtype="http://schema.org/Offer" itemscope>
            <link itemprop="url" href="<?= $url_base ?>ficha/<?= $id ?>/<?= $nombre ?>" />
            <?php if ($producto[0][14] > 0): ?>
            <meta itemprop="availability" content="https://schema.org/InStock" />
            <?php else: ?>
            <meta itemprop="availability" content="https://schema.org/OutOfStock" />
            <?php endif ?>
            <meta itemprop="priceCurrency" content="CLP" />
            <meta itemprop="itemCondition" content="https://schema.org/NewCondition" />
            <meta itemprop="price" content="<?= $precio_final ?>" />
        </div>
        <meta itemprop="sku" content="<?= $producto[0][8] ?>" />
        <div itemprop="brand" itemtype="http://schema.org/Thing" itemscope>
            <meta itemprop="name" content="<?= $producto[0][18] ?>" />
        </div>
    </div> 
</div>
