<?php 

$id = (int)mysqli_real_escape_string($conexion, $_GET['id']);
$nombre = mysqli_real_escape_string($conexion, $_GET['nombre']);

$producto = getProduct($id);

if (!$producto) {
    echo '<script>location.href = "home";</script>';
}

$productosVistos = arrVistos($id);


$precio_final = ($producto['datos']['descuento'] > 0) ? $producto['datos']['descuento'] : $producto['datos']['precio'];

?>

<div class="breadcrumbs">  
    <div class="container">
        <ul itemscope itemtype="http://schema.org/BreadcrumbList">
            <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="home" itemprop="item">
                    <span itemprop="name">Home</span>
                    <meta itemprop="position" content="1" />
                </a>
                <img src="img/icons/sliderArrow.svg">
            </li>

            <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="lineas/<?= $producto['breadcrumbs']['linea_id'] ?>/<?= url_amigables($producto['breadcrumbs']['linea']) ?>" itemprop="item">
                    <span itemprop="name"><?= $producto['breadcrumbs']['linea'] ?></span>
                    <meta itemprop="position" content="2" />
                </a>
                <img src="img/icons/sliderArrow.svg">
            </li>

            <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="categorias/<?= $producto['breadcrumbs']['categoria_id'] ?>/<?= url_amigables($producto['breadcrumbs']['categoria']) ?>" itemprop="item">
                    <span itemprop="name"><?= $producto['breadcrumbs']['categoria'] ?></span>
                    <meta itemprop="position" content="3" />
                </a>
                <img src="img/icons/sliderArrow.svg">
            </li>

            <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="subcategorias/<?= $producto['breadcrumbs']['subcategoria_id'] ?>/<?= url_amigables($producto['breadcrumbs']['subcategoria']) ?>" itemprop="item">
                    <span itemprop="name"><?= $producto['breadcrumbs']['subcategoria'] ?></span>
                    <meta itemprop="position" content="4" />
                </a>
                <img src="img/icons/sliderArrow.svg">
            </li>

            <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="#" itemprop="item" class="active">
                    <span itemprop="name"><?= $producto['datos']['nombre'] ?></span>
                    <meta itemprop="position" content="5" />
                </a>
            </li>
        </ul>
    </div>
</div>

<div class="container mt-20 mb-20">
    <div class="name-cel">
        <?php if ($producto['datos']['stock'] > 0): ?>
            <div class="stock">Stock disponible</div>
        <?php else: ?>
            <div class="stock">Stock no disponible</div>
        <?php endif ?>
        <div class="nombre"><?= $producto['datos']['nombre'] ?></div>
    </div>
    <div class="gr-ficha">
        <div class="col">
            <div class="slider-ficha-nav">
                <?php if ($producto['imagenes'] != false): ?>
                    <?php foreach ($producto['imagenes'] as $imgs_nav): ?>
                        <div class="item" style="background: url(imagenes/productos/<?= $imgs_nav ?>);"></div>
                    <?php endforeach ?>
                <?php else: ?>
                     <div class="item" style="background: url(img/not-imagef.jpg);"></div>
                <?php endif ?>
            </div>

            <div class="principal">

                <div class="ajx_descuento">
                    <?php if ($producto['datos']['descuento'] > 0): ?>
                        <div class="circulo_descuento">
                            -<?= round(100 - ($producto['datos']['descuento'] * 100) / $producto['datos']['precio']) ?>%
                        </div>
                    <?php endif ?>
                    
                </div>

                <div class="slider-ficha-for">
                    <?php if ($producto['imagenes'] != false): ?>
                        <?php foreach ($producto['imagenes'] as $imgs): ?>
                            <div class="item">
                                <img src="imagenes/productos/<?= $imgs ?>">
                            </div>
                        <?php endforeach ?>
                    <?php else: ?>
                        <div class="item">
                            <img src="img/not-imagef.jpg">
                        </div>
                    <?php endif ?>
                </div>

                <div class="prevFicha"><img src="img/arrowFicha.svg"></div>
                <div class="nextFicha"><img src="img/arrowFicha.svg"></div>
            </div>
        </div>
        <div class="col">
            <div class="box-info">
                <div class="name-desk">
                    <?php if ($producto['datos']['stock'] > 0): ?>
                        <div class="stock">Stock disponible</div>
                    <?php else: ?>
                        <div class="stock">Stock no disponible</div>
                    <?php endif ?>
                    <div class="nombre"><?= $producto['datos']['nombre'] ?></div>
                </div>

                <div class="precio_ficha">
                    <?php if ($producto['datos']['solo_cotizar'] != 1): ?>
                        <?php if ($producto['datos']['producto_cadena'] == 0): ?>
                            <?php if ($producto['datos']['descuento'] > 0): ?>
                                <span class="descuento">Antes $<?= number_format($producto['datos']['precio'], 0, ",", ".") ?> + IVA</span>
                                <span class="precio">$<?= number_format($producto['datos']['descuento'], 0, ",", ".") ?> + IVA</span>
                            <?php else: ?>
                                <span class="precio">$<?= number_format($producto['datos']['precio'], 0, ",", ".") ?> + IVA</span>
                            <?php endif ?>
                        <?php else: ?>
                            <span class="texto-precio">Selecciona las medidas para ver el precio</span>
                        <?php endif ?>
                    <?php endif ?>
                </div>

                <div class="descripcion_corta">
                    <?= $producto['datos']['descripcion_corta'] ?>
                </div>
                <div class="descargar_ficha">
                    <?php if ($producto['datos']['ficha_tecnica'] != ''): ?>
                        <a href="docs/productos/<?= $producto['datos']['ficha_tecnica'] ?>" target="_blank">Descargar ficha técnica <span class="material-icons">save_alt</span></a>
                    <?php endif ?>
                </div>
                
                <div class="box-dimensiones">
                    <?php if ($producto['datos']['producto_cadena'] == 1): 
                        $ancho  = consulta_bd('cadena_ancho', 'productos_detalles', "producto_id = {$id} GROUP BY cadena_ancho", '');
                        $perfil = consulta_bd('cadena_perfil', 'productos_detalles', "producto_id = {$id} GROUP BY cadena_perfil", '');
                        $aro    = consulta_bd('cadena_aro', 'productos_detalles', "producto_id = {$id} GROUP BY cadena_aro", '');   ?>
                    <div class="titulo">
                        Selecciona dimensiones
                        <div class="a-example"><span>Guía de medidas</span> <span class="material-icons">square_foot</span></div>

                        <div class="content-example">
                            <span class="material-icons close-example">close</span>
                            <div class="text">Ejemplo: Ancho 255 / Perfil 50 / Aro 17</div>
                        </div>
                    </div>

                    <div class="gr-dimensiones" data-id="<?= $id ?>">
                        <div class="col">
                            <div class="custom-select select-dim" data-name="Ancho" data-request="ancho">
                                <select>
                                    <option value="0">&nbsp;</option>
                                    <?php foreach ($ancho as $a): ?>
                                        <option value="<?= $a[0] ?>"><?= $a[0] ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>
                        <div class="col">
                            <div class="custom-select select-dim select-perfil" data-name="Perfil" data-request="perfil">
                                <select>
                                    <option value="0">&nbsp;</option>
                                </select>
                            </div>
                        </div>
                        <div class="col">
                            <div class="custom-select select-dim" data-name="Aro" data-request="aro">
                                <select>
                                    <option value="0">&nbsp;</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <?php endif ?>

                    <?php
                        $gridAcciones = ($producto['datos']['producto_cadena'] == 1 OR $producto['datos']['solo_cotizar'] == 1) ? 'gr-accionesCarro-dim' : '';
                    ?>

                    <div class="gr-accionesCarro <?= $gridAcciones ?>">
                        <div class="col">
                            <div class="box_main_qty">
                                <div class="box_qty" data-id="<?= $id_hijo ?>">
                                    <span>Cantidad:</span>
                                    <input type="text" class="cantidad_ficha" value="1" readonly>
                                    <div class="text_unidad">Unidad</div>
                                </div>
                                
                                <div class="box_select_stock">
                                        
                                    <?php for ($i=0; $i < 6; $i++) {
                                        $text_unidad = ($i+1 > 1) ? 'Unidades' : 'Unidad'; ?>
                                        <div class="row_stock_ficha" data-val="<?= $i+1 ?>"><?= $i+1 . ' ' . $text_unidad ?> </div>
                                    <?php } ?>

                                    <div class="row_stock_ficha" data-val="add">Más de 6 unidades</div>
                                    
                                </div>
                            </div>
                        </div>
                        <?php if ($producto['datos']['solo_cotizar'] == 1): ?>
                             <div class="col col-cotizar col-remove">
                                <a href="javascript:void(0)" class="btnCotizar" onclick="cotizarProducto(this, <?= $producto['datos']['hijo_id'] ?>)">Cotizar</a>
                            </div>
                        <?php else: ?>
                           <?php if ($producto['datos']['producto_cadena'] == 1): ?>
                                <div class="col col-cotizar col-remove">
                                    <a href="javascript:void(0)" class="buttonDisabledFicha">Selecciona las medidas para comprar</a>
                                </div>
                            <?php else: ?>
                                <div class="col col-cotizar col-remove">
                                    <a href="javascript:void(0)" class="btnCotizar" onclick="cotizarProducto(this, <?= $producto['datos']['hijo_id'] ?>)">Cotizar</a>
                                </div>
                                <div class="col col-remove">
                                    <a href="javascript:void(0)" class="btnComprar" onclick="comprarProducto(this, <?= $producto['datos']['hijo_id'] ?>)">Agregar al carro</a>
                                </div>
                            <?php endif ?>
                        <?php endif ?>
                        
                    </div>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>
    </div>

    <div class="content_tabs_ficha">
        <div class="w100-tabs">
            <div class="headTabs">
                <div class="tab active" data-tab="descripcionTab">Descripción</div>
                <!-- <div class="tab" data-tab="fichaTab">Ficha técnica</div> -->
                <div class="tab" data-tab="caracteristicasTab">Características</div>
                <div class="tab" data-tab="garantiaTab">Garantía</div>
            </div>
        </div>

        <div class="bodyTabs showBody" id="descripcionTab">
            <?= $producto['datos']['descripcion'] ?>
        </div>

        <!-- <div class="bodyTabs" id="fichaTab">
            <p>Ficha</p>
        </div> -->

        <div class="bodyTabs" id="caracteristicasTab">
            <?= $producto['datos']['caracteristicas'] ?>
        </div>

        <div class="bodyTabs" id="garantiaTab">
            <?= $producto['datos']['garantia'] ?>
        </div>
    </div>

</div>
    
<div class="bg-relacionados">
    <div class="container">
        <div class="titulo mb-20">Productos vistos recientemente</div>

        <div class="gr-destacadosBanner">
            <div class="col">
                <div class="productos-grilla">
                    
                    <?php foreach ($productosVistos as $item):
                        $url = 'ficha/'.$item['id'].'/' .url_amigables($item['nombre']); ?>
                        <div class="item">
                            <a href="<?= $url ?>" class="headerProduct">
                                <?php if ($item['descuento'] > 0): ?>
                                    <span class="oferta-grilla">-<?= round(100 - ($item['descuento'] * 100) / $item['precio']) ?>%</span>
                                <?php endif ?>
                                <img src="<?= $item['imagen'] ?>" class="imgProduct">
                            </a>

                            <div class="bodyProduct">
                                <a href="<?= $url ?>" class="name"><?= $item['nombre'] ?></a>
                                
                                <?php if ($item['solo_cotizar'] == 1): ?>
                                    <div class="description">
                                        <?= $item['descripcion'] ?>
                                        <!-- Porta cuña Plástico para cuñas 5 Ton. de poliuretano. (Camioneta Minera) -->
                                    </div>
                                <?php else: ?>
                                    <div class="prices">
                                        <?php if ($item['descuento'] > 0): ?>
                                            <a href="<?= $url ?>" class="discountPrice">Antes $<?= number_format($item['precio'], 0, ',', '.') ?> + IVA</a>
                                            <a href="<?= $url ?>" class="finalPrice">$<?= number_format($item['descuento'], 0, ',', '.') ?> + IVA</a>
                                        <?php else: ?>
                                            <a href="<?= $url ?>" class="finalPrice">$<?= number_format($item['precio'], 0, ',', '.') ?> + IVA</a>
                                        <?php endif ?>
                                        
                                        
                                    </div>
                                <?php endif ?>

                                <a href="<?= $url ?>" class="btnDetails">Ver ficha</a>
                            </div>
                        </div>
                    <?php endforeach ?>
                </div>
            </div>
            <div class="col">
                <a href="https://socadinonline.cl/categorias/17/izaje-de-carga" class="bannerSlider">
                    <img src="img/bannerSlider1-1.jpg">
                </a>
            </div>
        </div>
    </div>  
</div>

<div class="popupCot">
    <div class="head">
        <h3>Agregado al carro de cotización <img class="closeCot" src="img/icons/closeNegro.svg"></h3>
    </div>
    
    <div class="ajxLoad-cot">
        <div class="content-popcot">
            <?php 

            $carro_cot = getCarroCotizacion();

            ?>
            <?php if (qty_cotizacion() > 0): ?>
                <?php foreach ($carro_cot as $item_cot) { ?>
                    <div class="row_pr">
                        <div class="eliminar" onclick="eliminarItemCot(<?= $item_cot['id'] ?>)"><img src="img/icons/closeGris.svg"></div>
                        <a href="#">
                            <img src="<?= $item_cot['imagen'] ?>">
                        </a>

                        <div class="info">
                            <a href="#" class="nombre"><?= $item_cot['nombre'] ?></a>

                            <span class="unidad"><?= $item_cot['cantidad'] ?> Unidad</span>
                        </div>
                    </div>
                <?php } ?>
            <?php else: ?>
                <p class="carro_vaciopop">Su carro de cotización está vacío</p>
            <?php endif ?>
            
            <a href="carro-cotizacion" class="buttonPop">Cotizar</a>
        </div>
    </div>
</div>

<div>
    <div itemtype="http://schema.org/Product" itemscope>
        <meta itemprop="mpn" content="<?= $producto['datos']['sku'] ?>" />
        <meta itemprop="name" content="<?= $producto['datos']['nombre'] ?>" />
        <?php if ($producto['imagenes'] != false): ?>
            <?php foreach ($producto['imagenes'] as $imgs_nav): ?>
                <link itemprop="image" href="<?= $url_base ?>imagenes/productos/<?= $imgs_nav ?>" />
            <?php endforeach ?>
        <?php endif ?>
        <meta itemprop="description" content="<?= strip_tags($producto['datos']['descripcion']) ?>" />
        
        <div itemprop="offers" itemtype="http://schema.org/Offer" itemscope>
            <link itemprop="url" href="<?= $url_base ?>ficha/<?= $id ?>/<?= $nombre ?>" />
            <?php if ($producto['datos']['stock'] > 0): ?>
            <meta itemprop="availability" content="https://schema.org/InStock" />
            <?php else: ?>
            <meta itemprop="availability" content="https://schema.org/OutOfStock" />
            <?php endif ?>
            <meta itemprop="priceCurrency" content="CLP" />
            <meta itemprop="itemCondition" content="https://schema.org/NewCondition" />
            <meta itemprop="price" content="<?= $precio_final ?>" />
        </div>
        <meta itemprop="sku" content="<?= $producto['datos']['sku'] ?>" />
        <div itemprop="brand" itemtype="http://schema.org/Thing" itemscope>
            <meta itemprop="name" content="<?= $producto['datos']['marca'] ?>" />
        </div>
    </div> 
</div>
