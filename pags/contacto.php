<div class="breadcrumbs">  
    <div class="container">
        <ul itemscope itemtype="http://schema.org/BreadcrumbList">
            <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="home" itemprop="item">
                    <span itemprop="name">Home</span>
                    <meta itemprop="position" content="1" />
                </a>
                <img src="img/icons/sliderArrow.svg">
            </li>

            <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="contacto" itemprop="item" class="active">
                    <span itemprop="name">Contacto</span>
                    <meta itemprop="position" content="2" />
                </a>
            </li>
        </ul>
    </div>
</div>

<div class="container mb-50 mt-20 relative">
    <div class="left-contact">
        <div class="titulo-pag">Contáctanos</div>
        <p class="body-pag2">Certificación y garantía de productos.</p>

        <p class="body-pag2">Dirección: Salvador Gutiérrez 4539, Quinta Normal, Santiago. <br>
        Teléfono: + 56 2 2773 3726
        Teléfono: + 56 2 2773 8820</p>

        <p class="body-pag2">Hemos habilitado los siguientes números para que se comunique directamente o si lo prefiere, para atención vía Whatsapp con atención de Lunes a Viernes de 9:00 a 18:00 Hrs.:</p>

        <p class="body-pag2">Teléfono: + 56 9 3253 4231 <br>
        Teléfono: + 56 9 3253 4225 <br>
        Teléfono: + 56 9 3077 2825 <br>
        Teléfono: + 56 9 4414 4452</p>
    </div>

    <div class="contact_content-form">
        <?php if (isset($_GET['status']) AND $_GET['status'] == 'success' AND isset($_SESSION['flash'])): ?>
            <div class="content_success">
                <div class="vertical-align">
                    <div class="box">
                        <img src="img/correosuccess.svg" width="60px">
                        <div class="succ_msje">Su mensaje se ha enviado con éxito. <br> Muchas gracias.</div>
                        <div class="sub_m">En breve recibirá una respuesta.</div>
                        <a href="home" class="btnSucc">Aceptar</a>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <div class="titulo">Vamos, hablemos</div>
    
        <form action="enviarContacto" method="post" id="formContacto">

            <div class="main-input">
                <div class="input-form">
                    <label class="label_form">Nombre y apellido</label>
                    <input type="text" id="nombre" name="nombre" class="input_text">
                </div>
            </div>

            <div class="main-input">
                <div class="input-form">
                    <label class="label_form">Email</label>
                    <input id="email" type="email" name="email" class="input_text">
                </div>
            </div>

            <div class="main-input">
                <div class="input-form">
                    <label class="label_form">Teléfono</label>
                    <input id="telefono" type="text" name="telefono" class="input_text" onkeypress="return justNumbers(event);" maxlength="9">
                </div>
            </div>

            <div class="main-input">
                <div class="input-form">
                    <label class="label_form">Mensaje</label>
                    <textarea id="mensaje" name="mensaje" rows="4"></textarea>
                </div>
            </div>

            <button type="button" class="btnDistribuidor" id="enviarFormularioRecaptcha">Enviar</button>
        </form>        
    </div>
</div>

<div class="clearfix"></div>

<div class="contacto-map mt-50">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3329.9086231282463!2d-70.69962764974605!3d-33.4256265806867!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9662c41697ca576d%3A0xa9eeecbf2b455ee3!2sSalvador%20Guti%C3%A9rrez%204539%2C%20Quinta%20Normal%2C%20Regi%C3%B3n%20Metropolitana!5e0!3m2!1ses!2scl!4v1589062405895!5m2!1ses!2scl" width="100%" height="210" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
</div>

<?php if (isset($_SESSION['flash'])):
    $status = $_SESSION['flash']['status'];
    $message = $_SESSION['flash']['message'];
    if ($status != 'success'): ?>
         <script type="text/javascript">
            $(function(){
                swal('', "<?= $message ?>", "<?= $status ?>");
            })
        </script>
    <?php endif ?>
<?php
unset($_SESSION['flash']);
 endif ?>