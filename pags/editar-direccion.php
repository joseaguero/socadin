<?php 
    if(!isset($_COOKIE['usuario_id'])){
        echo '<script>location.href = "home"</script>';
    }

    $id = (int)mysqli_real_escape_string($conexion, $_GET['id']);
    $uid = (int)$_COOKIE['usuario_id'];

    $direccion = consulta_bd('d.calle, d.region_id, d.comuna_id, d.numero, d.numero_extra, r.nombre, c.nombre', 'clientes_direcciones d JOIN regiones r ON r.id = d.region_id JOIN comunas c ON c.id = d.comuna_id', "d.id = $id AND d.cliente_id = $uid", '');

    if (!is_array($direccion)) {
        echo '<script>location.href = "cuenta/direcciones"</script>';
    }

?>
<div class="breadcrumbs">  
    <div class="container">
        <ul itemscope itemtype="http://schema.org/BreadcrumbList">
            <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="home" itemprop="item">
                    <span itemprop="name">Home</span>
                    <meta itemprop="position" content="1" />
                </a>
                <img src="img/icons/sliderArrow.svg">
            </li>

            <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="cuenta" itemprop="item">
                    <span itemprop="name">Mi cuenta</span>
                    <meta itemprop="position" content="2" />
                </a>
                <img src="img/icons/sliderArrow.svg">
            </li>

            <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="cuenta/direcciones" itemprop="item" class="active">
                    <span itemprop="name">Direcciones</span>
                    <meta itemprop="position" content="3" />
                </a>
            </li>
        </ul>
    </div>
</div>


<div class="container mb-50 mt-20">
  <?php include('menu-dashboard.php'); ?>
  <div class="content-dashboard">
    <div class="center">
        <div class="titulo mb-20">Editar dirección</div>
        
        <form action="editarDireccion" method="post" id="editarDireccion">
            <input type="hidden" name="id" value="<?= $id ?>">
            <div class="gr-agregar-direccion">
                <?php 
                $regiones = consulta_bd("id, nombre","regiones","","posicion ASC");
                $comunas = consulta_bd("id, nombre", "comunas", "region_id = {$direccion[0][1]}", '');
                ?>
                <div class="col">
                    <div class="main-select">
                        <div class="custom-select select-region activeRegion" data-name="Región">
                            <select name="region_despacho">
                                <option value="0">&nbsp;</option>
                                <?php foreach($regiones as $reg){ ?>
                                    <option value="<?= $reg[0]; ?>" <?= ($direccion[0][1] == $reg[0]) ? 'selected':'';?>><?= $reg[1]; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <small class="error-description"></small>
                    </div>
                </div>
                <div class="col">
                    <div class="main-select">
                        <div class="custom-select select-comuna-pop activeRegion" data-name="Comuna">
                            <select name="comuna_despacho">
                                <option value="0">&nbsp;</option>
                                <?php foreach($comunas as $com){ ?>
                                    <option value="<?= $com[0]; ?>" <?= ($direccion[0][2] == $com[0]) ? 'selected':'';?>><?= $com[1]; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <small class="error-description"></small>
                    </div>
                </div>
                <div class="col">
                    <div class="input-form">
                        <label class="label_form active">Dirección</label>
                        <input type="text" name="direccion" class="input_text" value="<?= $direccion[0][0] ?>">
                    </div>
                </div>
                <div class="col">
                    <div class="input-form">
                        <label class="label_form active">Nº</label>
                        <input type="text" name="numero" class="input_text" value="<?= $direccion[0][3] ?>">
                    </div>
                </div>
                <div class="col">
                    <div class="input-form">
                        <label class="label_form <?= ($direccion[0][4] != '') ? 'active' : '' ?>">Nº depto, oficina, etc...</label>
                        <input type="text" name="numero_extra" class="input_text" value="<?= $direccion[0][4] ?>">
                    </div>
                </div>
            
            </div>
            <button class="btnAgregarDireccion mt-20">Editar dirección</button>
            <a href="cuenta/direcciones" class="cancelarEdit mt-20">Cancelar</a>
        </form>

    </div>
  </div>
  <div class="clearfix"></div>
</div>

<div class="clearfix"></div>
<?php if (isset($_SESSION['message_dashboard'])):
    $message = $_SESSION['message_dashboard']['message'];
    $status = $_SESSION['message_dashboard']['status']; ?>
    <script type="text/javascript">
        $(function(){
            swal('', "<?= $message ?>", "<?= $status ?>");
        })
    </script>
<?php
unset($_SESSION['message_dashboard']);
 endif ?>