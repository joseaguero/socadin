<?php
    $datos      = $_SESSION['datos'];
    $resumen    = false;

    $uid = $_COOKIE['usuario_id'];

    $usuario = consulta_bd("nombre, telefono, email, razon_social, rut_empresa, giro, direccion_empresa","clientes","id = $uid","");
    $direcciones = consulta_bd("d.id, d.nombre, r.nombre, c.nombre, d.calle, d.numero","clientes_direcciones d JOIN regiones r ON r.id = d.region_id JOIN comunas c ON c.id = d.comuna_id","cliente_id = $uid","");

    $e      = ($_GET['e']) ? mysqli_real_escape_string($conexion, $_GET['e']) : "0";

    if (strpos($_SERVER[REQUEST_URI], 'resumen-compra') !== false) {
        $resumen = true;
    }

    unset($_SESSION['codigo_descuento']);

    $factura            = "boleta";
    $email              = $usuario[0][2];
    $newsletter         = "";
    $nombre             = $usuario[0][0];
    $telefono           = $usuario[0][1];
    $tipo_despacho      = "domicilio";
    $region             = "";
    $comuna             = "";
    $direccion          = "";
    $tienda             = "";
    $nombre_retiro      = "";
    $rut_retiro         = "";
    $razon_social       = $usuario[0][3];
    $rut_empresa        = $usuario[0][4];
    $giro               = $usuario[0][5];
    $direccion_empresa  = $usuario[0][6];
    $guardar_info       = "";

    $_SESSION['correo'] = $email;

    if($datos AND $e){
        $factura            = mysqli_real_escape_string($conexion, $datos['factura']);
        $email              = mysqli_real_escape_string($conexion, $datos['email']);
        $newsletter         = mysqli_real_escape_string($conexion, $datos['newsletter']);
        $nombre             = mysqli_real_escape_string($conexion, $datos['nombre']);
        $telefono           = mysqli_real_escape_string($conexion, $datos['telefono']);
        $tipo_despacho      = mysqli_real_escape_string($conexion, $datos['tipo_despacho']);
        $region             = mysqli_real_escape_string($conexion, $datos['region']);
        $comuna             = mysqli_real_escape_string($conexion, $datos['comuna']);
        $direccion          = mysqli_real_escape_string($conexion, $datos['direccion']);
        $tienda             = mysqli_real_escape_string($conexion, $datos['tienda']);
        $nombre_retiro      = mysqli_real_escape_string($conexion, $datos['nombre_retiro']);
        $rut_retiro         = mysqli_real_escape_string($conexion, $datos['rut_retiro']);
        $razon_social       = mysqli_real_escape_string($conexion, $datos['razon_social']);
        $rut_empresa        = mysqli_real_escape_string($conexion, $datos['rut_empresa']);
        $giro               = mysqli_real_escape_string($conexion, $datos['giro']);
        $direccion_empresa  = mysqli_real_escape_string($conexion, $datos['direccion_empresa']);
        $guardar_info       = mysqli_real_escape_string($conexion, $datos['guardar_info']);
        $mis_direcciones    = mysqli_real_escape_string($conexion, $datos['mis_direcciones']);
    }

?>

<?php if($resumen){ ?>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.datos-cargados').load('ajax/cart/datos-cargados.php');
            $('.finalizar-compra').load('ajax/cart/finalizar-compra.php');
            $('.datos-compra').hide();
            $('.resumen-compra').hide();
            $('.datos-cargados').show();
            $('.finalizar-compra').show();
        });
    </script>
<?php } 

?>

<div class="envio-pago">
    <div class="bg1"></div>
    <div class="content-resumen-cel">
        <div class="btnMostrarResumen">
            <img src="img/icons/cart.svg">
            <span>Mostrar resumen del pedido</span> 
            <span><i class="fas fa-chevron-down"></i></span>
            <span class="totalCart2"><?= "$".number_format(totalCart(),0,",","."); ?></span>
        </div>
    </div>
    
    <div class="contenedor">

        <div class="resumen-compra resumen-desk" <?= ($resumen) ? 'style="display:none;"': ''; ?>>
            
            <div class="cartResumen">
                <?= ShowResumenCart(0, $email, $e, 1); ?>
            </div>
            <!--<div class="btn ir-al-pago">Siguiente</div>-->
        </div>

        <div class="datos-compra" <?= ($resumen) ? 'style="display:none;"': ''; ?>>
            <div class="tabs-carro">
                <div class="col active" id="domicilioLogin">
                    <div>
                        <span class="material-icons">local_shipping</span>
                        <span class="texto">Despacho a domicilio</span></div>
                    </div>
                <div class="col" id="retiroLogin">
                    <div>
                        <span class="material-icons">storefront</span>
                        <span class="texto">Retiro en tienda</span>
                    </div>
                </div>
            </div>
            <form name="formPagoLogin" id="formPagoLogin" method="POST" action="tienda/procesar_compra.php">
                
                <input type="hidden" name="tipoDespacho" value="domicilio">

                <div class="campo checkbox campo-f mt-10">
                    <div class="checkbtn">
                        <input type="checkbox" name="tipoPago" id="factura" <?= ($factura == 'true') ? 'checked':''; ?>>
                        <span class="checkmark"></span>
                    </div>  
                    <label for="factura">Necesito factura</label>
                </div>

                <div class="datos-factura" <?= ($factura == 'true') ? 'style="display:block;"':''; ?>>

                    <h4>Datos de facturación</h4>

                    <div class="clearfix"></div>
                    
                    <div class="double-form">
                        <div class="main-input">
                            <div class="input-form">
                                <label class="label_form">Razón social</label>
                                <input type="text" name="razon_social" class="input_text" value="<?= $razon_social ?>">
                            </div>
                            <small class="error-description"></small>
                        </div>
                        
                        <div class="main-input">
                            <div class="input-form">
                                <label class="label_form">RUT empresa</label>
                                <input type="text" name="rut_empresa" class="input_text rut-input" maxlength="12" value="<?= $rut_empresa ?>">
                            </div>
                            <small class="error-description"></small>
                        </div>
                        
                        <div class="main-input">
                            <div class="input-form">
                                <label class="label_form">Giro</label>
                                <input type="text" name="giro" class="input_text" value="<?= $giro ?>">
                            </div>
                            <small class="error-description"></small>
                        </div>
                        
                        <div class="main-input">
                            <div class="input-form">
                                <label class="label_form">Dirección facturación</label>
                                <input type="text" name="direccion_empresa" class="input_text" value="<?= $direccion_empresa ?>">
                            </div>
                            <small class="error-description"></small>
                        </div>
                    </div>
                </div>

                <div class="datos-envio" <?= ($tipo_despacho == 'retiro') ? 'style="display:none;"':''; ?>>
                    <div class="main-select main-direcciones">
                        <div class="custom-select select-direcciones" data-name="Mis direcciones" data-comuna="0">
                            <select name="mis_direcciones">
                                <option value="0">&nbsp;</option>
                                <?php foreach ($direcciones as $dir): ?>
                                    <option value="<?= $dir[0] ?>"><?= $dir[4] ?> #<?=$dir[5]?>, <?= $dir[3] ?>, <?= $dir[2] ?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <small class="error-description"></small>
                    </div>

                    <a href="javascript:void(0)" class="btnAgregarDireccion">Agregar dirección</a>
                    
                    <div class="clearfix"></div>
                    <div class="content_despachos mt-10">
                        <h4>¿Cuándo lo quieres?</h4>
                        
                        <div class="rb-despachos rb-rapido" data-type="rapido">
                            <input type="radio" name="metodoDespacho" value="rapido">
                            <div class="info">
                                <span class="title">Recibir Hoy</span>
                                <span class="horas">24 horas</span>
                            </div>

                            <div class="precio"></div>
                        </div>

                        <div class="rb-despachos" data-type="estandar">
                            <input type="radio" name="metodoDespacho" value="estandar" checked>
                            <div class="info">
                                <span class="title">Envío estandar</span>
                                <span class="horas">3-5 días</span>
                            </div>
                            <div class="precio"></div>
                        </div>

                    </div>

                </div>


                <?php
                    //SUCURSALES
                    $sucursales = consulta_bd("id, nombre, direccion, link_mapa","sucursales","publicado = 1 AND retiro_en_tienda = 1","posicion ASC");
                    if(!$tienda){
                        $tienda = $sucursales[0][0];
                    }
                ?>
                <div class="datos-retiro" <?= ($tipo_despacho == 'retiro') ? 'style="display:block;"':''; ?>>
                    <div class="opciones-retiro">
                        <?php $is = 0; foreach($sucursales as $suc){ ?>
                            <div class="row-sucursal">
                                <div class="suc">
                                    <div class="relative">
                                        <input type="radio" name="retiroTienda" value="<?= $suc[0] ?>" <?= ($is == 0) ? 'checked' : '' ?>>
                                        <div class="info">
                                            <div class="nombre"><?= $suc[1] ?></div>
                                            <div class="direc"><?= $suc[2] ?></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="map">
                                    <iframe src="<?= $suc[3] ?>" width="100%" height="95" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                                </div>
                                
                            </div>
                        <?php $is++; } ?>
                        
                    </div>

                    <h4>¿Quién retira?</h4>
                    
                    <div class="clearfix"></div>
                    <div id="yoRetiro" class="btnRetiro chked">
                        Yo retiraré
                    </div>

                    <div id="otraPersona" class="btnRetiro">
                        Otra persona
                    </div>
                    
                    <div class="content_quien_retira">

                        <div class="double-form mt-20">
                            <div class="main-input">
                                <div class="input-form">
                                    <label class="label_form">Nombre y apellido</label>
                                    <input type="text" name="nombre_retiro" class="input_text" value="<?= $nombre_retiro; ?>">
                                </div>
                                <small class="error-description"></small>
                            </div>

                            <div class="main-input">
                                <div class="input-form">
                                    <label class="label_form">RUT</label>
                                    <input type="text" name="rut_retiro" class="input_text rut-input" maxlength="12" value="<?= $rut_retiro; ?>">
                                </div>
                                <small class="error-description"></small>
                            </div>
                        </div>
                    </div>

                    <input type="hidden" name="tiro_retiro" value="yoRetiro">
                    

                </div>
                    
                <h4 class="mt-20">Información del cliente</h4>
                <div class="clearfix"></div>
                <div class="datos-info">
                    <div class="line">
                        <div>Email</div> <span> <?= $email; ?></span>
                        <input type="hidden" name="email" class="emailUsuario" value="<?= $email ?>">
                    </div>
                    <div class="line">
                        <div>Contacto</div> <span> <?= $nombre; ?></span>
                    </div>
                    <div class="line">
                        <div>Teléfono</div> <span> <?= $telefono; ?></span>
                    </div>
                </div>

                <a href="mi-carro" class="ver-carro">Ver carro</a>
                <div class="clearfix"></div>
                <h4 class="mt-20">Medios de pago</h4>
                
                    <div class="medios">
                        
                        <div class="col">
                            <div class="radiobtn">
                                <input type="radio" name="metodoPago" id="webpay" value="webpay" checked="checked">
                                <div class="check"></div>
                            </div>
                            <label for="webpay"><img src="img/webpay.png"></label>          
                        </div>

                        <div class="col">
                            <div class="radiobtn">
                                <input type="radio" name="metodoPago" id="mercadopago" value="mercadopago">
                                <div class="check"></div>
                            </div>
                            <label for="mercadopago"><img src="img/mpago.png"></small></label>
                        </div>

                        <div class="col">
                            <div class="radiobtn">
                                <input type="radio" name="metodoPago" id="transferencia" value="transferencia">
                                <div class="check"></div>
                            </div>
                            <label for="transferencia"><img src="img/transferencia.png"></small></label>
                        </div>

                    </div>
                    
                    <img src="img/bannermpago.php" width="100%" class="mt-20 mb-20">

                    <div class="terminos">
                        <div class="campo checkbox chkTerminos">
                            <div class="checkbtn">
                                <input type="checkbox" name="terminos" id="terminos">
                                <span class="checkmark"></span>
                            </div>  
                            <label for="terminos">Al comprar acepto los Términos y condiciones</label>
                        </div>
                    </div>
                <div class="clearfix"></div>
                <input type="submit" value="Pagar" class="btn pagarLogin">
            </form>
        </div>
        <div class="datos-cargados" <?= ($resumen) ? 'style="display:block;"': ''; ?>></div>
        <d2iv class="finalizar-compra" <?= ($resumen) ? 'style="display:block;"': ''; ?>></div>

    </div>
</div>
<div class="clearfix"></div>

<div class="bg-addDir"></div>
<div class="content_new_dir">
    <?php $regiones = consulta_bd("id, nombre","regiones","","posicion ASC"); ?>
    <div class="title mb-20">Agregar dirección</div>
    <div class="gr-agregar-direccion">

        <div class="col">
            <div class="main-select">
                <div class="custom-select select-region" data-name="Región">
                    <select name="region_despacho">
                        <option value="0">&nbsp;</option>
                        <?php foreach($regiones as $reg){ ?>
                            <option value="<?= $reg[0]; ?>" <?= ($region == $reg[0]) ? 'selected':'';?>><?= $reg[1]; ?></option>
                        <?php } ?>
                    </select>
                </div>
                <small class="error-description"></small>
            </div>
        </div>
        <div class="col">
            <div class="main-select">
                <div class="custom-select select-comuna-pop" data-name="Comuna">
                    <select name="comuna_despacho">
                        <option value="0">&nbsp;</option>

                    </select>
                </div>
                <small class="error-description"></small>
            </div>
        </div>
        <div class="col">
            <div class="input-form">
                <label class="label_form">Dirección</label>
                <input type="text" name="direccion" class="input_text">
            </div>
        </div>
        <div class="col">
            <div class="input-form">
                <label class="label_form">Nº</label>
                <input type="text" name="numero" class="input_text">
            </div>
        </div>
        <div class="col">
            <div class="input-form">
                <label class="label_form">Nº depto, oficina, etc...</label>
                <input type="text" name="numero_extra" class="input_text">
            </div>
        </div>
    
    </div>

    <a href="javascript:void(0)" class="agregarDirPop">Agregar dirección</a>
    <a href="javascript:void(0)" class="cancelarDir">Cancelar</a>
</div>