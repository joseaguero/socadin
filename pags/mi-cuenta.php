
<?php 
if(!isset($_COOKIE['usuario_id'])){
	echo '<script>location.href = "home"</script>';
}else{
  $nombre_cliente = consulta_bd('nombre', 'clientes', "id = {$_COOKIE['usuario_id']}", '');
  $nombre_cliente = $nombre_cliente[0][0];
}
?>
<div class="breadcrumbs">  
    <div class="container">
        <ul itemscope itemtype="http://schema.org/BreadcrumbList">
            <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="home" itemprop="item">
                    <span itemprop="name">Home</span>
                    <meta itemprop="position" content="1" />
                </a>
                <img src="img/icons/sliderArrow.svg">
            </li>

            <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="cuenta" itemprop="item" class="active">
                    <span itemprop="name">Mi cuenta</span>
                    <meta itemprop="position" content="2" />
                </a>
            </li>
        </ul>
    </div>
</div>


<div class="container mb-50 mt-20">
  <?php include('menu-dashboard.php'); ?>

  <div class="content-dashboard">
    <div class="center">
      <div class="titulo">Hola, <?= $nombre_cliente ?></div>
      <p class="row-text">. Manten tus datos personales actualizados para una mejor entrega de nuestros productos.</p>

      <p class="row-text">. Agrega diferentes direcciones como oficina, casa, etc... para tener distintos lugares de entrega.</p>

      <p class="row-text">. Revisa el historial de tus pedidos y repite tus ultimas compras.</p>

      <!-- <div class="titulo mt-20">Los productos más vendidos esta semana</div> -->
    </div>

    <!-- <div class="center-large">
      <div class="productos-grilla">
          <?php for ($i=0; $i < 3; $i++) { ?>
              <div class="item">
                  <a href="#" class="headerProduct">
                      <img src="img/producto.jpg" class="imgProduct">
                  </a>

                  <div class="bodyProduct">
                      <a href="#" class="name">Baliza Ambar 60 LED Magnética</a>
                      
                      <?php if ($i == 1): ?>
                          <div class="description">
                              Porta cuña Plástico para cuñas 5 Ton. de poliuretano. (Camioneta Minera)
                          </div>
                      <?php else: ?>
                          <div class="prices">
                              <a href="#" class="discountPrice">Antes $70.000</a>
                              <a href="#" class="finalPrice">$55.000</a>
                          </div>
                      <?php endif ?>

                      <a href="#" class="btnDetails">Ver ficha</a>
                  </div>
              </div>
          <?php } ?>
      </div>
    </div> -->
  </div>
  <div class="clearfix"></div>
</div>

<div class="clearfix"></div>
