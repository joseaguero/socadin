<div class="breadcrumbs">  
    <div class="container">
        <ul itemscope itemtype="http://schema.org/BreadcrumbList">
            <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="home" itemprop="item">
                    <span itemprop="name">Home</span>
                    <meta itemprop="position" content="1" />
                </a>
                <img src="img/icons/sliderArrow.svg">
            </li>

            <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="javascript:void(0)" itemprop="item" class="active">
                    <span itemprop="name">Error 404</span>
                    <meta itemprop="position" content="2" />
                </a>
            </li>
        </ul>
    </div>
</div>

<div class="container mt-20 mb-20">
    
    <div class="titulo_buscar">No podemos encontrar la página que estás buscando.</div>
    <div class="sub_busqueda">Le recomendamos intentar nuevamente o volver al <a href="home">Home</a> de nuestro sitio.</div>

    <div class="homeTitle mb-20 mt-20">Descubre Socadin</div>
    <?php $lineas_destacadas_home = consulta_bd('id, nombre, icono', 'lineas', 'home = 1', 'posicion asc'); ?>
    <section class="gr-catDestacadas">
        <?php foreach ($lineas_destacadas_home as $l): ?>
        <div class="col">
            <div class="imgCat">
                <img src="imagenes/lineas/<?= $l[2] ?>">
            </div>

            <div class="info">
                <span class="titulo"><?= $l[1] ?></span>
                <a href="lineas/<?= $l[0] ?>/<?= url_amigables($l[1]) ?>" class="linkB">Ver categoría</a>
            </div>
        </div>
        <?php endforeach ?>
    </section>

    <?php $banner_promociones = consulta_bd('imagen, link', 'banner_promociones', '', 'posicion asc'); ?>
    <?php $prd_superior = get_products(array('recom_sup' => true));
    $prd_superior = $prd_superior['productos']['producto']; ?>
    <section class="sliderProduct">
        <div class="homeTitle mb-20 mt-30">Te recomendamos</div>

        <div class="grSliderProduct">
            <div class="principalCol">
                <div class="SliderProduct">
                    <?php foreach ($prd_superior as $prd_sup):
                        $linktemp = 'ficha/' . $prd_sup['id_producto'] . '/' . $prd_sup['nombre_seteado']; ?>

                        <div class="item">
                            <a href="<?= $linktemp ?>" class="headerProduct">
                                <?php if ($prd_sup['descuento'] > 0): ?>
                                    <span class="oferta-grilla">-<?= round(100 - ($prd_sup['descuento'] * 100) / $prd_sup['precio']) ?>%</span>
                                <?php endif ?>
                                <img src="<?= $prd_sup['imagen_grilla'] ?>" class="imgProduct">
                            </a>

                            <div class="bodyProduct">
                                <a href="<?= $linktemp ?>" class="name"><?= $prd_sup['nombre'] ?></a>
                                
                                <?php if ($prd_sup['solo_cotizar']): ?>
                                    <div class="description">
                                        <?= $prd_sup['descripcion_grilla'] ?>
                                    </div>
                                <?php else: ?>
                                    <div class="prices">
                                        <?php if ($prd_sup['descuento'] > 0): ?>
                                            <a href="<?= $linktemp ?>" class="discountPrice">Antes $<?= number_format($prd_sup['precio'], 0, ',', '.') ?> + IVA</a>
                                            <a href="<?= $linktemp ?>" class="finalPrice">$<?= number_format($prd_sup['descuento'], 0, ',', '.') ?> + IVA</a>
                                        <?php else: ?>
                                            <a href="<?= $linktemp ?>" class="discountPrice"></a>
                                            <a href="<?= $linktemp ?>" class="finalPrice">$<?= number_format($prd_sup['precio'], 0, ',', '.') ?> + IVA</a>
                                        <?php endif ?>
                                        
                                    </div>
                                <?php endif ?>

                                <a href="<?= $linktemp ?>" class="btnDetails">Ver ficha</a>
                            </div>
                        </div>
                    <?php endforeach ?>
                </div>
            </div>
            <div class="principalCol">
                <a href="<?= $banner_promociones[0][1] ?>" class="bannerSlider">
                    <img src="imagenes/banner_promociones/<?= $banner_promociones[0][0] ?>">
                </a>
            </div>
        </div>
        
        <div class="arrowsAction">
            <div class="prevMove"><img src="img/icons/sliderArrow.svg"></div>
            <div class="nextMove"><img src="img/icons/sliderArrow.svg"></div>
        </div>
        
        <div class="clearfix"></div>
    </section>
    
</div>