<div class="cont100">
    <div class="cont100Centro">
        <ul class="breadcrumb">
          <li><a href="home">Home</a></li>
          <li class="active">Inicio de sesión</li>
        </ul>
    </div>
</div>



<div class="cont100">
    <div class="cont100Centro identificacionCentrado">
        <div class="acceso1 titulo2">Identifícate</div>
        <div class="acceso2 subtitulo2">Inicia sesion para obtener mejores descuentos o si no recuerdas tu contraseña recuperala <a href="recuperar-clave">aquí</a></div>
        
        <div class="cont50Identificacion">
        	<div class="acceso3 cont100">
            	<?php 
				if(isset($_COOKIE[nombreUsuario]) and isset($_COOKIE[claveUsuario])){
					$checked = 'checked="checked"';
					$usuarioCookie = $_COOKIE[nombreUsuario];
					$claveCookie = base64_decode($_COOKIE[claveUsuario]);
					} else {
						$checked = '';
						}
					?>
               <form action="ajax/login.php" method="post" class="formularioLogin" id="formularioLogin">
                   <label for="email">Usuario</label>
                   <input type="text" id="email" name="email" class="campoGrande" value="<?= $usuarioCookie; ?>" placeholder="Ingrese su correo" />
                   <label for="clave">Password <a href="recuperar-clave" class="btnRecuperarClave">¿olvidaste cu clave?</a></label>
                   <input type="password" id="clave" name="clave" class="campoGrande" value="<?= $claveCookie; ?>" placeholder="Ingrese su Clave" />
                   <input type="hidden" name="origen" value="inicio-sesion" />
                   <div class="cont100">
                   	<input type="checkbox" name="recordarCuenta" <?= $checked; ?> /> <span>Recordar mis datos</span>
                   </div>
                   
                   <a href="javascript:void(0)" id="inicioSesion" class="btnFormCompraRapida">Iniciar sesión</a>
                   <div style="clear:both"></div>
               </form>
           </div>
        </div><!--iniciar sesion -->
        
               
    </div>
</div>
