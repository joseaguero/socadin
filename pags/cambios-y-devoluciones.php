<div class="breadcrumbs">  
    <div class="container">
        <ul itemscope itemtype="http://schema.org/BreadcrumbList">
            <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="home" itemprop="item">
                    <span itemprop="name">Home</span>
                    <meta itemprop="position" content="1" />
                </a>
                <img src="img/icons/sliderArrow.svg">
            </li>

            <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="cambios-y-devoluciones" itemprop="item" class="active">
                    <span itemprop="name">Cambios y devoluciones</span>
                    <meta itemprop="position" content="2" />
                </a>
            </li>
        </ul>
    </div>
</div>

<div class="container mt-20 mb-20">
    <div class="titulo-pag">Cambios y devoluciones</div>

    <p class="body-pag">
        Si el producto comprado presenta fallas, éste podrá cambiarlo o devolverlo en un plazo de hasta 90 días después de la fecha en que lo ha recibido, en cualquier sucursal de SOCADIN, o si fue despachado a domicilio, SOCADIN podrá ir a buscarlo, debiendo pagar el Usuario sólo el valor del transporte, el cual será el mismo que el del despacho.</p>
    <p class="body-pag">Para poder hacer la devolución o cambio, es necesario que el producto esté sin uso, con todos sus accesorios y embalaje o empaque original. Además se requerirá de la revisión y aprobación de la falla de parte del personal técnico de SOCADIN Para el cambio o devolución, deberá presentarse la boleta, guía de despacho, ticket de cambio, o cualquier otro comprobante de haberse realizado la compra.</p>
    <p class="body-pag">Esta garantía no aplica a productos que hubieran sido confeccionados a la medida o pedido del Usuario. En caso de productos usados, abiertos, de segunda selección o con alguna deficiencia, SOCADIN informará de su debido estado, proporcionando toda la información relacionada. Si el Usuario accede a comprarlos bajo conocimiento de esta situación, y teniendo en consideración un precio reducido, no será aplicable el cambio o devolución.</p>
    <p class="body-pag">En caso de devolución de dinero, SOCADIN realizará un abono en el medio de pago que hubiera utilizado el Usuario, luego de haberse aprobado la devolución, lo que será informado debidamente al Usuario mediante el correo electrónico que hubiera proporcionado, o bien mediante algún otro medio de comunicación adicional.</p>

    <div class="subtitle-pag">Derecho de Retracto.</div>

    <p class="body-pag">El Usuario tendrá un plazo de 10 días para terminar el contrato celebrado por medios electrónicos desde que reciba el producto, o desde que contrate el Servicio, antes de que éste sea prestado o tenga acceso a un Contenido.</p>
    <p class="body-pag">El ejercicio de este derecho debe utilizar los mismos medios empleados para celebrar el contrato, y siempre que SOCADIN hubiera enviado comunicación al Usuario, informando el perfeccionamiento del contrato y entregando una copia del mismo, así como acceso claro e inequívoco de los Términos y Condiciones Generales, así como la posibilidad de conservar o imprimir dichos documentos.</p>
    <p class="body-pag">En caso de que SOCADIN no hubiera dado cumplimiento íntegro a lo establecido en los párrafos anteriores de este acápite, el plazo con el que cuenta el Usuario para terminar el contrato, se ampliará a 90 días.</p>
    <p class="body-pag">Este derecho no podrá ejercerse cuando el bien, materia del contrato, se haya deteriorado por hecho imputable al Usuario. Si el bien o Servicio se obtuvo por medio de un crédito, éste quedará sin efecto, pero los intereses serán de cargo del Usuario cuando se hubiera otorgado por un tercero.</p>
    <p class="body-pag">SOCADIN estará obligado a devolver las sumas abonadas, sin retención de gastos, a la mayor brevedad posible y, en cualquier caso, antes de 45 días a la comunicación del retracto. El Usuario deberá restituir en buen estado los elementos de embalaje, los manuales de uso, cajas, elementos de protección y todo otro elemento complementario que viniera con el bien.</p>


    <div class="subtitle-pag">Garantías Legales.</div>
    <p class="body-pag">En caso que el producto no cuente con las características informadas o publicitadas, si tuviera algún tipo de daño o le faltara algún componente, podrá ser cambiado inmediatamente. Si presenta fallas o defectos dentro de los 3 meses siguientes a la fecha de recepción, el Usuario puede optar por su reparación gratuita o, previa restitución, su cambio o bien la devolución del precio pagado, siempre que los daños o deterioros no hubieran sido por culpa del Usuario. Lo cuál será evaluado por el personal especializado de SOCADIN. La devolución puede realizarse en cualquier sucursal u oficina de SOCADIN, o bien en algún lugar acordado con algún representante del Sitio Web.</p>
    <p class="body-pag">Si se tratara de un producto que cuenta con garantía del fabricante, se aplicará el plazo de esa garantía, si es que éste fuera mayor al plazo de 3 meses contemplado en el párrafo anterior. Todos estos plazos se suspenderán por el tiempo que el bien esté siendo reparado en ejercicio de la garantía, y hasta que se complete dicha reparación.</p>
</div>