<?php
    $datos      = $_SESSION['datos'];
    $resumen    = false;

    if (strpos($_SERVER[REQUEST_URI], 'resumen-compra') !== false) {
        $resumen = true;
    }

    if (!$resumen) {
        unset($_SESSION['codigo_descuento']);
    }

    $email  = ($_GET['email']) ? mysqli_real_escape_string($conexion, $_GET['email']) : "";
    $e      = ($_GET['e']) ? mysqli_real_escape_string($conexion, $_GET['e']) : "0";
    $new    = ($_GET['n']) ? mysqli_real_escape_string($conexion, $_GET['n']) : "false";

    if ($email != '') {
        $_SESSION['correo'] = $email;
    }

    if($email){
        $factura            = mysqli_real_escape_string($conexion, $datos['factura']);
        $email              = mysqli_real_escape_string($conexion, $datos['email']);
        $tipo_despacho      = mysqli_real_escape_string($conexion, $datos['tipo_despacho']);
        $region             = mysqli_real_escape_string($conexion, $datos['region']);
        $comuna             = mysqli_real_escape_string($conexion, $datos['comuna']);
        $direccion          = mysqli_real_escape_string($conexion, $datos['direccion']);
        $comentarios        = mysqli_real_escape_string($conexion, $datos['comentarios']);
        $tienda             = mysqli_real_escape_string($conexion, $datos['tienda']);
        $nombre_retiro      = mysqli_real_escape_string($conexion, $datos['nombre_retiro']);
        $rut_retiro         = mysqli_real_escape_string($conexion, $datos['rut_retiro']);
        $giro               = mysqli_real_escape_string($conexion, $datos['giro']);
        $guardar_info       = mysqli_real_escape_string($conexion, $datos['guardar_info']);
        $mis_datos          = mysqli_real_escape_string($conexion, $datos['mis_datos']);
        $razon_social       = mysqli_real_escape_string($conexion, $datos['razon_social']);
        $rut_empresa        = mysqli_real_escape_string($conexion, $datos['rut_empresa']);
        $direccion_empresa  = mysqli_real_escape_string($conexion, $datos['direccion_empresa']);

        //die(print_r($datos));

        if($new == 'true'){
            $nombre             = ($datos['nombre']) ? mysqli_real_escape_string($conexion, $datos['nombre']) : '';
            $telefono           = ($datos['telefono']) ? mysqli_real_escape_string($conexion, $datos['telefono']) : '';
            $razon_social       = ($datos['razon_social']) ? mysqli_real_escape_string($conexion, $datos['razon_social']) : '';
            $rut_empresa        = ($datos['rut_empresa']) ? mysqli_real_escape_string($conexion, $datos['rut_empresa']) : '';
            $direccion_empresa  = ($datos['direccion_empresa']) ? mysqli_real_escape_string($conexion, $datos['direccion_empresa']) : '';
        }else{

            if($mis_datos){
                $nombre             = ($datos['nombre']) ? mysqli_real_escape_string($conexion, $datos['nombre']) : '';
                $telefono           = ($datos['telefono']) ? mysqli_real_escape_string($conexion, $datos['telefono']) : '';
            }else{
                $nombre             = ($datos['nombre']) ? mysqli_real_escape_string($conexion, $datos['nombre']) : '';
                $telefono           = ($datos['telefono']) ? mysqli_real_escape_string($conexion, $datos['telefono']) : '';
            }
        }

    }else{
        $factura            = "boleta";
        $email              = "";
        $newsletter         = "";
        $nombre             = "";
        $telefono           = "";
        $tipo_despacho      = "domicilio";
        $region             = 0;
        $comuna             = 0;
        $direccion          = "";
        $comentarios        = "";
        $tienda             = "";
        $nombre_retiro      = "";
        $rut_retiro         = "";
        $razon_social       = "";
        $rut_empresa        = "";
        $giro               = "";
        $direccion_empresa  = "";
        $guardar_info       = "";
    }

    $data_resumen = ($resumen) ? 1 : 0;

?>

<?php if($resumen){ ?>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.datos-cargados').load('ajax/cart/datos-cargados.php');
            $('.finalizar-compra').load('ajax/cart/finalizar-compra.php?resumen=<?=$data_resumen?>');
            $('.datos-compra').hide();
            $('.resumen-compra').hide();
            $('.datos-cargados').show();
            $('.finalizar-compra').show();
        });
    </script>
<?php } ?>

<?php if($email AND !$resumen AND !$e AND $new == 'false'){ ?>
    <script type="text/javascript">
        $(document).ready(function(){
            swal({
                title: "Tenemos una compra anterior con tu email ¿Son estos tus datos?",
                text: "Si necesitas modificar tus datos, Inicia Sesión\nNombre: <?= $nombre; ?>\nTeléfono: <?= $telefono; ?>",
                icon: "",
                buttons: {
                    cancel: "No son mis datos",
                    ok: "Si, son mis datos",
                },
                dangerMode: false,
            }).then((misDatos) => {
                if(misDatos){
                        $('#mis_datos').val("1");
                }else{
                    var formDA = document.forms['formDatos'];
                    
                    $('input[type=text]').val("");
                    
                    $('input:disabled').each(function(){
                        $(this).removeAttr("disabled");
                    });
                    $('#emailCompra').val("<?= $email; ?>");
                    $('#mis_datos').val("0");
                    //$('#domicilio').prop('checked','checked');
                }
            });
        });
    </script>
<?php } ?>

<div class="envio-pago">
    <div class="bg1"></div>
    <div class="content-resumen-cel">
        <div class="btnMostrarResumen">
            <img src="img/icons/cart.svg">
            <span>Mostrar resumen del pedido</span> 
            <span><i class="fas fa-chevron-down"></i></span>
            <div class="ajx_price">
                <span class="totalCart2"><?= "$".number_format(totalCart(),0,",","."); ?></span>
            </div>
        </div>
    </div>
    <div class="contenedor">
        <div class="resumen-compra resumen-desk" <?= ($resumen) ? 'style="display:none;"': ''; ?>>
            <div class="cartResumen">
                <?= ShowResumenCart($comuna, $email, $e, $tipo_despacho, $data_resumen); ?>
            </div>
        </div>

        

        <div class="datos-compra" <?= ($resumen) ? 'style="display:none;"': ''; ?>>
            <div class="disabled" <?= ($resumen OR $email) ? 'style="display:none;"': ''; ?>></div>
            <form id="formInfo">
                <input type="hidden" id="mis_datos" name="mis_datos" value="<?= $mis_datos; ?>">

                <h4>
                    Ingresa tu correo electrónico

                    <span class="block-desk">¿Ya tienes una cuenta? <a class="openLogin" href="javascript:;">iniciar sesión</a></span>
                    
                </h4>
                
                <div class="campo mail mb-20">
                    <div class="input-form <?= ($email) ? 'wd-100' : '' ?>">
                        <label class="label_form <?= ($email) ? 'active' : '' ?>">Correo electrónico</label>
                        <input type="email" name="email" id="emailCompra" class="input_text" value="<?= $email ?>">
                    </div>
                    <?php if (!$email): ?>
                        <button class="btn <?= ($email) ? 'disabl' : ''; ?>" id="btnEmailCompra">Validar</button>
                    <?php endif ?>
                </div>
                <h4 class="block-cel">
                    <span>¿Ya tienes una cuenta? <a class="openLogin" href="javascript:;">iniciar sesión</a></span>
                    
                </h4>


                <div class="campo checkbox campo-f">
                    <div class="checkbtn">
                        <input type="checkbox" name="tipoPago" id="factura" <?= ($factura == 'true') ? 'checked':''; ?>>
                        <span class="checkmark"></span>
                    </div>  
                    <label for="factura">Necesito factura</label>
                </div>

                <div class="datos-factura" <?= ($factura == 'true') ? 'style="display:block;"':''; ?>>

                    <h4>Datos de facturación</h4>

                    <div class="clearfix"></div>
                    
                    <div class="double-form">
                        <div class="main-input">
                            <div class="input-form">
                                <label class="label_form">Razón social</label>
                                <input type="text" name="razon_social" class="input_text" value="<?= $razon_social ?>">
                            </div>
                            <small class="error-description"></small>
                        </div>
                        
                        <div class="main-input">
                            <div class="input-form">
                                <label class="label_form">RUT empresa</label>
                                <input type="text" name="rut_empresa" class="input_text rut-input" maxlength="12" value="<?= $rut_empresa ?>">
                            </div>
                            <small class="error-description"></small>
                        </div>
                        
                        <div class="main-input">
                            <div class="input-form">
                                <label class="label_form">Giro</label>
                                <input type="text" name="giro" class="input_text" value="<?= $giro ?>">
                            </div>
                            <small class="error-description"></small>
                        </div>
                        
                        <div class="main-input">
                            <div class="input-form">
                                <label class="label_form">Dirección facturación</label>
                                <input type="text" name="direccion_empresa" class="input_text" value="<?= $direccion_empresa ?>">
                            </div>
                            <small class="error-description"></small>
                        </div>
                    </div>
                </div>

                <h4>Selecciona despacho o retiro en tienda</h4>
                <div class="clearfix"></div>
                <div class="tabs-carro">
                    <div class="col active" id="domicilio">
                        <div>
                            <span class="material-icons">local_shipping</span>
                            <span class="texto">Despacho a domicilio</span></div>
                        </div>
                    <div class="col" id="retiro">
                        <div>
                            <span class="material-icons">storefront</span>
                            <span class="texto">Retiro en tienda</span>
                        </div>
                    </div>
                </div>

                <input type="hidden" name="tipoDespacho" value="domicilio">

                <div class="double-form mt-20">
                    <div class="main-input">
                        <div class="input-form">
                            <label class="label_form <?= ($nombre) ? 'active' : '' ?>">Nombre y apellido</label>
                            <input type="text" name="nombre" class="input_text" value="<?= $nombre ?>">
                        </div>
                        <small class="error-description"></small>
                    </div>

                    <div class="main-input">
                        <div class="input-form">
                            <label class="label_form <?= ($telefono) ? 'active' : '' ?>">Telefono +56</label>
                            <input type="text" name="telefono" class="input_text" value="<?= $telefono ?>" maxlength="9" onkeypress="return justNumbers(event);">
                        </div>
                        <small class="error-description"></small>
                    </div>
                    
                </div>

                <?php
                    //REGIONES
                    $regiones = consulta_bd("id, nombre","regiones","","posicion ASC");
                    if($region){
                        $comunas = consulta_bd("c.id, c.nombre","comunas c","c.region_id = $region","c.nombre ASC");
                    }else{
                        $comunas = "";
                    }
                ?>
                <div class="datos-envio" <?= ($tipo_despacho == 'retiro') ? 'style="display:none;"':''; ?>>
                    
                    <div class="gr-direccion-carro">

                        <div class="col">
                            <div class="main-select">
                                <div class="custom-select select-region" data-name="Región">
                                    <select name="region_despacho">
                                        <option value="0">&nbsp;</option>
                                        <?php foreach($regiones as $reg){ ?>
                                            <option value="<?= $reg[0]; ?>" <?= ($region == $reg[0]) ? 'selected':'';?>><?= $reg[1]; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <small class="error-description"></small>
                            </div>
                        </div>
                        <div class="col">
                            <div class="main-select">
                                <div class="custom-select select-comuna" data-name="Comuna">
                                    <select name="comuna_despacho">
                                        <option value="0">&nbsp;</option>

                                    </select>
                                </div>
                                <small class="error-description"></small>
                            </div>
                        </div>
                        <div class="col">
                            <div class="main-input">
                                <div class="input-form">
                                    <label class="label_form">Dirección</label>
                                    <input type="text" name="direccion" class="input_text">
                                </div>
                                <small class="error-description"></small>
                            </div>
                        </div>
                        <div class="col">
                            <div class="main-input">
                                <div class="input-form">
                                    <label class="label_form">Nº</label>
                                    <input type="text" name="numero" class="input_text">
                                </div>
                                <small class="error-description"></small>
                            </div>
                        </div>
                        <div class="col">
                            <div class="main-input">
                                <div class="input-form">
                                    <label class="label_form">Nº depto, oficina, etc...</label>
                                    <input type="text" name="numero_extra" class="input_text">
                                </div>
                            </div>
                        </div>
                    
                    </div>
    
                    <div class="content_despachos">
                        <h4>¿Cuándo lo quieres?</h4>
                        
                        <div class="rb-despachos rb-rapido" data-type="rapido">
                            <input type="radio" name="metodoDespacho" value="rapido">
                            <div class="info">
                                <span class="title">Recibir Hoy</span>
                                <span class="horas">24 horas</span>
                            </div>

                            <div class="precio"></div>
                        </div>

                        <div class="rb-despachos" data-type="estandar">
                            <input type="radio" name="metodoDespacho" value="estandar" checked>
                            <div class="info">
                                <span class="title">Envío estandar</span>
                                <span class="horas">3-5 días</span>
                            </div>
                            <div class="precio"></div>
                        </div>

                    </div>

                </div>


                <?php
                    //SUCURSALES
                    $sucursales = consulta_bd("id, nombre, direccion, link_mapa","sucursales","publicado = 1 AND retiro_en_tienda = 1","posicion ASC");
                    if(!$tienda){
                        $tienda = $sucursales[0][0];
                    }
                ?>
                <div class="datos-retiro" <?= ($tipo_despacho == 'retiro') ? 'style="display:block;"':''; ?>>
                    <div class="opciones-retiro">
                        <?php $is = 0; foreach($sucursales as $suc){ ?>
                            <div class="row-sucursal">
                                <div class="suc">
                                    <div class="relative">
                                        <input type="radio" name="retiroTienda" value="<?= $suc[0] ?>" <?= ($is == 0) ? 'checked' : '' ?>>
                                        <div class="info">
                                            <div class="nombre"><?= $suc[1] ?></div>
                                            <div class="direc"><?= $suc[2] ?></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="map">
                                    <iframe src="<?= $suc[3] ?>" width="100%" height="95" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                                </div>
                                
                            </div>
                        <?php $is++; } ?>
                        
                    </div>

                    <h4>¿Quién retira?</h4>
                    
                    <div class="clearfix"></div>
                    <div id="yoRetiro" class="btnRetiro chked">
                        Yo retiraré
                    </div>

                    <div id="otraPersona" class="btnRetiro">
                        Otra persona
                    </div>
                    
                    <div class="content_quien_retira">

                        <div class="double-form mt-20">
                            <div class="main-input">
                                <div class="input-form">
                                    <label class="label_form">Nombre y apellido</label>
                                    <input type="text" name="nombre_retiro" class="input_text" value="<?= $nombre_retiro; ?>">
                                </div>
                                <small class="error-description"></small>
                            </div>

                            <div class="main-input">
                                <div class="input-form">
                                    <label class="label_form">RUT</label>
                                    <input type="text" name="rut_retiro" class="input_text rut-input" maxlength="12" value="<?= $rut_retiro; ?>">
                                </div>
                                <small class="error-description"></small>
                            </div>
                        </div>
                    </div>

                    <input type="hidden" name="tiro_retiro" value="yoRetiro">
                    

                </div>
                
                <div class="campo checkbox chkSaveInfo">
                    <div class="checkbtn">
                        <input type="checkbox" name="fcompra" id="fcompra" <?= ($guardar_info == 'true') ? 'checked':''; ?>>
                        <span class="checkmark"></span>
                    </div>  
                    <label for="fcompra">Guardar mi información para una futura compra</label>

                    <a href="mi-carro" class="ver-carro">Ver mi carro</a>
                    
                </div>
                <div class="campo">
                    <div class="btn buttonNext <?= (!$email) ? 'disabl' : '';?>" onclick="ir_al_pago()">Siguiente</div>
                </div>
            </form>
        </div>
        <div class="datos-cargados" <?= ($resumen) ? 'style="display:block;"': ''; ?>></div>
        <div class="finalizar-compra" <?= ($resumen) ? 'style="display:block;"': ''; ?>></div>

    </div>
</div>

<div class="clearfix"></div>