<?php 
    if(!isset($_COOKIE['usuario_id'])){
        echo '<script>location.href = "home"</script>';
    }


    $direcciones = consulta_bd('d.id, d.calle, r.nombre, c.nombre, d.numero, d.numero_extra', 'clientes_direcciones d JOIN regiones r ON r.id = d.region_id JOIN comunas c ON c.id = d.comuna_id', "d.cliente_id = {$_COOKIE['usuario_id']}", 'd.id desc');
?>
<div class="breadcrumbs">  
    <div class="container">
        <ul itemscope itemtype="http://schema.org/BreadcrumbList">
            <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="home" itemprop="item">
                    <span itemprop="name">Home</span>
                    <meta itemprop="position" content="1" />
                </a>
                <img src="img/icons/sliderArrow.svg">
            </li>

            <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="cuenta" itemprop="item">
                    <span itemprop="name">Mi cuenta</span>
                    <meta itemprop="position" content="2" />
                </a>
                <img src="img/icons/sliderArrow.svg">
            </li>

            <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="cuenta/direcciones" itemprop="item" class="active">
                    <span itemprop="name">Direcciones</span>
                    <meta itemprop="position" content="3" />
                </a>
            </li>
        </ul>
    </div>
</div>


<div class="container mb-50 mt-20">
  <?php include('menu-dashboard.php'); ?>
  <div class="content-dashboard">
    <div class="center">
        <div class="titulo">Mis direcciones</div>
        
        <?php if (is_array($direcciones)): ?>
            <div class="gr-direcciones mt-20 mb-20">
        <?php foreach ($direcciones as $dir) { ?>   
                <div class="col">
                    <div class="direccion"><?= $dir[1] . ' #' . $dir[4] . ' ' . $dir[5] . ', ' . $dir[2] . ', ' .$dir[3]  ?></div>
                    <div class="btn-actionsDir">
                        <a href="javascript:void(0)" class="eliminarDireccion" data-id="<?=$dir[0]?>">Eliminar</a>
                        <a href="cuenta/direcciones/editar/<?=$dir[0]?>" class="editarDireccion">Editar</a>
                    </div>
                </div>
        <?php } ?>
            </div>
        <?php else: ?>
            <p style="color: red;">Aún no tienes direcciones agregadas</p>
        <?php endif ?>

        <div class="titulo">Agregar dirección</div>
        <div class="subtitulo">Agrega diferentes direcciones como oficina, casa, trabajo, etc. Asi podras tener distintos lugares de entrega y disfrutar de nuestros productos.</div>
        
        <form action="agregarDireccionDash" method="post" id="formDireccionDash">
            <div class="gr-agregar-direccion">
                <?php $regiones = consulta_bd("id, nombre","regiones","","posicion ASC"); ?>
                <div class="col">
                    <div class="main-select">
                        <div class="custom-select select-region" data-name="Región *">
                            <select name="region_despacho">
                                <option value="0">&nbsp;</option>
                                <?php foreach($regiones as $reg){ ?>
                                    <option value="<?= $reg[0]; ?>" <?= ($region == $reg[0]) ? 'selected':'';?>><?= $reg[1]; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <small class="error-description"></small>
                    </div>
                </div>
                <div class="col">
                    <div class="main-select">
                        <div class="custom-select select-comuna-pop" data-name="Comuna *">
                            <select name="comuna_despacho">
                                <option value="0">&nbsp;</option>

                            </select>
                        </div>
                        <small class="error-description"></small>
                    </div>
                </div>
                <div class="col">
                    <div class="input-form">
                        <label class="label_form">Dirección *</label>
                        <input type="text" name="direccion" class="input_text">
                    </div>
                </div>
                <div class="col">
                    <div class="input-form">
                        <label class="label_form">Nº *</label>
                        <input type="text" name="numero" class="input_text">
                    </div>
                </div>
                <div class="col">
                    <div class="input-form">
                        <label class="label_form">Nº depto, oficina, etc...</label>
                        <input type="text" name="numero_extra" class="input_text">
                    </div>
                </div>
            
            </div>

            <button class="btnAgregarDireccion mt-20">Agregar dirección</button>
        </form>

    </div>
  </div>
  <div class="clearfix"></div>
</div>

<div class="clearfix"></div>
<?php if (isset($_SESSION['message_dashboard'])):
    $message = $_SESSION['message_dashboard']['message'];
    $status = $_SESSION['message_dashboard']['status']; ?>
    <script type="text/javascript">
        $(function(){
            swal('', "<?= $message ?>", "<?= $status ?>");
        })
    </script>
<?php
unset($_SESSION['message_dashboard']);
 endif ?>