<div class="menu-dashboard">
    <div class="head">
      <span class="titulo">Mi cuenta</span>
      <span class="nombre"><?= $nombre_cliente ?></span>
    </div>

    <a href="cuenta/datos" <?= ($op == 'mis-datos') ? 'class="active"' : '' ?>>Mis datos</a>
    <a href="cuenta/direcciones" <?= ($op == 'mis-direcciones' OR $op == 'editar-direccion') ? 'class="active"' : '' ?>>Mis direcciones</a>
    <a href="cuenta/pedidos" <?= ($op == 'mis-pedidos') ? 'class="active"' : '' ?>>Mis pedidos</a>
    <a href="cuenta/certificados" <?= ($op == 'mis-certificados') ? 'class="active"' : '' ?>>Certificados</a>
    <form action="logout" method="post" class="form-logout">
      <button class="btnLogout">Salir</button>
    </form>
  </div>