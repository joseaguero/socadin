<script>
<?php 
	if(opciones("cotizador") != 1){
		echo 'location.href = "404"';
	}
?>
</script>

<div class="breadcrumbs">  
    <div class="container">
        <ul itemscope itemtype="http://schema.org/BreadcrumbList">
            <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="home" itemprop="item">
                    <span itemprop="name">Home</span>
                    <meta itemprop="position" content="1" />
                </a>
                <img src="img/icons/sliderArrow.svg">
            </li>

            <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="carro-cotizacion" itemprop="item" class="active">
                    <span itemprop="name">Carro cotización</span>
                    <meta itemprop="position" content="2" />
                </a>
            </li>
        </ul>
    </div>
</div>

<div class="container mt-20 mb-50">
    <?php if (qty_cotizacion() > 0): ?>
        <div class="titulo_cotizacion mb-30">Carro de cotización. <span><?= qty_cotizacion() ?> <?= (qty_cotizacion() > 1) ? 'artículos' : 'artículo' ?></span></div>
    <?php else: ?>
        <div class="titulo_cotizacion mb-30">Tu carro de cotización esta vacío</div>
    <?php endif ?>
    
    <?php if (qty_cotizacion() > 0): ?>
        <?php $carro_cot = getCarroCotizacion(); ?>
        <div class="gr-cotizacion mb-30">
            <div class="col">
                <div class="ajx_cot">
                    <div class="content_carro_cot">
                        <?php foreach ($carro_cot as $item): ?>
                            <div class="row_cot">
                                <div class="img_cot">
                                    <img src="<?= $item['imagen'] ?>">
                                </div>

                                <div class="nombre"><?= $item['nombre'] ?></div>

                                <div class="action_cot">
                                    <a href="javascript:void(0)" class="action" onclick="eliminarCot(this)" data-id="<?= $item['id'] ?>">Eliminar</a>
                                </div>

                                <div class="box_main_qty_cot">
                                    <div class="box_qty" data-id="<?= $item['id'] ?>" onclick="openSelectCot(this)">
                                        <span>Cantidad:</span>
                                        <input type="text" class="cantidad_ficha" value="<?= $item['cantidad'] ?>" readonly>
                                        <div class="text_unidad"><?= ($item['cantidad'] > 1) ? 'Unidades' : 'Unidad' ?></div>
                                    </div>
                                    
                                    <div class="box_select_stock_cot">
                                            
                                        <?php for ($i=0; $i < 6; $i++) {
                                            $text_unidad = ($i+1 > 1) ? 'Unidades' : 'Unidad'; ?>
                                            <div class="row_stock_cot" onclick="rowStockCot(this)" data-val="<?= $i+1 ?>"><?= $i+1 . ' ' . $text_unidad ?> </div>
                                        <?php } ?>

                                        <div class="row_stock_cot" onclick="rowStockCot(this)" data-val="add">Más de 6 unidades</div>
                                        
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                            </div>

                        <?php endforeach ?>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="content_form_cotizacion">
                    <?php if (isset($_GET['status']) AND isset($_SESSION['flash']) AND $_SESSION['flash'] == 'success'): ?>
                        <div class="tCompleta mb-10">Cotización enviada con éxito</div>
                        <div class="box-message">
                            <span class="title">Gracias por contactarse con nosotros</span>
                            <span>Uno de nuestros vendedores se contactará con usted a la brevedad para continuar con el proceso de cotización.</span>
                        </div>

                        <a href="home" class="btnCotizacionSuccess">Seguir cotizando</a>
                    <?php else: ?>

                        <div class="tCompleta">Completa tus datos</div>
                        <form action="enviarCotizacion" method="post" id="formCotizacion">
                            <div class="main-input">
                                <div class="input-form">
                                    <label class="label_form">Nombre y apellido</label>
                                    <input type="text" name="nombre" class="input_text">
                                </div>
                                <small class="error-description"></small>
                            </div>

                            <div class="main-input">
                                <div class="input-form">
                                    <label class="label_form">Email</label>
                                    <input type="email" name="email" class="input_text">
                                </div>
                                <small class="error-description"></small>
                            </div>

                            <div class="main-input">
                                <div class="input-form">
                                    <label class="label_form">Teléfono</label>
                                    <input type="text" name="telefono" class="input_text" onkeypress="return justNumbers(event);" maxlength="9">
                                </div>
                                <small class="error-description"></small>
                            </div>

                            <div class="main-input">
                                <div class="input-form">
                                    <label class="label_form">Mensaje</label>
                                    <textarea name="mensaje" rows="4"></textarea>
                                </div>
                                <small class="error-description"></small>
                            </div>

                            <input type="submit" class="btnEnviarCotizacion" name="enviarCotizacion" value="Enviar cotización">
                        </form>

                    <?php endif ?>
                    
                </div>
                
            </div>
        </div>
    <?php else: ?>
        <p class="msjeCot">Comienza a navegar por nuestro sitio y agrega tus productos <br>
        preferidos al carro de cotización.</p>

        <a href="home" class="btnIrHome">Comenzar a cotizar</a>

        <?php 

        $filtro['solo_cotizar'] = true;
        $filtro['limite'] = 4;
        $filtro['random'] = true;
        $productos = get_products($filtro);

        ?>

        <div class="titulo_cotizacion mt-20 mb-20">Los clientes también miran</div>

        <div class="productos-grilla grilla-4">
            <?php 
            foreach ($productos['productos']['producto'] as $prod):
                $link = 'ficha/' . $prod['id_producto'] . '/' . $prod['nombre_seteado']; ?>
                <div class="item">
                    <a href="<?= $link ?>" class="headerProduct">
                        <?php if ($prod['descuento'] > 0): ?>
                            <span class="oferta-grilla">-<?= round(100 - ($prod['descuento'] * 100) / $prod['precio']) ?>%</span>
                        <?php endif ?>
                        <img src="<?= $prod['imagen_grilla'] ?>" class="imgProduct">
                    </a>

                    <div class="bodyProduct">
                        <a href="<?= $link ?>" class="name"><?= $prod['nombre'] ?></a>
                        
                        <?php if ($prod['solo_cotizar'] == 1): ?>
                            <div class="description">
                                <?= $prod['descripcion_grilla'] ?>
                            </div>
                        <?php else: ?>
                            <div class="prices">
                                <?php if ($prod['descuento'] > 0): ?>
                                    <a href="<?= $link ?>" class="discountPrice">Antes $<?= number_format($prod['precio'], 0, ',', '.') ?> + IVA</a>
                                    <a href="<?= $link ?>" class="finalPrice">$<?= number_format($prod['descuento'], 0, ',', '.') ?> + IVA</a>
                                <?php else: ?>
                                    <a href="<?= $link ?>" class="discountPrice"></a>
                                    <a href="<?= $link ?>" class="finalPrice">$<?= number_format($prod['precio'], 0, ',', '.') ?> + IVA</a>
                                <?php endif ?>
                                
                            </div>
                        <?php endif ?>

                        <a href="<?= $link ?>" class="btnDetails">Ver ficha</a>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>

    <?php endif ?>
    
</div>
<?php if (isset($_SESSION['flash'])):
unset($_SESSION['flash']);
 endif ?>