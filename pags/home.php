<?php
$popup = consulta_bd('activo', 'active_popup', '', '');  
$nombre_cliente = opciones('nombre_cliente');
$nombre_popup = 'POPUP'.strtoupper(str_replace(' ', '', $nombre_cliente));
if ($popup[0][0] == 1 AND !isset($_COOKIE[$nombre_popup])) { ?>
    <script>
        setTimeout(function(){
            $('.bg-popup').fadeIn();
            $('.popup-news').fadeIn();
        }, 2000)
    </script>
<?php }

?>
<div class="bg-popup" data-cliente="<?= $nombre_popup ?>">
    <div class="popup-news">
        <div class="contenido">
            <div class="close"></div>
            <h3 style="text-align: center;">ACÁ EL LOGO</h3>
            <p>¡Suscríbete a nuestro newsletter y <br>entérate primero de nuestras ofertas, novedades y más!</p>
            <div class="datos">
                <input type="text" name="nombre" placeholder="Nombre" id="nombre-suscribe">
                <input type="email" name="email" placeholder="Email" id="email-suscribe">
                <a href="#" id="suscribe-newsletter">SUSCRIBIRME</a>
                <h3 style="text-align: center;">LOGO PEQUEÑO</h3>
            </div>
        </div>
    </div>
</div>

<?php $slider = consulta_bd('link, imagen, imagen_tablet, imagen_movil', 'slider', "publicado = 1", 'posicion asc'); ?>
<div id="slider">
    <div id="progress"></div>
    <div class="cycle-slideshow" 
            data-cycle-fx=fade 
            data-cycle-timeout=5000 
            data-cycle-slides="> a.item-slider"
            data-cycle-prev=".prev"
            data-cycle-next=".next" 
            data-cycle-pager=".nav > .cycle-pager"
            data-cycle-pause-on-hover="true">
        
        <!-- PREV/NEXT SLIDER -->
        <div class="nav">
            <div class="cycle-pager col-nav"></div>
            
        </div>
        
        <!-- ITEMS -->
        <?php foreach ($slider as $sl): ?>
            <a href="<?= $sl[0] ?>" class="item-slider">
                <img src="imagenes/slider/<?= $sl[1] ?>" width="100%" class="slider-xl">
                <img src="imagenes/slider/<?= $sl[2] ?>" width="100%" class="slider-sm">
                <img src="imagenes/slider/<?= $sl[3] ?>" width="100%" class="slider-xs">
            </a>
        <?php endforeach ?>
        <!-- END ITEMS -->
        
        <div class="prev"><img src="img/icons/sliderArrow.svg" alt=""></div>
        <div class="next"><img src="img/icons/sliderArrow.svg" alt=""></div>
    </div>
</div>

<div class="container catSection">
    <div class="homeTitle mb-20">Descubre Socadin</div>
    <?php $lineas_destacadas_home = consulta_bd('id, nombre, icono', 'lineas', 'home = 1', 'posicion asc'); ?>
    <section class="gr-catDestacadas">
        <?php foreach ($lineas_destacadas_home as $l): ?>
        <div class="col">
            <div class="imgCat">
                <img src="imagenes/lineas/<?= $l[2] ?>">
            </div>

            <div class="info">
                <span class="titulo"><?= $l[1] ?></span>
                <a href="lineas/<?= $l[0] ?>/<?= url_amigables($l[1]) ?>" class="linkB">Ver categoría</a>
            </div>
        </div>
        <?php endforeach ?>
    </section>
    
    <?php $banner_superiores = consulta_bd('imagen, link', 'banner_home_uno', "", 'posicion asc LIMIT 3'); ?>

    <div class="homeTitle mb-20 mt-30">Las mejores cadenas en Socadin</div>

    <section class="gr-firstBanners">
        <div class="col">
            <a href="<?= $banner_superiores[0][1] ?>">
                <img src="imagenes/banner_home_uno/<?= $banner_superiores[0][0] ?>">
            </a>
        </div>
        <div class="col">
            <a href="<?= $banner_superiores[1][1] ?>">
                <img src="imagenes/banner_home_uno/<?= $banner_superiores[1][0] ?>">
            </a>
        </div>
        <div class="col">
            <a href="<?= $banner_superiores[2][1] ?>">
                <img src="imagenes/banner_home_uno/<?= $banner_superiores[2][0] ?>">
            </a>
        </div>
    </section>

    <section class="buscadorCadenas">
        <div class="homeTitle mb-20 mt-30">Busca tu cadena por dimensiones</div>

        <div class="gr-buscadorHome">
            <div class="col">
                <?php $ancho_medidas = consulta_bd('cadena_ancho', 'productos_detalles', 'id > 0 and publicado = 1 GROUP BY cadena_ancho', 'cadena_ancho asc'); ?>
                <div class="custom-select select-home" data-name="Ancho" data-request="ancho">
                    <select name="ancho">
                        <option value="0">&nbsp;</option>
                        <?php foreach ($ancho_medidas as $an): ?>
                            <option value="<?= $an[0] ?>"><?= $an[0] ?></option>
                        <?php endforeach ?>
                    </select>
                </div>
            </div>
            <div class="col">
                <div class="custom-select select-home" data-name="Perfil" data-request="perfil">
                    <select name="perfil">
                        <option value="0">&nbsp;</option>
                    </select>
                </div>
            </div>
            <div class="col">
                <div class="custom-select select-home" data-name="Aro" data-request="aro">
                    <select name="aro">
                        <option value="0">&nbsp;</option>
                    </select>
                </div>
            </div>
            <div class="col">
                <a href="javascript:void(0)" class="btnBuscarCadenas">Buscar</a>
            </div>
        </div>

        <div class="banner-cadenas mt-20">
            <img src="img/banner-cadenas.png" width="100%">
        </div>

    </section>

    <?php $banner_promociones = consulta_bd('imagen, link', 'banner_promociones', 'posicion = 1', 'posicion asc'); ?>
    <?php $prd_superior = get_products(array('recom_sup' => true));
    $prd_superior = $prd_superior['productos']['producto']; ?>
    <section class="sliderProduct">
        <div class="homeTitle mb-20 mt-30">Equipamiento vial Socadin</div>

        <div class="grSliderProduct">
            <div class="principalCol">
                <div class="SliderProduct">
                    <?php foreach ($prd_superior as $prd_sup):
                        $linktemp = 'ficha/' . $prd_sup['id_producto'] . '/' . $prd_sup['nombre_seteado']; ?>

                        <div class="item">
                            <a href="<?= $linktemp ?>" class="headerProduct">
                                 <?php if ($prd_sup['descuento'] > 0): ?>
                                    <span class="oferta-grilla">-<?= round(100 - ($prd_sup['descuento'] * 100) / $prd_sup['precio']) ?>%</span>
                                <?php endif ?>
                                <img src="<?= $prd_sup['imagen_grilla'] ?>" class="imgProduct">
                            </a>

                            <div class="bodyProduct">
                                <a href="<?= $linktemp ?>" class="name"><?= $prd_sup['nombre'] ?></a>
                                
                                <?php if ($prd_sup['solo_cotizar']): ?>
                                    <div class="description">
                                        <?= $prd_sup['descripcion_grilla'] ?>
                                    </div>
                                <?php else: ?>
                                    <div class="prices">
                                        <?php if ($prd_sup['descuento'] > 0): ?>
                                            <a href="<?= $linktemp ?>" class="discountPrice">Antes $<?= number_format($prd_sup['precio'], 0, ',', '.') ?> + IVA</a>
                                            <a href="<?= $linktemp ?>" class="finalPrice">$<?= number_format($prd_sup['descuento'], 0, ',', '.') ?> + IVA</a>
                                        <?php else: ?>
                                            <a href="<?= $linktemp ?>" class="discountPrice"></a>
                                            <a href="<?= $linktemp ?>" class="finalPrice">$<?= number_format($prd_sup['precio'], 0, ',', '.') ?> + IVA</a>
                                        <?php endif ?>
                                        
                                    </div>
                                <?php endif ?>

                                <a href="<?= $linktemp ?>" class="btnDetails">Ver ficha</a>
                            </div>
                        </div>
                    <?php endforeach ?>
                </div>
            </div>
            <div class="principalCol">
                <a href="<?= $banner_promociones[0][1] ?>" class="bannerSlider">
                    <img src="imagenes/banner_promociones/<?= $banner_promociones[0][0] ?>">
                </a>
            </div>
        </div>
        
        <div class="arrowsAction">
            <div class="prevMove"><img src="img/icons/sliderArrow.svg"></div>
            <div class="nextMove"><img src="img/icons/sliderArrow.svg"></div>
        </div>
        
        <div class="clearfix"></div>
    </section>

    <div class="sliderProduct-xs">
        <div class="homeTitle mb-20 mt-30">Equipamiento vial Socadin</div>
        <div class="productos-grilla grilla-4">
            <?php 
            $contador_xs = 1;
            foreach ($prd_superior as $prod):
                $link = 'ficha/' . $prod['id_producto'] . '/' . $prod['nombre_seteado'];
                if ($contador_xs <= 4) { ?>
                    <div class="item">
                        <a href="<?= $link ?>" class="headerProduct">
                            <?php if ($prod['descuento'] > 0): ?>
                                <span class="oferta-grilla">-<?= round(100 - ($prod['descuento'] * 100) / $prod['precio']) ?>%</span>
                            <?php endif ?>
                            <img src="<?= $prod['imagen_grilla'] ?>" class="imgProduct">
                        </a>

                        <div class="bodyProduct">
                            <a href="<?= $link ?>" class="name"><?= $prod['nombre'] ?></a>
                            
                            <?php if ($prod['solo_cotizar'] == 1): ?>
                                <div class="description">
                                    <?= $prod['descripcion_grilla'] ?>
                                </div>
                            <?php else: ?>
                                <div class="prices">
                                    <?php if ($prod['descuento'] > 0): ?>
                                        <a href="<?= $link ?>" class="discountPrice">Antes $<?= number_format($prod['precio'], 0, ',', '.') ?> + IVA</a>
                                        <a href="<?= $link ?>" class="finalPrice">$<?= number_format($prod['descuento'], 0, ',', '.') ?> + IVA</a>
                                    <?php else: ?>
                                        <a href="<?= $link ?>" class="discountPrice"></a>
                                        <a href="<?= $link ?>" class="finalPrice">$<?= number_format($prod['precio'], 0, ',', '.') ?> + IVA</a>
                                    <?php endif ?>
                                    
                                </div>
                            <?php endif ?>

                            <a href="<?= $link ?>" class="btnDetails">Ver ficha</a>
                        </div>
                    </div>
                <?php }
                ?>
            <?php $contador_xs++; endforeach; ?>
        </div>
    </div>

    <?php $banner_inferiores = consulta_bd('imagen, link, imagen_celular', 'banner_home_dos', "", 'posicion asc LIMIT 3'); ?>

    <section class="gr-secBanners">
        <div class="col">
            <a href="<?= $banner_inferiores[0][1] ?>">
                <img src="imagenes/banner_home_dos/<?= $banner_inferiores[0][0] ?>">
            </a>
        </div>
        <div class="col">
            <a href="<?= $banner_inferiores[1][1] ?>">
                <img src="imagenes/banner_home_dos/<?= $banner_inferiores[1][0] ?>">
            </a>
        </div>
        <div class="col">
            <a href="<?= $banner_inferiores[2][1] ?>" class="banner-inf_desk">
                <img src="imagenes/banner_home_dos/<?= $banner_inferiores[2][0] ?>">
            </a>
            <a href="<?= $banner_inferiores[2][1] ?>" class="banner-inf_cel">
                <img src="imagenes/banner_home_dos/<?= $banner_inferiores[2][2] ?>">
            </a>
        </div>
    </section>

    <?php $prd_inferior = get_products(array('recom_inf' => true));
    $prd_inferior = $prd_inferior['productos']['producto'];
    $banner_promociones_dos = consulta_bd('imagen, link', 'banner_promociones', 'posicion = 2', 'posicion asc'); ?>
    <section class="sliderProduct">
        <div class="homeTitle mb-20 mt-30">Equipamiento vial Socadin</div>

        <div class="grSliderProduct">
            <div class="principalCol">
                <div class="SliderProduct-2">
                    <?php foreach ($prd_inferior as $prd_sup):
                        $linktemp = 'ficha/' . $prd_sup['id_producto'] . '/' . $prd_sup['nombre_seteado']; ?>

                        <div class="item">
                            <a href="<?= $linktemp ?>" class="headerProduct">
                                <?php if ($prd_sup['descuento'] > 0): ?>
                                    <span class="oferta-grilla">-<?= round(100 - ($prd_sup['descuento'] * 100) / $prd_sup['precio']) ?>%</span>
                                <?php endif ?>
                                <img src="<?= $prd_sup['imagen_grilla'] ?>" class="imgProduct">
                            </a>

                            <div class="bodyProduct">
                                <a href="<?= $linktemp ?>" class="name"><?= $prd_sup['nombre'] ?></a>
                                
                                <?php if ($prd_sup['solo_cotizar'] == 1): ?>
                                    <div class="description">
                                        <?= $prd_sup['descripcion_grilla'] ?>
                                    </div>
                                <?php else: ?>
                                    <div class="prices">
                                        <?php if ($prd_sup['descuento'] > 0): ?>
                                            <a href="<?= $linktemp ?>" class="discountPrice">Antes $<?= number_format($prd_sup['precio'], 0, ',', '.') ?> + IVA</a>
                                            <a href="<?= $linktemp ?>" class="finalPrice">$<?= number_format($prd_sup['descuento'], 0, ',', '.') ?> + IVA</a>
                                        <?php else: ?>
                                            <a href="<?= $linktemp ?>" class="discountPrice"></a>
                                            <a href="<?= $linktemp ?>" class="finalPrice">$<?= number_format($prd_sup['precio'], 0, ',', '.') ?> + IVA</a>
                                        <?php endif ?>
                                        
                                    </div>
                                <?php endif ?>

                                <a href="<?= $linktemp ?>" class="btnDetails">Ver ficha</a>
                            </div>
                        </div>
                    <?php endforeach ?>
                </div>
            </div>
            <div class="principalCol">
                <a href="<?= $banner_promociones_dos[0][1] ?>" class="bannerSlider">
                    <img src="imagenes/banner_promociones/<?= $banner_promociones_dos[0][0] ?>">
                </a>
            </div>
        </div>
        
        <div class="arrowsAction">
            <div class="prevMove2"><img src="img/icons/sliderArrow.svg"></div>
            <div class="nextMove2"><img src="img/icons/sliderArrow.svg"></div>
        </div>
        
        <div class="clearfix"></div>
    </section>

    <div class="sliderProduct-xs">
        <div class="homeTitle mb-20 mt-30">Equipamiento vial Socadin</div>
        <div class="productos-grilla grilla-4">
            <?php 
            $contador_xs = 1;
            foreach ($prd_inferior as $prod):
                $link = 'ficha/' . $prod['id_producto'] . '/' . $prod['nombre_seteado'];
                if ($contador_xs <= 4) { ?>
                    <div class="item">
                        <a href="<?= $link ?>" class="headerProduct">
                            <?php if ($prod['descuento'] > 0): ?>
                                <span class="oferta-grilla">-<?= round(100 - ($prod['descuento'] * 100) / $prod['precio']) ?>%</span>
                            <?php endif ?>
                            <img src="<?= $prod['imagen_grilla'] ?>" class="imgProduct">
                        </a>

                        <div class="bodyProduct">
                            <a href="<?= $link ?>" class="name"><?= $prod['nombre'] ?></a>
                            
                            <?php if ($prod['solo_cotizar'] == 1): ?>
                                <div class="description">
                                    <?= $prod['descripcion_grilla'] ?>
                                </div>
                            <?php else: ?>
                                <div class="prices">
                                    <?php if ($prod['descuento'] > 0): ?>
                                        <a href="<?= $link ?>" class="discountPrice">Antes $<?= number_format($prod['precio'], 0, ',', '.') ?> + IVA</a>
                                        <a href="<?= $link ?>" class="finalPrice">$<?= number_format($prod['descuento'], 0, ',', '.') ?> + IVA</a>
                                    <?php else: ?>
                                        <a href="<?= $link ?>" class="discountPrice"></a>
                                        <a href="<?= $link ?>" class="finalPrice">$<?= number_format($prod['precio'], 0, ',', '.') ?> + IVA</a>
                                    <?php endif ?>
                                    
                                </div>
                            <?php endif ?>

                            <a href="<?= $link ?>" class="btnDetails">Ver ficha</a>
                        </div>
                    </div>
                <?php }
                ?>
            <?php $contador_xs++; endforeach; ?>
        </div>
    </div>

    <div class="clearfix"></div>

    <div class="banner40">
        <img src="img/40anios-1.jpg">

        <div class="contentInfo">
            <div class="title">Quiénes somos</div>
            <div class="texto">Socadin, es una empresa del GRUPO ATLAS, fundada en 1977. Nuestra casa matriz se encuentra en la ciudad de Santiago de Chile</div>

            <a href="somos" class="btn40">Ver más</a>
        </div>
    </div>

</div>

<div class="bodyGray iconos_home">
    <div class="container gr-icons">
        <div class="col">
            <div class="icon"><img src="img/icons/delivery.svg"></div>
            <div class="name">Envío a todo Chile</div>
        </div>
        <div class="col">
            <div class="icon"><img src="img/icons/money.svg"></div>
            <div class="name">Calidad y precios bajos</div>
        </div>
        <div class="col">
            <div class="icon"><img src="img/icons/padlock.svg"></div>
            <div class="name">Pago seguro</div>
        </div>
        <div class="col">
            <div class="icon"><img src="img/icons/card.svg"></div>
            <div class="name">Todo medio de pago</div>
        </div>
    </div>
</div>
