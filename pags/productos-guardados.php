<script>
<?php 
	if(!isset($_COOKIE['usuario_id'])){
		echo 'location.href = "inicio-sesion"';
	}
?>
</script>
<div class="cont100">
    <div class="cont100Centro">
        <ul class="breadcrumb">
          <li><a href="home">Home</a></li>
          <li class="active">Productos guardados</li>
        </ul>
    </div>
</div>



<div class="cont100">
    <div class="cont100Centro identificacionCentrado">
        <div class="acceso1 titulo2">Productos guardados</div>
        <?php include("pags/menuMiCuenta.php"); ?>
        
        <div class="contenidoMiCuenta">
			<div id="contProductosGuardados">
                <?= saveForLater(); ?>
            </div>
            
        </div><!--fin contenidoMiCuenta-->
    
    </div>
</div>
