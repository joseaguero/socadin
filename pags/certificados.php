<?php 
    if(!isset($_COOKIE['usuario_id'])){
        echo '<script>location.href = "home"</script>';
    }

    $uid = (int)$_COOKIE['usuario_id'];

    $pedidos = consulta_bd('oc, fecha_creacion, id', 'pedidos', "estado_id = 2 AND cliente_id = $uid", '');
?>
<div class="breadcrumbs">  
    <div class="container">
        <ul itemscope itemtype="http://schema.org/BreadcrumbList">
            <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="home" itemprop="item">
                    <span itemprop="name">Home</span>
                    <meta itemprop="position" content="1" />
                </a>
                <img src="img/icons/sliderArrow.svg">
            </li>

            <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="cuenta" itemprop="item">
                    <span itemprop="name">Mi cuenta</span>
                    <meta itemprop="position" content="2" />
                </a>
                <img src="img/icons/sliderArrow.svg">
            </li>

            <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="cuenta/certificados" itemprop="item" class="active">
                    <span itemprop="name">Certificados</span>
                    <meta itemprop="position" content="3" />
                </a>
            </li>
        </ul>
    </div>
</div>


<div class="container mb-50 mt-20">
  <?php include('menu-dashboard.php'); ?>
  <div class="content-dashboard">
    <div class="center">
        <div class="titulo">Certificados</div>
        <div class="subtitulo"></div>
        
        <?php foreach ($pedidos as $ped):
            $oc = $ped[0];
            $fecha_pedido = date('Y-m-d', time($ped[1]));
            $fecha_pedido = setFechaPedido($fecha_pedido); ?>
            <div class="row-certificados">
                <div class="header-row">
                    <div class="col">
                        <div class="titulo_row">Fecha del pedido</div>
                        <div class="descrip"><?= $fecha_pedido ?></div>
                    </div>
                    <div class="col">
                        <div class="titulo_row">Orden</div>
                        <div class="descrip"><?= $ped[0] ?></div>
                    </div>
                </div>
                
                <?php 

                $productos_pedidos = consulta_bd('p.thumbs, p.nombre, p.cadena_medidas, pd.cadena_ancho, pd.cadena_perfil, pd.cadena_aro, pp.certificado, p.certificado', 'productos p JOIN productos_detalles pd ON p.id = pd.producto_id JOIN productos_pedidos pp ON pp.productos_detalle_id = pd.id', "pp.pedido_id = {$ped[2]}",'');

                ?>
                <?php foreach ($productos_pedidos as $prd): ?>
                    <div class="row-producto">
                        <div class="col">
                            <div class="img">
                                <img src="imagenes/productos/<?= $prd[0] ?>">
                            </div>
                            <div class="nombre"><?= $prd[1] ?></div>
                            <?php if ($prd[2] == 1): ?>
                                <div class="nombre">Ancho: <?= $prd[3] ?> | Perfil: <?= $prd[4] ?> | Aro: <?= $prd[5] ?></div>
                            <?php endif ?>
                        </div>
                        <div class="col">
                            <?php if ($prd[7] != ''): ?>
                                <a href="docs/productos/<?= $prd[7] ?>" target="_blank" class="descargarCertificado">Descargar certificado</a>
                            <?php else: ?>
                                <?php if ($prd[6] != ''): ?>
                                    <a href="docs/productos_pedidos/<?= $prd[6] ?>" target="_blank" class="descargarCertificado">Descargar certificado</a>
                                <?php else: ?>
                                    <a href="javascript:void(0)" target="_blank" class="blockCertificado">Descargar certificado</a>
                                <?php endif ?>
                                
                            <?php endif ?>
                            
                        </div>
                    </div>
                <?php endforeach ?>
            </div>    
        <?php endforeach ?>
    </div>
  </div>
  <div class="clearfix"></div>
</div>

<div class="clearfix"></div>