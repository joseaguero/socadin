<?php 
require_once 'paginador/paginator.class.php';

$page = (isset($_GET["page"])) ? mysqli_real_escape_string($conexion, $_GET["page"]) : 0;

$orden = (isset($_GET['orden']) AND $_GET['orden'] != '') ? mysqli_real_escape_string($conexion, $_GET['orden']) : 0;
$desde = (isset($_GET['desde']) AND $_GET['desde'] != '') ? mysqli_real_escape_string($conexion, $_GET['desde']) : 0;
$hasta = (isset($_GET['hasta']) AND $_GET['hasta'] != '') ? mysqli_real_escape_string($conexion, $_GET['hasta']) : 0;

if ($orden != 0 OR $desde != 0 OR $hasta != 0) {
    $conteo_de_filtros = 1;
}

if ($desde != 0) {
    $filtros_grilla['desde'] = $desde;
    $precios_display = true;
}

if ($hasta != 0) {
    $filtros_grilla['hasta'] = $hasta;
    $precios_display = true;
}

if($orden === "desc"){
    $orderSql = ' valorMenor desc';
    $ordenStr = 'Precio mayor';
} else if($orden === "asc"){
    $orderSql = ' valorMenor asc';
    $ordenStr = 'Precio menor';
} else if($orden === 'rel'){
    $orderSql = "posicion asc";
    $ordenStr = 'Destacados';
} else {
    $orderSql = "posicion asc";
}

$filtros_grilla['ofertas'] = true;

$productos = get_products($filtros_grilla);

$total = $productos['productos']['total'][0];

$pages = new Paginator;
$pages->items_total = $total;
$pages->mid_range = 3; 
$rutaRetorno = "ofertas?desde=$desde&hasta=$hasta&orden=$orden";
$pages->paginate($rutaRetorno);

$filtros_grilla['limit'] = $pages->limit;
$filtros_grilla['orden'] = $orderSql;
$productos = get_products($filtros_grilla);

$total = ($productos['productos']['total'][0] > 5) ? $productos['productos']['total'][0]+1 : $productos['productos']['total'][0];
$productos = $productos['productos']['producto'];

$filtro_max['ofertas'] = $true;
$filtro_max['max'] = true;
$maximo_valor = get_products($filtro_max);

?>

<div class="breadcrumbs">  
    <div class="container">
        <ul itemscope itemtype="http://schema.org/BreadcrumbList">
            <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="home" itemprop="item">
                    <span itemprop="name">Home</span>
                    <meta itemprop="position" content="1" />
                </a>
                <img src="img/icons/sliderArrow.svg">
            </li>

            <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="javascript:void(0)" itemprop="item" class="active">
                    <span itemprop="name">Busqueda</span>
                    <meta itemprop="position" content="2" />
                </a>
            </li>
        </ul>
    </div>
</div>

<div class="container mt-20 mb-20">
    <div class="gr-grilla">
        <div class="col">
            <div class="tituloFiltros" data-ref="ofertas">
                Filtros 
                <i class="fas fa-chevron-down arrow-filtro"></i>
                <div class="open-fil-res"></div>
            </div>
            <div class="content-order-xs">
                <div class="selected-order" data-order="<?= $orden ?>">Ordenar por<i class="fas fa-chevron-down"></i></div>
            
                <div class="box_hidden_order">
                    <a href="ofertas?desde=<?=$desde?>&hasta=<?=$hasta?>&orden=rel&page=1" <?= ($orden === 'rel') ? 'class="active-order"' : '' ?> >Destacados</a>
                    <a href="ofertas?desde=<?=$desde?>&hasta=<?=$hasta?>&orden=desc&page=1" <?= ($orden === 'desc') ? 'class="active-order"' : '' ?> >Precio mayor</a>
                    <a href="ofertas?desde=<?=$desde?>&hasta=<?=$hasta?>&orden=asc&page=1" <?= ($orden === 'asc') ? 'class="active-order"' : '' ?> >Precio menor</a>
                </div>
            </div>
            <div class="bg-fil-res"></div>
            <div class="contenedor-filtros">
                <div class="titulo-filtros-xs">Filtros</div>
                <?php if ($conteo_de_filtros == 1): ?>
                    <div class="filtros_aplicados">
                        <span class="filter_apply">Filtros aplicados</span>
                        <?php $filtros_seleccionados = filtros_aplicados(0, $desde, $hasta, 0); ?>

                        <?php foreach ($filtros_seleccionados['filtro_subcategorias'] as $item): ?>
                            <a href="javascript:void(0)" class="aplicado cat_aplicadas" data-id="<?= $item['id'] ?>" data-action="subc"><span><?= $item['nombre'] ?></span> <i class="material-icons">close</i></a>
                        <?php endforeach ?>

                        <?php foreach ($filtros_seleccionados['filtro_filtros'] as $item): ?>
                            <a href="javascript:void(0)" class="aplicado filtros_aplicadas" data-id="<?= $item['id'] ?>" data-action="filtros"><span><?= $item['nombre'] ?></span> <i class="material-icons">close</i></a>
                        <?php endforeach ?>
                        
                        <?php if ($filtros_seleccionados['filtro_precios']): ?>
                            <a href="javascript:void(0)" class="aplicado precios_aplicados" data-action="precio"><span><?= $filtros_seleccionados['filtro_precios'][0] ?> <i class="material-icons">close</i></span></a>
                        <?php endif ?>

                        <div class="clearfix"></div>
                        <a href="ofertas" class="limpiar_filtros"><i class="fas fa-redo-alt"></i> <span>Limpiar filtros</span></a>

                    </div>
                <?php endif ?>

                <div class="box_filtro">
                    <h3 class="head-filtro">Precio</h3>
                    <?php $display_precio = ($precios_display) ? 'onCatBox' : ''; ?>
                    <ul class="lista-filtro <?= $display_precio ?>">
                        <div class="box_price precio_filtro <?= $display_precio ?>" data-ref="prices" data-max="<?= $maximo_valor['valor_max']+5000 ?>" data-max-actual="<?= $hasta ?>">
                            <span class="min-filtro" data-info="<?= $desde ?>" data-valor="0"></span> - 
                            <span class="max-filtro" data-info="<?= $hasta ?>" data-valor="<?= $maximo_actual ?>"></span>
                            <div class="clearfix"></div>
                            <div id="slider-range"></div>
                        </div>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="banner_oferta">
                <?php $banner_oferta = consulta_bd('imagen, link', 'banner_promociones', 'posicion = 3', ''); ?>
                <a href="<?= $banner_oferta[0][1] ?>"><img src="imagenes/banner_promociones/<?= $banner_oferta[0][0] ?>"></a>
            </div>
            <div class="header-grilla">
                <h2>Productos en oferta</h2>

                <div class="content-order">
                    <div class="selected-order" data-order="<?= $orden ?>">Ordenar por: <?= $ordenStr ?> <i class="fas fa-chevron-down"></i></div>

                    <div class="box_hidden_order">
                        <a href="ofertas?desde=<?=$desde?>&hasta=<?=$hasta?>&orden=rel&page=1">Destacados</a>
                        <a href="ofertas?desde=<?=$desde?>&hasta=<?=$hasta?>&orden=desc&page=1">Precio mayor</a>
                        <a href="ofertas?desde=<?=$desde?>&hasta=<?=$hasta?>&orden=asc&page=1">Precio menor</a>
                    </div>
                </div>

                <div class="clearfix"></div>
            </div>

            <div class="productos-grilla">
                <?php 
                $contador = 1; 
                $contador_prod = 0;
                for ($i=0; $i < $total; $i++):
                    $link = 'ficha/' . $productos[$contador_prod]['id_producto'] . '/' . $productos[$contador_prod]['nombre_seteado'];
                    if ($contador == 6): 
                        $banner_promocion = consulta_bd('link, imagen', 'banner_promociones', "grillas = 1", 'id asc LIMIT 1'); ?>
                         <div class="bannerCol">
                             <a href="<?= $banner_promocion[0][0] ?>">
                                 <img src="imagenes/banner_promociones/<?= $banner_promocion[0][1] ?>">
                             </a>
                         </div>
                    <?php else: ?>
                        <div class="item">
                            <a href="<?= $link ?>" class="headerProduct">
                                <?php if ($productos[$contador_prod]['descuento'] > 0): ?>
                                    <span class="oferta-grilla">-<?= round(100 - ($productos[$contador_prod]['descuento'] * 100) / $productos[$contador_prod]['precio']) ?>%</span>
                                <?php endif ?>
                                <img src="<?= $productos[$contador_prod]['imagen_grilla'] ?>" class="imgProduct">
                            </a>

                            <div class="bodyProduct">
                                <a href="<?= $link ?>" class="name"><?= $productos[$contador_prod]['nombre'] ?></a>
                                
                                <?php if ($productos[$contador_prod]['solo_cotizar'] == 1): ?>
                                    <div class="description">
                                        <?= $productos[$contador_prod]['descripcion_grilla'] ?>
                                    </div>
                                <?php else: ?>
                                    <div class="prices">
                                        <?php if ($productos[$contador_prod]['descuento'] > 0): ?>
                                            <a href="<?= $link ?>" class="discountPrice">Antes $<?= number_format($productos[$contador_prod]['precio'], 0, ',', '.') ?> + IVA</a>
                                            <a href="<?= $link ?>" class="finalPrice">$<?= number_format($productos[$contador_prod]['descuento'], 0, ',', '.') ?> + IVA</a>
                                        <?php else: ?>
                                            <a href="<?= $link ?>" class="discountPrice"></a>
                                            <a href="<?= $link ?>" class="finalPrice">$<?= number_format($productos[$contador_prod]['precio'], 0, ',', '.') ?> + IVA</a>
                                        <?php endif ?>
                                        
                                    </div>
                                <?php endif ?>

                                <a href="<?= $link ?>" class="btnDetails">Ver ficha</a>
                            </div>
                        </div>
                    <?php $contador_prod++; endif; ?>
                <?php $contador++; endfor; ?>
            </div>

        </div>
    </div>
    <div class="paginador">
        <?= $pages->display_pages(); ?>
    </div>
    
</div>