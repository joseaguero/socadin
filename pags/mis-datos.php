<?php 
if(!isset($_COOKIE['usuario_id'])){
    echo '<script>location.href = "home"</script>';
}else{
    $datos_cliente = consulta_bd('nombre, email, fecha_nacimiento', 'clientes', "id = {$_COOKIE['usuario_id']}", '');
    $nombre_cliente = $datos_cliente[0][0];

    if ($datos_cliente[0][2] != '') {
        $explode = explode('-', $datos_cliente[0][2]);
        $dia = $explode[2];
        $mes = $explode[1];
        $anio = $explode[0];
    }
}
?>
<div class="breadcrumbs">  
    <div class="container">
        <ul itemscope itemtype="http://schema.org/BreadcrumbList">
            <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="home" itemprop="item">
                    <span itemprop="name">Home</span>
                    <meta itemprop="position" content="1" />
                </a>
                <img src="img/icons/sliderArrow.svg">
            </li>

            <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="cuenta" itemprop="item">
                    <span itemprop="name">Mi cuenta</span>
                    <meta itemprop="position" content="2" />
                </a>
                <img src="img/icons/sliderArrow.svg">
            </li>

            <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="cuenta/datos" itemprop="item" class="active">
                    <span itemprop="name">Datos</span>
                    <meta itemprop="position" content="3" />
                </a>
            </li>
        </ul>
    </div>
</div>


<div class="container mb-50 mt-20">
  <?php include('menu-dashboard.php'); ?>

  <div class="content-dashboard">
    <div class="center">
    
      <form action="guardarDatos" method="post" id="formDatos">
        <div class="titulo">Mis datos</div>
        <div class="subtitulo">Modifica tus datos personales a continuación para que tu cuenta esté actualizada.</div>

        <div class="main-input">
            <div class="input-form">
                <label class="label_form <?= (trim($datos_cliente[0][0]) != '') ? 'active' : ''; ?>">Nombre y apellido</label>
                <input type="text" name="nombre" class="input_text" value="<?= $datos_cliente[0][0] ?>">
            </div>
            <small class="error-description"></small>
        </div>

        <div class="titulo mt-20">Fecha de nacimiento</div>
        <div class="subtitulo">Recopilamos la fecha de nacimiento. ¡Te haremos un regalo por tu cumpleaños!</div>

        <div class="gr-form-3">
            <div class="main-input">
                <div class="input-form">
                    <label class="label_form <?= (trim($datos_cliente[0][2]) != '') ? 'active' : ''; ?>">Día</label>
                    <input type="text" name="dia" class="input_text" value="<?= $dia ?>" maxlength="2" onkeypress="return justNumbers(event);">
                </div>
            </div>
          
            <div class="main-input">
                <div class="input-form">
                    <label class="label_form <?= (trim($datos_cliente[0][2]) != '') ? 'active' : ''; ?>">Mes</label>
                    <input type="text" name="mes" class="input_text" value="<?= $mes ?>" maxlength="2" onkeypress="return justNumbers(event);">
                </div>
            </div>

            <div class="main-input">
                <div class="input-form">
                    <label class="label_form <?= (trim($datos_cliente[0][2]) != '') ? 'active' : ''; ?>">Año</label>
                    <input type="text" name="anio" class="input_text" value="<?= $anio ?>" maxlength="4" onkeypress="return justNumbers(event);">
                </div>
            </div>
        </div>

        <div class="titulo mt-20">Datos de acceso</div>
        
        <div class="main-input">
            <div class="input-form mt-20">
                <label class="label_form <?= (trim($datos_cliente[0][1]) != '') ? 'active' : ''; ?>">Correo electrónico</label>
                <input type="email" name="email" class="input_text" value="<?= $datos_cliente[0][1] ?>">
            </div>
            <small class="error-description"></small>
        </div>     

        <div class="titulo mt-20">Cambia tu contraseña</div>
        
        <div class="main-input">
            <div class="input-form mt-20">
                <label class="label_form">Contraseña actual</label>
                <input type="password" name="actual" class="input_text">
            </div>
            <small class="error-description"></small>
        </div>

        <div class="main-input">
            <div class="input-form mt-20">
                <label class="label_form">Contraseña nueva</label>
                <input type="password" name="nueva" class="input_text">
            </div>
            <small class="error-description"></small>
        </div>

        <div class="main-input">
            <div class="input-form mt-20">
                <label class="label_form">Repetir contraseña nueva</label>
                <input type="password" name="renueva" class="input_text">
            </div>
            <small class="error-description"></small>
        </div>

        <button class="btnFormDatos mt-20">Guardar cambios</button>
      </form>

    </div>
  </div>
  <div class="clearfix"></div>
</div>

<div class="clearfix"></div>

<?php if (isset($_SESSION['message_dashboard'])):
    $message = $_SESSION['message_dashboard']['message'];
    $status = $_SESSION['message_dashboard']['status']; ?>
    <script type="text/javascript">
        $(function(){
            swal('', "<?= $message ?>", "<?= $status ?>");
        })
    </script>
<?php
unset($_SESSION['message_dashboard']);
 endif ?>
