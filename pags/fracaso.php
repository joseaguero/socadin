<?php
    $oc = (isset($_GET["oc"])) ? mysqli_real_escape_string($conexion, $_GET["oc"]) : 0;
    
    $PA = consulta_bd("id, estado_id, oc","pedidos","oc = '$oc'","");
	$cant = mysqli_affected_rows($conexion);
	if($oc == 'anulada'){
        update_bd("pedidos","estado_id = 3","oc='$oc'");
    } else if ($cant == 0){
		echo '<script>parent.location = "'.$url_base.'404";</script>';
	} else {
		update_bd("pedidos","estado_id = 3","oc='$oc' and payment_type_code IS NULL");
	}
	
?>

<div class="breadcrumbs">  
    <div class="container">
        <ul itemscope itemtype="http://schema.org/BreadcrumbList">
            <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="home" itemprop="item">
                    <span itemprop="name">Home</span>
                    <meta itemprop="position" content="1" />
                </a>
                <img src="img/icons/sliderArrow.svg">
            </li>

            <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="javascript:void(0)" itemprop="item" class="active">
                    <span itemprop="name">Fracaso</span>
                    <meta itemprop="position" content="2" />
                </a>
            </li>
        </ul>
    </div>
</div>
    
<div class="container">
  <div class="head_exito mb-40 mt-30">
    <img src="img/fracaso.png" class="mb-30">

    <div class="title_exito">Fracaso en la compra</div>
    
    <p>
      Transacción rechazada <span><?= $oc ?></span>
    </p>

    <p>Las posibles causas de este rechazo son:</p>

    <p>Error en el ingreso de los datos de su tarjeta de crédito o Debito (fecha y/o código de seguridad).</p>
    <p>Su tarjeta de crédito o debito no cuenta con el cupo necesario para cancelar la compra.</p>
    <p>Tarjeta aún no habilitada en el sistema financiero. </p>

    <a href="home" class="btnComprobante">Volver al sitio</a>
    <a href="mi-carro" class="btnReturn">Volver a mi carro</a>

  </div>
</div>