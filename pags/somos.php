<div class="breadcrumbs">  
    <div class="container">
        <ul itemscope itemtype="http://schema.org/BreadcrumbList">
            <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="home" itemprop="item">
                    <span itemprop="name">Home</span>
                    <meta itemprop="position" content="1" />
                </a>
                <img src="img/icons/sliderArrow.svg">
            </li>

            <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="somos" itemprop="item" class="active">
                    <span itemprop="name">Socadin 40 años</span>
                    <meta itemprop="position" content="2" />
                </a>
            </li>
        </ul>
    </div>
</div>

<div class="container mt-20 mb-20">
    <div class="gr-somos">
        <div class="col">
            <div class="titulo-pag">SOCADIN</div>

            <p class="body-pag">Socadin, es una empresa del GRUPO ATLAS, fundada en 1977. Nuestra casa matriz se encuentra en la ciudad de Santiago de Chile.</p>

            <p class="body-pag">Especializada en cadenas para nieve, barro y roca, por más de 38 Años ha estado presente en el crecimiento sostenido de la industria y desarrollos mineros tanto en Chile como en otros países de la región.</p>

            <p class="body-pag">Nuestra empresa está formada por profesionales, Ingenieros Civiles especializados en el área mecánica y minería, lo que nos permite contar con los conocimientos necesarios para entregar cada día mejores productos y servicios a nuestros clientes.</p>

            <p class="body-pag">El personal técnico y profesional de Socadin Ltda.-SAGA® está altamente comprometido con las actividades de la empresa, siempre abiertos a mejorar sus habilidades para entregar respuestas rápidas y oportunas ante las necesidades de nuestros clientes. Esto nos diferencia.</p>

            <div class="titulo-pag">Misión - Visión</div>

            <p class="body-pag">Nuestra Misión, satisfacer a particulares y empresas con cadenas para procesos industriales, desplazamiento vehiculares en nieve, barro y roca, brindando calidad, seguridad y protección en todo Chile. Lograr los objetivos financieros de la compañía cumpliendo las normativas medioambientales y legales vigentes. Mantener altos estándares de calidad y servicio, para lograr ser la primera opción de los clientes. Lograr un buen ambiente grato de trabajo.</p>

            <p class="body-pag">Nuestra Visión, Ser la empresa líder en cadenas en todo el país con capacidad de adaptación a los cambios del mercado, acumulando experiencia, siendo confiable, segura, rentable e innovadora, dando respuestas asertivas a nuestros clientes.</p>

        </div>
        <div class="col">
            <img src="img/somosimg-1.jpg">
        </div>
    </div>
    
</div>