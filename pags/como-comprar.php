<div class="breadcrumbs">  
    <div class="container">
        <ul itemscope itemtype="http://schema.org/BreadcrumbList">
            <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="home" itemprop="item">
                    <span itemprop="name">Home</span>
                    <meta itemprop="position" content="1" />
                </a>
                <img src="img/icons/sliderArrow.svg">
            </li>

            <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="como-comprar" itemprop="item" class="active">
                    <span itemprop="name">¿Cómo comprar?</span>
                    <meta itemprop="position" content="2" />
                </a>
            </li>
        </ul>
    </div>
</div>

<div class="container mt-20 mb-50">
    <div class="titulo-pag">Cómo comprar</div>

    <div class="gr-como-comprar">
        <div class="col">
            <img src="img/comocomprar1.jpg" alt="">

            <p class="texto">Vitrinea y luego selecciona el producto que quieras comprar y agrégalo al carro de compras.</p>
            <p class="texto">Puedes revisar tu carro de compras las veces que quieras, para agregar o quitar productos.</p>
        </div>
        <div class="col">
            <img src="img/comocomprar2.jpg" alt="">

            <p class="texto">Encontrarás el resumen de tu compra donde estarán él o los productos agregados al carro.</p>
            <p class="texto">Si tienes un Cupón de Descuento o Gift Card, actívalo en este paso.</p>
        </div>
        <div class="col">
            <img src="img/comocomprar3.jpg" alt="">
            <p class="texto">Proceso de identificación, envío, documento de compra y pago = TODO EN UN SOLO PASO.</p>
            <p class="texto">Debes ingresar tu correo electrónico para luego completar tus datos, seleccionar el documento de compra (boleta o factura), dirección de envío o retiro en tienda; según sea tu elección.</p>
            <p class="texto">Revisa bien tus datos que aparecen en el resumen antes de hacer click en comprar. Una vez que presiones el botón comprar irás al sistema seguro de pago Webpay.</p>
        </div>
    </div>
</div>