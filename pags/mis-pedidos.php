<?php 
    if(!isset($_COOKIE['usuario_id'])){
        echo '<script>location.href = "home"</script>';
    }

    $uid = (int)$_COOKIE['usuario_id'];

    $pedidos = consulta_bd('oc, fecha_creacion, total_pagado, id, id_envio', 'pedidos', "estado_id = 2 AND cliente_id = $uid", 'id desc');
?>
<div class="breadcrumbs">  
    <div class="container">
        <ul itemscope itemtype="http://schema.org/BreadcrumbList">
            <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="home" itemprop="item">
                    <span itemprop="name">Home</span>
                    <meta itemprop="position" content="1" />
                </a>
                <img src="img/icons/sliderArrow.svg">
            </li>

            <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="cuenta" itemprop="item">
                    <span itemprop="name">Mi cuenta</span>
                    <meta itemprop="position" content="2" />
                </a>
                <img src="img/icons/sliderArrow.svg">
            </li>

            <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="cuenta/pedidos" itemprop="item" class="active">
                    <span itemprop="name">Pedidos</span>
                    <meta itemprop="position" content="3" />
                </a>
            </li>
        </ul>
    </div>
</div>


<div class="container mb-50 mt-20">
  <?php include('menu-dashboard.php'); ?>
  <div class="content-dashboard">
    <div class="center">
        <div class="titulo">Mis pedidos</div>
        <div class="subtitulo">Revisa el historial de tus pedidos, repite tus últimas compras y sigue tu pedido.</div>
        
        <?php foreach ($pedidos as $ped):
            $oc = $ped[0];
            $fecha_pedido = date('Y-m-d', strtotime($ped[1]));
            $fecha_pedido = setFechaPedido($fecha_pedido); 
            $id_pedido = $ped[4]; ?>
            <div class="row-pedido">
                <div class="header-row">
                    <div class="col">
                        <div class="titulo_row">Fecha del pedido</div>
                        <div class="descrip"><?= $fecha_pedido ?></div>
                    </div>
                    <div class="col">
                        <div class="titulo_row">Total pagado</div>
                        <div class="descrip">$<?= number_format($ped[2], 0, ',', '.') ?></div>
                    </div>
                    <div class="col">
                        <div class="titulo_row">Orden</div>
                        <div class="descrip_oc"><?= $oc ?></div>
                        <div class="btn-ver-detalle"></div>
                    </div>
                </div>
                <div class="open-body">
                    <div class="body-detalle">
                        <?php 
                        $detalle = consulta_bd("
                        id,
                        oc,
                        total_pagado, 
                        fecha_creacion, 
                        medio_de_pago, 
                        retiro_en_tienda, 
                        direccion, 
                        comuna_id, 
                        region_id, 
                        total, 
                        valor_despacho, 
                        total_pagado, 
                        accounting_date, 
                        card_number, 
                        card_expiration_date, 
                        authorization_code, 
                        payment_type_code, 
                        shares_number, 
                        amount, 
                        transaction_date, 
                        mp_payment_type, 
                        mp_payment_method, 
                        mp_auth_code, 
                        mp_paid_amount, 
                        mp_card_number, 
                        mp_id_paid, 
                        mp_cuotas, 
                        mp_valor_cuotas, 
                        mp_transaction_date, 
                        descuento, estado_envio, descuento, tipo_envio","pedidos","oc = '$oc'", "fecha_creacion desc");
                        
                        $medio_de_pago = ($detalle[0][4] == 'webpay') ? 'tbk' : 'mp';
                        $num_cuotas    = ($detalle[0][4] == 'webpay') ? $detalle[0][17] : $detalle[0][26];
                        $tipo_pago     = ($detalle[0][4] == 'webpay') ? $detalle[0][16] : $detalle[0][20];
                        $card          = ($detalle[0][4] == 'webpay') ? $detalle[0][13] : $detalle[0][24];
                        $cod_auth      = ($detalle[0][4] == 'webpay') ? $detalle[0][15] : $detalle[0][22];
                        $tipo_pago = tipo_pago($tipo_pago,$num_cuotas,$medio_de_pago);

                        if ($detalle[0][4] == 'transferencia') {
                            $total_pagado = $ped[2];
                        }else{
                            $total_pagado = ($detalle[0][4] == 'webpay') ?  $detalle[0][18] : $detalle[0][23];
                        }

                        $fecha = ($detalle[0][4] == 'webpay') ? $detalle[0][19] : $detalle[0][28];

                        $estado_envio = $detalle[0][30];
                        $descuento = $detalle[0][31];
                        ?>
                        <div class="col">
                            <div class="titulo_detalle">Retiro en tienda</div>
                        </div>
                        <div class="col">
                            <div class="titulo_detalle">Pago</div>
                            <span class="info">Medio de pago: <?= $detalle[0][4] ?></span>
                            <span class="info">Tipo de transacción: Venta</span>
                            <span class="info">N° de Cuotas: <?= $num_cuotas ?></span>
                            <span class="info">Tipo de cuota: <?= $tipo_pago['tipo_cuota'] ?></span>
                            <span class="info">Tarjeta terminada en:**** **** **** <?= $card ?></span>
                            <span class="info">Tipo de pago: <?= $tipo_pago['tipo_pago'] ?></span>
                            <span class="info">Fecha y hora de transacción: <?= $fecha ?></span>
                            <span class="info">Valor pagado: $<?= number_format($total_pagado, 0, ',', '.') ?></span>
                            <span class="info">Código de autorización: <?= $cod_auth ?></span>
                            <span class="info">Tipo de moneda: CLP / PESO Chileno</span>

                            <div class="titulo_detalle mt-20">Totales</div>
                            <?php 
                                $subtotal = $detalle[0][9];
                                $total_neto = $subtotal - $descuento;
                                $iva = ($subtotal * 1.19) - $subtotal;
                            ?>
                            <div class="row-total">Subtotal <span>$<?= number_format($subtotal, 0, ',', '.') ?></span></div>
                            <div class="row-total">Descuento <span>-$<?= number_format($descuento, 0, ',', '.') ?></span></div>
                            <div class="row-total">Iva <span>$<?= number_format($iva, 0, ',', '.') ?></span></div>
                            <div class="row-total">Envio (<?=$detalle[0][32]?>) <span>$<?= number_format($detalle[0][10], 0, ',', '.') ?></span></div>
                            <div class="row-total">Total cancelado <span class="total">$<?= number_format($total_pagado, 0, ',', '.') ?></span></div>
                            
                        </div>
                    </div>
                </div>
                
                <?php 

                $productos_pedidos = consulta_bd('p.thumbs, p.nombre, SUM(pp.precio_unitario), SUM(pp.cantidad)', 'productos p JOIN productos_detalles pd ON p.id = pd.producto_id JOIN productos_pedidos pp ON pp.productos_detalle_id = pd.id', "pp.pedido_id = {$ped[3]} GROUP BY pp.productos_detalle_id",'');

                ?>
                <?php foreach ($productos_pedidos as $prd): ?>
                    <div class="row-producto">
                        <div class="col">
                            <div class="img">
                                <img src="imagenes/productos/<?= $prd[0] ?>">
                            </div>
                            <div class="nombre"><?= $prd[1] ?></div>
                        </div>
                        <div class="col">
                            <div class="precio_final">$<?= number_format($prd[2], 0, ',', '.') ?></div>
                        </div>
                    </div>
                <?php endforeach ?>

                <div class="btn-pedidos">
                    <a href="javascript:void(0)" class="button volverAComprar">Volver a comprar</a>
                    <?php if ($id_pedido != null and $id_pedido != ''): ?>
                        <a data-fancybox data-type="iframe" class="button seguirPedido" data-src="https://api.enviame.io/s2/companies/2141/deliveries/<?= substr($oc, 3) ?>/tracking" href="javascript:;">Seguir mi pedido</a>
                    <?php endif ?>
                </div>
            </div>    
        <?php endforeach ?>
    </div>
  </div>
  <div class="clearfix"></div>
</div>

<div class="clearfix"></div>
<?php if (isset($_SESSION['message_dashboard'])):
    $message = $_SESSION['message_dashboard']['message'];
    $status = $_SESSION['message_dashboard']['status']; ?>
    <script type="text/javascript">
        $(function(){
            swal('', "<?= $message ?>", "<?= $status ?>");
        })
    </script>
<?php
unset($_SESSION['message_dashboard']);
 endif ?>