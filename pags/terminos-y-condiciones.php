<div class="breadcrumbs">  
    <div class="container">
        <ul itemscope itemtype="http://schema.org/BreadcrumbList">
            <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="home" itemprop="item">
                    <span itemprop="name">Home</span>
                    <meta itemprop="position" content="1" />
                </a>
                <img src="img/icons/sliderArrow.svg">
            </li>

            <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="terminos-y-condiciones" itemprop="item" class="active">
                    <span itemprop="name">Términos y condiciones</span>
                    <meta itemprop="position" content="2" />
                </a>
            </li>
        </ul>
    </div>
</div>

<div class="container mt-20 mb-20">
    <div class="titulo-pag">Términos y condiciones</div>

    <div class="cont-indice">
        <div class="subtitle">Indice</div>

        <a href="javascript:void(0)" class="linkTerminos" data-scroll="#term1">PRIMERO: GENERALIDADES</a>
        <a href="javascript:void(0)" class="linkTerminos" data-scroll="#term2">SEGUNDO: INFORMACIÓN DEL SITIO WEB</a>
        <a href="javascript:void(0)" class="linkTerminos" data-scroll="#term3">TERCERO: USUARIO</a>
        <a href="javascript:void(0)" class="linkTerminos" data-scroll="#term4">CUARTO: DERECHOS Y GARANTÍAS DEL USUARIO DEL SITIO</a>
        <a href="javascript:void(0)" class="linkTerminos" data-scroll="#term5">QUINTO: CONTRATACIÓN</a>
        <a href="javascript:void(0)" class="linkTerminos" data-scroll="#term6">SEXTO: MEDIOS DE PAGO</a>
        <a href="javascript:void(0)" class="linkTerminos" data-scroll="#term7">SÉPTIMO: ENTREGA Y DESPACHO</a>
        <a href="javascript:void(0)" class="linkTerminos" data-scroll="#term8">OCTAVO: RESPONSABILIDAD</a>
        <a href="javascript:void(0)" class="linkTerminos" data-scroll="#term9">NOVENO: POLÍTICA DE SEGURIDAD DE DATOS</a>
        <a href="javascript:void(0)" class="linkTerminos" data-scroll="#term10">DÉCIMO: PROPIEDAD INTELECTUAL Y DERECHOS RESERVADOS</a>
    </div>

    <div class="box-scroll" id="term1">
        <div class="tituloBox">PRIMERO: GENERALIDADES</div>
    
        <div class="description">
            <p>El presente documento, elaborado con fecha 11 de junio de 2020, regula los términos y condiciones generales aplicables al acceso y uso que el Usuario realizará del Sitio Web: www.socadin.cl (en adelante "SOCADIN", "www.socadin.cl" o "el Sitio Web" indistintamente), así como a cualquier tipo de información, contenido, imagen, video, audio u otro material comunicado o presente en el Sitio Web.</p>
            <p>Para efectos del presente documento se entenderá como "Sitio Web": la apariencia externa de los interfaces de pantalla, tanto de forma estática como de forma dinámica, es decir, el árbol de la navegación; y todos los elementos integrados tanto en los interfaces de pantalla como en el árbol de navegación (en adelante, "Contenidos") y todos aquellos servicios o recuerdos en línea que en su caso ofrezca a los usuarios (en adelante, "Servicios").</p>
            <p>SOCADIN se reserva la facultad de modificar, en cualquier momento, y sin previo aviso, la presentación y configuración del Sitio Web y de los Contenidos y Servicios que en él pudieran estar incorporados. El Usuario reconoce y acepta que en cualquier momento SOCADIN pueda interrumpir, desactivar y/o cancelar cualquiera de estos elementos que se integran en el Sitio Web o el acceso a los mismos.</p>
            <p>El acceso y uso del sitio web y del contenido relacionado o anexo, descrito en el presente documento, se regirá íntegramente por las leyes de la República de Chile. Por consiguiente, las visitas, así como también los posibles contratos o transacciones que el usuario realice en la plataforma que representa el sitio web, así como los efectos jurídicos que éstas pudieran tener, quedan sometidos a las leyes y a la jurisdicción de los tribunales de justicia de la República de Chile. De particular importancia resultan la aplicación de la Ley N° 19.628 de Protección de Datos Personales y la Ley N° 19.496 sobre Derechos del Consumidor.</p>
            <p>Adicionalmente, este sitio web y sus contenidos, así como también las transacciones que se pudieran llevar a cabo en él, están destinados únicamente a personas con residencia o domiciliadas en Chile, sea que éstas sean chilenos o extranjeros.</p>
            <p>Por lo mismo, SOCADIN no puede asegurar a personas que no cumplan con los requisitos previamente descritos, que el Sitio Web vaya a cumplir, total o parcialmente, con legislaciones extranjeras. Así, SOCADIN no se hace responsable en aquellos casos en que Usuarios extranjeros, sin residencia ni domicilio en Chile, tengan acceso o procedan a navegar en el Sitio Web.</p>
            <p>Adicionalmente, www.socadin.cl adhiere en su totalidad al Código de Buenas Prácticas para el Comercio Electrónico de la Cámara de Comercio de Santiago.</p>
            <p>Se recomienda al usuario leer detenidamente el contenido de estos Términos y Condiciones Generales de Uso, así como imprimir y conservar una copia de éstas en una unidad de disco local de almacenamiento, o de almacenamiento portátil, para su conveniencia y seguridad.</p>

            <a href="javascript:void(0)" class="btnSubir">Subir</a>
        </div>

    </div>

    <div class="box-scroll" id="term2">
        <div class="tituloBox">SEGUNDO: INFORMACIÓN DEL SITIO WEB</div>
    
        <div class="description">
            <p>La titularidad de SOCADIN corresponde a Sociedad de Cadenas Industriales Limitada, RUT: 85.385.800-5.
            Adicionalmente, para efectos de cualquier tipo de comunicación, presentación, consulta o reclamo relacionados con el uso o funcionamiento del Sitio Web, o con los contratos y transacciones que se hubieran llevado a cabo, SOCADIN recibirá todo tipo de comunicaciones al correo electrónico: ventas@socadin.cl, así como al número de teléfono: +56227733726, el cuál sólo estará disponible para efectos de funciones de Servicio al Cliente. Sociedad de Cadenas Industriales Limitada, se encuentra domiciliado(a) para estos efectos en Salvador Gutiérrez 4539, comuna de Quinta Normal.</p>
            <p>El Sitio Web también cuenta con un servicio de atención al cliente, ante el cual se pueden hacer valer distintas presentaciones o reclamos relacionados con el uso o acceso a la página así como también con los negocios y transacciones realizados en el sitio web, el cual puede ser contactado a la dirección de correo electrónico ventas@socadin.cl, al número telefónico +56227733726 o en las oficinas ubicadas en Salvador Gutiérrez 4539, comuna de Quinta Normal, Santiago de Chille.</p>

            <a href="javascript:void(0)" class="btnSubir">Subir</a>
        </div>

    </div>

    <div class="box-scroll" id="term3">
        <div class="tituloBox">TERCERO.- USUARIO</div>
    
        <div class="description">
        <p>El acceso al Sitio Web por parte del Usuario tendrá un carácter libre y gratuito, sin que éste tenga que pagar por acceder al sitio o sus contenidos, más allá del costo de la conexión a internet, en los casos que correspondiera.</p>
        
        <p><strong>Registro del Usuario.</strong></p>
        <p>Será requisito necesario para el uso del Sitio Web, así como para realizar transacciones y adquirir productos ofrecidos por esta vía, la aceptación de los presentes Términos y Condiciones al momento del registro por parte del Usuario. Adicionalmente, el Usuario deberá designar una clave o contraseña de acceso.</p>
        <p>Se entenderán conocidos y aceptados estos Términos y Condiciones por el sólo hecho de registrarse el Usuario, acto en el cual se incluirá una manifestación expresa del Usuario sobre el conocimiento de las presentes condiciones de uso.</p>
        <p>El registro de cada Usuario se verificará completando y suscribiendo el formulario que para estos efectos se proporcionará por parte del Sitio Web, así como su posterior envío, el cual se concretará por medio de hacer "click" el Usuario en el elemento respectivo dentro del Sitio Web.</p>
        <p><strong>Clave Secreta.</strong></p>
        <p>Una vez registrado, el Usuario tendrá a su disposición un nombre de usuario y una contraseña o clave secreta, por medio de los cuales no sólo podrá ingresar al Sitio Web y tener acceso a sus contenidos, sino también un acceso personalizado, confidencial y seguro. El Usuario tendrá la posibilidad de cambiar su contraseña o clave secreta, para lo cual existirá un procedimiento regulado dentro del Sitio Web y su estructura.</p>
        <p>El Usuario asume totalmente su responsabilidad por el mantenimiento de la confidencialidad de su contraseña o clave secreta registrada en el Sitio Web. Dicha contraseña o clave es de uso personal, por lo que su entrega voluntaria a terceros por parte del Usuario, no acarrea ningún tipo de responsabilidad por parte de SOCADIN, ni de sus afiliados y representantes, en caso de uso malicioso.</p>

        <p><strong>Responsabilidad de los Usuarios.</strong></p>
        <p>SOCADIN entrega al Usuario un servicio caracterizado por la diversidad del contenido proporcionado. El Usuario asume su responsabilidad al ingresar al Sitio Web, para realizar un correcto uso del mismo y sus contenidos. Así, esta responsabilidad se extenderá, de forma no taxativa, a:</p>
        <p>a.- Usar la información, Contenidos y/o Servicios y datos ofrecidos por SOCADIN, sin que sea contrario a estos Términos y Condiciones, así como al Ordenamiento Jurídico Chileno y a la Moral y el Orden Público, o que de cualquier otra forma pudieran acarrear la vulneración de derechos de terceros, o el funcionamiento y operaciones normales del Sitio Web.</p>
        <p>b.- La veracidad y licitud de los datos e información aportados por el Usuario en los formularios de registro presentes en el Sitio Web. En todo caso, el Usuario notificará de forma inmediata a SOCADIN acerca de cualquier hecho relacionado con el uso indebido de información registrada en dichos formularios, tales como, pero no sólo, robo, extravío, o el acceso no autorizado a identificadores y/o contraseñas, con el fin de proceder a su inmediata cancelación.</p>
        <p>El simple acceso a www.socadin.cl no supone una relación comercial entre SOCADIN y el Usuario.</p>
        <p><strong>Capacidad Legal.</strong></p>
        <p>El Sitio Web no requiere una edad mínima para poder acceder a su Contenido y Servicios; por lo tanto, estos Términos y Condiciones se aplicarán indistintamente a todos los Usuarios del Sitio Web que cumplan con lo estipulado en este documento.</p>
        <p>Aún así, las reglas legales de capacidad establecidas en el ordenamiento jurídico regirán en todos aquellos casos donde su aplicación sea pertinente. Por tanto, si una persona no tiene capacidad legal para contratar, debe abstenerse de utilizar los Contenidos y Servicios que requieran de esta capacidad. SOCADIN podrá, en cualquier momento, en forma temporal o definitiva, suspender la participación de usuarios sobre los cuales se compruebe que carecen de capacidad legal para usar de los Servicios y Contenido que la requieran, o que proporcionen información falsa, inexacta o fraudulenta al Sitio Web.</p>
        <p>El Sitio Web www.socadin.cl está dirigido principalmente a Usuarios residentes en la República de Chile. SOCADIN no asegura que el Sitio Web cumpla, parcial o totalmente, con legislaciones de otros países. Si el Usuario reside o está domiciliado en un país extranjero, y decide acceder y/o navegar en este Sitio Web, lo hará bajo su propia responsabilidad, asegurándose de que tal acceso y navegación cumpla con lo dispuesto en la legislación local que le sea aplicable, no asumiendo SOCADIN responsabilidad alguna que pueda derivar de dicho acceso.</p>


            <a href="javascript:void(0)" class="btnSubir">Subir</a>
        </div>

    </div>

    <div class="box-scroll" id="term4">
        <div class="tituloBox">CUARTO.- DERECHOS Y GARANTÍAS DEL USUARIO DEL SITIO</div>
    
        <div class="description">
            <p>Condiciones. Además, el Usuario dispondrá en todo momento de los derechos de información, rectificación y cancelación de los datos personales conforme a la Ley N° 19.628 sobre protección de datos personales.</p>
            <p><strong>Cambios y Devoluciones.</strong></p>
            <p>Si el producto comprado presenta fallas, éste podrá cambiarlo o devolverlo en un plazo de hasta 90 días después de la fecha en que lo ha recibido, en cualquier sucursal de SOCADIN, o si fue despachado a domicilio, SOCADIN podrá ir a buscarlo, debiendo pagar el Usuario sólo el valor del transporte, el cual será el mismo que el del despacho.</p>
            <p>Para poder hacer la devolución o cambio, es necesario que el producto esté sin uso, con todos sus accesorios y embalaje o empaque original. Además se requerirá de la revisión y aprobación de la falla de parte del personal técnico de SOCADIN Para el cambio o devolución, deberá presentarse la boleta, guía de despacho, ticket de cambio, o cualquier otro comprobante de haberse realizado la compra.</p>
            <p>Esta garantía no aplica a productos que hubieran sido confeccionados a la medida o pedido del Usuario. En caso de productos usados, abiertos, de segunda selección o con alguna deficiencia, SOCADIN informará de su debido estado, proporcionando toda la información relacionada. Si el Usuario accede a comprarlos bajo conocimiento de esta situación, y teniendo en consideración un precio reducido, no será aplicable el cambio o devolución.</p>
            <p>En caso de devolución de dinero, SOCADIN realizará un abono en el medio de pago que hubiera utilizado el Usuario, luego de haberse aprobado la devolución, lo que será informado debidamente al Usuario mediante el correo electrónico que hubiera proporcionado, o bien mediante algún otro medio de comunicación adicional.</p>


            <p><strong>Derecho de Retracto.</strong></p>
            <p>El Usuario tendrá un plazo de 10 días para terminar el contrato celebrado por medios electrónicos desde que reciba el producto, o desde que contrate el Servicio, antes de que éste sea prestado o tenga acceso a un Contenido.</p>
            <p>El ejercicio de este derecho debe utilizar los mismos medios empleados para celebrar el contrato, y siempre que SOCADIN hubiera enviado comunicación al Usuario, informando el perfeccionamiento del contrato y entregando una copia del mismo, así como acceso claro e inequívoco de los Términos y Condiciones Generales, así como la posibilidad de conservar o imprimir dichos documentos.</p>
            <p>En caso de que SOCADIN no hubiera dado cumplimiento íntegro a lo establecido en los párrafos anteriores de este acápite, el plazo con el que cuenta el Usuario para terminar el contrato, se ampliará a 90 días.</p>
            <p>Este derecho no podrá ejercerse cuando el bien, materia del contrato, se haya deteriorado por hecho imputable al Usuario.
            Si el bien o Servicio se obtuvo por medio de un crédito, éste quedará sin efecto, pero los intereses serán de cargo del Usuario cuando se hubiera otorgado por un tercero.</p>
            <p>SOCADIN estará obligado a devolver las sumas abonadas, sin retención de gastos, a la mayor brevedad posible y, en cualquier caso, antes de 45 días a la comunicación del retracto. El Usuario deberá restituir en buen estado los elementos de embalaje, los manuales de uso, cajas, elementos de protección y todo otro elemento complementario que viniera con el bien.</p>
            <p><strong>Garantías Legales.</strong></p>
            <p>En caso que el producto no cuente con las características informadas o publicitadas, si tuviera algún tipo de daño o le faltara algún componente, podrá ser cambiado inmediatamente. Si presenta fallas o defectos dentro de los 3 meses siguientes a la fecha de recepción, el Usuario puede optar por su reparación gratuita o, previa restitución, su cambio o bien la devolución del precio pagado, siempre que los daños o deterioros no hubieran sido por culpa del Usuario. Lo cuál será evaluado por el personal especializado de SOCADIN. La devolución puede realizarse en cualquier sucursal u oficina de SOCADIN, o bien en algún lugar acordado con algún representante del Sitio Web.</p>
            <p>Si se tratara de un producto que cuenta con garantía del fabricante, se aplicará el plazo de esa garantía, si es que éste fuera mayor al plazo de 3 meses contemplado en el párrafo anterior. Todos estos plazos se suspenderán por el tiempo que el bien esté siendo reparado en ejercicio de la garantía, y hasta que se complete dicha reparación.</p>

            <a href="javascript:void(0)" class="btnSubir">Subir</a>
        </div>

    </div>

    <div class="box-scroll" id="term5">
        <div class="tituloBox">QUINTO.- CONTRATACIÓN</div>
    
        <div class="description">
            <p>Para realizar compras o contratar servicios en el Sitio Web, se deben seguir los siguientes pasos, haciendo click cuando corresponda:</p>
            <p>a.- Para dar inicio al proceso de contratación es necesario que se confirme haber leído y aceptado los presentes Términos y Condiciones.</p>
            <p>b.- Seleccionar el producto o servicio que interesa y agregarlo al "carro de compra", o alguna opción similar con nombres parecidos ("canasto", "cesta", por nombrar algunos).</p>
            <p>c.- Iniciar sesión en el sitio o ingresar correo electrónico y contraseña. En caso de no estar registrado y si se desea hacerlo, haga uso del ícono y menú que corresponda a esta opción.</p>
            <p>d.- Seleccionar la modalidad de envío y entrega entre las alternativas disponibles en el sitio (link con las alternativas disponibles). Ante la eventualidad de no existir alternativas disponibles, seguir las instrucciones para la entrega de acuerdo a lo que se señale en el sitio.</p>
            <p>e.- Seleccionar el medio de pago.</p>
            <p>f.- Una vez que se ha realizado la orden de compra o de contratación de servicio, se desplegará en la pantalla la siguiente información, la cual debe aparecer antes de la selección del medio de pago:</p>
            <blockquote>I. Descripción del producto adquirido o servicio contratado.</blockquote>
            <blockquote>II. Precio del producto o servicio.</blockquote>
            <blockquote>III. Indicación del costo del envío, cuando corresponda.</blockquote>
            <blockquote>IV. Fecha de entrega del producto adquirido, según el tipo de entrega escogido; o fecha de inicio del servicio contratado.</blockquote>
            <blockquote>V. Medio de pago que escogerá el usuario.</blockquote>
            <blockquote>VI. Valor total de la operación.</blockquote>
            <blockquote>VII. Otras condiciones de la orden de producto o servicio adquirido.</blockquote>
            <blockquote>VIII. Posibilidad de imprimir y conservar la orden, por parte del Usuario.</blockquote>
            <blockquote>IX. Número de identificación único de la orden, con la cual el Usuario puede hacer seguimiento en línea de ella, o bien utilizarlo para identificarla al momento de realizarse la entrega o retiro.</blockquote>
            <p>g.- Envío de la información al correo electrónico registrado por el Usuario.</p>
            <p>h.- La orden luego será enviada de forma automática a un proceso de confirmación de identidad, en el cual se resguardará siempre la seguridad y privacidad del usuario y del proceso mismo de contratación, disponibilidad, vigencia y cupo del medio de pago que se haya seleccionado.</p>
            <p>i.- Una vez cumplido lo anterior, se perfecciona el contrato, realizándose el cargo en el medio de pago seleccionado. Posteriormente se realiza el envío del comprobante de compra con la boleta o factura que corresponda en formato electrónico y será despachado el producto, de acuerdo al modo de entrega seleccionado.</p>
            <p>j.- No se verá afectado el comprador en sus derechos ni tampoco se le efectuarán cargos, sin que sea confirmada su identidad.</p>


            <a href="javascript:void(0)" class="btnSubir">Subir</a>
        </div>

    </div>

    <div class="box-scroll" id="term6">
        <div class="tituloBox">SEXTO.- MEDIOS DE PAGO</div>
    
        <div class="description">
            <p>A menos que se indique un medio distinto para casos u ofertas específicos, los productos y servicios que se informan y transan en este sitio sólo podrán ser pagados por medio de:</p>
            <p>a.- Tarjeta de crédito bancarias Visa, Mastercard, Dinners Club International, American Express u otras habilitadas y válidamente emitidas en Chile o en el extranjero, siempre que mantengan un contacto vigente para este efecto con SOCADIN.</p>
            <p>b.- Tarjetas de débito bancarias acogidas al sistema Redcompra o habilitadas para realizar pagos y transacciones a través del sistema WebPay, válidamente emitidas en Chile por bancos nacionales, que mantengan un contrato vigente para tales efectos con SOCADIN.</p>
            <p>c.- Transferencia bancaria, la cual se realizará a la cuenta N° 190217752, la cual SOCADIN mantiene en el Banco Scotiabank.
            Otros medios de pago que pudieran ser anunciados y aceptados a futuro, los cuales serán informados pertinentemente por los canales y conductos que correspondan.</p>
            <p>El pago con tarjetas de débito se realizará a través de WebPay, que es un sistema de pago electrónico que se encarga de hacer el cargo automático a la cuenta bancaria del usuario. Del mismo modo, se  puede realizar el pago a través de Mercado Pago, cuyo funcionamiento es similar a Webpay.</p>
            <p>El Usuario declara que entiende que estos medios o portales de pago son de propiedad de terceras empresas o personas proveedoras de dichos servicios, independientes y no vinculadas a SOCADIN, por lo que la continuidad de su prestación de servicios en el tiempo, así como el correcto funcionamiento de sus herramientas y botones de pago en línea, son de exclusiva responsabilidad de las proveedoras de estos servicios y en ningún caso de SOCADIN.</p>


            <a href="javascript:void(0)" class="btnSubir">Subir</a>
        </div>

    </div>

    <div class="box-scroll" id="term7">
        <div class="tituloBox">SÉPTIMO.- ENTREGA Y DESPACHO</div>
    
        <div class="description">
            <p>Los bienes y productos adquiridos a través del Sitio Web estarán sujetos a las condiciones de despacho y entrega elegidas por el usuario, dentro de aquellas que estén disponibles en el sitio. La información del domicilio o lugar de envío es de exclusiva responsabilidad del Usuario.</p>

            <a href="javascript:void(0)" class="btnSubir">Subir</a>
        </div>

    </div>

    <div class="box-scroll" id="term8">
        <div class="tituloBox">OCTAVO.- RESPONSABILIDAD</div>
    
        <div class="description">
            <p>SOCADIN no responderá, en ningún caso, por los siguientes hechos:</p>
            <p>a.- Uso indebido que Usuarios o visitantes al Sitio Web realicen del contenido almacenado, o de los productos que adquirieran en el mismo, así como de los derechos de propiedad industrial o intelectual contenidos en el Sitio Web, o en los productos o servicios.</p>
            <p>b.- Daños y perjuicios, concretos o eventuales, causados a Usuarios derivados del funcionamiento de las herramientas de búsqueda del Sitio Web, así como de errores generados por elementos técnicos del Sitio o del motor de búsqueda.</p>
            <p>c.- Contenido de Sitios Web a las que los Usuarios puedan acceder con o sin autorización de SOCADIN.</p>
            <p>d.- Pérdida, mal uso o uso no autorizado de contraseña o códigos de validación, sea por parte del Usuario o de un tercero, de la forma expresada en estos Términos y Condiciones. De la misma forma, las partes reconocen y dejan constancia que el soporte computacional entregado por www.socadin.cl no es infalible, por lo tanto, pueden verificarse circunstancias ajenas a la voluntad de SOCADIN que puedan causar que el Sitio Web o su plataforma no se encuentren operativos durante un cierto período de tiempo.</p>
            <p>e.- Información referida a SOCADIN que se encuentre en Sitios Web distintos o ajenos a www.socadin.cl.</p>
            <p>En caso de estar ante alguno de estos supuestos, SOCADIN tomará todas las medidas para reestablecer el correcto funcionamiento del Sitio Web y de su sistema comunicacional lo más pronto posible, sin que pudiera imputársele algún tipo de responsabilidad por aquello.</p>
            <p>SOCADIN no asegura disponibilidad ni continuidad de funcionamiento del Sitio Web, y tampoco que en cualquier momento, los Usuarios puedan acceder a él o a las promociones y ofertas que estuvieran disponibles.</p>
            <p>SOCADIN tampoco es responsable por la existencia de virus u otros elementos perjudiciales en los documentos o archivos almacenados en los sistemas informáticos de todo tipo de propiedad de los Usuarios. SOCADIN no responderá de perjuicios causados al Usuario, derivados del uso indebido de las tecnologías y plataformas puestas a su disposición, cualquiera sea la forma en que se utilicen inadecuadamente estos elementos tecnológicos. Asimismo, SOCADIN no responderá por daños producidos por el uso indebido o mala fe de los Usuarios al utilizar www.socadin.cl.</p>
            <p>No obstante, en el caso de producirse un doble pago por algún Usuario en el Sitio Web, SOCADIN realizará la devolución íntegra de la suma del sobrepago, dentro de los 7 días siguientes a la recepción del correspondiente reclamo realizado por el Usuario o sus representantes, el cual debe contar con un anexo que incluya los comprobantes de pago originales correspondientes al sobrepago realizado.</p>
            <p>Al momento de registrarse, hacer login, para la realización de transferencias y para comunicarse, www.socadin.cl utiliza certificados digitales de seguridad (SSL), con el fin de encriptar la información. SOCADIN no almacena ni conserva datos financieros de sus Usuarios.</p>


            <a href="javascript:void(0)" class="btnSubir">Subir</a>
        </div>

    </div>

    <div class="box-scroll" id="term9">
        <div class="tituloBox">NOVENO.- POLÍTICA DE SEGURIDAD DE DATOS</div>
    
        <div class="description">
            <p>SOCADIN adoptará todas las medidas necesarias para resguardar los datos de sus Usuarios y clave secreta, como sistemas de encriptado de información, certificados de seguridad y otros semejantes o equivalentes que SOCADIN considere prudentes para estos efectos. En caso de realizarse cambios hechos por el Usuario en la información registrada o ingresada en el Sitio Web, o bien, si se produce la detección de cualquier tipo de irregularidad atribuible al Usuario relacionada con la información entregada o con transferencias realizadas por el Usuario, así como con todo lo relacionado a la identificación en las mismas o el uso de medios de pago o simplemente como medida de protección de identidad, el personal del Sitio Web se pondrá en contacto con el Usuario, sea por vía telefónica o por correo electrónico, para corroborar los datos y la información, así como para evitar la existencia de posibles fraudes.</p>
            <p>En la eventualidad de no establecerse contacto en un plazo de 24 horas, y pensando en la seguridad del Usuario, cualquier compra de un producto o contratación de un servicio no será confirmada. Se informará al Usuario por vía telefónica o por correo electrónico el hecho de haber quedado sin efectos producto de no haber podido comprobarse su identidad o medio de pago utilizado. Adicionalmente, los comprobantes en que consten las gestiones realizadas para intentar contactarse con el Usuario con el objetivo de confirmar la transacción quedarán a disposición de éste último por un plazo de 45 días, en caso que el Usuario quisiera confirmar la compra o contratación. Consultas adicionales pueden realizarse por las vías de contacto con el Sitio Web establecidas en el párrafo relativo a la Información del Sitio Web, presente en este mismo documento de Términos y Condiciones.</p>
            <p>Con todo lo anterior, los Usuarios son plenamente responsables por el extravío o pérdida, mal uso o uso no autorizado que se diera del producto o servicio adquirido o contratado sea que incurran ellos o terceros en los hechos descritos previamente, luego de realizada una compra o contratación de servicio siguiendo la forma estipulada en los presentes Términos y Condiciones.
            Datos Personales.</p>
            <p>Al acceder al Sitio Web www.socadin.cl, los Usuarios o visitantes garantizan que la información que proporcionan para ingresar o para la compra de productos o contratación de servicios es veraz, completa, exacta y actualizada.</p>
            <p>De acuerdo a lo establecido en la Ley N° 19.628 de Protección de Datos Personales, los datos de este tipo que se suministren al Sitio Web, pasan a formar parte de una base de datos perteneciente a SOCADIN y serán destinados única y exclusivamente para los fines que motivaron su entrega, especialmente para la comunicación entre SOCADIN y sus Usuarios, validar datos relativos a compras o contratación de servicios, concretar despachos, y responder consultas. Estos datos nunca serán comunicados o compartidos con otras empresas sin expresa autorización de su titular (el Usuario) ni serán transferidos internacionalmente.
            SOCADIN jamás solicitará a sus Usuarios la entrega de datos personales o financieros a través de correos electrónicos.</p>
            <p>SOCADIN presume que los datos incorporados por los Usuarios o por personas autorizadas por éstos son correctos y exactos. Los Usuarios, al aceptar los presentes Términos y Condiciones, manifiestan su conformidad con que los datos personales aportados al rellenar formularios presentes en www.socadin.cl puedan ser utilizados posteriormente para la elaboración y envío de ofertas que puedan diferir de las ofrecidas en el Sitio Web.</p>
            <p>Sin perjuicio de lo anterior, SOCADIN garantiza a todos sus Usuarios el libre ejercicio de los derechos contemplados en la Ley N° 19.628 de Protección de Datos Personales en lo relativo a la información, modificación, cancelación y bloqueo de sus datos personales. En consecuencia, los Usuarios podrán realizar requerimientos relacionados con los ya mencionados derechos, los que serán respondidos por SOCADIN en un plazo no superior a dos días corridos.</p>
            <p><strong>Documentos Electrónicos.</strong></p>
            <p>Los Usuarios, como receptores manuales de documentos electrónicos según lo establecido en la Resolución Exenta N° 11 del 14 de febrero de 2003, dictada por el Servicio De Impuestos Internos, la cual establece el procedimiento para que contribuyentes autorizados para emitir documentos electrónicos puedan también realizar su envío por estos mismos medios a los receptores manuales; declaran y aceptan que, al aprobar estos Términos y Condiciones, autorizan a SOCADIN, RUT: 85.385.800-5, para que el documento tributario correspondiente a la transacción por compra de producto o contratación de servicio, le sea entregada únicamente a través de un medio electrónico. Igualmente, se autoriza que el aviso de publicación del documento tributario sea enviado a los Usuarios por correo electrónico.</p>
            <p>De conformidad con la normativa ya mencionada, y en caso de que el Usuario necesite respaldar su información contable, éste asumirá, en relación a los documentos tributarios ya mencionados, las siguientes obligaciones:</p>
            <p>a.- Imprimir los documentos recibidos electrónicamente, para cada período tributario, inmediatamente luego de recibirlos de parte del emisor.</p>
            <p>b.- Imprimir el documento en tamaño y forma original.</p>
            <p>c.- Utilizar papel blanco tipo original de tamaño mínimo 21,5 cm x 14 cm (media carta) y de tamaño máximo 21,5 cm x 33 cm (oficio).</p>
            <p>d.- Imprimir en una calidad que asegure la legibilidad del documento durante al menos seis años, según establece la legislación vigente. La impresión se realizará mediante métodos de láser o de inyección de tinta, a menos que se establezca un método distinto por una legislación o normativa en la materia dictada a futuro.</p>


            <a href="javascript:void(0)" class="btnSubir">Subir</a>
        </div>

    </div>

    <div class="box-scroll" id="term10">
        <div class="tituloBox">DÉCIMO.- PROPIEDAD INTELECTUAL Y DERECHOS RESERVADOS</div>
    
        <div class="description">
            <p>Todos los derechos presentes en el Sitio Web, incluyendo la propiedad intelectual, respecto al mismo Sitio Web, páginas y otro tipo de contenido, como podría ser, de forma no taxativa, textos de todo tipo, material gráfico o audiovisual (incluyendo, pero no limitado a imágenes, clips de audio y videos), logos y logotipos, íconos de todo tipo, material y contenido descargable, recopilaciones de datos e información y códigos fuente de la página, son de propiedad de SOCADIN.</p>
            <p>Así mismo, todo o parte del contenido mencionado en el párrafo anterior, se encuentra registrado como marca comercial, creación o imagen comercial, por lo tanto, los derechos son de propiedad de SOCADIN, y se prohíbe su uso indebido y reproducción con fines comerciales sin previa autorización, así como también se prohíbe su uso de cualquier forma que pudiera desprestigiar o afectar negativamente a SOCADIN.</p>
            <p>Cualquier otra marca comercial que tuviera presencia en el Sitio Web y cuyos derechos no sean de propiedad de SOCADIN, pertenecerá a sus debidos dueños.</p>


            <a href="javascript:void(0)" class="btnSubir">Subir</a>
        </div>

    </div>
</div>