<?php
$oc = (isset($_GET["oc"])) ? mysqli_real_escape_string($conexion, $_GET["oc"]) : 0;
$PA = consulta_bd("id, estado_id, oc, medio_de_pago","pedidos","oc = '$oc'","");

$method = (isset($_GET["method"])) ? mysql_real_escape_string($_GET["method"]) : 'tbk';

if(is_array($PA)){
  $method = ($PA[0][3] == 'webpay') ? 'tbk' : 'mp';

  $camposMetodo = ''; // 20 - 25

  if ($method == 'tbk') {
    $camposMetodo = 'transaction_date, amount, card_number,shares_number,authorization_code,payment_type_code';
  }else{
    $camposMetodo = "mp_transaction_date, mp_paid_amount, mp_card_number, mp_cuotas, mp_auth_code, mp_payment_type";
  }

  $pedido = consulta_bd("
    id,
    cliente_id,
    nombre,
    email,
    telefono,
    region_id,
    comuna_id,
    direccion,
    retiro_en_tienda,
    sucursal_id,
    factura,
    razon_social,
    direccion_factura,
    rut_factura,
    email_factura,
    giro,
    total, 
    valor_despacho,
    total_pagado,
    visto,
    $camposMetodo
  ", 'pedidos', "oc= '$oc'", '');

  $cliente_activo = consulta_bd('activo', 'clientes', "id = {$pedido[0][1]}", '');

  // Variables condiciones
  $retiro_en_tienda = $pedido[0][8];
  $sucursal_id = $pedido[0][9];
  $factura = $pedido[0][10];

  $razon_social = $pedido[0][11];
  $direccion_factura = $pedido[0][12];
  $rut_factura = $pedido[0][13];
  $email_factura = $pedido[0][14];
  $giro = $pedido[0][15];

  // Detalle comprador
  $nombre = $pedido[0][2];
  $email = $pedido[0][3];
  $telefono = $pedido[0][4];

  // Detalle despacho - retiro
  if ($retiro_en_tienda == 1) {
    $sucursal = consulta_bd('s.direccion, r.nombre, c.nombre', 'sucursales s JOIN comunas c ON c.id = s.comuna_id JOIN regiones r ON r.id = c.region_id', "s.id = {$sucursal_id}", "");

    $direccion = $sucursal[0][0];
    $region = $sucursal[0][1];
    $comuna = $sucursal[0][2];

  }else{
    $direccion_cliente = consulta_bd('r.nombre, c.nombre', 'regiones r JOIN comunas c ON c.region_id = r.id', "r.id = {$pedido[0][5]} and c.id = {$pedido[0][6]}", '');
    $direccion = $pedido[0][7];
    $region = $direccion_cliente[0][0];
    $comuna = $direccion_cliente[0][1];
  }

  //detalle pedido
  $pedido_id     = $pedido[0][0];
  $subtotal      = $pedido[0][16];
  $despacho    = $pedido[0][17];
  $total_pagado = number_format($pedido[0][18],0,",",".");

  $fecha       = date('d/m/Y', strtotime($pedido[0][20]));
  $hora_pago       = date('H:s:i', strtotime($pedido[0][20]));

  //detalle transbank
  $num_tarjeta     = $pedido[0][22];
  $num_cuotas      = $pedido[0][23];
  $cod_aurizacion   = $pedido[0][24];
  $tipo_pago     = $pedido[0][25];

  //funcion tipo pago
  $tipo_pago = tipo_pago($tipo_pago,$num_cuotas, $method);

  //detalle productos
  $detalle_productos = consulta_bd("productos_detalle_id, cantidad, precio_unitario","productos_pedidos","pedido_id= $pedido_id","");
}else{

  echo '<script>location.href="404";</script>';
  
}
?>

<div class="breadcrumbs">  
    <div class="container">
        <ul itemscope itemtype="http://schema.org/BreadcrumbList">
            <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="home" itemprop="item">
                    <span itemprop="name">Home</span>
                    <meta itemprop="position" content="1" />
                </a>
                <img src="img/icons/sliderArrow.svg">
            </li>

            <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="javascript:void(0)" itemprop="item" class="active">
                    <span itemprop="name">Éxito transferencia</span>
                    <meta itemprop="position" content="2" />
                </a>
            </li>
        </ul>
    </div>
</div>


<div class="container">
  <div class="head_exito mb-40 mt-30">
    <img src="img/exito.png" class="mb-30">

    <div class="title_exito">Hemos recibido su solicitud</div>
    
    <p>
      ¡Muchas gracias por comprar con SOCADIN! <br>
      Su número de orden de compra es: <span><?= $oc ?></span>
    </p>

    <p>A continuación encontrarás los datos para que realices la transferencia antes de 48 horas, después de ese periodo se anulara el pedido.</p>

    <p>Para asistencia sobre el pago o dudas del producto, por favor contáctenos al +56 2 2773 3726 o al mail ventas@socadin.cl, en horario de atención de tienda (lunes a viernes de 10:00 a 19:00 hrs.).</p>

    <p>En el caso que el producto incluya despacho pagado o por pagar el o los productos serán enviados 24 horas después de haber realizado el pago, previa confirmación de la transacción. En el caso que la transacción se realice un día viernes, fin de semana o feriado el despacho se realizara en los primeros días hábiles siguientes.</p>

    <p>Socadin Limitada<br>
    Banco Scotiabank<br>
    Tipo de cuenta: Cuenta Corriente <br>
    Cuenta: 190217752 <br>
    RUT: 85.385.800-5 <br>
    Correo: ventas@socadin.cl</p>

  </div>

  <div class="gr-exito">
    <div class="col">
      <div class="title">Información personal</div>

      <div class="row">Nombre: <?= $nombre ?></div>
      <div class="row">Teléfono: <?= $telefono ?></div>
      <div class="row">Email: <?= $email ?></div>

    </div>
    <div class="col">
      <?php if ($retiro_en_tienda == 1): ?>
        <div class="title">Información de retiro</div>
      <?php else: ?>
        <div class="title">Información de envío</div>
      <?php endif ?>

      <div class="row">Dirección: <?= $direccion ?>, <?= $comuna ?>, <?= $region ?></div>
      
    </div>
    <div class="col">
      <?php if ($factura == 1): ?>
        <div class="title">Información de facturación</div>

        <div class="row">Giro: <?= $giro ?></div>
        <div class="row">Razón social: <?= $razon_social ?></div>
        <div class="row">Rut: <?= $rut_factura ?></div>
        <div class="row">Dirección: <?= $direccion_factura ?></div>
      <?php endif ?>
    </div>
    <div class="col">
      
    </div>
  </div>

  <?php if ($cliente_activo[0][0] == 0): ?>
    <div class="content_crear_cliente mt-20 mb-30">
      <div class="title">Tu cuenta a un solo clic!</div>
      <div class="subc">Crea tu cuenta a un solo clic y podrás, revisar el estado de tus despachos, <br>
historial de compras, productos guardados en tu dashboard y mucho más.</div>

      <form action="registrarClienteExito" method="post">
        <button class="btnClienteExito">Crear cuenta</button>
      </form>

    </div>
  <?php endif ?>

</div>