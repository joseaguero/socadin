<?php 

$ancho = mysqli_real_escape_string($conexion, $_GET['ancho']);
$perfil = mysqli_real_escape_string($conexion, $_GET['perfil']);
$aro = mysqli_real_escape_string($conexion, $_GET['aro']);

$whereStr = '';

$existe_ancho = false;
$existe_perfil = false;
$existe_aro = false;

if ($ancho != '' and $ancho != null) {
    $existe_ancho = true;
    $ancho_clean = (is_numeric($ancho)) ? $ancho : "'$ancho'";
    $filtros_grilla['ancho'] = $ancho_clean;
    $whereAncho = "AND cadena_ancho = $ancho_clean";
}

if ($perfil != '' and $perfil != null) {
    $existe_perfil = true;
    $perfil_clean = (is_numeric($perfil)) ? $perfil : "'$perfil'";
    $filtros_grilla['perfil'] = $perfil_clean;
    $wherePerfil = "AND cadena_perfil = $perfil_clean";
}

if ($aro != '' and $aro != null) {
    $existe_aro = true;
    $aro_clean = (is_numeric($aro)) ? $aro : "'$aro'";
    $filtros_grilla['aro'] = $aro_clean;
}

$productos = get_products($filtros_grilla);



?>

<div class="breadcrumbs">  
    <div class="container">
        <ul itemscope itemtype="http://schema.org/BreadcrumbList">
            <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="home" itemprop="item">
                    <span itemprop="name">Home</span>
                    <meta itemprop="position" content="1" />
                </a>
                <img src="img/icons/sliderArrow.svg">
            </li>

            <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="javascript:void(0)" itemprop="item" class="active">
                    <span itemprop="name">Buscar cadenas</span>
                    <meta itemprop="position" content="2" />
                </a>
            </li>
        </ul>
    </div>
</div>

<div class="container mt-20 mb-50">
    <section class="buscadorCadenas">
        <div class="homeTitle mb-20 mt-30">Busca tu cadena por dimensiones</div>

        <div class="gr-buscadorHome">
            <div class="col">
                <?php $ancho_medidas = consulta_bd('cadena_ancho', 'productos_detalles', 'id > 0 and publicado = 1 GROUP BY cadena_ancho', 'cadena_ancho asc'); ?>
                <div class="custom-select select-home select-ancho <?= ($existe_ancho) ? 'activeRegion' : '' ?>" data-name="Ancho" data-request="ancho">
                    <select name="ancho" data-active="<?= ($existe_ancho) ? 1 : 0 ?>">
                        <?php foreach ($ancho_medidas as $an): ?>
                            <option value="<?= $an[0] ?>" <?= ($an[0] == $ancho) ? 'selected':'';?>><?= $an[0] ?></option>
                        <?php endforeach ?>
                    </select>
                </div>
            </div>
            <div class="col">
                <?php $perfil_medidas = consulta_bd('cadena_perfil', 'productos_detalles', "cadena_ancho = $ancho and publicado = 1 GROUP BY cadena_perfil", 'cadena_perfil asc'); ?>
                <div class="custom-select select-home select-perfil <?= ($existe_perfil) ? 'activeRegion' : '' ?>" data-name="Perfil" data-request="perfil">
                    <select name="perfil" data-active="<?= ($existe_perfil) ? 1 : 0 ?>">
                        <option value="0">&nbsp;</option>
                        <?php foreach ($perfil_medidas as $per): ?>
                            <option value="<?= $per[0] ?>" <?= ($per[0] == $perfil) ? 'selected':'';?>><?= $per[0] ?></option>
                        <?php endforeach ?>
                    </select>
                </div>
            </div>
            <div class="col">
                <?php $aro_medidas = consulta_bd('cadena_aro', 'productos_detalles', "id > 0 and publicado = 1 {$whereAncho} and cadena_perfil = '$perfil' GROUP BY cadena_aro", 'cadena_aro asc'); ?>
                <div class="custom-select select-home select-aro <?= ($existe_aro) ? 'activeRegion' : '' ?>" data-name="Aro" data-request="aro">
                    <select name="aro" data-active="<?= ($existe_aro) ? 1 : 0 ?>">
                        <option value="0">&nbsp;</option>
                        <?php foreach ($aro_medidas as $ar): ?>
                            <option value="<?= $ar[0] ?>" <?= ($ar[0] == $aro) ? 'selected':'';?>><?= $ar[0] ?></option>
                        <?php endforeach ?>
                    </select>
                </div>
            </div>
            <div class="col">
                <a href="javascript:void(0)" class="btnBuscarCadenas">Buscar</a>
            </div>
        </div>

        <div class="banner-cadenas mt-20">
            <img src="img/banner-cadenas.png" width="100%">
        </div>

    </section>

    <div class="homeTitle mb-20 mt-30">Resultados</div>

    <div class="productos-grilla grilla-4">
        <?php 
        foreach ($productos['productos']['producto'] as $prod):
            $link = 'ficha/' . $prod['id_producto'] . '/' . $prod['nombre_seteado']; ?>
            <div class="item">
                <a href="<?= $link ?>" class="headerProduct">
                    <?php if ($prod['descuento'] > 0): ?>
                        <span class="oferta-grilla">-<?= round(100 - ($prod['descuento'] * 100) / $prod['precio']) ?>%</span>
                    <?php endif ?>
                    <img src="<?= $prod['imagen_grilla'] ?>" class="imgProduct">
                </a>

                <div class="bodyProduct">
                    <a href="<?= $link ?>" class="name"><?= $prod['nombre'] ?></a>
                    
                    <?php if ($prod['solo_cotizar'] == 1): ?>
                        <div class="description">
                            <?= $prod['descripcion_grilla'] ?>
                        </div>
                    <?php else: ?>
                        <div class="prices">
                            <?php if ($prod['descuento'] > 0): ?>
                                <a href="<?= $link ?>" class="discountPrice">Antes $<?= number_format($prod['precio'], 0, ',', '.') ?> + IVA</a>
                                <a href="<?= $link ?>" class="finalPrice">$<?= number_format($prod['descuento'], 0, ',', '.') ?> + IVA</a>
                            <?php else: ?>
                                <a href="<?= $link ?>" class="discountPrice"></a>
                                <a href="<?= $link ?>" class="finalPrice">$<?= number_format($prod['precio'], 0, ',', '.') ?> + IVA</a>
                            <?php endif ?>
                            
                        </div>
                    <?php endif ?>

                    <a href="<?= $link ?>" class="btnDetails">Ver ficha</a>
                </div>
            </div>
        <?php endforeach; ?>
    </div>

</div>