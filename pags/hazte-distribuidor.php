<div class="breadcrumbs">  
    <div class="container">
        <ul itemscope itemtype="http://schema.org/BreadcrumbList">
            <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="home" itemprop="item">
                    <span itemprop="name">Home</span>
                    <meta itemprop="position" content="1" />
                </a>
                <img src="img/icons/sliderArrow.svg">
            </li>

            <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="hazte-distribuidor" itemprop="item" class="active">
                    <span itemprop="name">Hazte distribuidor</span>
                    <meta itemprop="position" content="2" />
                </a>
            </li>
        </ul>
    </div>
</div>

<div class="container mt-20 relative">
    <div class="titulo-pag">Hazte distribuidor oficial de socadin</div>
    <div class="descripcion-pag">Certificación y garantía de productos.</div>

    <div class="hd_content-form">
        <div class="titulo">Regístrate como distribuidor</div>
        
        <form action="hazteDistribuidor" method="post" id="formDistribuidor">
            <div class="main-input mbxs-20">
                <div class="input-form">
                    <label class="label_form">Nombre empresa</label>
                    <input type="text" id="nombre_empresa" name="nombre_empresa" class="input_text">
                </div>
            </div>

            <div class="main-input mbxs-20">
                <div class="input-form">
                    <label class="label_form">Rut empresa</label>
                    <input type="text" id="rut_empresa" name="rut_empresa" class="input_text rut-input" maxlength="12">
                </div>
            </div>

            <div class="main-input mbxs-20">
                <div class="input-form">
                    <label class="label_form">Email</label>
                    <input type="email" id="email" name="email" class="input_text">
                </div>
            </div>

            <div class="main-input mbxs-20">
                <div class="input-form">
                    <label class="label_form">Teléfono</label>
                    <input type="text" id="telefono" name="telefono" class="input_text" onkeypress="return justNumbers(event);" maxlength="9">
                </div>
            </div>

            <div class="main-input mbxs-20">
                <div class="input-form">
                    <label class="label_form">Mensaje</label>
                    <textarea name="mensaje" id="mensaje" rows="4"></textarea>
                </div>
            </div>

            <button type="button" class="btnDistribuidor" id="enviarFormularioRecaptcha2">Enviar</button>
        </form>

    </div>
</div>

<div class="bg-distribuidor mb-50">
    <img src="img/bg-distribuidor-1.jpg">
</div>

<div class="clearfix"></div>
<?php $catalogos = consulta_bd('id, nombre, archivo', 'catalogos', '', 'posicion asc'); ?>
<?php if (is_array($catalogos)): ?>
    <div class="container mt-100 mb-50">
        <div class="titulo-pag">Catálogos</div>
        <div class="descripcion-pag">Descarga nuestros catálogos de productos, y entérate de novedades.</div>

        <div class="gr-catalogos mt-20">
            <?php foreach ($catalogos as $cat): ?>
                <div class="col">
                    <div class="colPdf"><img src="img/pdf.svg"></div>
                    <div class="info">
                        <span class="name"><?= $cat[1] ?></span>
                        <a href="docs/catalogos/<?= $cat[2] ?>" target="_blank" class="link">Descargar catálogo</a>
                    </div>
                </div>
            <?php endforeach ?>
        </div>
    </div>
<?php endif ?>


<?php if (isset($_SESSION['flash'])):
    $status = $_SESSION['flash']['status'];
    $message = $_SESSION['flash']['message']; ?>
    <script type="text/javascript">
        $(function(){
            swal('', "<?= $message ?>", "<?= $status ?>");
        })
    </script>
<?php
unset($_SESSION['flash']);
 endif ?>