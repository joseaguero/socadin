<?php
include('../conf.php');
require_once('../includes/tienda/cart/inc/functions.inc.php');
include('../../PHPMailer/PHPMailerAutoload.php');

$oc 	= ($_POST['oc']) ? $_POST['oc'] : '0';
$opc 	= ($_POST['opc']) ? $_POST['opc'] : '0';

$asunto = ($opc == 'Apruebo') ? 'Tu Pedido ha sido Confirmado': 'Tu pedido ha sido anulado';

$pedido = consulta_bd('nombre, total_pagado, email, id, estado_id, retiro_en_tienda', 'pedidos', "oc = '$oc'", '');

$id_pedido = $pedido[0][3];
$id_estado = $pedido[0][4];

$retiro_tienda = $pedido[0][5];

if ($id_estado == 2) {
	header('Location: ../index.php?op=272a&error=El pedido ya ha sido aprobado anteriormente.&tipo=error');
}else{

	$nombre_sitio = opciones("nombre_cliente");
    $nombre_corto = opciones("dominio");
    $noreply = "no-reply@".$nombre_corto;
    $url_sitio="https://".$nombre_corto;
    $logo = opciones("logo_mail");
	$color_logo = opciones("color_logo");

	if($opc == 'Apruebo'){
		$estado_id = 2;
		$body = '<body style="background: #f4f4f4;">
		<div style="border-top: 2px solid #db4437; border-bottom: 2px solid #db4437; max-width: 600px; margin: auto; background: #fff; padding: 15px;">
			<img src="'.$logo.'">

			<h4 style="color: #db4437;">Estimado(a) '.$pedido[0][0].'</h4>

			<p>Su pago ha sido aprobado</p>

			<p style="color: #4e4e4e;"><strong>A continuación le enviamos los datos de su pedido:</strong> <br>
			OC: '.$oc.' <br>
			Monto: $'.number_format($pedido[0][1], 0, ",", ".").'</p>

			<p>Agradecemos tu confianza y te invitamos a seguir viendo nuestras ofertas y promociones en nuestra página web: <a href="https://socadin.cl/">socadin.cl</a></p>
		</div>
		</body>';

		$estado_pago = 'Pago confirmado';
	}else if($opc == 'Rechazo'){
		$estado_id = 3;
		$body = '<body style="background: #f4f4f4;">
		<div style="border-top: 2px solid #db4437; border-bottom: 2px solid #db4437; max-width: 600px; margin: auto; background: #fff; padding: 15px;">
			<img src="'.$logo.'">

			<h4 style="color: #db4437;">Estimado(a) '.$pedido[0][0].'</h4>

			<p style="color: #4e4e4e;">Damos aviso que su pedido con número de orden '.$oc.' ha sido anulado, ya que, no se realizó la transferencia correspondiente.</p>

			<p>Cualquier duda comunicarse con ventas@socadin.cl</p>

			<p>Atte, Socadin.cl</p>
		</div>
		</body>';

		$estado_pago = 'Pago anulado';
	}

	$mail = new PHPMailer;
	$mail->isSMTP();
	$mail->SMTPDebug = 0;
	$mail->Debugoutput = 'html';
	$mail->Host = opciones("Host");
	$mail->Port = opciones("Port");
	$mail->SMTPSecure = opciones("SMTPSecure");
	$mail->SMTPAuth = true;
	$mail->Username = opciones("Username");
	$mail->Password = opciones("Password");

	$mail->setFrom(opciones("setFrom_mail"), opciones("nombre_setFrom_mail"));
	//Set an alternative reply-to address

	$mail->addAddress($pedido[0][2], $pedido[0][0]);
	$mail->Subject = $asunto;
	$mail->msgHTML($body);
	$mail->AltBody = $body;
	$mail->CharSet = 'UTF-8';
	//send the message, check for errors
	if (!$mail->send()) {
	    $mensaje = 'Error al realizar la acción.';
	    $status = 'error';
	} else {
		$fecha = date("Y-m-d H:i:s", time());

		update_bd('pedidos', " estado_envio = '$estado_pago', estado_id = $estado_id, fecha_modificacion = '$fecha'", "oc = '$oc'");

		if ($opc == "Apruebo" AND $retiro_tienda == 0) {
			exec("php -f ../../ajax/postEnviame.php $id_pedido", $output, $return_var);
		}
		
		$mensaje = 'Transferencia actualizada correctamente';
	    $status = 'exito';
	}

	$update = update_bd("pedidos","estado_id = $estado_id","oc = '$oc'");	

	header('Location: ../index.php?op=272a&error='.$mensaje.'&tipo='.$status);

}

?>