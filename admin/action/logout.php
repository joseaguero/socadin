<?php 
include('../conf.php');
$nombre_sitio = get_nombre_sitio($base);
$remote_ip = $_SERVER['REMOTE_ADDR'];
$user = $_SESSION[$nombre_sitio."_admin"];
$user_id = (int)$user;
if ($_SESSION[$nombre_sitio."_admin"]!='')
{
	$_SESSION[$nombre_sitio."_admin"]=false;
	$_SESSION[$nombre_sitio."_admin_sh"]=false;
    /*Log*/
    $log = insert_bd("system_logs","tabla, accion, fila, administrador_id,date, remote_ip","'administradores','Termino de sesión', '','$user_id', NOW(), '$remote_ip'");
    /*Fin Log*/
	header("location:../index.php");
}
else
{
	$error = "No se pudo hacer logout.&tipo=error";
	header("location:../index.php?error=$error");
}
?>