<?php

/********************************************************\
|  Moldeable CMS - Excel generator.			             |
|  Fecha Modificación: 16/06/2011		                 |
|  Todos los derechos reservados © Moldeable S.A. 2012   |
|  Prohibida su copia parcial o total  					 |
|  http://www.moldeable.com/                             |
\********************************************************/

date_default_timezone_set('America/Santiago');
include('../conf.php');
mysqli_query ($conexion, "SET NAMES 'latin1'");


header("Content-type: application/octet-stream");
header("Content: charset=UTF-8");
header("Content-Disposition: attachment; filename=excel_stock_y_precio.xls"); 
header("Pragma: no-cache" );
header("Expires: 0"); 

// $hoy = date("d/m/Y");
$lista_id = mysqli_real_escape_string($conexion, $_GET['lista']);
$productos = consulta_bd("l.lista_de_precio_id, pd.sku, l.precio, l.descuento","lista_de_precio_productos l JOIN productos_detalles pd ON l.productos_detalle_id = pd.id", "l.lista_de_precio_id = $lista_id","l.id asc");
?>
<table width="700" border="1">
        <thead>
            <tr>
                <th><h3>lista</h3></th>
            	<th><h3>sku</h3></th>
                <th><h3>precio</h3></th>
                <th><h3>descuento</h3></th>		
            </tr>
        </thead>
        <tbody>
            <?php if (is_array($productos)): ?>
                <?php for($i=0; $i<sizeof($productos); $i++) {?>
                    <tr>
                        <td><?= $productos[$i][0] ?></td>
                        <td><?= $productos[$i][1] ?></td>
                        <td><?= $productos[$i][2] ?></td>
                        <td><?= $productos[$i][3] ?></td>
                    </tr>
                <?php } ?>
            <?php else: 
                $productos = consulta_bd('sku, precio, descuento', 'productos_detalles', "", 'id asc');?>
                <?php for($i=0; $i<sizeof($productos); $i++) {?>
                    <tr>
                        <td><?= $lista_id ?></td>
                        <td><?= $productos[$i][0] ?></td>
                        <td><?= $productos[$i][1] ?></td>
                        <td><?= $productos[$i][2] ?></td>
                    </tr>
                <?php } ?>
            <?php endif ?>
        	
        </tbody>

</table>