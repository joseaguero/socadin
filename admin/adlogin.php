<?php include('conf.php');
	$cliente = consulta_bd("valor","opciones","nombre='nombre_cliente'","");
	if($cliente[0][0] == ''){
		$nombre_cliente = 'Cliente Anonimo';
	} else {
		$nombre_cliente = $cliente[0][0];
	}
	
	//print_r($_SESSION);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Moldeable CMS</title>
	<link href="css/config.css" rel="stylesheet" type="text/css" />
	<link href="css/style_login.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="https://code.jquery.com/jquery-1.9.1.js"></script>
  	<script type="text/javascript" src="js/jquery.label_better.min.js"></script>

	<script type="text/javascript">
		function fwp(){
			$('#logeo').css('display', 'none');
			$('#fwp').fadeIn(200);
		}
		function cancel(){
			$('#fwp').css('display', 'none');
			$('#logeo').fadeIn(200);
		};
	</script>

	<link rel="stylesheet" href="css/alertify.core.css" />
	<link rel="stylesheet" href="css/alertify.default.css" id="toggleCSS" />
	
	<script>
	  $(document).ready( function() {
	    $(".label_better").label_better({
	      easing: "bounce"
	    });
	  });
		
	</script>
</head>

<body>


<div class="wrap" style="background-image:url(pics/fondos/<?= rand(1, 11) ?>.jpg); background-size:cover;">
	<?php
		if ($_GET[op] != 'fgp')
		{
	?>
		
		<div class="logo">
			<img src="pics/logo-moldeable-cms3.png" width="276" height="32" alt="moldeable ">
		</div><!-- / fin logotipo modeable cms-->
        <!-- login -->
		<div class="login" id="logeo">
            <!-- logotipo modeable cms-->
        	<h2 class="tituloLogin">Inicio de sesión</h2>
			<h4 class="subtituloLogin">Si no tienes cuenta solicita una con tu administrador.</h4>
            <form action="action/login.php" method="POST" name="login" class="formLogin bl_form">
            	<input name="username" type="text" id="usuario" class="textfield user campoLogin label_better" data-new-placeholder="Usuario" placeholder="Usuario"/>
                
                <input name="pass" type="password" id="contra" class="textfield pass campoLogin label_better" placeholder="Contraseña" data-new-placeholder="Contraseña" />
                <input type="submit" class="boton accede" name="Submit" value="Acceder" />
                <div><a href="javascript:fwp();">¿Olvidaste tu usuario o Password?</a></div>
            </form>
            <div class="footer2">
            	<p class="para">Sistema de administración desarrollado exclusivamente para <strong class="ncam"><?php echo $nombre_cliente; ?></strong></p>
                <p class="de">@2020 moldeable.com</p>
            </div>
         </div><!-- / login -->

    
    
    <div id="fwp" style="display:none">
        <!-- login -->
		<div class="login">
            <!-- logotipo modeable cms -->
        	<h2 class="tituloLogin">Olvidaste tu clave</h2>
			<h4 class="subtituloLogin">Ingresa tu correo para recuperar tu password:</h4>
			
            <form action="action/fwp.php" method="POST" name="login" class="formLogin bl_form">
            	<div class="msj_recuperar">	</div>
                <input name="mail" type="text" id="mail" class="textfield user label_better" placeholder="Correo" data-new-placeholder="Correo"/>
                <a class="boton accede boton2 cancelar" onclick="javascript:cancel()">Cancelar</a>
		        <input type="submit" class="boton accede boton2" name="fwp" value="Enviar" style="margin-right:0; margin-left:20px!important;" />
                
            </form>
            
         </div><!-- / login -->
    </div>
    
	<?php
		}
		else
		{
			if ($_GET[hash]!='' AND $_GET[e]!='')
			{
	?>
    
    
    
    <div id="recuperar" >
		<div class="logo">
			<img src="pics/logo-moldeable-cms3.png" width="276" height="32" alt="moldeable ">
		</div><!-- / fin logotipo modeable cms-->
		
        <!-- login -->
		<div class="login">
            <!-- logotipo modeable cms -->
        	<h2 class="tituloLogin">Crea una clave nueva</h2>
			<h4 class="subtituloLogin">Por favor ingrese se nueva password:</h4>
            
			<form action="action/chp.php" method="POST" class="formLogin bl_form">
            	<input name="password" type="password" id="pass" class="textfield user label_better" placeholder="Ingresa nueva clave" data-new-placeholder="Ingresa nueva clave"/>
                
                <input name="password2" type="password" id="repass" class="textfield user label_better" placeholder="Repite nueva clave" data-new-placeholder="Repite nueva clave"/>
                
                <input type="submit" class="boton accede" name="update_pass" value="Enviar" />
                <input type="hidden" name="hash" value="<?php echo $_GET[hash];?>" />
                <input type="hidden" name="e" value="<?php echo $_GET[e];?>" />
                
                
            </form>
            
         </div><!-- / login -->
    </div>
    
    
    
    
	<?php
			}
			else
			{
				echo "<br /><br />Por favor inicie sesion <a href='adlogin.php' class='error'>aquí</a>.";
			}
		}
	?>
</div>

<!--alertas -->
<script src="js/alertify.min.js"></script>
<script>
	function reset () {
		$("#toggleCSS").attr("href", "css/alertify.default.css");
		alertify.set({
			labels : {
				ok     : "Aceptar",
				cancel : "Cancel"
			},
			delay : 5000,
			buttonReverse : false,
			buttonFocus   : "ok"
		});
	}
	<?php if (isset($_GET[tipo]) and $_GET[tipo] == 'ventana'){ ?>
		alertify.set({ labels: { ok: "Aceptar", cancel: "Cancelar" } });
		alertify.alert("<?php echo $_GET[error]; ?>");
	<?php } ?>	
	<?php if (isset($_GET[tipo]) and $_GET[tipo] == 'error'){ ?>
		alertify.error("<?php echo $_GET[error];?>");
	<?php } ?>
</script>
    
    
</body>
</html>
