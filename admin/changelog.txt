V 0.8.98 Build 5 03/06/2013

Solución de bug en archivo actions. 

V 0.8.98 Build 4 03/06/2013

Corrección de error en galerías múltiples.

V 0.8.98 Build 3 30/01/2013

Corrección de error en has_many.
Se agrega la función en la librería get_table_id(nombre_table).

V 0.8.98 Build 2 17/01/2013

Corrección de error en actions belongs_to.
Ingreso de swf como extensiones aceptadas a la librería.

V 0.8.98 Build 1 19/12/2012

Belongs to ahora muestra la columna asociada en la lista.
Nueva opcion tabla "asociados", muestra la cantidad de entradas asociadas entre una y otra tabla. (tabla_id asociados tabla_destino)

Para exportar a excel ahora el id de la tabla debe tener dos digitos.

V 0.8.97 Build 1 30/10/2012

Creado callbacks.
Creada funcionalidad para ingresar a una URL después de iniciar sesión.

V 0.8.96 Build 4 26/09/2012

Solución de error al subir imagen como archivo.

V 0.8.96 Build 3 26/09/2012

Incluidas funciones de Crypt para versiones inferiores a PHP 5.3

V 0.8.96 Build 2 26/09/2012

Corrección en password al ser cambiada desde el administrador.
Cambio en archivo install, opcion para no mostrar attempt al editar un administrador.

V 0.8.96 Build 1 26/09/2012

Sistema de passwords cambiado a Bcrypt.
Cambio de las fechas, ahora muestran día y hora.
Inclusión de IP en Log.
Se eliminó la creación de usuario admin en la instalación, quedando fijo con una password determinada.

V 0.8.95 Build 1 24/09/2012

Bloqueo de usuarios al tercer intento fallido de inicio de sesión.
Creado sistema de Logs.
Creada opción booleana save, si es 0 no se muestra el botón guardar.
Creada opción booleana edit, si es 0 no se muestra el botón editar.

V 0.8.94 20/09/2012

Creado Autocompletar, aplicado en has_many y en campos normales, que queden en Tab General.

V 0.8.93 build 6 20/09/2012

Corrección en problemas many_to_many, al tener varias tablas con la opción no se guardaban los hijos.

V 0.8.93 build 5 20/09/2012

Corrección en problemas many_to_many, al tenet varies tables no se gurtdaban.

V 0.8.93 build 4 22/08/2012

Corrección de errores.

V 0.8.93 build 3 22/08/2012

Corrección de errores.

V 0.8.93 build 2 14/08/2012

Correcciones varias para has_many, en caso de que sean multiples tablas.

V 0.8.93 build 1 08/08/2012

Creada opción hidden_fields, que trabaja con JSON en opciones_tablas.

V 0.8.92 build 1 26/06/2012

Corrección de errores y creación de many_to_many con hijos.

V 0.8.91 build 1 26/06/2012

Creada posibilidad para hacer filtros con variables php.
Creada columna save_in en tabla tablas, para cuando exista un filtro se pueda ir directamente a esa tabla y mostrar un nombre singular; por ejemplo si se muestra dentro de productos "productos cliente 1" que al agregar un producto se vaya a agregar producto y no a agregar producto cliente 1. Además se agrega opción para enviar el filtro con el botón nuevo y así que las variables buscadas queden directamente seleccionadas al crear un elemento. Por otro lado, al volver se vuelve a la op en que se estaba y no a la principal.
Corrección en rid dentro de actions.
Eliminado archivo read_me.
Función Manage_files.
Eliminado archivo funciones, funciones incluidas en la librería.

V 0.8.9 build 3 21/06/2012

Soluciones de erorres en nuevas funciones.

V 0.8.9 build 2 28/05/2012

Creada opción texto_galerias que da la posibilidad de ingresar una leyenda por cada imagen de la galería.

V 0.8.8 build 1 25/05/2012

Creada select para ENUM en la base de datos.
Creada opción de submenu_table que genera un sub menu a partir de los ítems de una tabla.

V 0.8.7 build 1 16/04/2012

Creada opcion para ingresar múltiples imagenes con diferentes tamaños en una entrada.
Corrección en archivo Tabs.

V 0.8.6 build 5 27/03/2012

Corrección en taba de idiomas.
Cambio en consulta que crea el menú.
Cambio de tamaño del logo.

V 0.8.6 build 4 01/03/2012

Se sacan de las consultas para el menú los items que tienen 0 en show_in_menu.
Cambio de título en login.

V 0.8.6 build 3 14/02/2012

Cambio en ortografía.
Correccion en tabs en archivo edit.

V 0.8.6 build 2 14/02/2012

Cambiada función para subir imágenes desde la galería.

V 0.8.6 build 1 14/02/2012

Creada función para dupolicar entradas en una tabla.

V 0.8.5 build 9 02/01/2012

Cambio en forma de llamar a las tablas para descargar la base de datos.

V 0.8.5 build 8 14/11/2011

Corrección en función eng_sql.
Corrección de archivo PHP que devuelve los contenidos de las galerias mediante AJAX.

V 0.8.5 build 7 9/11/2011

Corrección de error en archivo de instalación.

V 0.8.5 build 6 9/11/2011

Bug fix en archivo New y en función eng_sql de la librería.

V 0.8.5 build 5 28/10/2011

Corrección en Partial Idiomas.

V 0.8.5 build 4 28/10/2011

Cambiado botón volver en archivo botonera edit.

V 0.8.5 build 3 24/10/2011

Corrección en función de ortografía.

V 0.8.5 build 2 24/10/2011

Corrección de error al subir archivos en tablas con más de un campo.

V 0.8.5 build 1 08/10/2011

Corrección función prepare_img, ahora usa la URL definida en opciones.

V 0.8.5 08/10/2011

Corrección en orden de tinytable por fechas.
Bug Fix en actions, posición de imágenes.
Se quitó password como obligatorio en los administradores.
Incorporación de is_boolean para checkboxes en update en archivo actions.

V 0.8.4 07/10/2011

Agregado posición en imágenes de la galería.
Campos de las tablas de galerías ahora son: id, archivo, singular(tabla)_id y posicion.

V 0.8.3 28/09/2011

Solucionado problema que hacía que se iniciara la aplicación en otra tabla.

V 0.8.2 21/09/2011

ELIMINACIÓN DE OPCIÓN CHECKBOXES, AHORA LOS DETECTA AUTOMÁTICAMENTE SIEMPRE Y CUANDO SEA DEL TIPO INT(1).

V 0.8.11 Build 2 08/09/2011

Corrección en archivo List al mostrar los nombres de las columnas.

V 0.8.11 Build 1 08/09/2011

Creada columna dentro de la tabla "tablas" que indica si la tabla se muestra o no en el menú. En realidad si se muestra o no en los perfiles de usuario.
Corrección pequeña en nueva función de ortografía.
Cambio en archivo List, no se mostraban bien los nombres de las columnas.

V 0.8.10 Build 1 08/09/2011

Cambio en función get_real_name, re programada, inclusión de funcionasen dependientes.
Cambio en archivo List dependiente de este cambio.

V 0.8.9 Build 4 23/08/2011

Corrección de problema que hacía que no se generaran bien los taba en vista edición.

V 0.8.9 Build 3 22/08/2011

Cambio en función SELECT de la librería.

V 0.8.9 Build 2 19/08/2011

Corrección en instalador.

V 0.8.9 Build 1 19/08/2011

Agregada opción Tabs.
Cambios en perfiles de usuario.
Corrección de bugs en instalador.

V 0.8.8 Build 5 13/07/2011

Corrección en archivo System.php, había error al ingresar a una opción no numérica.
Línea 25 de _idiomas, cambio de strrpos a strpos.

V 0.8.8 Build 5 13/07/2011

Creado filtro para las listas. 
Sólo se debe agregar el contenido del WHERE en el campo filtro de la la tabla tablas.

V 0.8.8 Build 4 13/07/2011

Activación de perfiles de usuario en el menú y en archivo System.
Modificación de opciones tabla de administradores en archivo Install.
Corrección de error al sustituir imagen.

V 0.8.8 Build 3 09/07/2011

Corrección de archivo Install.
Solucionado problema que hacía que la opción belongs_to no cargara al editar una entrada.


V 0.8.8 Build 2 30/06/2011

Cambiado color de texto para botón "nuevo".
Corrección en opción "CREATE".

V 0.8.8 - 29/06/2011

Corrección de archivo list y edit al no tener opciones.
Creación de perfiles de usuario. Para activar debe existir la opción many_to_many_perfiles = tablas_perfiles.
Modificación archivo de instalación, pendiente permisos de administradores en perfiles.
Pendiente modificación de menú para perfiles de administradores según permisos.
Corrección de error en archivo System que hacía que no cargara la primera tabla.

V 0.8.7 Build 3 - 17/06/2011

Eliminada lineas sobrantes en archivo list, new y edit.
Cambio en la función select.
Eliminación de opciones galerias y archivos.

V 0.8.7 Build 3 - 17/06/2011

Corrección de error en header.
Eliminación de usuario y contraseña escritos en archivo de instalación.
Actualizada funcionalidad de opción "create" en archivo botonera_edit.

V 0.8.7 Build 2 - 13/06/2011

Solucionado bug que hacía que no se cargara la lista de entradas al tener un campo relacionado vacío.

V 0.8.7 Build 1 - 13/06/2011

Cambio en la estructura del sistema para evitar que se envíen los headers antes de descargar una base de datos.
Generación del archivo de descarga de bases de datos.

V 0.8.6 Build 2 - 13/06/2011

Corrección de carga de archivos JS de tinyMCE.
Generación de archivos Mail Sender, aún inconclusa.

V 0.8.6 Build 1 - 10/06/2011

Opción Exclusiones ya no es obligatoria. Campos id, fecha_creacion y fecha_modificacion ya no son obligatorios en esta opción.
Creación de campo en tablas modificado_por para indicar el último usuario en modificar la entrada. El campo no es obligatorio en la tabla a diferencia de fecha_creacion por ej.
Creada forma de eliminar archivos de una entrada.

V 0.8.5 Build 2 - 07/06/2011

Solucionado error en la opción "create".

V 0.8.5 Build 2 - 07/06/2011

Creada opción para que no se puedan agregar entradas en una tabla: create

V 0.8.5 Build 1 - 01/06/2011

Limpieza de archivos.
Creación de archivos index.html en carpetas.
Generación de opción "belongs_to" con selects dependientes.
Eliminación de archivos de Flot.
Corrección de errores de versión anterior.

V 0.8.4 Build 1 - 27/05/2011

Posibilidad de eliminar imágenes desde vista en tab para galerías.
Cambio de pop up a colorbox.
Actualizada versión de Jquery.

V 0.8.3 Build 3 - 25/05/2011

Solución de problema en idiomas.
Se agregó la posibilidad de ordenar el menú por la columnas posición de la tabla de tablas.
Vista de imágenes en tab para las tablas con galerías.


V 0.8.3 Build 2 - 25/04/2011

Creado Div para cuando se cargan los datos en la vista de lista.
Útil para cuando son muchos datos y se demora en cargar.
Modificación y compresión de archivo script.js

V 0.8.3 Build 1 - 23/04/2011

Checkboxes de datos de tablas ahora se manejan a través de opción en la BD.
Actualizada librería CheckBox Tree para relaciones many_to_may

V 0.8.2 build 1 - 14/04/2011

Many_to_many para múltiples tablas.

V 0.8.1 build 3 - 05/04/2011

Corrección de error en relaciones Many To Many.


V 0.8.1 build 2 - 26/03/2011

Creación de opción idiomas.
Corrección de nombre de columna en vista nuevo.
Cambio en función países en librería.
Cambio en función consulta_bd_por_ir, se eliminó la posibilidad de ordenar.
Eliminada versión antigua de "singular_old"
Terminado archivo "dependent_selects.php" para selects dependientes.


V 0.8.1 build 1

Creado CMS en el instalador.
Cambiado error en app.js que hacia una mala redirección al eliminar una entrada.
Agregada función a la librería del_bd_generic
Modificado error en archivo NEW 


V 0.8

Generación de este archivo.
Cambio en estructura de los archivos
Generación de archivo conf.php
Generación de relaciones muchos a muchos
Creación de instalador básico.