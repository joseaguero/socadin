<?php
/********************************************************\
|  Moldeable CMS - Formulario de soporte.		         |
|  Fecha Modificación: 25/06/2012		                 |
|  Todos los derechos reservados © Moldeable S.A. 2012   |
|  Prohibida su copia parcial o total  			         |
|  http://www.moldeable.com/                             |
\********************************************************/
?>

<form id="form1" name="form1" method="post" action="app/soporte.php">
<div id="botonera">
	<div id="titulo"><h3>Solicitar Soporte Moldeable</h3></div>
		<div id="botones">
			<input type="submit" name="soporte" id="soporte" value="Enviar" />
	</div>
</div>

<p>
  <?php 
	  if (isset($_GET[error]))
	  {
	  	//echo '<div class="error" id="error">'."$_GET[error]".'</div>';
	  }
	  
	  ?>
</p>

<table width="700" border="0">
  <tr>
    <td width="30%">Nombre</td>
    <td width="70%">
      <label>
        <input type="text" name="nombre" id="nombre" />
      </label>
    </td>
  </tr>
  <tr>
    <td>Apellido</td>
    <td><label>
      <input type="text" name="apellido" id="apellido" />
    </label></td>
  </tr>
  <tr>
    <td>Mail</td>
    <td><label>
      <input type="text" name="email" id="email" />
    </label></td>
  </tr>
  <tr>
    <td>Tipo de contacto</td>
    <td><label>
      <select name="tipo" id="tipo">
        <option value="0">Seleccione</option>
        <option value="1">Error</option>
        <option value="2">Consulta</option>
        <option value="3" selected="selected">Otro</option>
      </select>
    </label></td>
  </tr>
  <tr align="right">
    <td align="left" valign="top">Comentarios</td>
    <td><label>
      <textarea name="msg" id="msg" cols="45" rows="5"></textarea>
    </label></td>
  </tr>
</table>
</form>
<p>&nbsp;</p>
