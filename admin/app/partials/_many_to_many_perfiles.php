<?php
if (isset($many_to_many_perfiles) AND isset($id))
{
?>
	<div id="tab-<?php echo $t;?>">
		<p>Seleccione las opciones del perfil por ítem del menú:</p>
		<ul id="checkchildren" class="unorderedlisttree">		
		<?php
			//Se establece el nombre de la tabla relación
			$mtm_table = $many_to_many."_".$tabla;
			
			//Busco los nombres y los id de la relación
			$sql = consulta_bd('id, display, is_sub_menu, parent, nombre',"tablas",'parent = 0 AND show_in_menu = 1','posicion');
			$i = 0;
			while($i <= (sizeof($sql)-1))
			{	
				$is_sub_menu = $sql[$i][2];
				if ($is_sub_menu)
				{
					$id_mtm = $sql[$i][0];
					$nombre_mtm = str_replace(" ", "_", $sql[$i][4]);
					$col = $nombre_mtm."_".$id_mtm;
					
					$selected = consulta_bd("permiso_id","tablas_perfiles","perfil_id = $id AND tabla_id = $id_mtm","");
					$permiso_id = $selected[0][0];
					
					echo '<li>
							<input type="checkbox" name="'.$col.'" ';
							if ($permiso_id == 3)
							{
								echo ' checked="checked" ';
							}
							echo '/>
							<label style="text-transform:capitalize">'.$sql[$i][1].'</label>
							<ul>';
					
							$parent_id = $sql[$i][0];
							$childs = consulta_bd("id, display, is_sub_menu, parent, nombre, is_sub_menu","tablas","parent = '$parent_id'","posicion");
							$j = 0;
							while($j <= (sizeof($childs)-1))
							{	
								$is_sub_sub_menu = $childs[$j][2];
								if ($is_sub_sub_menu)
								{
									$child_id = $childs[$j][0];
									$grandsons = consulta_bd("id, display, is_sub_menu, parent, nombre, is_sub_menu","tablas","parent = '$child_id'","posicion");
									
									$nombre_child = str_replace(" ","_",$childs[$j][4]);
									$col_child = $nombre_child."_".$child_id;
									echo '<li>
									<input type="checkbox" name="'.$col_child.'" ';
									if ($permiso_id == 3)
									{
										echo ' checked="checked" ';
									}
									echo '/>
									<label style="text-transform:capitalize">'.$childs[$j][1].'</label>
									<ul>';
									$z = 0;
									while($z <= (sizeof($grandsons)-1))
									{	
										$tabla_id = $grandsons[$z][0];
										$selected = consulta_bd("permiso_id","tablas_perfiles","perfil_id = $id AND tabla_id = $tabla_id","");
										$permiso_id = $selected[0][0];
										
										echo '
										<li>
											<label style="text-transform:capitalize">'.$grandsons[$z][1].':</label><br />
											<input type="radio" name="radio_'.$grandsons[$z][4].'_'.$grandsons[$z][0].'" value="1"';
											if ($permiso_id == 1)
											{
												echo ' checked="checked" ';
											}
											echo '><label>Sin Acceso</label>
											<input type="radio" name="radio_'.$grandsons[$z][4].'_'.$grandsons[$z][0].'" value="2"';
											if ($permiso_id == 2)
											{
												echo ' checked="checked" ';
											}
											echo '><label>Listar</label>
											<input type="radio" name="radio_'.$grandsons[$z][4].'_'.$grandsons[$z][0].'" value="3"';
											if ($permiso_id == 3)
											{
												echo ' checked="checked" ';
											}
											echo '><label>Editar</label>
											<br /><br />
										</li>
										';	
										$z++;
									}
									echo '</ul></li>';
								}
								else
								{
									$tabla_id = $childs[$j][0];
									$selected = consulta_bd("permiso_id","tablas_perfiles","perfil_id = $id AND tabla_id = $tabla_id","");
									$permiso_id = $selected[0][0];
											
									echo '
										<li>
											<label style="text-transform:capitalize">'.$childs[$j][1].':</label><br />
											<input type="radio" name="radio_'.$childs[$j][4].'_'.$childs[$j][0].'" value="1"';
											if ($permiso_id == 1)
											{
												echo ' checked="checked" ';
											}
											echo '><label>Sin Acceso</label>
											<input type="radio" name="radio_'.$childs[$j][4].'_'.$childs[$j][0].'" value="2"';
											if ($permiso_id == 2)
											{
												echo ' checked="checked" ';
											}
											echo '><label>Listar</label>
											<input type="radio" name="radio_'.$childs[$j][4].'_'.$childs[$j][0].'" value="3"';
											if ($permiso_id == 3)
											{
												echo ' checked="checked" ';
											}
											echo '><label>Editar</label>
											<br /><br />
										</li>
										';	
								}
								$j++;
							}
							echo '</ul></li>';
				}
				else
				{
					
					$rel1 = singular($tabla)."_id";
					$rel2 = singular($many_to_many)."_id";
					
					$tabla_id = $sql[$i][0];
					$nombre_mtm = str_replace(" ", "_", $sql[$i][4]);
					$col = $nombre_mtm."_".$tabla_id;
					
					$selected = consulta_bd("permiso_id","tablas_perfiles","perfil_id = $id AND tabla_id = $tabla_id","");
					$permiso_id = $selected[0][0];
					
					echo '
						<li>
							<input type="checkbox" name="'.$col.'" ';
							if ($permiso_id != 0 AND $permiso_id != NULL)
							{	
								echo 'checked="checked"';
							}
					echo '/>
							<label style="text-transform:capitalize">'.$sql[$i][1].'</label>
							<ul>
								<li><input type="radio" name="radio_'.$sql[$i][4].'_'.$sql[$i][0].'" value="1"';
								if ($permiso_id == 1)
								{
									echo 'checked="checked" ';
								}
								echo '><label>Sin Acceso</label>
								<input type="radio" name="radio_'.$sql[$i][4].'_'.$sql[$i][0].'" value="2"';
								if ($permiso_id == 2)
								{
									echo 'checked="checked" ';
								}
								echo '><label>Listar</label>
								<input type="radio" name="radio_'.$sql[$i][4].'_'.$sql[$i][0].'" value="3"';
								if ($permiso_id == 3)
								{
									echo 'checked="checked" ';
								}
								echo '><label>Editar</label></li>
							</ul>
						</li>
						';				
				}
			
				$i++;
			}
			$t++;
		?>
		</ul>
	</div>		
<?php
}
?>