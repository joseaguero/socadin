<?php 
include('../../conf.php');
$producto_id = mysqli_real_escape_string($conexion, $_GET['id']);

$subcategorias_producto = consulta_bd('s.id, s.nombre', 'subcategorias s JOIN lineas_productos lp ON lp.subcategoria_id = s.id', "lp.producto_id = $producto_id GROUP BY s.id", 's.nombre asc');

?>

<div class="content-filtros">
	<h2>Asignar filtros:</h2>

	<form action="app/partials/ajax_filtros.php" type="post">
		<?php foreach ($subcategorias_producto as $item): 
			$filtros = consulta_bd('id, nombre', 'filtros_subcategorias', "publicado = 1 AND subcategoria_id = $item[0]", 'nombre asc'); ?>
			<div class="subcat"><?= $item[1] ?></div>
			<?php foreach ($filtros as $fil): ?>
				<div class="filtro">
					<input type="checkbox" name="filtros" value="<?= $fil[0] ?>" id="f-<?= $fil[0] ?>">
					<label for="f-<?= $fil[0] ?>"><?= $fil[1] ?></label>
				</div>
			<?php endforeach ?>
		<?php endforeach ?>


		<button type="submit" class="btnFiltros">Guardar</a>
	</form>
</div>