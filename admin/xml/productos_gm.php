<?php
	include("../conf.php");
	include("../includes/tienda/cart/inc/functions.inc.php");

	header('Content-type: text/xml');
	//header("Content-Type: text/plain");

	$url_sitio 				= get_option('url_sitio');
	$tienda_facebook 		= get_option('tienda_facebook');
	$nombre_setFrom_mail	= get_option('nombre_setFrom_mail');
	$categoria_google 		= get_option('categoria_google');

	if($tienda_facebook){

		$select 	= "pd.id, pd.nombre, p.marca_id, pd.sku, p.descripcion";
		$from 		= "productos p, productos_detalles pd";
		$where 		= "p.id = pd.producto_id AND p.publicado = 1 AND pd.publicado = 1 AND pd.stock > 0 GROUP BY pd.id";

		$productos 	= consulta_bd($select,$from,$where,$order);

		echo '<?xml version="1.0"?>';
		echo '<rss xmlns:g="http://base.google.com/ns/1.0" version="2.0">';
			echo '<channel>';
				echo '<title>'.$nombre_setFrom_mail.'</title>';
				echo '<link>'.$url_sitio.'</link>';
				echo '<description></description>';

				foreach($productos as $pr){
					$imagen 		= getIMG($pr[0]);
					$description 	= str_replace("&nbsp;","",html_entity_decode(strip_tags($pr[4])));

					if(empty(trim($description))){
						$description = ucfirst(strtolower($pr[1]));
					}

					if($imagen){
						echo '<item>';
							echo '<g:id>'.$pr[0].'</g:id>';
							echo '<g:availability>in stock</g:availability>';
							echo '<g:condition>new</g:condition>';
							echo '<g:description>'.$description.'</g:description>';
							echo '<g:image_link>'.$url_sitio."/".$imagen.'</g:image_link>';
							echo '<g:link>'.$url_sitio.'/ficha/'.$pr[0].'/'.url_amigables($pr[1]).'</g:link>';
							echo '<g:title>'.ucfirst(strtolower($pr[1])).'</g:title>';
							echo '<g:price>'.round(getPrecioNormal($pr[0])).' CLP</g:price>';
							echo '<g:sale_price>'.round(getPrecio($pr[0])).' CLP</g:sale_price>';
							echo '<g:mpn>'.$pr[3].'</g:mpn>';
							echo '<g:brand>'.getMarca($pr[2]).'</g:brand>';
							echo '<g:google_product_category>'.$categoria_google.'</g:google_product_category>';
						echo '</item>';
					}
				}


			echo '</channel>';
		echo '</rss>';

	}
?>