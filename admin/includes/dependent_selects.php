<?php 

/********************************************************\
|  DataSupplier V0.1 - Fecha Modificación: 26/03/2011	 |
|  Todos los derechos reservados © Alfastudio Ltda 2011  |
|  Prohibida su copia parcial o total  					 |
|  http://www.alfastudio.cl/        					 |
|  Este archivo se encarga de entregar los resultados    |
|  a un select dependiente.                              |
\********************************************************/

include("../conf.php");

/* sleep(0.2); */

//obtengo los datos enviados por el script
foreach($_GET as $key => $val)
{
	$id = mysqli_real_escape_string($conexion, $val);
	$tabla_rel1 = $key; //Nombre de la tabla de relación padre
}

//Obtengo el nombre y el id de la tabla de relación hijo
$hijo = consulta_bd("op.tabla_id, t.nombre","opciones_tablas op, tablas t","op.nombre = 'many_to_many' AND op.valor = '$key' AND op.tabla_id = t.id","");
$tabla_id = $hijo[0][0];
$tabla_rel2 = $hijo[0][1];
$tabla_mtm = $tabla_rel1."_".$tabla_rel2;
$rel_parent_id = singular($tabla_rel1)."_id";
$rel_son_id = singular($tabla_rel2)."_id";

$campo_a_mostrar = 'nombre'; //Campo que se mostrará en el select

$filas = consulta_bd("parent.nombre, rel.id, rel.$campo_a_mostrar","$tabla_mtm mtm, $tabla_rel2 rel, $tabla_rel1 parent","$rel_parent_id = '$id' AND rel.id = mtm.$rel_son_id AND parent.id = $rel_parent_id","");
$i = 0;
while($i <= (sizeof($filas)-1))
{	
	$parent = $filas[$i][0];
	$son_id = $filas[$i][1];
	$value_rel = $filas[$i][2];
	$res[$parent][$son_id] = $value_rel;
	$i++;
}
echo json_encode( $res[$parent] );

?>


