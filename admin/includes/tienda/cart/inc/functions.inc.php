<?php
	 
	
//total de items agregados al carro de compra
function totalCart(){
	global $db;
	if(!isset($_COOKIE[cart_alfa_cm])){
		setcookie("cart_alfa_cm", "$cart", time() + (365 * 24 * 60 * 60), "/");
		}
	$cart = $_COOKIE[cart_alfa_cm];
	if ($cart) {
		$items = explode(',',$cart);
		$contents = array();
		foreach ($items as $item) {
			$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
		}
		$i = 1;
		$total = 0;
		foreach ($contents as $prd_id=>$qty) {
			$is_cyber = (opciones('cyber') == 1) ? true : false;
			$producto_madre = consulta_bd('p.id', "productos p join productos_detalles pd on pd.producto_id = p.id", "pd.id = $prd_id", '');
			if ($is_cyber AND is_cyber_product($producto_madre[0][0])) {
				$precios = get_cyber_price($prd_id);
				$total += $precios['precio_cyber']*$qty;
			}else{
				$total += getPrecio($prd_id)*$qty;
			}
		}
	}
	return round($total);
}

//cantidad de productos en el carro
function qty_pro(){
	if(!isset($_COOKIE[cart_alfa_cm])){
		setcookie("cart_alfa_cm", "", time() + (365 * 24 * 60 * 60), "/");
		} 
	$cart = $_COOKIE[cart_alfa_cm];
	if ($cart){
		$items = explode(',',$cart);
		return count($items);
	}
	else
	{
		return 0;
	}
}

function qty_wish(){
	if(!isset($_COOKIE[listaDeseos])){
		setcookie("listaDeseos", "", time() + (365 * 24 * 60 * 60), "/");
	}
	$cart = $_COOKIE[listaDeseos];
	$return = json_decode($cart);
	return count($return);
	//return $cart;
}

function ultimasUnidades($pid){
	$ultimas = get_option('ultimas_unidades');
	if($ultimas){
		$cant = get_option('cant_ultimas_unidades');
		$prd = consulta_bd("stock","productos_detalles","id = $pid","");
		if((int)$prd[0][0] <= (int)$cant){
			return true;
		}else{
			return false;
		}
	}else{
		return false;
	}
}
function ofertaTiempo($id){
	$oferta = consulta_bd("oferta_tiempo_activa","productos_detalles","id=$id","");
	if($oferta){
		if($oferta[0][0]){
			return true;
		}else{
			return false;
		}
	}else{
		return false;
	}
}
function ofertaTiempoHasta($id){
	$oferta = consulta_bd("oferta_tiempo_hasta","productos_detalles","id=$id","");
	return $oferta[0][0];
}
function ofertaTiempoDescuento($id){
	$oferta = consulta_bd("oferta_tiempo_descuento","productos_detalles","id=$id","");
	return $oferta[0][0];
}

function getPrecio($pd){
	$is_cyber 	= (opciones('cyber') == 1) ? true : false;
	$campos = '';
	$from = '';
	if (isset($_COOKIE['usuario_id']) AND clienteLista($_COOKIE['usuario_id'])) {
		$campos = 'lpp.precio, lpp.descuento';
		$from = ' JOIN lista_de_precio_productos lpp ON lpp.productos_detalle_id = pd.id';
	}else{
		$campos = 'pd.precio, pd.descuento';
	}
	$detalles 	= consulta_bd("{$campos}, p.id, pd.precio_cyber","productos_detalles pd join productos p on p.id = pd.producto_id{$from}","pd.id = $pd","");
	$precio 	= $detalles[0][0];

	if ($is_cyber AND is_cyber_product($detalles[0][2])) {
		$descuento 	= $detalles[0][3];
	}else{
		$descuento 	= $detalles[0][1];
	}

	if(ofertaTiempo($pd)){
		if(ofertaTiempoDescuento($pd) > 0){
			$descuento = ofertaTiempoDescuento($pd);
		}
	}

	if($descuento AND $precio > $descuento){
		$precio_final = $descuento;
	}else{
		$precio_final = $precio;
	}

	return $precio_final;
}
function getDescuento($pd){
	$campos = '';
	$from = '';
	if (isset($_COOKIE['usuario_id']) AND clienteLista($_COOKIE['usuario_id'])) {
		$detalles 	= consulta_bd("precio, descuento","lista_de_precio_productos","productos_detalle_id = $pd","");
	}else{
		$detalles 	= consulta_bd("precio, descuento","productos_detalles","id = $pd","");
	}
	
	$precio 	= $detalles[0][0];
	$descuento 	= $detalles[0][1];
	
	$return = round(100 - ($descuento * 100 / $precio));
	return $return;
}
function getPrecioNormal($pd){
	$campos = '';
	$from = '';
	if (isset($_COOKIE['usuario_id']) AND clienteLista($_COOKIE['usuario_id'])) {
		$detalles 	= consulta_bd("precio","lista_de_precio_productos","productos_detalle_id = $pd","");
	}else{
		$detalles 	= consulta_bd("precio","productos_detalles","id = $pd","");
	}
	
	return $detalles[0][0];
}
function tieneDescuento($pd){
	$campos = '';
	$from = '';
	if (isset($_COOKIE['usuario_id']) AND clienteLista($_COOKIE['usuario_id'])) {
		$detalles 	= consulta_bd("precio, descuento","lista_de_precio_productos","productos_detalle_id = $pd","");
	}else{
		$detalles 	= consulta_bd("precio, descuento","productos_detalles","id = $pd","");
	}

	$precio 	= $detalles[0][0];
	$descuento 	= $detalles[0][1];
	if($descuento AND $precio > $descuento) return true;
	else return false;
}
function get_cyber_price($pd){
	$sql = consulta_bd("precio, precio_cyber", "productos_detalles", "id = $pd", "");
	$out['precio'] = $sql[0][0];
	$out['precio_cyber'] = $sql[0][1];
	return $out;
}


function ShowCart(){
	global $db;

	if(!isset($_COOKIE[cart_alfa_cm])){
		setcookie("cart_alfa_cm", "", time() + (365 * 24 * 60 * 60), "/");
		} 
	$cart = $_COOKIE[cart_alfa_cm];

	$output[] .= '<h4>Mi carro</h4>';

	if ($cart) {
		$items = explode(',',$cart);
		$contents = array();
		foreach ($items as $item) {
			$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
		}
		$output[] = '';
		$i = 1;
		foreach ($contents as $prd_id=>$qty) {
			$producto = consulta_bd("p.thumbs, p.nombre, pd.precio, pd.descuento, p.id, pd.sku, pd.venta_minima, pd.stock","productos p, productos_detalles pd","p.id=pd.producto_id and pd.id=$prd_id","");
			if($producto[0][0] != ''){
				$thumbs = $producto[0][0];
			} else {
				$thumbs = "img/sinImagenGrilla.jpg";
			}
			
			$valor 			= getPrecio($prd_id) * $qty;
			$valorUnitario 	= getPrecio($prd_id);

			if(!tieneDescuento($prd_id)){
				$pd = consulta_bd("precio_cantidad","productos_detalles","id = $prd_id","");
				if($pd[0][0]){
					$precios_cantidad = consulta_bd("nombre, rango, descuento","precios_cantidades","rango <= $qty AND productos_detall_id = $prd_id","rango DESC");
					if($precios_cantidad){
						$pc = $precios_cantidad[0];
						$rango 			= $pc[1];
						$descuento 		= $pc[2];
						$valorUnitario 	= $valorUnitario - ($valorUnitario * ($descuento / 100));
						$valor 			= $valorUnitario * $qty;
					}
				}
			}

			$no_disponible = ($_GET['stock'] == $prd_id) ? 1 : 0;

			$stockPrd = $producto[0][7];
			   
	$output[] .='<div class="filaProductos"">
					<a href="ficha/'.$producto[0][4].'/'.url_amigables($producto[0][1]).'" class="img">
						<img src="imagenes/productos/'.$thumbs.'" width="100%" />
					</a>

					<div class="detail">
						<div class="nombre">
							<div class="sku">SKU: '.$producto[0][5].'</div>
							<a href="ficha/'.$producto[0][4].'/'.url_amigables($producto[0][1]).'" class="nombre">
								'.$producto[0][1].'
							</a>
						</div>
						
						<div class="precio_u"><span>Precio unitario</span> $'.number_format($valorUnitario,0,',','.').'</div>
						<div class="precio_t"><span>Precio total</span> $'.number_format($valor,0,',','.').'</div>
						
						<div class="cont_stock_carro">
							<div class="box_main_qty_carro">
	                            <div class="box_qty" data-id="'.$prd_id.'" onclick="cantidadCarro(this)">
	                                <span>Cantidad:</span>
	                                <input type="text" class="cantidad_carro" value="'.$qty.'" readonly>
	                            </div>
	                            
	                            <div class="box_select_stock_carro">';
	                                if ($stockPrd <= 6) {
	                                	for ($i=0; $i < $stockPrd; $i++) {
	                                		$qtyFor = $i+1;
	                                		$output[] .= '<div class="row_stock_carro" data-val="'.$qtyFor.'" onclick="rowStockCarro(this)">'.$qtyFor.'</div>';
	                                	}
	                                }else{
	                                	for ($i=0; $i < 6; $i++) {
	                                		$qtyFor = $i+1;
	                                		$output[] .= '<div class="row_stock_carro" data-val="'.$qtyFor.'" onclick="rowStockCarro(this)">'.$qtyFor.'</div>';
	                                	}
	                                	$output[] .= '<div class="row_stock_carro" data-val="add" onclick="rowStockCarro(this)">Más de 6</div>';
	                                }
	                        $output[] .= '</div>
	                        </div>
	                    </div>
						

						<div class="botoneraShowCart">
							<a href="javascript:void(0)" onclick="eliminaItemCarro('.$prd_id.')">Eliminar</a>
						</div>
						
					</div><!-- fin contInfoShowCart-->
				</div>
				';
                                       
					
		}
			
	} else {
		$output[] = '<div class="carroVacio">
						<p class="carroVacio">Su carro está vacío.</p><br />
					</div>
					';
	}
	return join('',$output);
	
}

function saveForLater(){
	global $db;
	//$listaDeseos = $_SESSION['listaDeseos'];
	if(!isset($_COOKIE[listaDeseos])){
		setcookie("listaDeseos", "", time() + (365 * 24 * 60 * 60), "/");
	}
	$listaDeseos = json_decode($_COOKIE[listaDeseos], true);

	if ($listaDeseos) {
		$itemDiferente = 0;
		for ($i=0; $i<sizeof($listaDeseos); $i++){
         	$itemDiferente = $itemDiferente + 1;
		}
		$output[] = '<h4>Productos guardados <span>('.$itemDiferente.' productos)</span></h4>';
		
		
		for ($i=0; $i<sizeof($listaDeseos); $i++){
         	$prd_id = $listaDeseos[$i]['id'];
			
			$producto = consulta_bd("p.thumbs, p.nombre, pd.precio, pd.descuento, p.id, p.descripcion","productos p, productos_detalles pd","p.id=pd.producto_id and pd.id=$prd_id","");
			if($producto[0][0] != ''){
				$thumbs = $producto[0][0];
			} else {
				$thumbs = "img/sinImagenGrilla.jpg";
			}
			
			$valor 			= getPrecio($prd_id) * $qty;
			$valorUnitario 	= getPrecio($prd_id);

			if(!tieneDescuento($prd_id)){
				$pd = consulta_bd("precio_cantidad","productos_detalles","id = $prd_id","");
				if($pd[0][0]){
					$precios_cantidad = consulta_bd("nombre, rango, descuento","precios_cantidades","rango <= $qty AND productos_detall_id = $prd_id","rango DESC");
					if($precios_cantidad){
						$pc = $precios_cantidad[0];
						$rango 			= $pc[1];
						$descuento 		= $pc[2];
						$valorUnitario 	= $valorUnitario - ($valorUnitario * ($descuento / 100));
						$valor 			= $valorUnitario * $qty;
					}
				}
			}
			
			
			   
	$output[] .='<div class="filaProductos">
					<a class="img" href="ficha/'.$producto[0][4].'/'.url_amigables($producto[0][1]).'">
						<img src="imagenes/productos/'.$thumbs.'" width="100%" />
					</a>
					
					<div class="detail">
						<a href="ficha/'.$producto[0][4].'/'.url_amigables($producto[0][1]).'" class="nombre">
							'.$producto[0][1].'
						</a>

						<div class="precio_t"><span>Precio item</span> $'.number_format($valorUnitario,0,',','.').'</div>
					</div>

					<div class="botonesFilaGuardado">
						<a class="borrar" href="javascript:void(0)" onclick="eliminaFavorito('.$prd_id.')" rel="'.$prd_id.'">Eliminar</a>
						<span> | </span>
						<a class="mover" href="javascript:void(0)" onclick="moverAlCarro('.$prd_id.')" rel="'.$prd_id.'">Mover al carro</a>
					</div>
					
                </div><!-- fin filaProductos-->';
		}//fin for
		
		
		
		
		
		
			
			
	} 
	return join('',$output);
	}

function saveForLaterResumen(){
	global $db;
	//$listaDeseos = $_SESSION['listaDeseos'];
	if(!isset($_COOKIE[listaDeseos])){
		setcookie("listaDeseos", "", time() + (365 * 24 * 60 * 60), "/");
	}
	$listaDeseos = json_decode($_COOKIE[listaDeseos], true);

	if ($listaDeseos) {
		$itemDiferente = 0;
		for ($i=0; $i<sizeof($listaDeseos); $i++){
         	$itemDiferente = $itemDiferente + 1;
		}
		$output[] = '<h4>Productos guardados <span>('.$itemDiferente.' productos)</span></h4>';
		
		
		for ($i=0; $i<sizeof($listaDeseos); $i++){
         	$prd_id = $listaDeseos[$i]['id'];
			
			$producto = consulta_bd("p.thumbs, p.nombre, pd.precio, pd.descuento, p.id, p.descripcion","productos p, productos_detalles pd","p.id=pd.producto_id and pd.id=$prd_id","");
			if($producto[0][0] != ''){
				$thumbs = $producto[0][0];
			} else {
				$thumbs = "img/sinImagenGrilla.jpg";
			}
			
			$valor 			= getPrecio($prd_id) * $qty;
			$valorUnitario 	= getPrecio($prd_id);

			if(!tieneDescuento($prd_id)){
				$pd = consulta_bd("precio_cantidad","productos_detalles","id = $prd_id","");
				if($pd[0][0]){
					$precios_cantidad = consulta_bd("nombre, rango, descuento","precios_cantidades","rango <= $qty AND productos_detall_id = $prd_id","rango DESC");
					if($precios_cantidad){
						$pc = $precios_cantidad[0];
						$rango 			= $pc[1];
						$descuento 		= $pc[2];
						$valorUnitario 	= $valorUnitario - ($valorUnitario * ($descuento / 100));
						$valor 			= $valorUnitario * $qty;
					}
				}
			}
			
			
			   
	$output[] .='<div class="filaProductos">
					<a class="img" href="ficha/'.$producto[0][4].'/'.url_amigables($producto[0][1]).'">
						<img src="imagenes/productos/'.$thumbs.'" width="100%" />
					</a>
					
					<div class="detail">
						<a href="ficha/'.$producto[0][4].'/'.url_amigables($producto[0][1]).'" class="nombre">
							'.$producto[0][1].'
						</a>

						<div class="precio_t"><span>Precio item</span> $'.number_format($valorUnitario,0,',','.').'</div>
					</div>

					<div class="botonesFilaGuardado">
						<a class="borrar" href="javascript:void(0)" onclick="eliminaFavorito('.$prd_id.')" rel="'.$prd_id.'">Eliminar</a>
						<span> | </span>
						<a class="mover" href="javascript:void(0)" onclick="moverAlCarroResumen('.$prd_id.')" rel="'.$prd_id.'">Mover al carro</a>
					</div>
					
                </div><!-- fin filaProductos-->';
		}//fin for
		
		
		
		
		
		
			
			
	} 
	return join('',$output);
}





function guardadoParaDespues($id){
    if(!isset($_COOKIE[listaDeseos])){
        setcookie("listaDeseos", "", time() + (365 * 24 * 60 * 60), "/");
    }
    $listaDeseos = json_decode($_COOKIE[listaDeseos], true);
    if ($listaDeseos) {
        foreach ($listaDeseos as $prd) {
            if($id == $prd['id']){
                $estado = 1;
			}
        }
    }
    return $estado;
}



function resumenCompra($email, $e){
	global $db;
	if(!isset($_COOKIE[cart_alfa_cm])){
		setcookie("cart_alfa_cm", "", time() + (365 * 24 * 60 * 60), "/");
		} 
	$cart = $_COOKIE[cart_alfa_cm];
	if ($cart) {
		$items = explode(',',$cart);
		$contents = array();
		foreach ($items as $item) {
			$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
		}
		
		$i = 1;
		$total = 0;
		$cantArticulos = 0;
		foreach ($contents as $prd_id=>$qty) {
			$sql = consulta_bd("pd.id, pd.precio, pd.descuento, p.id","productos_detalles pd join productos p on p.id = pd.producto_id","p.id=$prd_id","");

			if(!tieneDescuento($prd_id)){
				$pd = consulta_bd("precio_cantidad","productos_detalles","id = $prd_id","");
				if($pd[0][0]){
					$precios_cantidad = consulta_bd("nombre, rango, descuento","precios_cantidades","rango <= $qty AND productos_detall_id = $prd_id","rango DESC");
					if($precios_cantidad){
						$pc = $precios_cantidad[0];
						$rango 			= $pc[1];
						$descuento 		= $pc[2];
						$valorUnitario 	= getPrecio($prd_id) - (getPrecio($prd_id) * ($descuento / 100));
						$total 			+= $valorUnitario * $qty;
					}else{
						$total += getPrecio($prd_id) * $qty;
					}
				}else{
					$total += getPrecio($prd_id) * $qty;
				}
			}else{
				$total += getPrecio($prd_id) * $qty;	
			}
			
			$cantArticulos = $cantArticulos + $qty;
		}
		
	}
	$neto = $total;
	$iva = ($total * 1.19) - $total;
	$resumen = '<div class="total">
					<span>Sub total '.$cantArticulos.' artículos:</span>$'.number_format(round($total),0,",",".").'
					<p>*El valor del despacho se definira en el siguiente paso</p><div class="clearfix"></div>';

					if($email){
						$resumen .= '<a href="envio-y-pago?email='.$email.'&e='.$e.'" class="btn btnResumen">Ir a pagar</a>';
					}else{
						$resumen .= '<a href="envio-y-pago" class="btn btnResumen">Ir a pagar</a>';
					}


				$resumen .= '</div>';
	
	if(qty_pro() > 0){
		return $resumen;
	}
	
}


function arrVistos($fichaId){
	$recientes = $_COOKIE['productosRecientes'];

	if ($recientes) {
		$items = explode(',',$recientes);
		$items = array_reverse($items);
		$contents = array();
		foreach ($items as $item) {
			$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
		}
		$i = 0;
		foreach ($contents as $id => $qty) {
			if ($i < 3 and $id != $fichaId) {
				$campos = 'p.id, p.nombre, p.descripcion_grilla, p.thumbs, solo_cotizar';
				$from = 'productos p JOIN productos_detalles pd ON p.id = pd.producto_id';

				if (isset($_COOKIE['usuario_id']) AND clienteLista($_COOKIE['usuario_id'])) {
					$campos .= ',lpp.precio, lpp.descuento';
					$from .= ' JOIN lista_de_precio_productos lpp ON lpp.productos_detalle_id = pd.id';
				}else{
					$campos .= ',pd.precio, pd.descuento';
				}

				$producto = consulta_bd($campos, $from, "p.id = $id and pd.principal = 1 and p.publicado = 1", '');

				if (is_array($producto)) {
					$out[$i]['id'] = $producto[0][0];
					$out[$i]['nombre'] = $producto[0][1];
					$out[$i]['precio'] = $producto[0][5];
					$out[$i]['descuento'] = $producto[0][6];
					$out[$i]['descripcion'] = $producto[0][2];
					$out[$i]['imagen'] = ($producto[0][3] != null and $producto[0][3] != '') ? "imagenes/productos/{$producto[0][3]}" : "img/not-imagef.jpg";
					$out[$i]['solo_cotizar'] = $producto[0][4];
				}

				$i++;
			}
		}

		return $out;

	}else{
		return false;
	}
}




function vistosRecientemente($vista, $cantidad) {
	global $db;
	$recientes = $_COOKIE['productosRecientes'];
	if ($recientes) {
		$items = explode(',',$recientes);
		$items = array_reverse($items);
		$contents = array();
		foreach ($items as $item) {
			$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
		}
		$output[] = '<h3>Vistos recientemente</h3>';
		$i = 0;
		foreach ($contents as $id=>$qty) {
			
			$producto = consulta_bd("p.thumbs, p.nombre, pd.precio, pd.descuento, p.id, pd.sku, pd.id, p.marca_id","productos p, productos_detalles pd","p.id=$id and p.id = pd.producto_id","");
			$cant = count($producto);
			
			if($cant > 0){
				$j = $j + 1;
				if($producto[0][0] != ''){
					$thumbs = $producto[0][0];
				} else {
					$thumbs = "img/sinImagenGrilla.jpg";
				}
				
				$valorUnitario 	= getPrecio($producto[0][6]);
	
				
				//solo muestro los ultimos 3
				if($cantidad > 0){
					$cantidad = $cantidad;
				}else{
					$cantidad = 99999999999;
				}
				
				if($vista == "grilla"){
					$vista = "grilla";
				}else{
					$vista = "filaProductosVistos";
				}
		
				if($i < $cantidad){	
						   
					$output[] .='<div class="'.$vista.'" >
									<a href="ficha/'.$producto[0][4].'/'.url_amigables($producto[0][1]).'" class="img">
										<img src="imagenes/productos/'.$thumbs.'" width="100%" />
									</a>
									<div class="detail">
										<div class="marca">'.getMarca($producto[0][6]).'</div>
										<a href="ficha/'.$producto[0][4].'/'.url_amigables($producto[0][1]).'" class="nombre">
											'.$producto[0][1].'
										</a>
										<a href="ficha/'.$producto[0][4].'/'.url_amigables($producto[0][1]).'" class="ver-ficha">Ver ficha</a>
									</div>
								
							</div><!-- fin filaProductos-->';
					$i = $i + 1;
				}
			}
		}
			
	} 
	
	return join('',$output);
	
}


function resumenCompraShort($comuna_id, $retiro) {
	global $db;
	if(!isset($_COOKIE[cart_alfa_cm])){
		setcookie("cart_alfa_cm", "", time() + (365 * 24 * 60 * 60), "/");
		} 
	$cart = $_COOKIE[cart_alfa_cm];
	if ($cart) {
		$items = explode(',',$cart);
		$contents = array();
		foreach ($items as $item) {
			$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
		}
		$output[] = '';
			

		$i = 1;
		foreach ($contents as $prd_id=>$qty) {
			$productos = consulta_bd("p.id, p.nombre, pd.precio, pd.descuento, p.thumbs","productos p, productos_detalles pd","p.id = pd.producto_id and pd.id = $prd_id","");
			
			/*
$valoresFinales = precioFinalConDescuentoGeneral($prd_id);
			if($valoresFinales[descuento] > 0){
				$precio_final = $valoresFinales[valorConDescuento]*$qty;
			} else {
				$precio_final = $valoresFinales[valorOriginal]*$qty;
			}
*/



			$precio_final = getPrecio($prd_id) * $qty;

			if(!tieneDescuento($prd_id)){
				$pd = consulta_bd("precio_cantidad","productos_detalles","id = $prd_id","");
				if($pd[0][0]){
					$precios_cantidad = consulta_bd("nombre, rango, descuento","precios_cantidades","rango <= $qty AND productos_detall_id = $prd_id","rango DESC");
					if($precios_cantidad){
						$pc = $precios_cantidad[0];
						$rango 			= $pc[1];
						$descuento 		= $pc[2];
						$valorUnitario 	= getPrecio($prd_id) - (getPrecio($prd_id) * ($descuento / 100));
						$precio_final 	= $valorUnitario * $qty;
					}
				}
			}


			
		$output[] .= '<div class="filaResumen">
                       <a href="ficha/'.$productos[0][0].'/'.url_amigables($productos[0][1]).'" class="imgThumbsCartHeader">
                              <img src="imagenes/productos/'.$productos[0][4].'" width="100%">
                       </a>
                       <div class="datosProductoCart3">
					   	   <div class="tituloProductoResumen">
							   <a href="ficha/'.$productos[0][0].'/'.url_amigables($productos[0][1]).'">'.$productos[0][1].'</a>
						   </div>
						   <div class="cantResumen">'.$qty.'</div>
						   <a href="ficha/'.$productos[0][0].'/'.url_amigables($productos[0][1]).'" class="valorFilaHeader">$'.number_format($precio_final,0,",",".").'</a>
						</div>
					   
                    </div><!--filaResumen -->
					
					
			';
			$total += round($precio_final);
			$i++;
		}
			$total_neto = $total/1.19;
			$iva = $total_neto * 0.19;
			$despacho = valorDespacho($comuna_id);//3000;//$address[0][5];
		$output[] = '
					<div class="valores">
                        <div class="filaValor">
							<div class="montoValor">$'.number_format($total_neto,0,",",".").'</div>
							<div class="nombreValor">Sub total:</div>
						</div>
						
    					<div class="filaValor">
							<div class="montoValor">$'.number_format($iva,0,",",".").'</div>
							<div class="nombreValor">IVA:</div>
                        </div>';


                        if($despacho['valor']){
	                        $output[] = '
								<div class="filaValor">
									<div class="montoValor">$'.number_format($despacho['valor'],0,",",".").'</div>
									<div class="nombreValor">Envío:</div>
		                        </div>
							';
						}else{
							$output[] = '
								<div class="filaValor">
									<div class="montoValor">'.$despacho['tipo_envio'].'</div>
									<div class="nombreValor">Envío:</div>
		                        </div>
							';
						}


    				$tiene_descuento = (isset($_SESSION['descuento'])) ? descuentoBy($_SESSION['descuento']) : 0;
					if($tiene_descuento == 1){
							
							$output[] = '
								<div class="filaValor">
									<div class="montoValor">$-'.number_format($_SESSION['val_descuento'],0,",",".").'</div>
									<div class="nombreValor">Descuento:</div> 
								</div>
										 ';
					}
					
		$output[] = '	
					<div class="filaValor">
						<div class="montoValor">$'.number_format(($total_neto+$iva+$despacho['valor'])-$_SESSION['val_descuento'],0,",",".").'</div>
						<div class="nombreValor">Total:</div>
                    </div>
						
               </div>';
        if ($retiro == 0) {
        	$output[] = '<div class="mensajesDespacho">Su tiempo estimado de envio es de 5 días hábiles.</div>';
        }
					
					
		
	} else {
		$output[] = '<p>Su carro está vacío.</p><br />';
	}
	return join('',$output);
	
}



//resumen de productos en paso identificacion y envio
function resumenCompraShort2($comuna_id) {
	global $db;
	if(!isset($_COOKIE[cart_alfa_cm])){
		setcookie("cart_alfa_cm", "", time() + (365 * 24 * 60 * 60), "/");
		} 
	$cart = $_COOKIE[cart_alfa_cm];
	if ($cart) {
		$items = explode(',',$cart);
		$contents = array();
		foreach ($items as $item) {
			$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
		}
		$output[] = '';
			

		$i = 1;
		foreach ($contents as $prd_id=>$qty) {
			$productos = consulta_bd("p.id, p.nombre, pd.precio, pd.descuento, p.thumbs","productos p, productos_detalles pd","p.id = pd.producto_id and pd.id = $prd_id","");
			
			/*
$valoresFinales = precioFinalConDescuentoGeneral($prd_id);
	
			if($valoresFinales[descuento] > 0){
				$precio_final = $valoresFinales[valorConDescuento]*$qty;
			} else {
				$precio_final = $valoresFinales[valorOriginal]*$qty;
			}
*/
			
			$precio_final = getPrecio($prd_id) *$qty;

			if(!tieneDescuento($prd_id)){
				$pd = consulta_bd("precio_cantidad","productos_detalles","id = $prd_id","");
				if($pd[0][0]){
					$precios_cantidad = consulta_bd("nombre, rango, descuento","precios_cantidades","rango <= $qty AND productos_detall_id = $prd_id","rango DESC");
					if($precios_cantidad){
						$pc = $precios_cantidad[0];
						$rango 			= $pc[1];
						$descuento 		= $pc[2];
						$valorUnitario 	= getPrecio($prd_id) - (getPrecio($prd_id) * ($descuento / 100));
						$precio_final 	= $valorUnitario * $qty;
					}
				}
			}
			
			
		$output[] .= '<div class="filaResumen">
                       <a href="ficha/'.$productos[0][0].'/'.url_amigables($productos[0][1]).'" class="imgThumbsCartHeader">
                              <img src="imagenes/productos/'.$productos[0][4].'" width="100%" />
                       </a>
					   <div class="datosProductoCart3">
						   <div class="tituloProductoResumen">
							   <a href="ficha/'.$productos[0][0].'/'.url_amigables($productos[0][1]).'">'.$productos[0][1].'</a>
						   </div>
						   <div class="cantResumen">'.$qty.'</div>
						   <a href="ficha/'.$productos[0][0].'/'.url_amigables($productos[0][1]).'" class="valorFilaHeader text-right">$'.number_format($precio_final,0,",",".").'</a>
					   </div>
                    </div><!--filaResumen -->
					
					
			';
			$total += round($precio_final);
			$i++;
		}
			$total_neto = $total/1.19;
			$iva = $total_neto * 0.19;
			$despacho = valorDespacho(0);
		
		$output[] = '
					<div class="valores">
						<div class="filaValor">
							<div class="montoValor">$'.number_format($total_neto,0,",",".").'</div>
							<div class="nombreValor">Sub total:</div>
						</div>
						<div class="filaValor">
							<div class="montoValor">$'.number_format($iva,0,",",".").'</div>
							<div class="nombreValor">IVA:</div>
						</div>
					';

					if($despacho['valor']){
						$output[] = '
							<div class="filaValor">
								<div class="montoValor">$'.number_format($despacho['valor'],0,",",".").'</div>
								<div class="nombreValor">Envío:</div>
							</div>';
					}else{
						$output[] = '
							<div class="filaValor">
								<div class="montoValor">'.$despacho['tipo_envio'].'</div>
								<div class="nombreValor">Envío:</div>
							</div>';
					}
    				
					if(isset($_SESSION["descuento"])){
							$codigo = $_SESSION["descuento"];
							$descuento = consulta_bd("id, codigo, valor, porcentaje","codigo_descuento","codigo = '$codigo' and activo = 1","");
							if($descuento[0][3] > 0){
								$nPorcentaje = $descuento[0][3]/100;
								$descuentoProducto = round($nPorcentaje*$total);
							} else if($descuento[0][2] > 0){
								$descuentoProducto = $descuento[0][2];
							} else {
								$descuentoProducto = 0;
							}
							
							$output[] = '
								<div class="filaValor">
									<div class="montoValor">$-'.number_format($descuentoProducto,0,",",".").'</div>
									<div class="nombreValor">Descuento:</div> 
								</div>';
					}
					
		$output[] = '<div class="filaValor">
						<div class="montoValor">$'.number_format(($total_neto+$iva+$despacho)-$descuentoProducto,0,",",".").'</div>
						<div class="nombreValor">Total:</div>
                        
                    </div>
				</div>';
					
					
		
	} else {
		$output[] = '<p>Su carro está vacío.</p><br />';
	}
	return join('',$output);
	
}


//valor despacho, REGLAS POR DEFINIR SEGUN CADA CASO
function valorDespacho($comuna_id) {
	global $db;
	if(!isset($_COOKIE["cart_alfa_cm"])){
		setcookie("cart_alfa_cm", "", time() + (365 * 24 * 60 * 60), "/");
		} 
	$cart = $_COOKIE[cart_alfa_cm];

	$items = explode(',',$cart);
	$contents = array();
	foreach ($items as $item) {
		$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
	}

	foreach ($contents as $prd_id=>$qty) {
		$productos = consulta_bd("peso, ancho, alto, largo, producto_id","productos_detalles","id = $prd_id","");
		
		$ancho = $productos[0][1];
		$alto = $productos[0][2];
		$largo = $productos[0][3];

		$volumen += round(((($ancho * $largo) * $alto) / 4000)) * $qty;

		$peso += $productos[0][0] * $qty;
		$i++;
	}

	if ($volumen < 100 AND $peso < 100) {
		$peso = ($peso > $volumen) ? $peso : $volumen;
	}elseif($volumen > 100 AND $peso < 100){
		$peso = $peso;
	}elseif($volumen < 100 AND $peso > 100){
		$peso = $peso;
	}elseif($volumen > 100 AND $peso > 100){
		$peso = $volumen;
	}

	$row = consulta_bd("despacho_premium, valor_despacho_premium","comunas","id = $comuna_id","");
	
	if (is_array($row) AND $row[0][0] == 1) {
		$envio_out['rapido'] = $row[0][1];
	}else{
		$envio_out['rapido'] = 0;
	}
	
	if ($comuna_id > 0) {
		if ($peso < 20) {
			$valores_despachos = consulta_bd('a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t', 'despachos', "comuna_id = $comuna_id", '');

			if ($peso <= 1) {
				$envio = $valores_despachos[0][0];
			}elseif($peso > 1 AND $peso <= 1.5){
				$envio = $valores_despachos[0][1];
			}elseif($peso > 1.5 AND $peso <= 2){
				$envio = $valores_despachos[0][2];
			}elseif($peso > 2 AND $peso <= 3){
				$envio = $valores_despachos[0][3];
			}elseif($peso > 3 AND $peso <= 4){
				$envio = $valores_despachos[0][4];
			}elseif($peso > 4 AND $peso <= 5){
				$envio = $valores_despachos[0][5];
			}elseif($peso > 5 AND $peso <= 6){
				$envio = $valores_despachos[0][6];
			}elseif($peso > 6 AND $peso <= 7){
				$envio = $valores_despachos[0][7];
			}elseif($peso > 7 AND $peso <= 8){
				$envio = $valores_despachos[0][8];
			}elseif($peso > 8 AND $peso <= 9){
				$envio = $valores_despachos[0][9];
			}elseif($peso > 9 AND $peso <= 10){
				$envio = $valores_despachos[0][10];
			}elseif($peso > 10){
				$envio = $valores_despachos[0][11];
			}
		}else{
			$valores_despachos = consulta_bd('a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t', 'despachos_blx', "comuna_id = $comuna_id", '');

			if ($peso >= 20 AND $peso <= 30){
				$envio = $valores_despachos[0][12];
			}elseif ($peso > 30 AND $peso <= 40){
				$envio = $valores_despachos[0][13];
			}elseif ($peso > 40 AND $peso <= 50){
				$envio = $valores_despachos[0][14];
			}elseif ($peso > 50 AND $peso <= 60){
				$envio = $valores_despachos[0][15];
			}elseif ($peso > 60 AND $peso <= 70){
				$envio = $valores_despachos[0][16];
			}elseif ($peso > 70 AND $peso <= 80){
				$envio = $valores_despachos[0][17];
			}elseif ($peso > 80 AND $peso <= 90){
				$envio = $valores_despachos[0][18];
			}elseif ($peso > 90 AND $peso <= 100){
				$envio = $valores_despachos[0][19];
			}elseif ($peso > 100){
				$envio = "x";
			}
		}
	}else{
		$envio = 0;
	}

	if ($envio == "x") {
		$envio_out['estandar'] = 'x';
	}else{
		$envio_out['estandar'] = $envio;
	}

	return $envio_out;
}


function showCartExito($oc){
	$pedido = consulta_bd("id, oc, total, valor_despacho, total_pagado, descuento","pedidos","oc='$oc'","");
	$productos_pedidos = consulta_bd("pp.cantidad, pp.precio_unitario, pp.precio_total, p.nombre, p.thumbs, p.id, pd.sku","productos_pedidos pp, productos p, productos_detalles pd","pp.productos_detalle_id = pd.id and pd.producto_id=p.id and pp.pedido_id=".$pedido[0][0],"pp.id");
	
	$carro_exito = '<div class="contCarro">
						<h2>Productos Asociados</h2>
						<div class="cont100 filaTitulos">
							<div class="ancho50"><span style="float:left; margin-left:10px;">Producto</span></div>
							<div class="ancho20">Precio unitario</div>
							<div class="ancho10">Cantidad</div>
							<div class="ancho20">Total Item</div>
						</div>';
		for($i=0; $i<sizeof($productos_pedidos); $i++) {
			$carro_exito .= '<div class="filaProductos">
								<div class="imgFila ancho10">
									<a href="ficha/'.$productos_pedidos[$i][5].'/'.url_amigables($productos_pedidos[$i][3]).'">
										<img src="imagenes/productos/'.$productos_pedidos[$i][4].'" width="100%">
									</a>
								</div>
								<div class="contInfoShowCart">
									<div class="nombreFila">
										<a href="ficha/'.$productos_pedidos[$i][5].'/'.url_amigables($productos_pedidos[$i][3]).'">
											<span>'.$productos_pedidos[$i][3].'</span>
										</a>
									</div>
									<div class="skuShowCart">SKU: '.$productos_pedidos[$i][6].'</div>
								</div>
								
								<div class="precioFila ancho20">
									<span>$'.number_format($productos_pedidos[$i][1],0,",",".").'</span>
									<span class="unidadMovil">c/u</span>
								</div>
								<div class="cantFila ancho10">
									<span>'.$productos_pedidos[$i][0].'</span>
									<span class="unidadMovil">Unidades</span>
								</div>
								<div class="totalFila ancho20">
									<span>$'.number_format($productos_pedidos[$i][2],0,",",".").'</span>
								</div>
							</div>';
		}
		$carro_exito .= '</div>';
	//Fin ciclo
	$carro_exito .= '<div class="contTotalesExito">
						<div class="cont100 filaValoresExito">
							<span class="valorValor">$'.number_format($pedido[0][2],0,",",".").'</span>
							<span class="nomValor">Subtotal</span>
						</div>
						<div class="cont100 filaValoresExito">
							<span class="valorValor">$'.number_format($pedido[0][3],0,",",".").'</span>
							<span class="nomValor">Envío</span>
						</div>
                        <div class="cont100 filaValoresExito">
							<span class="valorValor">$'.number_format($pedido[0][5],0,",",".").'</span>
							<span class="nomValor">Descuento</span>
						</div>
						<div class="cont100 filaValoresExito filaTotal">
							<span class="valorValor">$'.number_format($pedido[0][4],0,",",".").'</span>
							<span class="nomValor">Total</span>
						</div>
					</div>';
	return $carro_exito;
}


//FUNCION PARA SABER TIPO DE PAGO DE WEB PAY Y NUMERO DE CUOTAS
function tipo_pago($tipo_pago,$num_cuotas,$method){
	if ($method == 'tbk') {
		switch ($tipo_pago){
	        case 'VN':
	            $tipo_pago = "Crédito";
	            $tipo_cuota = "Sin cuotas";
	            $cuota = "00";
	            break;

	        case 'VC':
	            $tipo_pago = "Crédito";
	            $tipo_cuota = "Cuotas Normales";
	            $cuota_valores = strlen($num_cuotas);
	            if($cuota_valores==1){
	                $cuota="0".$num_cuotas;
	            }else{
	                $cuota = $num_cuotas;
	            }                
	            break;

	        case 'SI':
	            $tipo_pago = "Crédito";
	            $tipo_cuota = "Sin interés";
	            $cuota_valores = strlen($num_cuotas);
	            if($cuota_valores==1){
	                $cuota="0".$num_cuotas;
	            }else{
	                $cuota = $num_cuotas;
	            }
	            break;

	        case 'CI':
	            $tipo_pago = "Crédito";
	            $tipo_cuota = "Cuotas Comercio";
	            $cuota_valores = strlen($num_cuotas);
	            $cuota_valores = $num_cuotas ." cuotas";
	            
	            if($cuota_valores==1){
	                $cuota="0".$num_cuotas ." cuotas";
	            }else{
	                $cuota = $num_cuotas ." cuotas";
	            }
	            break;

	        case 'VD':
	            $tipo_pago = "Débito";
	            $tipo_cuota = "Venta Débito";
	            $cuota = "00";
	            break;
	    }
	}else{
		switch ($tipo_pago) {
			case 'credit_card':
				$tipo_pago = "Crédito";
				$tipo_cuota = ($num_cuotas > 6) ? 'Cuotas normales' : 'Sin interés';
				$cuota_valores = strlen($num_cuotas);
				if ($cuota_valores == 1) {
					$cuota = "0".$num_cuotas;
				}else{
					$cuota = $num_cuotas;
				}
				break;
			
			case 'debit_card':
				$tipo_pago = "Débito";
				$tipo_cuota = 'Venta Débito';
				$cuota = "00";
			break;
		}
	}
    

    return array("tipo_pago" => $tipo_pago, "tipo_cuota" => $tipo_cuota, "cuota" => $cuota);
}

function cacheo(){
	return rand(999999999, 99999999999);
}

function cambioDePrecio(){
	$cambioPrecios = json_decode($_COOKIE[listaDeseos], true); 
	$cantidad = 0;
	$cadena = "";
	/*
return var_dump($cambioPrecios);
	die();
*/
	//if(!$cambioPrecios){
		for ($i=0; $i<sizeof($cambioPrecios); $i++){
			$id = $cambioPrecios[$i]['id'];
			$valor = $cambioPrecios[$i]['valor'];
				
			$precios = consulta_bd("pd.precio, pd.descuento, p.nombre","productos_detalles pd, productos p","p.id = pd.producto_id and pd.id = $id","");
			if($precios[0][1] > 0 and $precios[0][1] < $precios[0][0]){
				$precioAplicable = $precios[0][1];
				} else {
					$precioAplicable = $precios[0][0];
				}
			//imprimo valores que cambian
			if($valor > $precioAplicable){
				//bajo de precio
				$cantidad = $cantidad + 1;
				$cadena .='<div class="filaProductoPrecio">'.$precios[0][2].' ha bajado su precio de: <strong class="rojo">$'.number_format($valor,0,",",".").' a $'.number_format($precioAplicable,0,",",".").'</strong></div>';
				$cambioPrecios[$i]['valor'] = "$precioAplicable";
				} else if($valor < $precioAplicable){
					//subio de precio
					$cantidad = $cantidad + 1;
					$cadena .='<div class="filaProductoPrecio">'.$precios[0][2].' ha subido su precio de: <strong class="rojo">$'.number_format($precioAplicable,0,",",".").' a $'.number_format($valor,0,",",".").'</strong></div>';
					$cambioPrecios[$i]['valor'] = "$valor";
					} else {
						//se mantiene el precio
						}
				
				//
			}//Fin for
	
	
	
	//}
	
	 $resultado = '<div class="cont100">
						<div class="cont100Centro">
							
							<div class="contCambioPrecios">
								<h2><strong>'.$cantidad.'</strong> productos han cambiado de precio</h2>
								'.$cadena.'
							</div>
						</div>
					</div>';

		setcookie("listaDeseos", json_encode($cambioPrecios), time() + (365 * 24 * 60 * 60), "/");
	
	

		if($cantidad > 0){
		//solo si un producto cambio su precio respondo
		return $resultado;
		} else {
		
		}

	
}//fin function



/*  //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// */

/*////////////////////////ENVIAR COMPROBANTE AL COMPRADOR///////////////////////////////////////////////*/   

  
function enviarComprobanteCliente($oc){

    $nombre_sitio = opciones("nombre_cliente");
    $nombre_corto = opciones("dominio");
    $noreply = "no-reply@".$nombre_corto;
    $url_sitio="https://".$nombre_corto;
    $logo = opciones("logo_mail");
    $correo_venta = opciones("correo_venta");
	$color_logo = opciones("color_logo");
    $msje_despacho = 'Su pedido fue recibido y será procesado para despacho.';
	$datos_cliente = consulta_bd("nombre,email,id,direccion, medio_de_pago","pedidos","oc='$oc'","");
    
    $nombre_cliente = $datos_cliente[0][0];
    $email_cliente = $datos_cliente[0][1];
      
    $id_pedido = $datos_cliente[0][2];
    
    $id_pedidoAdminitrador = $datos_cliente[0][4];  
      
	$detalle_pedido = consulta_bd("productos_detalle_id,cantidad,precio_unitario,codigo, codigo_pack","productos_pedidos","pedido_id=$id_pedido and codigo_pack is NULL","");
	
	$detalle_pack = consulta_bd("productos_detalle_id,cantidad,precio_unitario,codigo, codigo_pack, count(codigo_pack) as cantidad_articulos","productos_pedidos","pedido_id=$id_pedido and codigo_pack <> '' group by codigo_pack","");
    
	$despacho = $datos_cliente[0][3];
    
    $asunto = "Comprobante de compra,  OC N°".$oc."";
    $tabla_compra = '<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">';
           
                    for ($i=0; $i <sizeof($detalle_pedido) ; $i++) {
                        $pD = consulta_bd("producto_id, nombre","productos_detalles","id=".$detalle_pedido[$i][0],"");
						$id_prod = $pD[0][0];
                        $campos = "id, nombre,thumbs";
                        $tabla  = "productos";
                        $where  = "id=$id_prod";
                        $productos = consulta_bd($campos,$tabla,$where,"");
                        $precio_unitario = $detalle_pedido[$i][2];
                        $cantidad = $detalle_pedido[$i][1];
                        $subtotal = $precio_unitario * $cantidad;

                        $tabla_compra .= '<tr>';
                        $tabla_compra .= '  <td valign="top" align="left" width="30%" style="border-bottom: 2px solid #ccc;">
												<p style="float:left; width:100%; margin:10px 0 5px 0;">
													<img src="'.$url_sitio."/".getIMG($detalle_pedido[$i][0]).' " width="90%"/>
												</p>
												
											</td>';
                        $tabla_compra .= '  <td valign="top" align="left" width="70%" style="border-bottom: 2px solid #ccc;color:#797979;">
												<p style="float:left; width:100%; margin:10px 0 5px 0; font-size: 16px;"">'.$productos[0][1].'</p>
												<p style="float:left; width:100%; margin:0 0 5px 0;"><small><strong>SKU: '.$detalle_pedido[$i][3].'</strong></small></p>
												<p style="float:left; width:100%; margin:0 0 5px 0;"><small>Cantidad: '.$detalle_pedido[$i][1].'</small></p>
												<p style="float:left; width:100%; margin:0 0 5px 0; text-align: right;"><strong>$'.number_format($detalle_pedido[$i][2],0,",",".").'</strong></p>
											</td>'; //nombre producto
                        
                        $tabla_compra .= '</tr>';

                    }
					
					
					for ($i=0; $i <sizeof($detalle_pack) ; $i++) {
						$skuPack = $detalle_pack[$i][4];
						
						$cantProdPack = $detalle_pack[$i][5];
                        $pDP = consulta_bd("p.codigos, pd.precio, pd.descuento","productos_detalles pd, productos p","p.id= pd. producto_id and pd.sku='$skuPack'","");
						$productosPorPack = explode(",", $pDP[0][0]);
						$cantArreglo = count($productosPorPack);
					
						
						$pD = consulta_bd("producto_id, nombre","productos_detalles","sku='$skuPack'","");
						$id_prod = $pD[0][0];
                        $campos = "id, nombre,thumbs";
                        $tabla  = "productos";
                        $where  = "id=$id_prod";
                        $productos = consulta_bd($campos,$tabla,$where,"");
                        
						if($pDP[0][2] > 0){
							$precio_unitario = $pDP[0][2];
						} else {
							$precio_unitario = $pDP[0][1];
						}
						//die("$cantProdPack");
                        $cantidad = $detalle_pack[$i][1];
                        $subtotalFila = $precio_unitario;

						
                        $tabla_compra .= '<tr>';
                        $tabla_compra .= '  <td valign="top" align="left" width="30%" style="border-bottom: 2px solid #ccc;">
												<p style="float:left; width:100%; margin:10px 0 5px 0;">
													<img src="'.$url_sitio.'/imagenes/productos/'.$productos[0][2].' " width="90%"/>
												</p>
												
											</td>';
                        $tabla_compra .= '  <td  align="left" width="70%" style="border-bottom: 2px solid #ccc;color:#797979;">
												<p style="float:left; width:100%; margin:10px 0 5px 0;">'.$productos[0][1].'</p>
												<p style="float:left; width:100%; margin:0 0 5px 0;><strong>'.$skuPack.'</strong></p>
												<p style="float:left; width:100%; margin:0 0 5px 0;">'.$cantidad.'</p>
												<p style="float:left; width:100%; margin:0 0 5px 0;">$'.number_format($subtotalFila,0,",",".").'</p>
											</td>'; //nombre producto
						$tabla_compra .= '</tr>';

                    }
					

                 

            $tabla_compra .= '</table>';
            
$totales = consulta_bd("id, descuento, fecha_creacion, tipo_envio, total, total_pagado, valor_despacho","pedidos","oc='$oc'","");
			$subtotal_ped = $totales[0][4];
			$total_neto = $subtotal_ped - $totales[0][1];
			$iva = round(($total_neto * 1.19) - $total_neto);
            $tabla_compra .= '<table width="100%" style="border-bottom: 2px solid #ccc;font-family: Trebuchet MS, sans-serif;padding:10px;font-weight:bold;">';
            $tabla_compra .='   <tfoot>';

            $tabla_compra .='     <tr class="cart_total_price">';
            $tabla_compra .='       <td align="right" width="85%"><span style="color:#999; float:right;">Sub Total:</span></td>';
            $tabla_compra .='       <td align="right" width="15%"><span style="color:#999; text-align:right;">$'.number_format($subtotal_ped,0,",",".") .'</span></td>';
            $tabla_compra .='     </tr>';

            if($totales[0][1] != '' || $totales[0][1] != 0){
				$tabla_compra .='<tr class="cart_total_price">';
            	$tabla_compra .='   <td align="right" width="90%"><span style="color:#999">Descuento:</span></td>';
            	$tabla_compra .='   <td align="right" width="10%"><span style="color:#999">-$'.number_format($totales[0][1],0,",",".").'</span></td>';
				$tabla_compra .='</tr>';
			}

            $tabla_compra .='     <tr class="cart_total_price">';
            $tabla_compra .='       <td align="right" width="85%"><span style="color:#999; float:right;">Iva:</span></td>';
            $tabla_compra .='       <td align="right" width="15%"><span style="color:#999; text-align:right;">$'.number_format($iva,0,",",".") .'</span></td>';
            $tabla_compra .='     </tr>';

			
			$tabla_compra .='     <tr class="cart_total_price">';
            $tabla_compra .='       <td align="right" width="85%"><span style="color:#999; float:right;">Valor envío ('.$totales[0][3].'):</span></td>';
            $tabla_compra .='       <td align="right" width="15%"><span style="color:#999; text-align:right;">$'.number_format($totales[0][6], 0, ',', '.').'</span></td>';
            $tabla_compra .='     </tr>';

            $tabla_compra .='     <tr class="cart_total_price">';
            $tabla_compra .='       <td align="right" width="85%"><span style="color:'.$color_logo.'; float:right;">Total Pedido:</span></td>';
            $tabla_compra .='       <td align="right" width="15%"><span style="color:'.$color_logo.'; text-align:right;">$'.number_format($totales[0][5],0,",",".").'</span></td>';        
            $tabla_compra .='     </tr>';

            $tabla_compra .='   </tfoot>';
            $tabla_compra .='</table>';

            $tabla_compra .='<br /><br />';

    
    
    $msg2 = '
    <html>
        <head>
		<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
        <title>'.$nombre_sitio.'</title>
        </head>
        <body style="background:#f9f9f9;">
			<div style="background:#fff; width:86%; max-width: 500px; margin: 0 auto; padding-left:2%; padding-right:2%; border-top:solid 4px '.$color_logo.'; border-bottom:solid 4px '.$color_logo.'; font-family: Open Sans, sans-serif;">
            
                        <table cellspacing="0" cellpadding="0" border="0" width="100%" style="margin-top:10px;margin-bottom:10px;">
                            <tr>
                                <th align="center" width="100%">
									<p style="margin:20px 0 30px 0;">
										<a href="'.$url_sitio.'" style="color:#8CC63F;">
											<img src="'.$logo.'" alt="'.$logo.'" border="0" />
										</a>
									</p>
                                </th>
                            </tr>
							<tr>
								<th align="center" width="100%" style="color:#797979;"> 
                                    <p style="text-transform: uppercase;float:right;width:100%;margin: 0px; font-size:20px;">Estimado '.$nombre_cliente.'</p>
                                    <p style="float:right;width:100%;margin: 0px;">Gracias por su compra</p>
                                    <p style="float:right;width:100%;margin: 0px;">Su número de compra es: <br />
<strong style="color:'.$color_logo.';">'.$oc.'</strong></p>
                                    
                                    <p style="float:right;width:100%;margin: 15px 0px;">
                                        
                                        <a style="display:inline-block; color:#fff;padding:10px 20px; margin: 5px auto; text-decoration:none; background-color:'.$color_logo.';" href="'.$url_sitio.'/comprobante/'.substr($oc, 3).'">
                                            VER COMPROBANTE
                                        </a>
										
                                    </p>
                                </th>
							</tr>
                        </table>
                        <br/><br/>';
                        if($datos_cliente[0][4] == 'transferencia'){
							$msg2 .= '<p style="float:left; width: calc(100% - 40px); text-align: center; padding: 20px; background: #f6f6f6; color: #039;">
								A continuación encontrarás los datos para que realices la transferencia antes de 48 horas, después de ese periodo se anulara el pedido. <br> <br>

					            Para asistencia sobre el pago o dudas del producto, por favor contáctenos al mail ventas@socadin.cl, en horario de atención de tienda. <br> <br>

					            En el caso que el producto incluya despacho pagado o por pagar el o los productos serán enviados 24 horas después de haber realizado el pago, previa confirmación de la transacción. En el caso que la transacción se realice un día viernes, fin de semana o feriado el despacho se realizara  en los primeros días hábiles siguientes. <br> <br>

					            Banco: Banco Scotiabank<br/>
					            Cuenta Corriente: 190217752 <br/>
					            Rut: 85.385.800-5 <br/>
					            Nombre: Socadin Limitada<br/>
					            Email: ventas@socadin.cl
							</p>';
						}
                        $msg2 .= '<br/><br/>
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tr>
                                <td valign="top">
                                   ';
                                    
                            $msg2.='<p style="color: #333;">PRODUCTOS COMPRADOS</p>

                                    <p>'.$tabla_compra.'</p>';
                                    
                            $msg2.='<p align="center" style="margin:0;color:#000;">'.$msje_despacho.'</p> 
                                   
                                    <p align="center" style="margin:0;color:#999;">Gracias,</p>
                                    <p align="center" style="margin:0 0 20px 0;color:#999;">Saludos cordiales, <strong>Equipo '.$nombre_sitio.'</strong></p>
                                </td>
                            </tr>
                        </table>
            </div>
        </body>
    </html>';
	// return $msg2;
    
	//save_in_mailchimp($oc, 'exito');
    //save_in_mailchimp($oc, 'todas_compras');
    
    $hoy = date('Y-m-d H:i:s');

    $mailin = new Mailin('grupoatlasholding@gmail.com', 'VSU8IBE6jn21kNYJ');
	$mailin->
		addTo("$email_cliente","$nombre_cliente")->
        setFrom(opciones("setFrom_mail"), opciones("nombre_setFrom_mail"))->
        setSubject("$asunto")->
        setText("$msg2")->
            setHtml("$msg2");
    $res = $mailin->send();
    $res2 = json_decode($res);

    if ($res2->{'result'} == true) {
    	$msgCompra = 'COMPRA exitoso';
    	$insert =  insert_bd("cron_logs","cron, fecha","'$msgCompra', '$hoy'");	
        return 'envio exitoso';
    } else {
    	$msgCompra = 'COMPRA ERROR';
    	$insert =  insert_bd("cron_logs","cron, fecha","'$msgCompra', '$hoy'");	
        return "Mailer Error: " . "error";
    }
}


function enviarComprobanteAdmin($oc, $imprimir, $correoForzarNotificacion){

	$nombre_sitio = opciones("nombre_cliente");
    $nombre_corto = opciones("dominio");
    $noreply = "no-reply@".$nombre_corto;
    $url_sitio="https://".$nombre_corto;
    $logo = opciones("logo_mail");
    $correo_venta = opciones("correo_venta");
	$color_logo = opciones("color_logo");
	
	
	$fontFamily = ($imprimir == 1) ? "font-family: Trebuchet MS, sans-serif;" : "font-family: Open Sans, sans-serif;";
    
    /*
$email_admin = 'ventas@moldeable.com';
	$email_admin = 'htorres@moldeable.com';
*/
	
    
    $msje_despacho = 'Su pedido sera procesado y despachado dentro de 24 horas.';
	$datos_cliente = consulta_bd("nombre,email,id,direccion","pedidos","oc='$oc'","");
    $nombre_cliente = $datos_cliente[0][0];
    $email_cliente = $datos_cliente[0][1];
    $id_pedido = $datos_cliente[0][2];
    
    $datos_cliente = consulta_bd("nombre,
    email,
    id,
    direccion, 
    region_id,
    region_id,
    comuna_id,
    direccion,
    telefono,
    rut,
    factura,
    direccion_factura,
    giro,
    email_factura,
    rut_factura,
    fecha_creacion,
    telefono,
    regalo,
	razon_social,
	payment_type_code,
	shares_number, 
	transaction_date, 
    comentarios_envio, 
    nombre_retiro,
    apellidos_retiro, 
    rut_retiro, 
    nombre, 
    retiro_en_tienda, medio_de_pago","pedidos","oc='$oc'","");
    $nombre_cliente = $datos_cliente[0][0];
    
	$method = ($datos_cliente[0][28] == "webpay") ? "tbk" : "mpago";
	
    $tipo_pago = tipo_pago($datos_cliente[0][19], $datos_cliente[0][20], $method);
	
	$detalle_pedido = consulta_bd("productos_detalle_id,cantidad,precio_unitario,codigo, codigo_pack, descuento","productos_pedidos","pedido_id=$id_pedido","");
    $despacho = $datos_cliente[0][3];
    
    


    $tabla_compra = '
                <table width="100%"  border="0" cellspacing="0" cellpadding="5" style="border-bottom: 2px solid #ccc;border-top: 2px solid #ccc; '.$fontFamily.' color:#666; float:left;">
                    <thead>
					<tr>
                        <th align="left" width="10%" style="border-bottom:2px solid #ccc;"></th>
                        <th align="left" width="30%" style="border-bottom:2px solid #ccc;">Producto</th>
                        <th align="center" width="5%" style="border-bottom:2px solid #ccc;">Cantidad</th>
                        <th align="right" width="10%" style="border-bottom:2px solid #ccc;">Precio <br>Unitario</th>
						<th align="right" width="10%" style="border-bottom:2px solid #ccc;">Total item</th>
                    </tr>
					</thead>
                <tbody>';
           
					
                    for ($i=0; $i <sizeof($detalle_pedido) ; $i++) {
                        $pD = consulta_bd("producto_id, nombre","productos_detalles","id=".$detalle_pedido[$i][0],"");
						$id_prod = $pD[0][0];
                        $campos = "id, nombre,thumbs";
                        $tabla  = "productos";
                        $where  = "id=$id_prod";
                        $productos = consulta_bd($campos,$tabla,$where,"");
                        $precio_unitario = $detalle_pedido[$i][2];
                        $cantidad = $detalle_pedido[$i][1];
                        $subtotal = $precio_unitario * $cantidad;
						
						if($detalle_pedido[$i][4] != ""){
							$tabla_compra .= '<tr bgcolor="#efefef">';
							} else {
							$tabla_compra .= '<tr>';
							}
                        
						
                        $tabla_compra .= '  <td style="border-bottom: 2px solid #ccc;">
												<img src="'.$url_sitio.'/imagenes/productos/'.$productos[0][2].'" width="100%"/>
											</td>';
                        $tabla_compra .= '  <td style="border-bottom: 2px solid #ccc;color:#797979; '.$fontFamily.'">'.$productos[0][1].'<br/><small><strong>SKU: '.$detalle_pedido[$i][3].'</strong></small></td>'; //nombre producto //SKU
						
                        $tabla_compra .= '  <td align="center" style="border-bottom: 2px solid #ccc;color:#797979; '.$fontFamily.'">'.$detalle_pedido[$i][1].'</td>'; //cantidad
						
						if($detalle_pedido[$i][5] > 0){
							$tabla_compra .= '  <td align="right" style="border-bottom: 2px solid #ccc; color:'.$color_logo.'; '.$fontFamily.'">$'.number_format($detalle_pedido[$i][2],0,",",".").'</td>'; //precio producto
							$tabla_compra .= '  <td align="right" style="border-bottom: 2px solid #ccc; color:'.$color_logo.'; '.$fontFamily.'">$'.number_format(($detalle_pedido[$i][2]*$detalle_pedido[$i][1]),0,",",".").'</td>'; //precio producto
                        $tabla_compra .= '</tr>';
							} else {
							$tabla_compra .= '  <td align="right" style="border-bottom: 2px solid #ccc; color:'.$color_logo.'; '.$fontFamily.'">$'.number_format($detalle_pedido[$i][2],0,",",".").'</td>'; 
							$tabla_compra .= '  <td align="right" style="border-bottom: 2px solid #ccc; color:'.$color_logo.'; '.$fontFamily.'">$'.number_format(($detalle_pedido[$i][2]*$detalle_pedido[$i][1]),0,",",".").'</td>'; //precio producto
                        $tabla_compra .= '</tr>';
							}
                        

                    }

                 
			$tabla_compra .= '</tbody>';
            $tabla_compra .= '</table>';
            
$totales = consulta_bd("id, descuento, fecha_creacion, tipo_envio, total, total_pagado, valor_despacho","pedidos","oc='$oc'","");
			$subtotal_ped = $totales[0][4];
			$total_neto = $subtotal_ped - $totales[0][1];
			$iva = round(($total_neto * 1.19) - $total_neto);
            $tabla_compra .= '<table width="100%" style="border-bottom: 2px solid #ccc;font-family: Trebuchet MS, sans-serif;padding:10px;font-weight:bold;">';
            $tabla_compra .='   <tfoot>';

            $tabla_compra .='     <tr class="cart_total_price">';
            $tabla_compra .='       <td align="right" width="85%"><span style="color:#999; float:right;">Total neto:</span></td>';
            $tabla_compra .='       <td align="right" width="15%"><span style="color:#999; text-align:right;">$'.number_format($subtotal_ped,0,",",".") .'</span></td>';
            $tabla_compra .='     </tr>';

            if($totales[0][1] != '' || $totales[0][1] != 0){
				$tabla_compra .='<tr class="cart_total_price">';
            	$tabla_compra .='   <td align="right" width="90%"><span style="color:#999">Descuento:</span></td>';
            	$tabla_compra .='   <td align="right" width="10%"><span style="color:#999">-$'.number_format($totales[0][1],0,",",".").'</span></td>';
				$tabla_compra .='</tr>';
			}
			
			$tabla_compra .='     <tr class="cart_total_price">';
            $tabla_compra .='       <td align="right" width="85%"><span style="color:#999; float:right;">IVA:</span></td>';
            $tabla_compra .='       <td align="right" width="15%"><span style="color:#999; text-align:right;">$'.number_format($iva,0,",",".") .'</span></td>';
            $tabla_compra .='     </tr>';
			

            $tabla_compra .='     <tr class="cart_total_price">';
            $tabla_compra .='       <td align="right" width="85%"><span style="color:#999">Valor envío ('.$totales[0][3].'):</span></td>';
            $tabla_compra .='       <td align="right" width="15%"><span style="color:#999">$'.number_format($totales[0][6], 0, ',', '.').'</span></td>';
            $tabla_compra .='     </tr>';

            $tabla_compra .='     <tr class="cart_total_price">';
            $tabla_compra .='       <td align="right" width="85%"><span style="color:'.$color_logo.'">Total Pedido:</span></td>';
            $tabla_compra .='       <td align="right" width="15%"><span style="color:'.$color_logo.'">$'.number_format($totales[0][5],0,",",".").'</span></td>';        
            $tabla_compra .='     </tr>';

            $tabla_compra .='   </tfoot>';
            $tabla_compra .='</table>';

            $tabla_compra .='<br /><br />';
	

    
    $asunto = "Comprobante de compra,  OC N°".$oc."";
    $msg2 = "<html>";
	
	if($imprimir != 1){
		$msg2 .= '
			<head>
				<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
				<title>'.$nombre_sitio.'</title>
			</head>';
	}
	
	if($imprimir != 1){
$msg2 .= '<body style="background:#f9f9f9; float:left;">
			<div style="background:#fff; width:86%; margin-left:5%; padding-left:2%; padding-right:2%; border-top:solid 4px '.$color_logo.'; border-bottom:solid 4px '.$color_logo.'; '.$fontFamily.' float:left;">';
	} else {
$msg2 .= '<body style="background:#fff; float:left;">
			<div style="background:#fff; width:96%; margin-left:0; padding-left:2%; padding-right:2%; border-top:solid 4px '.$color_logo.'; border-bottom:solid 4px '.$color_logo.'; float:left; '.$fontFamily.'">';
	}
            
             $msg2 .= '<table cellspacing="0" cellpadding="0" border="0" width="100%" style="margin-top:10px;margin-bottom:10px;">
                            <tr>
                                <th align="left" width="50%">
                                	<p>
										<a href="'.$url_sitio.'" style="color:#8CC63F;">
											<img src="'.$logo.'" alt="'.$logo.'" border="0" />
										</a>
									</p>
                                </th>
                                <th align="right" width="50%"> 
                                    <p style="text-transform: uppercase;float:right;width:100%;margin: 0px; '.$fontFamily.'">'.$nombre_cliente.'</p>
                                    <p style="color:#797979;float:right;width:100%;margin: 0px;line-height:20px; '.$fontFamily.'">Ha generado una compra web.</p>
                                    <p style="color:#797979; float:right; width:100%; margin: 0px; '.$fontFamily.'">Su número de compra es: <strong style="color:'.$color_logo.'; '.$fontFamily.'">'.$oc.'</strong></p>';
								
								if($imprimir != 1){

									$msg2 .= '<p style="float:right;width:100%;margin: 10px 0px 0 0;">
                                        <a style="text-decoration: none; float:right; background-color:'.$color_logo.'; padding:10px 20px; color:#fff;" href="'.$url_sitio.'/comprobante/'.substr($oc, 3).'">
                                            Ver comprobante
                                        </a>
                                    </p>';

								}


                        $msg2 .= '</th>
                            </tr>

                            <tr>
                            	<td colspan="2">';

                            	if($datos_cliente[0][28] == 'transferencia'){
									$msg2 .= '<p style="float:left; width: calc(100% - 40px); text-align: center; padding: 20px; background: #f6f6f6; color: #039;">
										<strong>Compra con transferencia</strong>
									</p>';
								}

								$msg2 .= '
                            	</td>
                            </tr>

                        </table>
                        <br/><br/>
                        
                        
                        <table cellspacing="0" cellpadding="0" border="0" width="100%" style="border-top:solid 1px #ccc; padding-top:10px; float:left;">
                            <tr>
                            	<td valign="top" width="50%">

                                    <h3 style="'.$fontFamily.'">Datos usuario</h3>
                                    <p>
                                        <ul>
                                            <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Nombre: </strong>'.$datos_cliente[0][0].'</li>
                                            <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Correo: </strong>'.$datos_cliente[0][1].'</li>
                                            <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Teléfono: </strong>'.$datos_cliente[0][8].'</li>
                                            <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Comenatrios Cliente: </strong>'.$datos_cliente[0][22].'</li>
                                        </ul>
                                    </p>
    
                                </td>

                                <td valign="top" width="50%">';
                                    if($datos_cliente[0][27] == 0){
                                        
                                		$msg2.='<h3 style="'.$fontFamily.'">Dirección de entrega</h3>
                                        <ul>
                                            <li style="'.$fontFamily.'">
                                            	<strong style="color:'.$color_logo.';">Direccion: </strong>'.$datos_cliente[0][3].'
                                            </li>
                                            <li style="'.$fontFamily.'">
                                            	<strong style="color:'.$color_logo.';">Region: </strong>'.getRegion($datos_cliente[0][4]).'
                                            </li>
                                            <li style="'.$fontFamily.'">
                                            	<strong style="color:'.$color_logo.';">Comuna: </strong>'.getComuna($datos_cliente[0][6]).'
                                            </li>
                                        </ul>';

                                    } else {
                                		
                                		$msg2.='<h3 style="'.$fontFamily.'">Datos retiro</h3>
                                        <ul>
                                            <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Nombre: </strong>'.$datos_cliente[0][23].' '.$datos_cliente[0][24].'</li>
                                            <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Rut: </strong>'.$datos_cliente[0][25].'</li>
                                        </ul>';

                                    }
                                    
                                    
                                    
                        $msg2.='</td>
                                
                            </tr>
                        </table>
                        <br/>
                        
                        
                        
                        <table cellspacing="0" cellpadding="0" border="0" width="100%" style="border-top:solid 1px #ccc; padding-top:10px; float:left;">';
					$msg2.='<tr>';

						if($datos_cliente[0][28] == 'webpay'){
							$msg2.='
									<td valign="top" width="50%" height="100">
										<h3 style="'.$fontFamily.'">Datos Transbank</h3>
	                                    <p>
	                                        <ul>
	                                            <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Orden de compra: </strong>'.$oc.'</li>
	                                            <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Tipo de pago: </strong>'.$tipo_pago[tipo_pago].'</li>
	                                            <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Tipo de cuota: </strong>'.$tipo_pago[tipo_cuota].'</li>
												<li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Nº Cuotas: </strong>'.$tipo_pago[cuota].'</li>
												<li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Fecha: </strong>'.$datos_cliente[0][21].'</li>
	                                        </ul>
	                                    </p>
									</td>';
						}
                           
                        $msg2.='<td valign="top" width="50%">';
                                if($datos_cliente[0][14] != ''){
                            $msg2.='<h3 style="'.$fontFamily.'">Datos empresa</h3>
                                    <ul>
                                        <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Nombre: </strong>'.$datos_cliente[0][18].'</li>
                                        <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Direccion: </strong>'.$datos_cliente[0][11].'</li>
                                        <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Giro: </strong>'.$datos_cliente[0][12].'</li>
                                        <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Rut: </strong>'.$datos_cliente[0][14].'</li>
                                        <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Email: </strong>'.$datos_cliente[0][13].'</li>
                                        <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Telefono: </strong>'.$datos_cliente[0][16].'</li>
                                    </ul>';
                                }
                            $msg2.='</td>';
    
	
                            
							
                                   
                        $msg2.='
                            </tr>
                        </table>
                        
                        
                        
                        
                        <table cellspacing="0" cellpadding="0" border="0" width="100%" style="float:left; margin-top:50px;">
                            <tr>
                                <td valign="top">';
                            $msg2.='<h3 style="'.$fontFamily.' margin-bottom:20px;">PRODUCTOS COMPRADOS</h3>
                                    <p>'.$tabla_compra.'</p>';
                         
						//Muestro los datos solo si es para enviar por correo
						if($imprimir != 1){
                            $msg2.='<p align="center" style="margin:0;color:#000; '.$fontFamily.'">Para ver el detalle de la compra y datos del cliente puede pinchar el siguiente <a href="'.$url_sitio.'admin/index.php?op=35c&id='.$id_pedido.'">link</a></p> 
                                   
                                    <p align="center" style="margin:0;color:#999; '.$fontFamily.'">Gracias,</p>
                                    <p align="center" style="margin:0; margin-bottom:10px;color:#999;'.$fontFamily.'">Saludos cordiales, <strong>Equipo '.$nombre_sitio.'</strong></p>';
						}
						//Muestro los datos solo si es para enviar por correo
                       $msg2.='</td>
                            </tr>
                        </table>
            </div>
        </body>
    </html>';
	
    // return $msg2;

	if($imprimir == 1){
		//Muestro el html para transformarlo en un pdf
		return $msg2;	
		
	} else if($correoForzarNotificacion != ""){
		$mailin = new Mailin('grupoatlasholding@gmail.com', 'VSU8IBE6jn21kNYJ');
		$mailin->
			addTo("$correoForzarNotificacion","Notificacion Venta")->
	        setFrom(opciones("setFrom_mail"), opciones("nombre_setFrom_mail"))->
	        setSubject("$asunto")->
	        setText("$msg2")->
	            setHtml("$msg2");
	    $res = $mailin->send();
	    $res2 = json_decode($res);

	    if ($res2->{'result'} == true) {
	        return 1;
	    } else {	
	        return 2;
	    }
		//envio la notificacion al correo que me indican en la variable
		
	} else {
		//si envio las otras 2 variables vacias ejecuto la funcion con normalidad

		$hoy = date('Y-m-d H:i:s');

		$mailin = new Mailin('grupoatlasholding@gmail.com', 'VSU8IBE6jn21kNYJ');
	    if(opciones("correo_admin1") != ""){
			$mailin-> addTo(opciones("correo_admin1"),opciones("nombre_correo_admin1"));
		}
		if(opciones("correo_admin2") != ""){
			$mailin-> addTo(opciones("correo_admin2"),opciones("nombre_correo_admin2"));
		}
		if(opciones("correo_admin3") != ""){
			$mailin-> addTo(opciones("correo_admin3"),opciones("nombre_correo_admin3"));
		}

		$mailin->
	        setFrom(opciones("setFrom_mail"), opciones("nombre_setFrom_mail"))->
	        setSubject("$asunto")->
	        setText("$msg2")->
	            setHtml("$msg2");
	    $res = $mailin->send();
	    $res2 = json_decode($res);

	    if ($res2->{'result'} == true) {
	    	$msgCompra = 'COMPRA exitoso';
	    	$insert =  insert_bd("cron_logs","cron, fecha","'$msgCompra', '$hoy'");	
	        return 'envio exitoso';	    	
	    } else {
	    	$msgCompra = 'COMPRA ERROR';
	    	$insert =  insert_bd("cron_logs","cron, fecha","'$msgCompra', '$hoy'");	
	        return "Mailer Error: Error";
    	}
		//si envio las otras 2 variables vacias ejecuto la funcion con normalidad
	}
		
}



/*  //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// 
FUNCTIONES COTIZACION*/


function totalCartCotizacion($cotizacion){
	global $db;
	if(!isset($_COOKIE[cotizacion])){
		setcookie("cotizacion", "$cotizacion", time() + (365 * 24 * 60 * 60), "/");
		}
	$cotizacion = $_COOKIE[cotizacion];
	if ($cotizacion) {
		$items = explode(',',$cotizacion);
		$contents = array();
		foreach ($items as $item) {
			$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
		}
		$i = 1;
		$total = 0;
		foreach ($contents as $prd_id=>$qty) {
			$is_cyber = (opciones('cyber') == 1) ? true : false;
			$producto = consulta_bd("pd.id, pd.precio, pd.descuento, p.id","productos_detalles pd join productos p on p.id = pd.producto_id","pd.id=$prd_id","");
			if ($is_cyber AND is_cyber_product($producto[0][3])) {
				$precios = get_cyber_price($prd_id);
				$total += $precios['precio_cyber']*$qty;
			}else{
				if($producto[0][2] > 0){
					$total += $producto[0][2]*$qty;
				} else {
					$total += $producto[0][1]*$qty;
				}
			}
		}
	}
	return round($total);
}

//cantidad de productos en el carro
function qty_proCotizacion(){
	if(!isset($_COOKIE[cotizacion])){
		setcookie("cotizacion", "", time() + (365 * 24 * 60 * 60), "/");
		} 
	$cotizacion = $_COOKIE[cotizacion];
	if ($cotizacion){
		$items = explode(',',$cotizacion);
		return count($items);
	}
	else
	{
		return 0;
	}
}





function ShowCartCotizacion() {
	global $db;
	if(!isset($_COOKIE[cotizacion])){
		setcookie("cotizacion", "", time() + (365 * 24 * 60 * 60), "/");
		} 
	$cotizacion = $_COOKIE[cotizacion];
	if ($cotizacion) {
		$items = explode(',',$cotizacion);
		$contents = array();
		foreach ($items as $item) {
			$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
		}
		$output[] = '';
		$i = 1;
		foreach ($contents as $prd_id=>$qty) {
			$producto = consulta_bd("p.thumbs, p.nombre, pd.precio, pd.descuento, p.id, pd.sku","productos p, productos_detalles pd","p.id=pd.producto_id and pd.id=$prd_id","");
			if($producto[0][0] != ''){
				$thumbs = $producto[0][0];
			} else {
				$thumbs = "img/sinImagenGrilla.jpg";
			}

			$valor = getPrecio($prd_id)*$qty;
			$valorUnitario = getPrecio($prd_id);
			
			
			   
	$output[] .='<div class="filaProductos" id="fila_carro_'.$prd_id.'">
                	<div class="imgFila ancho10">
						<a href="ficha/'.$producto[0][4].'/'.url_amigables($producto[0][1]).'">
							<img src="imagenes/productos/'.$thumbs.'" width="100%" />
						</a>
					</div>
					<div class="contInfoShowCart">
						<div class="nombreFila">
							<a href="ficha/'.$producto[0][4].'/'.url_amigables($producto[0][1]).'">
								<span>'.$producto[0][1].'</span>
							</a>
						</div>
						<div class="skuShowCart">SKU: '.$producto[0][5].'</div>
						<div class="botoneraShowCart">
							<a href="javascript:void(0)" onclick="eliminaItemCarroCotizacion('.$prd_id.')">Eliminar</a>
						</div>
						
					</div><!-- fin contInfoShowCart-->
                    
                    
					<div class="precioFila ancho20"><span>$'.number_format($valorUnitario,0,",",".").'</span></div>
                    <div class="cantFila ancho10">
                    	<div class="pull-left spinnerCarro">
                        	<input type="text" name="cant" class="campoCantCarroResumen" value="'.$qty.'" />
                            <div class="contFlechas">
                                <span class="mas" onclick="agregarElementoCarroCotizacion('.$prd_id.')"  rel="'.$prd_id.'">▲</span>
                                <span class="menos" onclick="quitarElementoCarroCotizacion('.$prd_id.')" rel="'.$prd_id.'">▼</span>
                            </div>
                    	</div>
                    </div>
                    <div class="totalFila ancho20"><span>$'.number_format($valor,0,",",".").'</span></div>
                    
                </div><!-- fin filaProductos-->';
		}
			
	} else {
		$output[] = '<div class="carroVacio">
						<p class="carroVacio">Su carro está vacío.</p><br />
					</div>';
	}
	return join('',$output);
	
}


/*  //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// */

/*////////////////////////ENVIAR COMPROBANTE AL COMPRADOR///////////////////////////////////////////////*/   

  
  function enviarComprobanteClienteCotizacion($oc){

	$nombre_sitio = opciones("nombre_cliente");
    $nombre_corto = opciones("dominio");
    $noreply = "no-reply@".$nombre_corto;
    $url_sitio="https://".$nombre_corto;
    $logo = opciones("logo_mail");
    $correo_venta = opciones("correo_venta");
	$color_logo = opciones("color_logo");
	
	$datos_cliente = consulta_bd("nombre,email,id, apellido","cotizaciones","oc='$oc'","");
    
    $nombre_cliente = $datos_cliente[0][0]." ".$datos_cliente[0][3];
    $email_cliente = $datos_cliente[0][1];
      
    $id_cotizacion = $datos_cliente[0][2];
    
    $id_pedidoAdminitrador = $datos_cliente[0][4];  
      
	$detalle_pedido = consulta_bd("productos_detalle_id,cantidad,precio_unitario,codigo, codigo_pack","productos_cotizaciones","cotizacion_id=$id_cotizacion and codigo_pack is NULL","");
	
	$detalle_pack = consulta_bd("productos_detalle_id,cantidad,precio_unitario,codigo, codigo_pack, count(codigo_pack) as cantidad_articulos","productos_cotizaciones","cotizacion_id=$id_cotizacion and codigo_pack <> '' group by codigo_pack","");
    
	$despacho = $datos_cliente[0][3];
    
    $asunto = "Cotización N°".$oc."";
    $tabla_compra = '<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">';
           
                    for ($i=0; $i <sizeof($detalle_pedido) ; $i++) {
                        $pD = consulta_bd("producto_id, nombre","productos_detalles","id=".$detalle_pedido[$i][0],"");
						$id_prod = $pD[0][0];
                        $campos = "id, nombre,thumbs";
                        $tabla  = "productos";
                        $where  = "id=$id_prod";
                        $productos = consulta_bd($campos,$tabla,$where,"");
                        $precio_unitario = $detalle_pedido[$i][2];
                        $cantidad = $detalle_pedido[$i][1];
                        $subtotal = $precio_unitario * $cantidad;

                        $tabla_compra .= '<tr>';
                        $tabla_compra .= '  <td valign="top" align="left" width="30%" style="border-bottom: 2px solid #ccc;">
												<p style="float:left; width:100%; margin:10px 0 5px 0;">
													<img src="'.$url_sitio.'/imagenes/productos/'.$productos[0][2].' " width="90%"/>
												</p>
												
											</td>';
                        $tabla_compra .= '  <td valign="top" align="left" width="70%" style="border-bottom: 2px solid #ccc;color:#797979;">
												<p style="float:left; width:100%; margin:10px 0 5px 0;">'.$productos[0][1].'</p>
												<p style="float:left; width:100%; margin:0 0 5px 0;">'.$detalle_pedido[$i][3].'</p>
												<p style="float:left; width:100%; margin:0 0 5px 0;">'.$detalle_pedido[$i][1].'</p>
												<p style="float:left; width:100%; margin:0 0 5px 0;">$'.number_format($detalle_pedido[$i][2],0,",",".").'</p>
											</td>'; //nombre producto
                        
                        $tabla_compra .= '</tr>';

                    }
					
					
					for ($i=0; $i <sizeof($detalle_pack) ; $i++) {
						$skuPack = $detalle_pack[$i][4];
						
						$cantProdPack = $detalle_pack[$i][5];
                        $pDP = consulta_bd("p.codigos, pd.precio, pd.descuento","productos_detalles pd, productos p","p.id= pd. producto_id and pd.sku='$skuPack'","");
						$productosPorPack = explode(",", $pDP[0][0]);
						$cantArreglo = count($productosPorPack);
					
						
						$pD = consulta_bd("producto_id, nombre","productos_detalles","sku='$skuPack'","");
						$id_prod = $pD[0][0];
                        $campos = "id, nombre,thumbs";
                        $tabla  = "productos";
                        $where  = "id=$id_prod";
                        $productos = consulta_bd($campos,$tabla,$where,"");
                        
						if($pDP[0][2] > 0){
							$precio_unitario = $pDP[0][2];
						} else {
							$precio_unitario = $pDP[0][1];
						}
						//die("$cantProdPack");
                        $cantidad = $detalle_pack[$i][1];
                        $subtotalFila = $precio_unitario;

						
                        $tabla_compra .= '<tr>';
                        $tabla_compra .= '  <td valign="top" align="center" width="30%" style="border-bottom: 2px solid #ccc;">
												<p style="float:left; width:100%; margin:10px 0 5px 0;">
													<img src="'.$url_sitio.'/imagenes/productos/'.$productos[0][2].' " width="90%"/>
												</p>
												
											</td>';
                        $tabla_compra .= '  <td  align="center" width="70%" style="border-bottom: 2px solid #ccc;color:#797979;">
												<p style="float:left; width:100%; margin:10px 0 5px 0;">'.$productos[0][1].'</p>
												<p style="float:left; width:100%; margin:0 0 5px 0;">'.$skuPack.'</p>
												<p style="float:left; width:100%; margin:0 0 5px 0;">'.$cantidad.'</p>
												<p style="float:left; width:100%; margin:0 0 5px 0;">$'.number_format($subtotalFila,0,",",".").'</p>
											</td>'; //nombre producto
						$tabla_compra .= '</tr>';

                    }
					

                 

            $tabla_compra .= '</table>';
            
			$totales = consulta_bd("id, fecha_creacion, total","cotizaciones","oc='$oc'","");
			
            $tabla_compra .= '<table width="100%" style="border-bottom: 2px solid #ccc;font-family: Trebuchet MS, sans-serif;padding:10px;font-weight:bold;">';
            $tabla_compra .='   <tfoot>';

            $tabla_compra .='     <tr class="cart_total_price">';
            $tabla_compra .='       <td align="right" width="85%"><span style="color:#999; float:right;">Sub Total:</span></td>';
            $tabla_compra .='       <td align="right" width="15%"><span style="color:#999; text-align:right;">$'.number_format($totales[0][2],0,",",".") .'</span></td>';
            $tabla_compra .='     </tr>';

			
			$tabla_compra .='     <tr class="cart_total_price">';
            $tabla_compra .='       <td align="right" width="85%"><span style="color:#999; float:right;">Valor envío:</span></td>';
            $tabla_compra .='       <td align="right" width="15%"><span style="color:#999; text-align:right;">por acordar</span></td>';
            $tabla_compra .='     </tr>';

            $tabla_compra .='     <tr class="cart_total_price">';
            $tabla_compra .='       <td align="right" width="85%"><span style="color:'.$color_logo.'; float:right;">Total Pedido:</span></td>';
            $tabla_compra .='       <td align="right" width="15%"><span style="color:'.$color_logo.'; text-align:right;">$'.number_format($totales[0][2],0,",",".").'</span></td>';        
            $tabla_compra .='     </tr>';

            $tabla_compra .='   </tfoot>';
            $tabla_compra .='</table>';

            $tabla_compra .='<br /><br />';

    $msg2 = '
    <html>
        <head>
		<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
        <title>'.$nombre_sitio.'</title>
        </head>
        <body style="background:#f9f9f9;">
			<div style="background:#fff; width:86%; margin-left:5%; padding-left:2%; padding-right:2%; border-top:solid 4px '.$color_logo.'; border-bottom:solid 4px '.$color_logo.'; font-family: Open Sans, sans-serif;">
            
                        <table cellspacing="0" cellpadding="0" border="0" width="100%" style="margin-top:10px;margin-bottom:10px;">
                            <tr>
                                <th align="center" width="100%">
									<p style="margin:20px 0 30px 0;">
										<a href="'.$url_sitio.'" style="color:#8CC63F;">
											<img src="'.$logo.'" alt="'.$logo.'" border="0" width="200"/>
										</a>
									</p>
                                </th>
                            </tr>
							<tr>
								<th align="center" width="100%" style="color:#797979;"> 
                                    <p style="text-transform: uppercase;float:right;width:100%;margin: 0px; font-size:20px;">Estimado '.$nombre_cliente.'</p>
                                    <p style="float:right;width:100%;margin: 0px;">Gracias por cotizar</p>
                                    <p style="float:right;width:100%;margin: 0px;">Su número de cotización es: <br />
<strong style="color:'.$color_logo.';">'.$oc.'</strong></p>
                                </th>
							</tr>
                        </table>
                        <br/><br/>
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tr>
                                <td valign="top">
                                   ';
                                    
                            $msg2.='<p style="color: #333;">PRODUCTOS COTIZADOS</p>

                                    <p>'.$tabla_compra.'</p>';
                                    
                            $msg2.='<p align="center" style="margin:0;color:#000;">'.$msje_despacho.'</p> 
                                   
                                    <p align="center" style="margin:0;color:#999;">Gracias,</p>
                                    <p align="center" style="margin:0 0 20px 0;color:#999;">Saludos cordiales, <strong>Equipo '.$nombre_sitio.'</strong></p>
                                </td>
                            </tr>
                        </table>
            </div>
        </body>
    </html>';
	//return $msg2;
    $mailin = new Mailin('grupoatlasholding@gmail.com', 'VSU8IBE6jn21kNYJ');
	$mailin->
	    addTo("$email_cliente", "$nombre_cliente")->
        setFrom(opciones("setFrom_mail"), opciones("nombre_setFrom_mail"))->
        setSubject("$asunto")->
        setText("$msg2")->
            setHtml("$msg2");
    $res = $mailin->send();
    $res2 = json_decode($res);

    if ($res2->{'result'} == true) {
    	return 'envio exitoso';
    }else{
    	return "Mailer Error: Error";
    }

}


function enviarComprobanteAdminCotizacion($oc){

    $nombre_sitio = opciones("nombre_cliente");
    $nombre_corto = opciones("dominio");
    $noreply = "no-reply@".$nombre_corto;
    $url_sitio="https://".$nombre_corto;
    $logo = opciones("logo_mail");
    $correo_venta = opciones("correo_venta");
	$color_logo = opciones("color_logo");
	
	$email_admin = 'ventas@moldeable.com';
	
    
    $datos_cliente = consulta_bd("nombre,email,id,apellido","cotizaciones","oc='$oc'","");
    $nombre_cliente = $datos_cliente[0][0]." ".$datos_cliente[0][3];
    $email_cliente = $datos_cliente[0][1];
    $id_cotizacion = $datos_cliente[0][2];
    
    $datos_cliente = consulta_bd("nombre, apellido,
    email,
    id,
    telefono,
    rut,
    fecha_creacion,
    comentarios","cotizaciones","oc='$oc'","");
    $nombre_cliente = $datos_cliente[0][0]." ".$datos_cliente[0][1];
    $nombre = $nombre_cliente;
	$email = $datos_cliente[0][2];
	$telefono = $datos_cliente[0][4];
	$rut = $datos_cliente[0][5];
    $comentarios = $datos_cliente[0][7];
	
	$detalle_pedido = consulta_bd("productos_detalle_id,cantidad,precio_unitario,codigo, codigo_pack","productos_cotizaciones","cotizacion_id=$id_cotizacion","");
    
    
    


    $tabla_compra = '
                <table width="100%" style="border-bottom: 2px solid #ccc;border-top: 2px solid #ccc;font-family: Trebuchet MS, sans-serif; color:#666;">
                    <tr>
                        <th align="center" width="16%">Imagen</th>
                        <th align="center" width="16%">Producto</th>
                        <th align="center" width="16%">SKU</th>
						<th align="center" width="16%">CÓDIGO PACK</th>
                        <th align="center" width="16%">Cantidad</th>
                        <th align="center" width="16%">Precio Unitario</th>
                    </tr>
                </table>

                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">';
           
                    for ($i=0; $i <sizeof($detalle_pedido) ; $i++) {
                        $pD = consulta_bd("producto_id, nombre","productos_detalles","id=".$detalle_pedido[$i][0],"");
						$id_prod = $pD[0][0];
                        $campos = "id, nombre,thumbs";
                        $tabla  = "productos";
                        $where  = "id=$id_prod";
                        $productos = consulta_bd($campos,$tabla,$where,"");
                        $precio_unitario = $detalle_pedido[$i][2];
                        $cantidad = $detalle_pedido[$i][1];
                        $subtotal = $precio_unitario * $cantidad;

                        $tabla_compra .= '<tr>';
                        $tabla_compra .= '  <td  align="center" width="16%" style="border-bottom: 2px solid #ccc;">
												<img src="'.$url_sitio.'/imagenes/productos/'.$productos[0][2].'" width="100"/>
											</td>';
                        $tabla_compra .= '  <td  align="center" width="16%" style="border-bottom: 2px solid #ccc;color:#797979;">'.$productos[0][1].'</td>'; //nombre producto
                        $tabla_compra .= '  <td  align="center" width="16%" style="border-bottom: 2px solid #ccc;color:#797979;">'.$detalle_pedido[$i][3].'</td>'; //codigo producto
						$tabla_compra .= '  <td  align="center" width="16%" style="border-bottom: 2px solid #ccc;color:#797979;">'.$detalle_pedido[$i][4].'</td>'; //codigo pack
						
                        $tabla_compra .= '  <td  align="center" width="16%" align="right" style="border-bottom: 2px solid #ccc;color:#797979;">'.$detalle_pedido[$i][1].'</td>'; //cantidad
                        $tabla_compra .= '  <td  align="center" width="16%" align="right" style="border-bottom: 2px solid #ccc;color:'.$color_logo.';">$'.number_format($detalle_pedido[$i][2],0,",",".").'</td>'; //precio producto
                        $tabla_compra .= '</tr>';

                    }

                 

            $tabla_compra .= '</table>';
            
			$totales = consulta_bd("id, fecha_creacion,total","cotizaciones","oc='$oc'","");
			
            $tabla_compra .= '<table width="100%" style="border-bottom: 2px solid #ccc;font-family: Trebuchet MS, sans-serif;padding:10px;font-weight:bold;">';
            $tabla_compra .='   <tfoot>';

            $tabla_compra .='     <tr class="cart_total_price">';
            $tabla_compra .='       <td align="right" width="85%"><span style="color:#999; float:right;">Total neto:</span></td>';
            $tabla_compra .='       <td align="right" width="15%"><span style="color:#999; text-align:right;">$'.number_format($totales[0][2],0,",",".") .'</span></td>';
            $tabla_compra .='     </tr>';
			
			
            $tabla_compra .='     <tr class="cart_total_price">';
            $tabla_compra .='       <td align="right" width="85%"><span style="color:#999">Valor envío:</span></td>';
            $tabla_compra .='       <td align="right" width="15%"><span style="color:#999">Según acuerdo</span></td>';
            $tabla_compra .='     </tr>';

            $tabla_compra .='     <tr class="cart_total_price">';
            $tabla_compra .='       <td align="right" width="85%"><span style="color:'.$color_logo.'">Total Pedido:</span></td>';
            $tabla_compra .='       <td align="right" width="15%"><span style="color:'.$color_logo.'">$'.number_format($totales[0][2],0,",",".").'</span></td>';        
            $tabla_compra .='     </tr>';

            $tabla_compra .='   </tfoot>';
            $tabla_compra .='</table>';

            $tabla_compra .='<br /><br />';

    
    $asunto = "Cotización N°".$oc."";
    $msg2 = '
    <html>
        <head>
		<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
        <title>'.$nombre_sitio.'</title>
        </head>
        <body style="background:#f9f9f9;">
			<div style="background:#fff; width:86%; margin-left:5%; padding-left:2%; padding-right:2%; border-top:solid 4px '.$color_logo.'; border-bottom:solid 4px '.$color_logo.'; font-family: Open Sans, sans-serif;">
            
                        <table cellspacing="0" cellpadding="0" border="0" width="100%" style="margin-top:10px;margin-bottom:10px;">
                            <tr>
                                <th align="left" width="50%">
                                	<p>
										<a href="'.$url_sitio.'" style="color:#8CC63F;">
											<img src="'.$logo.'" alt="'.$logo.'" border="0" width="200"/>
										</a>
									</p>
                                </th>
                                <th align="right" width="50%"> 
                                    <p style="text-transform: uppercase;float:right;width:100%;margin: 0px;line-height:20px;">'.$nombre_cliente.'</p>
                                    <p style="color:#797979;float:right;width:100%;margin: 0px;line-height:20px;">ha solicitado una cotización.</p>
                                    <p style="color:#797979;float:right;width:100%;margin: 0px;line-height:20px;">Su número de cotización es: <strong style="color:'.$color_logo.';">'.$oc.'</strong></p>
                                    
                                </th>
                            </tr>
                        </table>
                        <br/><br/>
                        
                        
                        <table cellspacing="0" cellpadding="0" border="0" width="100%" style="border-top:solid 1px #ccc; padding-top:10px;">
                            <tr>';
                        $msg2.='
                                <td valign="top" width="50%">
                                    <h3>Datos usuario</h3>
                                    <p>
                                        <ul>
                                            <li><strong style="color:'.$color_logo.';">Nombre: </strong>'.$nombre.'</li>
                                            <li><strong style="color:'.$color_logo.';">Correo: </strong>'.$email.'</li>
                                            <li><strong style="color:'.$color_logo.';">Teléfono: </strong>'.$telefono.'</li>
                                            <li><strong style="color:'.$color_logo.';">Rut: </strong>'.$rut.'</li>
                                            <li><strong style="color:'.$color_logo.';">Comenatrios Cliente: </strong>'.$comentarios.'</li>
                                            
                                        </ul>
                                    </p>
                                    
    
                                </td>
                            </tr>
                        </table>
                        <br/>';
                        
                        
                        
                        $msg2.='
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tr>
                                <td valign="top">
                                   ';
                                    
                            $msg2.='<p style="color: #797979;">PRODUCTOS COTIZADOS</p>

                                    <p>'.$tabla_compra.'</p>';
                                    
                            $msg2.='<p align="center" style="margin:0;color:#000;">Para ver el detalle de la cotización y datos del cliente puede pinchar el siguiente <a href="'.$url_sitio.'admin/index.php?op=250c&id='.$id_cotizacion.'">link</a></p> 
                                   
                                    <p align="center" style="margin:0;color:#999;">Gracias,</p>
                                    <p align="center" style="margin:0; margin-bottom:10px;color:#999;">Saludos cordiales, <strong>Equipo '.$nombre_sitio.'</strong></p>
                                </td>
                            </tr>
                        </table>
            </div>
        </body>
    </html>';
	//return $msg2;
	$mailin = new Mailin('grupoatlasholding@gmail.com', 'VSU8IBE6jn21kNYJ');
    if(opciones("correo_admin1") != ""){
		$mailin-> addTo(opciones("correo_admin1"),opciones("nombre_correo_admin1"));
	}
	if(opciones("correo_admin2") != ""){
		$mailin-> addTo(opciones("correo_admin2"),opciones("nombre_correo_admin2"));
	}
	if(opciones("correo_admin3") != ""){
		$mailin-> addTo(opciones("correo_admin3"),opciones("nombre_correo_admin3"));
	}

	$mailin->
        setFrom(opciones("setFrom_mail"), opciones("nombre_setFrom_mail"))->
        setSubject("$asunto")->
        setText("$msg2")->
            setHtml("$msg2");
    $res = $mailin->send();
    $res2 = json_decode($res);

    if ($res2->{'result'} == true) {
    	return 'envio exitoso';
    }else{
    	return "Mailer Error: error al enviar";
    }

}

/* FIN FUNCIONES COTIZACION  //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// */



function get_precio($id, $tipo) {
	$filas = consulta_bd("precio","productos", "id = $id", "");
	$precio = $filas[0][0];
	$descuento = $filas[0][1];
	if ($tipo == 'normal')
	{
		$valor = $precio;
	}
	else if ($tipo == 'oferta')
	{
		if ($descuento != 0 and $descuento > 0)
		{
			$valor = round($precio*(1-$descuento/100));
		}
		else
		{
			$valor = $precio;
		}	
	}
	return $valor;
}

function writeMiniCart() {
	if(!isset($_COOKIE[cart_alfa_cm])){
		setcookie("cart_alfa_cm", "", time() + (365 * 24 * 60 * 60), "/");
		} 
	$cart = $_COOKIE[cart_alfa_cm];
	if ($cart){
		// Parse the cart session variable
		$items = explode(',',$cart);
		$s = (count($items) > 1) ? 's':'';
		return count($items).' producto'.$s;
	}
	else
	{
		return ' Mis compras';
	}
}
function writeShoppingCart(){
	if(!isset($_COOKIE[cart_alfa_cm])){
		setcookie("cart_alfa_cm", "", time() + (365 * 24 * 60 * 60), "/");
		}
	$cart = $_COOKIE[cart_alfa_cm];
	if ($cart){
		// Parse the cart session variable
		$items = explode(',',$cart);
		$s = (count($items) > 1) ? 's':'';
		return '<p>Ud tiene <strong>'.count($items).' producto'.$s.'</strong> en su carro de compras:</p>';
	}
}



function get_total_price(){
	$total = 0;
	if(!isset($_COOKIE[cart_alfa_cm])){
		setcookie("cart_alfa_cm", "", time() + (365 * 24 * 60 * 60), "/");
		} 
	$cart = $_COOKIE[cart_alfa_cm];
	
	$items = explode(',',$cart);
	$contents = array();
	foreach ($items as $item) {
		$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
	}
	
	$i = 1;
	foreach ($contents as $prd_id=>$qty) {

		$precio_final = getPrecio($prd_id) * $qty;

		if(!tieneDescuento($prd_id)){
			$pd = consulta_bd("precio_cantidad","productos_detalles","id = $prd_id","");
			if($pd[0][0]){
				$precios_cantidad = consulta_bd("nombre, rango, descuento","precios_cantidades","rango <= $qty AND productos_detall_id = $prd_id","rango DESC");
				if($precios_cantidad){
					$pc = $precios_cantidad[0];
					$rango 			= $pc[1];
					$descuento 		= $pc[2];
					$valorUnitario 	= getPrecio($prd_id) - (getPrecio($prd_id) * ($descuento / 100));
					$precio_final 	= $valorUnitario * $qty;
				}
			}
		}



		$total += round($precio_final);
		$i++;
	}
	
	return $total;
}

function generateToken($oc, $monto) {    
	// generar token de forma aleatoria (sin ninguna relación al monto, la relación solo está en la bd)
	$token = md5(uniqid(microtime(), true));
	$insert = insert_bd('checkout_token', 'token, monto, oc', "'$token', $monto, '$oc'");
	
	if ($insert == true) {
		return $token;
	}else{
		return false;
	}
}

function save_in_mailchimp($oc, $donde){
	$mailchimp_module = opciones('mailchimp');
	if ($mailchimp_module == '1') {
		$list = consulta_bd('id_lista', 'mailchimp', "short_name = '$donde'", '');
		$cliente = consulta_bd('nombre, email', 'pedidos', "oc = '$oc'", '');

		$apikey = opciones('key_mailchimp');

		$n_cliente = explode(' ', $cliente[0][0]);
		$nombre = $n_cliente[0];
		$apellido = $n_cliente[1];

		$data['email'] = $cliente[0][1];
	  	$data['listId'] = $list[0][0];
	  	$data['nombre'] = $nombre;
	  	$data['apellido'] = $apellido;

	  	$mcc = new MailChimpClient($apikey);

	  	$mcc->subscribe($data);

	  	save_all_mailchimp($cliente[0][1], $nombre, $apellido);
	}
}

function save_all_mailchimp($email, $nombre, $apellido){
	$list = consulta_bd('id_lista', 'mailchimp', "short_name = 'todos_mail'", '');
	$apikey = opciones('key_mailchimp');

	$data['email'] = $email;
  	$data['listId'] = $exito_list[0][0];
  	$data['nombre'] = $nombre;
  	$data['apellido'] = $apellido;

  	$mcc = new MailChimpClient($apikey);

  	$mcc->subscribe($data);
}

function descuentoBy($cod){
	global $db;

	$session_correo = $_SESSION['correo'];

	/* Consultamos si el codigo de descuento es válido */
	$fecha = date('Ymd');
	$consulta = consulta_bd('id, valor, porcentaje, codigo, descuento_opcion_id,donde_el_descuento, cliente_id', 'codigo_descuento',"codigo = '{$cod}' COLLATE utf8_bin and activo = 1 and $fecha >= fecha_desde and $fecha <= fecha_hasta and (cantidad - usados > 0)", "");

	/* Si es válido, seguimos con el proceso de cálculo */
	if (is_array($consulta)) {

		if ($consulta[0][6] == 0) {
			$whatfor = $consulta[0][4]; // A qué se le asigna el descuento (Marca[1], Categorías[2], Subcategorías[3]).
			$id = $consulta[0][5]; // Nombre de la marca, categoría, subcategoría.
			$condition;
			if ($whatfor == 1) {
				$dev = consulta_bd('id, nombre', 'marcas', "", "");
				$tbl = 'marcas';
				$condition = 'and p.marca_id = ';
			}elseif($whatfor == 2){
				$dev = consulta_bd('id, nombre', 'categorias', "", "");
				$tbl = 'categorias';
				$condition = 'and cp.categoria_id = ';
			}else if($whatfor == 3){
				$dev = consulta_bd('id, nombre', 'subcategorias', "", "");
				$tbl = 'subcategorias';
				$condition = 'and cp.subcategoria_id = ';
			}else{
				$condition = '';
			}

			$id_dev = 0;
			for ($i=0; $i < sizeof($dev); $i++) { 
				$nom_compare = url_amigables($dev[$i][1]);
				if ($id == $dev[$i][0]) {
					$id_dev = $dev[$i][0];
				}
			}

			if ($condition != ''){
				$condition .= $id_dev;
			}

			/* Si el descuento es por porcentaje o por valor */
			$porcentaje = ($consulta[0][2] > 0) ? true : false;

			$cart = $_COOKIE['cart_alfa_cm'];
			if ($cart) {
				$items = explode(',',$cart);
				$contents = array();
				foreach ($items as $item) {
					$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
				}
				$output[] = '';

				$valorProducto = 0;
				$encontrados = 0;
				$no_descuento = 0;

				/* Recorremos los productos que tiene el carro */
				foreach ($contents as $prd_id=>$qty) {
					$campos = '';
					$from = '';
					if (isset($_COOKIE['usuario_id']) AND clienteLista($_COOKIE['usuario_id'])) {
						$campos = 'lpp.precio, lpp.descuento';
						$from = ' JOIN lista_de_precio_productos lpp ON lpp.productos_detalle_id = pd.id';
					}else{
						$campos = 'pd.precio, pd.descuento';
					}

					$productos = consulta_bd("p.id, pd.precio, pd.descuento","productos p JOIN productos_detalles pd ON p.id = pd.producto_id{$from}","pd.id = $prd_id","");
					
					$valorProducto = (($productos[0][2] != 0) ? $productos[0][2] : $productos[0][1]) * $qty;
					$valorTotal += (($productos[0][2] != 0) ? $productos[0][2] : $productos[0][1]) * $qty;

					/* Consulta que devuelve el id del producto si es que cumple la condición  */
					$productosDescuento = consulta_bd('p.id', 'productos p join lineas_productos cp on cp.producto_id = p.id', "p.id = {$productos[0][0]} {$condition} group by p.id","");

					if (is_array($productosDescuento)) { // Si el producto pertenece a la condición del descuento
						$encontrados += $qty;
						$valProductosEncontrados += $valorProducto;

						if ($porcentaje) {
							if ($consulta[0][2] > $valProductosEncontrados) {
								$calculoFinalValor = $valProductosEncontrados;
								$resultado = false;
							}else{
								$muestraDesc = number_format($valProductosEncontrados, 0, ',', '.') . ' - ' .$consulta[0][2].'%';
								$calculoFinalValor = ($valProductosEncontrados * $consulta[0][2] / 100);
							}
							
						}else{
							if ($consulta[0][1] > $valProductosEncontrados) {
								$calculoFinalValor = $valProductosEncontrados;
								$resultado = false;
							}else{
								$muestraDesc = number_format($valProductosEncontrados, 0, ',', '.') . ' - $' .$consulta[0][1];
								$calculoFinalValor = $consulta[0][1];
							}	
						}
					}else{ 
						$no_descuento += $valorProducto;
					}
				} // End foreach

				$total = $calculoFinalValor + $no_descuento;
					
			}
			if ($encontrados > 0) {
				$resultado = true;
			}else{
				$resultado = false;
			}	
		}else{
			// Si el código existe pero tiene a un usuario asociado
			$tieneAssoc = consulta_bd('cd.cliente_id, cd.codigo, c.email', 'codigo_descuento cd join clientes c on c.id = cd.cliente_id', "c.email = '{$_SESSION['correo']}'", '');

			if (is_array($tieneAssoc) > 0) {

				// Recorremos los descuentos que tiene asociado el cliente
				foreach ($tieneAssoc as $codigo) {

					if ($codigo[1] == $cod) {
						/* Si el descuento es por porcentaje o por valor */
						$porcentaje = ($consulta[0][2] > 0) ? true : false;

						// Trabajamos el carro activo
						$cart = $_COOKIE['cart_alfa_cm'];
						if ($cart) {
							$items = explode(',',$cart);
							$contents = array();
							foreach ($items as $item) {
								$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
							}
							$output[] = '';

							$valorProducto = 0;
							$encontrados = 0;
							$no_descuento = 0;

							$contador_ofertas = 0;
							$cont_general = 0;

							/* Recorremos los productos que tiene el carro */
							foreach ($contents as $prd_id=>$qty) {
								$campos = '';
								$from = '';
								if (isset($_COOKIE['usuario_id']) AND clienteLista($_COOKIE['usuario_id'])) {
									$campos = 'lpp.precio, lpp.descuento';
									$from = ' JOIN lista_de_precio_productos lpp ON lpp.productos_detalle_id = pd.id';
								}else{
									$campos = 'pd.precio, pd.descuento';
								}

								$productos = consulta_bd("p.id, pd.precio, pd.descuento","productos p JOIN productos_detalles pd ON p.id = pd.producto_id{$from}","pd.id = $prd_id","");

								if ($productos[0][2] > 0) {
									$valorTotal += 0;
									$contador_ofertas++;
								}else{
									$valorTotal += (($productos[0][2] != 0) ? $productos[0][2] : $productos[0][1]) * $qty;
								}
								$cont_general++;
								
							} // End foreach carro

							if ($porcentaje) {
								$calculoFinalValor = ($valorTotal * $consulta[0][2] / 100);
							}else{
								$calculoFinalValor = $consulta[0][1];
							} // End if porcentaje

							if ($cont_general == 1) {
								if ($contador_ofertas > 0) {
									$resultado = false;
								}else{
									$resultado = true;
								}
							}else{
								$resultado = true;
							}
							// Si encuentra el código rompo el ciclo.
							break;
						}else{
							$resultado = false;
						}
					} // End if codigo == cod

				} // End foreach tieneAssoc

			}else{ // Else tieneAssoc

				$resultado = false;

			}
		}
		
	}else{ /* Else ---> if cont > 0 */
		$resultado = false;
	}/* End if cont > 0 */

	if ($resultado == true) {
		if (validarMontoDescuento($calculoFinalValor)) {
			$_SESSION['codigo_descuento']['valor'] = $calculoFinalValor;
			$_SESSION['codigo_descuento']['codigo'] = $cod;
			return 1;
		}else{
			unset($_SESSION['codigo_descuento']);
			return 0;
		}	
	}else{
		unset($_SESSION['codigo_descuento']);
		return 0;
	}
}

function validarMontoDescuento($valor){
	$precio_carro = (int)get_total_price();

	if ($precio_carro < (int)$valor || $precio_carro == (int)$valor) {
		return false;
	}else{
		return true;
	}
}

/* Retorna si un producto pertenece al cyberday
Parámetro => id producto madre */
function is_cyber_product($id){
	$sql = consulta_bd('p.cyber, pd.precio_cyber','productos p join productos_detalles pd on pd.producto_id = p.id', "p.id = $id", '');

	if ($sql[0][0] == 1 AND $sql[0][1] > 0) {
		return true;
	}else{
		return false;
	}
}

// Retorna true si no encuentra al usuario en la tabla primera_compra.
// True: Enviar codigo, False: No enviar codigo
function enviarCodigo($oc){
	$usuario = consulta_bd('cliente_id', 'pedidos', "oc = '{$oc}'", '');
	$id_usuario = $usuario[0][0];

	$tieneDescuento = consulta_bd('cliente_id', 'first_buy', "cliente_id = {$id_usuario}");
	
	if (!is_array($tieneDescuento)) {
		return true;
	}else{
		return false;
	}

	if ($cont < 1) {
		return true;
	}else{
		return false;
	}
}

function enviarCodigoDescuento($oc, $notification){

	// Consulto si ya tiene un codigo de primera compra
	$consulta_pc = consulta_bd("codigo", "first_buy", "oc = '{$oc}'" , "");
	if ($consulta_pc[0][0] == '' OR $consulta_pc[0][0] == NULL) {
		// Si no tiene un código de descuento, se le crea uno (Notificación 1)
		$codigo_desc = explode("_", $oc);
		$codigo = "COD_".$codigo_desc[1];

		$hoy = date('Y-m-d'); // Fecha desde que se envía el código (HOY)

		update_bd("first_buy", "codigo = '{$codigo}', fecha = '{$hoy}'", "oc = '{$oc}'");

		$id_cliente = consulta_bd('c.id', 'clientes c join pedidos p on c.email = p.email', "p.oc = '{$oc}'");

		// +3 meses de duración
	    $nuevaFecha = strtotime('+3 month' , strtotime($hoy));

	    $hasta = date('Y-m-d', $nuevaFecha); // Fecha hasta

	    // y le asignamos un codigo de descuento 
		insert_bd('codigo_descuento', 'cliente_id, codigo, porcentaje, activo, oc, fecha_desde, fecha_hasta, cantidad, descuento_opcion_id, donde_el_descuento', "{$id_cliente[0][0]},'$codigo', 10, 1, '$oc', '$hoy', '$hasta', 1, 4, 'Primera compra'");

	}else{
		$codigo = $consulta_pc[0][0];
	}

	$nombre_sitio = opciones("nombre_cliente");
    $nombre_corto = opciones("dominio");
    $noreply = "no-reply@".$nombre_corto;
    $url_sitio="https://".$nombre_corto;
    $logo = opciones("logo_mail");
    $correo_venta = opciones("correo_venta");
	$color_logo = opciones("color_logo");

	$datos_cliente = consulta_bd("nombre,email,id,direccion, cliente_id, cliente_id","pedidos","oc='$oc'","");
    $nombre_cliente = $datos_cliente[0][0];
    $email_cliente = $datos_cliente[0][1];
    $id_cliente = $datos_cliente[0][5];
	
    /*$header = "From: ".$nombre_sitio." <".$noreply.">\nReply-To:".$noreply."\n";
    $header .= "X-Mailer:PHP/".phpversion()."\n";
    $header .= "Mime-Version: 1.0\n";
    $header .= "Content-Type: text/html";*/

    if ($notification == 1) {
    	$asunto = "Código descuento para tu próxima compra";
    	$body_message = '<p>Muchas gracias por su preferencia. Adjuntamos un código por un 10% de descuento. Con él su próxima compra en nuestro sitio web será más conveniente. Para acceder al descuento, usted simplemente debe ingresar en nuestro sitio web <a href="'.$nombre_corto.'">'.$nombre_sitio.'</a> y seguir los pasos que el sistema propone, al final del proceso de compra y antes de pagar ingrese el código indicado.<br />
									
			<br />Recuerda que el código tiene una vigencia de 3 meses a partir de hoy.</p>
			<h2><strong>Código de descuento: '.$codigo.'</strong></h2>';
    }else{
    	$asunto = "Recuerda que tienes un descuento para tu próxima compra";
    	$body_message = '<p>Recuerda que tienes un código de descuento por un 10% en tú próxima compra. Para acceder al descuento, usted simplemente debe ingresar en nuestro sitio web <a href="https://www.mercadojardin.cl">mercadojardin.cl</a> y seguir los pasos que el sistema propone, al final del proceso de compra y antes de pagar ingrese el código indicado.<br />

			<h2><strong>Código de descuento: '.$codigo.'</strong></h2>';
    }
	
    $msg2 = '
    <html>
        <head>
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel="stylesheet">
        <title>'.$nombre_sitio.'</title>
		<style type="text/css">
				p, ul, a { 
					color:#666; 
					font-family: "Open Sans", sans-serif;
					background-color: #ffffff; 
					font-weight:300;
				}
				strong{
					font-weight:600;
				}
				a {
					color:#666;
				}
			</style>
        </head>
        <body style="background:#f9f9f9;">
			<div style="background:#fff; width:86%; margin-left:5%; padding-left:2%; padding-right:2%; border-top:solid 4px '.$color_logo.'; border-bottom:solid 4px '.$color_logo.'; font-family: Open Sans, sans-serif; padding-bottom: 20px;">
            
					<table cellspacing="0" cellpadding="0" border="0" width="100%" style="margin-top:20px;margin-bottom:10px;">
						<tr>
							<th align="left" width="50%">
								<p>
									<a href="'.$url_sitio.'" style="color:#8CC63F;">
										<img src="'.$logo.'" alt="'.$logo.'" border="0" width="200"/>
									</a>
								</p>
							</th>
						</tr>
					</table>
					<br/><br/>
                        
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tr>
                                <td valign="top">
                                   <p><strong>Estimado '.$nombre_cliente.':</strong></p>'.
									$body_message
									.'<p></p>
									<p>Muchas gracias<br /> Atte,</p>
									<p><strong>Equipo de '.$nombre_sitio.'</strong></p>
                                </td>
                            </tr>
                        </table>
            </div>
        </body>
    </html>
	';

	$mail = new PHPMailer;
    $mail->isSMTP();
    $mail->SMTPDebug = 0;
    $mail->Debugoutput = 'html';
    $mail->Host = opciones("Host");
	$mail->Port = opciones("Port");
	$mail->SMTPSecure = opciones("SMTPSecure");
	$mail->SMTPAuth = true;
	$mail->Username = opciones("Username");
	$mail->Password = opciones("Password");
	
    $mail->setFrom(opciones("setFrom_mail"), opciones("nombre_setFrom_mail"));
    //Set an alternative reply-to address

    $mail->addAddress($email_cliente, $nombre_cliente);
    $mail->Subject = $asunto;
    $mail->msgHTML($msg2);
    $mail->AltBody = $msg2;
    $mail->CharSet = 'UTF-8';
    //send the message, check for errors
    if (!$mail->send()) {
        return "Mailer Error: " . $mail->ErrorInfo;
    } else {
        return true;
    }
}


function getIMG($pd, $opc = null){
	$producto 	= consulta_bd("p.thumbs","productos_detalles pd, productos p","p.id = pd.producto_id AND pd.id = $pd","");
	if($opc == 'ficha'){
		if ($producto[0][0]){
			$return = imagen('imagenes/productos/',$producto[0][0]);
		}else{
			$return = imagen('imagenes/productos/','sinFoto.jpg');
		}
	}else{
		if($producto[0][0]){
			if(file_exists(imagen('imagenes/productos/thumbs/thumbs_',$producto[0][0]))){
				$return = imagen('imagenes/productos/thumbs/thumbs_',$producto[0][0]);
			}else{
				$return = imagen('imagenes/productos/',$producto[0][0]);
			}
		}else{
			$return = imagen('imagenes/productos/thumbs/thumbs_','sinFoto.jpg');
		}
	}
	return $return;
}
function tieneDescuentoXCant($pd){
	$row = consulta_bd("precio_cantidad","productos_detalles","id = $pd","");
	if($row[0][0]) return true;
	return false;
}
function getLinea($id){
	if($id){
		$row = consulta_bd("nombre","lineas","id = $id","");
		return $row[0][0];
	}
}
function getCategoria($id){
	if($id){
		$row = consulta_bd("nombre","categorias","id = $id","");
		return $row[0][0];
	}
}
function getSubcategoria($id){
	if($id){
		$row = consulta_bd("nombre","subcategorias","id = $id","");
		return $row[0][0];
	}
}
function getMarca($id){
	if($id){
		$row = consulta_bd("nombre","marcas","id = $id","");
		return $row[0][0];
	}
}
function getComuna($id){
	if($id){
		$row = consulta_bd("nombre","comunas","id = $id","");
		return $row[0][0];
	}
}
function getRegion($id){
	if($id){
		$row = consulta_bd("nombre","regiones","id = $id","");
		return $row[0][0];
	}
}
function encryptData($data){
	$encrypt = "********";
	$new = substr($data, 0, 5);
	$new .= $encrypt;
	return $new;
}
function getStock($id){
	if($id){
		$row = consulta_bd("stock","productos_detalles","id = $id","");
		if($row[0][0]){
			return "disponible";
		}else{
			return "no disponible";
		}
	}
}
function getStockNum($id){
	if($id){
		$row = consulta_bd("stock","productos_detalles","id = $id","");
		return $row[0][0];
	}
}

function hasCatLin($lin){
	if($lin){
		$row = consulta_bd("id","categorias","linea_id = $lin AND publicado = 1","");
		return count($row[0][0]);
	}else{
		return 0;
	}
}
function hasSubCat($cat){
	if($cat){
		$row = consulta_bd("id","subcategorias","categoria_id = $cat AND publicado = 1","");
		return count($row[0][0]);
	}else{
		return 0;
	}
}
function cantPrdCat($cat, $nivel, $id_nivel){
	if($cat){
		switch($nivel){
			case 'linea':
				$row = consulta_bd("p.id","lineas_productos lp, productos p","lp.producto_id = p.id AND p.publicado = 1 AND lp.categoria_id = $cat AND lp.linea_id = $id_nivel GROUP BY p.id","p.id");
			break;
		}

		return count($row);
	}else{
		return 0;
	}
}
function cantPrdMar($marca, $nivel, $id_nivel, $lin_id){
	if($marca){
		switch($nivel){
			case 'linea':
				$row = consulta_bd("p.id","productos p, lineas_productos lp","lp.producto_id = p.id AND p.publicado = 1 AND p.marca_id = $marca AND lp.linea_id = $id_nivel GROUP BY p.id","p.id");
				break;
			
			case 'cat':
				$row = consulta_bd("p.id","productos p, lineas_productos lp","lp.producto_id = p.id AND p.publicado = 1 AND p.marca_id = $marca AND lp.categoria_id = $id_nivel AND lp.linea_id = $lin_id GROUP BY p.id","p.id");
				break;
			
			case 'bus':
				$row = consulta_bd("p.id","productos p, productos_detalles pd, marcas m","p.id = pd.producto_id AND m.id = p.marca_id AND m.id = $marca AND p.publicado = 1 AND (p.nombre like '%$id_nivel%' OR pd.nombre like '%$id_nivel%' OR pd.sku like '%$id_nivel%' OR m.nombre like '%$id_nivel%') GROUP BY p.id","p.id");
				break;

			case 'mar':
				$row = consulta_bd("p.id","productos p","p.publicado = 1 AND p.marca_id = $marca GROUP BY p.id","p.id");
				break;

			case 'oferta':
				$row = consulta_bd("p.id","productos p, productos_detalles pd","p.id = pd.producto_id AND p.publicado = 1 AND p.marca_id = $marca AND pd.descuento > 0 GROUP BY p.id","p.id");
				break;
		}
		
		return count($row);
	}else{
		return 0;
	}
}

function ShowAddCart(){

	if(!isset($_COOKIE['cart_alfa_cm'])){
        setcookie("cart_alfa_cm", "", time() + (365 * 24 * 60 * 60), "/");
	} 
    $cart = $_COOKIE['cart_alfa_cm'];
	$output[] = '';

	if($cart){
		
	
    $output[] = '
						<a href="javascript:cerrar()" class="cerrar"><i class="fas fa-times"></i></a>
        				<h3>Carro de compras</h3>
        				<div class="prods-add">';
        
			            $items = explode(',',$cart);
			            $contents = array();
			            foreach ($items as $item) {
			                $contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
			            }
			            $output[] = '';
			            $i = 1;
			            $total = 0;

			            foreach ($contents as $prd_id=>$qty) {
			                $producto = consulta_bd("pd.nombre, pd.sku, p.marca_id", "productos p, productos_detalles pd", "pd.id = $prd_id and pd.producto_id = p.id","");
			                $pd_nombre  = $producto[0][0];
			                $pd_sku     = $producto[0][1];
			                $p_marca_id = $producto[0][2];
			                $total      += getPrecio($prd_id) * $qty;

	            			$output[] = '
				                <div class="line">
				                    <div class="img">
				                        <img rel="thumbs" src="'.getIMG($prd_id).'">
				                    </div>
				                    <div class="detail">
				                        <div class="nombre">
				                            '.$pd_nombre.'
				                        </div>
				                        <div class="precio">
				                            $'.number_format(getPrecio($prd_id),0,',','.').'
				                        </div>
				                        <div class="cantidad" rel="'.$prd_id.'">';

				                        $masde10i = 'style="display:none"';
				                        if($qty > 10) $masde10i = 'style="display:block"';

				                        $output[] = '<input value="'.$qty.'" class="cant-in-cart" '.$masde10i.'/>';

				                        $masde10s = 'style="display:block"';
				                        if($qty > 10) $masde10s = 'style="display:none"';
				            $output[] = '
				                            <select class="cant-cart" '.$masde10s.'>';

				                            	for($x = 1;$x < 11;$x++){
				                            		if($x == $qty) $isQty = "selected";
				                            		else $isQty = "";
				                            		$output[] = '<option value="'.$x.'" '.$isQty.'>'.$x.'</option>';
				                            	}

				                            	$output[] = '<option value="11">más</option>';

							$output[] = '
				                            </select>
				                        </div>
				                        <a class="delete" onClick="eliminaItemShowAddCart('.$prd_id.');">
				                            Eliminar
				                        </a>
				                    </div>
				                </div>';
        				}
        			$output[] = '
        				</div>
				        <div class="foot">
				            <p>Los costos de envío se calcularán previo al pago</p>

				            <a href="envio-y-pago" class="btn">
				                Finalizar compra $'.number_format($total,0,',','.').'
				            </a>
							<div class="seguir" onClick="cerrar();">Seguir comprando</div>
						</div>
						<script src="js/agrega_desde_carro.js"></script>

    				';
		
		} else{
			$output[] = '<a href="javascript:cerrar()" class="cerrar"><i class="fas fa-times"></i></a>
        				<h3>Su carro esta vacio</h3>';
		}
	return join('',$output);
	
}

function totalPriceCel($comuna, $tipo_despacho){
	if(!isset($_COOKIE['cart_alfa_cm'])){
        setcookie("cart_alfa_cm", "", time() + (365 * 24 * 60 * 60), "/");
	}

	$cart = $_COOKIE['cart_alfa_cm'];
	if($cart){
		
	}

}

function ShowResumenCart($comuna, $email, $e, $tipo_despacho, $data_resumen = 0){

	if(!isset($_COOKIE['cart_alfa_cm'])){
        setcookie("cart_alfa_cm", "", time() + (365 * 24 * 60 * 60), "/");
	} 
    $cart = $_COOKIE['cart_alfa_cm'];
	if($cart){

	            $items = explode(',',$cart);

	            $contents = array();
	            foreach ($items as $item) {
	                $contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
	            }
				$output[] = '<div class="title">Resumen carro</div>';

    			$output[] = '<div class="productos">';
	            $i = 1;
	            $total = 0;

	            foreach ($contents as $prd_id=>$qty) {
	                $producto = consulta_bd("pd.nombre, pd.sku, p.marca_id", "productos p, productos_detalles pd", "pd.id = $prd_id and pd.producto_id = p.id","");
	                $pd_nombre  = $producto[0][0];
	                $pd_sku     = $producto[0][1];
	                $p_marca_id = $producto[0][2];
	                $total      += getPrecio($prd_id) * $qty;

        			$output[] = '
		                <div class="line">
		                    <div class="img">
		                        <img src="'.getIMG($prd_id).'">
		                        <div class="cant">'.$qty.'</div>
		                    </div>
		                    <div class="detail">
								<div class="nombre">
									'.$pd_nombre.'
								</div>';

								if($email){
									$output[] = '<a href="mi-carro?email='.$email.'&e='.$e.'" class="edit">Editar</a>';
								}else{	
									$output[] = '<a href="mi-carro" class="edit">Editar</a>';
								}

					$output[] = '	
							</div>
							<div class="precio">';
								if(tieneDescuento($prd_id)){
									$output[] = '
										<span>Antes $'.number_format(getPrecioNormal($prd_id),0,',','.').'</span>
									';
								}

					$output[] = '
								$'.number_format(getPrecio($prd_id),0,',','.').'
							</div>
							<a class="delete" onClick="eliminaItemShowResumenCart('.$prd_id.');">
	                            x
	                        </a>
		                </div>';
				}

				if (isset($_SESSION['codigo_descuento'])) {
					$input_validado = 'inputValidado'; 
					$disabled = 'disabled';
					$codigo = 'value="'.$_SESSION['codigo_descuento']['codigo'].'"';
				}else{
					$input_validado = '';
					$disabled = '';
					$codigo = '';
				}

				if ($data_resumen == 0) {
					$output[] = '<div class="content_descuento">
						<input name="codigo_descuento" class="codigo_descuento '.$input_validado.'" '.$codigo.' placeholder="Código de descuento" data-resumen="'.$data_resumen.'" '.$disabled.'>
						<a href="javascript:void(0)" class="btnCodigoDescuento" onclick="codigoDescuento()">Usar</a> 
					</div>';
				}

				if($_SESSION['codigo_descuento']){ 
					$descuento = $_SESSION['codigo_descuento']['valor'];
				}else{
					$descuento = 0;
				}

				$subtotal 	= $total;
				$total_neto = $subtotal - $descuento;
				$iva 		= round(($total_neto * 1.19) - $total_neto);

				$despacho 	= ($comuna) ? valorDespacho($comuna) : 0;

				if ($tipo_despacho == 1) {
					$por_pagar 	= (is_numeric($despacho['estandar'])) ? 0 : 1;
				}else{
					$por_pagar = 0;
				}
				
				$despacho   = ($tipo_despacho == 1) ? $despacho['estandar'] : $despacho['rapido'];
				$total 		= $total_neto + $iva + $despacho;


			$output[] = '
			
				</div>
				<div class="totales">
					<div class="valor">Subtotal: <span>$'.number_format($subtotal,0,',','.').'</span></div>';

					if ($descuento > 0) {
						$output[] = '
							<div class="valor">Descuento: <span>- $'.number_format($descuento,0,',','.').'</span></div>';
					}else{
						$output[] = '
							<div class="valor">Descuento: <span>$'.number_format($descuento,0,',','.').'</span></div>';
					}

					$output[] = '
					<div class="valor">IVA: <span>$'.number_format($iva,0,',','.').'</span></div>';

					if ($comuna > 0) {
						if ($por_pagar == 1) {
							$output[] = '<div class="valor">Despacho: <span>Por pagar</span></div>';
						}else{
							$output[] = '<div class="valor">Despacho: <span>$'.number_format($despacho,0,',','.').'</span></div>';
						}
					}else{
						$output[] = '<div class="valor">Despacho: <span>$0</span></div>';
					}

					$output[] = '
					<div class="valor total">TOTAL<span>$'.number_format($total,0,',','.').'</span></div>
				</div>
			';
			return join('',$output);
		
	
		} else {
			$output[] = '<h1>Su carro esta vacio</h1>
						 <a href="home"><div class="btn">seguir vitrineando</div></a>';
			return join('',$output);	
		}
		
}
function ShowTotalesCart($comuna_id){

	if(!isset($_COOKIE['cart_alfa_cm'])){
        setcookie("cart_alfa_cm", "", time() + (365 * 24 * 60 * 60), "/");
	} 
    $cart = $_COOKIE['cart_alfa_cm'];
	$output[] = '';

    $output[] = '';

	            $items = explode(',',$cart);
	            $contents = array();
	            foreach ($items as $item) {
	                $contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
	            }
	            $output[] = '';
	            $i = 1;
	            $total = 0;

	            foreach ($contents as $prd_id=>$qty) {
	                $total      += getPrecio($prd_id) * $qty;
				}

				$iva 		= $total - ($total / 1.19);
				$subtotal 	= $total;

				if($_SESSION['codigo_descuento']){ 

					$codigo = $_SESSION['codigo_descuento'];
					$row = consulta_bd("id, codigo, valor, porcentaje","codigo_descuento","codigo = '$codigo'","");
					$valor 			= $row[0][2];
					$porcentaje 	= $row[0][3];


					if($porcentaje > 0){
						$nPorcentaje 	= $porcentaje/100;
						$descuento 		= round($nPorcentaje*$subtotal);
					}else if($valor > 0){
						$descuento 	= $valor;
					}else{	
						$descuento 	= 0;
					}

				}else{
					$descuento = 0;
				}

				$subtotal 	= $subtotal;
				$total_neto = $subtotal - $descuento;
				$iva 		= $total_neto * 0.19;

				$despacho 	= valorDespacho($comuna_id);
				$total 		= $total_neto + $iva + $despacho['valor'];


			$output[] = '
				<div class="totales">
					<div class="valor">Subtotal: <span>$'.number_format($subtotal,0,',','.').'</span></div>';

					if($descuento){
						$output[] = '
						<div class="valor">Descuento: <span>- $'.number_format($descuento,0,',','.').'</span></div>
						<div class="valor">Total neto: <span>$'.number_format($total_neto,0,',','.').'</span></div>
						';

					}
					
					$output[] = '
					<div class="valor">IVA: <span>$'.number_format($iva,0,',','.').'</span></div>';

					if($despacho['valor']){
						$output[] = '<div class="valor">Envío: <span>'.number_format($despacho['valor'],0,',','.').'</span></div>';
					}else{
						$output[] = '<div class="valor">Envío: <span>$'.$despacho['tipo_envio'].'</span></div>';
					}

					$output[] = '
					<div class="valor total">TOTAL<span>$'.number_format($total,0,',','.').'</span></div>
				</div>

			';
		
	return join('',$output);	
}

function estadoDelPedido($estado, $medio_de_pago){

	$row = consulta_bd("nombre","estados","id = $estado","");

	switch ($estado) {
		case '1':
				if($medio_de_pago == 'transferencia'){
					$return = 'Transferencia pendiente';
				}else{
					$return = 'Transacción abandonada';
				}
			break;
		case '2':
				if($medio_de_pago == 'transferencia'){
					$return = 'Transferencia exitosa';
				}else{
					$return = 'Transacción exitosa';
				}
			break;
		case '3':
				$return = 'Transacción rechazada';
			break;
	}
	return $return;
}



function enviarMensajeContacto($nombre_cl, $email_cl, $telefono_cl, $comentario_cl){

	$nombre_sitio 	= opciones("nombre_cliente");
    $nombre_corto 	= opciones("dominio");
    $noreply 		= "no-reply@".$nombre_corto;
    $url_sitio 		= "https://".$nombre_corto;
    $logo 			= opciones("logo_mail");
    $correo_venta 	= opciones("correo_venta");
	$color_logo 	= opciones("color_logo");
	
	
	$fontFamily = "font-family: Open Sans, sans-serif;";
        
    $asunto = "Mensaje de contacto de ".$nombre_corto;
    
    $msg = '
		<!DOCTYPE html>
		<html>
		<head>
			<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel="stylesheet">
		    <title>ACO</title>
			<style type="text/css">
					body{
						max-width: 600px;
						margin: 0 auto;
						background: #fff;
						font-family: "Open Sans", sans-serif;
						font-weight:300;
						color:#666; 
					}
					p, ul, a { 
						font-family: "Open Sans", sans-serif;
						font-weight:300;
						color:#666; 	
					}
					strong{
						font-weight:600;
					}
					a {
						color:#666;
					}
					section{
					    background: #fff;
			    		border-left: 25px solid #000;
			    		border-right: 25px solid #000;
			    		margin: 0;
			    		padding: 20px;
					}
				</style>
		    </head>
		</head>
		<body>
			<header>
				<a href="'.$url_sitio.'"><img src="'.$logo.'" width="140px"></a>
			</header>
			<section style="background: #fff; width: 510px; margin: 0 auto;">
				<p>
					Se ha enviado el siguiente contacto desde la web:
				</p>

				<p>
					Rogamos contactarse con el cliente lo antes posible.
				</p>
				
				<table cellspacing="0" cellpadding="0" border="0" width="600px">
				
					<tr>
						<td width="100%">
							<ul>
								<li>Nombre: '.$nombre_cl.'</li>
								<li>Email: '.$email_cl.'</li>
								<li>Teléfono: '.$telefono_cl.'</li>
							</ul>
							<p>
								'.$comentario_cl.'
							</p>
						</td>
					</tr>
		
				</table>
		
				<p>&nbsp;</p>
				<p style="text-align: center;"><strong>Equipo de ACO.cl</strong></p>
		
			</section>
			<footer style="background: url('.$url_sitio.'img/footer-mail.png) top center no-repeat; height: 84px; width: 600px; margin: 0 auto;">
				<a href="#">&nbsp;</a>
			</footer>
		</body>
		</html>
	
	';
	
	//envio la notificacion al correo que me indican en la variable
	$mail = new PHPMailer;
	$mail->isSMTP();
	$mail->SMTPDebug = 0;
	$mail->Debugoutput = 'html';
	$mail->Host = opciones("Host");
	$mail->Port = opciones("Port");
	$mail->SMTPSecure = opciones("SMTPSecure");
	$mail->SMTPAuth = true;
	$mail->Username = opciones("Username");
	$mail->Password = opciones("Password");
	$mail->setFrom(opciones("setFrom_mail"), opciones("nombre_setFrom_mail"));
	//envio el correo al mail que envio por variable
	$mail->addAddress("obarrera@moldeable.com", "Moldeable");

	$mail->Subject = $asunto;
	$mail->msgHTML($msg);
	$mail->AltBody = $msg;
	$mail->CharSet = 'UTF-8';
	if (!$mail->send()) {
		//return "Mailer Error: " . $mail->ErrorInfo;
		return 2;
	} else {
		return 1;
	}

}

function getProduct($id){
	$campos = 'p.nombre, p.cadena_medidas, pd.stock, pd.cadena_ancho, pd.cadena_perfil, pd.cadena_aro, p.descripcion_corta, p.descripcion, p.ficha_tecnica, l.nombre, c.nombre, sb.nombre, p.id, pd.id, p.solo_cotizar, l.id, c.id, sb.id, pd.sku, m.nombre, p.caracteristicas, p.garantia';
	$from = 'productos p JOIN productos_detalles pd ON p.id = pd.producto_id JOIN lineas_productos lp ON lp.producto_id = p.id JOIN lineas l ON l.id = lp.linea_id JOIN categorias c ON c.id = lp.categoria_id JOIN subcategorias sb ON sb.id = lp.subcategoria_id JOIN marcas m ON m.id = p.marca_id';

	if (isset($_COOKIE['usuario_id']) AND clienteLista($_COOKIE['usuario_id'])) {
		$campos .= ",lpp.precio, lpp.descuento";
		$from .= " JOIN lista_de_precio_productos lpp ON lpp.productos_detalle_id = pd.id";
	}else{
		$campos .= ',pd.precio, pd.descuento';
	}

	$query = consulta_bd($campos, $from, "p.id = $id and pd.principal = 1", '');

	if (is_array($query)) {

		$imagenes = consulta_bd('archivo', 'img_productos', "producto_id = $id", 'posicion asc');
		$campos = 'p.nombre, p.cadena_medidas, pd.stock, pd.cadena_ancho, pd.cadena_perfil, pd.cadena_aro, p.descripcion_corta, p.descripcion, p.ficha_tecnica, l.nombre, c.nombre, sb.nombre, p.id, pd.id, p.solo_cotizar, l.id, c.id, sb.id, pd.sku, m.nombre, p.caracteristicas, p.garantia';

		$out['datos']['id'] 				= $query[0][12];
		$out['datos']['hijo_id'] 			= $query[0][13];
		$out['datos']['nombre'] 			= $query[0][0];
		$out['datos']['producto_cadena'] 	= $query[0][1];
		$out['datos']['precio'] 			= $query[0][22];
		$out['datos']['descuento'] 			= $query[0][23];
		$out['datos']['stock'] 				= $query[0][2];
		$out['datos']['cadena_ancho'] 		= $query[0][3];
		$out['datos']['cadena_perfil'] 		= $query[0][4];
		$out['datos']['cadena_aro'] 		= $query[0][5];
		$out['datos']['descripcion_corta'] 	= $query[0][6];
		$out['datos']['descripcion'] 		= $query[0][7];
		$out['datos']['ficha_tecnica'] 		= $query[0][8];
		$out['datos']['solo_cotizar'] 		= $query[0][14];
		$out['datos']['sku'] 				= $query[0][18];
		$out['datos']['marca'] 				= $query[0][19];
		$out['datos']['caracteristicas'] 	= $query[0][20];
		$out['datos']['garantia'] 			= $query[0][21];

		$out['breadcrumbs']['linea'] 		= $query[0][9];
		$out['breadcrumbs']['categoria'] 	= $query[0][10];
		$out['breadcrumbs']['subcategoria'] = $query[0][11];
		$out['breadcrumbs']['linea_id'] 	= $query[0][15];
		$out['breadcrumbs']['categoria_id'] 	= $query[0][16];
		$out['breadcrumbs']['subcategoria_id'] 	= $query[0][17];

		if (is_array($imagenes)) {
			$i = 0;
			foreach ($imagenes as $imgs) {
				$out['imagenes'][$i] = $imgs[0];
				$i++;
			}
		}else{
			$out['imagenes'] == false;
		}

		return $out;
	}

	return false;

}

function getCarro(){
	if(!isset($_COOKIE["cart_alfa_cm"])){
		setcookie("cart_alfa_cm", "$cart", time() + (365 * 24 * 60 * 60), "/");
	}
	$cart = $_COOKIE["cart_alfa_cm"];
	if ($cart) {
		$items = explode(',',$cart);
		$contents = array();
		foreach ($items as $item) {
			$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
		}
		$i = 0;
		$total = 0;
		foreach ($contents as $prd_id=>$qty) {
			$campos = '';
			if (isset($_COOKIE['usuario_id']) AND clienteLista($_COOKIE['usuario_id'])) {
				$campos = 'lpp.precio, lpp.descuento';
				$from = ' JOIN lista_de_precio_productos lpp ON lpp.productos_detalle_id = pd.id';
			}else{
				$campos = 'pd.precio, pd.descuento';
			}
			$producto = consulta_bd("p.id, p.nombre, {$campos}, p.cadena_medidas, pd.cadena_ancho, pd.cadena_perfil, pd.cadena_aro, p.thumbs, pd.id", "productos p JOIN productos_detalles pd ON p.id = pd.producto_id{$from}", "pd.id = $prd_id", '');

			$resp[$i]['producto_id'] 	= $producto[0][0];
			$resp[$i]['nombre'] 		= $producto[0][1];
			$resp[$i]['precio'] 		= $producto[0][2];
			$resp[$i]['descuento'] 		= $producto[0][3];
			$resp[$i]['es_cadena'] 		= $producto[0][4];
			$resp[$i]['ancho'] 			= $producto[0][5];
			$resp[$i]['perfil'] 		= $producto[0][6];
			$resp[$i]['aro'] 			= $producto[0][7];
			$resp[$i]['imagen'] 		= $producto[0][8];
			$resp[$i]['cantidad']		= $qty;
			$resp[$i]['id_hijo']		= $producto[0][9];

			$i++;

		}
	}else{
		return false;
	}

	return $resp;

}

function setFechaPedido($fecha){
	$fecha 	= explode('-', $fecha);
	$anio 	= $fecha[0];
	$mes 	= $fecha[1];
	$dia 	= $fecha[2];

	switch ($mes) {
		case '01':
			$mes = 'Enero';
			break;
		case '02':
			$mes = 'Febrero';
			break;
		case '03':
			$mes = 'Marzo';
			break;
		case '04':
			$mes = 'Abril';
			break;
		case '05':
			$mes = 'Mayo';
			break;
		case '06':
			$mes = 'Junio';
			break;
		case '07':
			$mes = 'Julio';
			break;
		case '08':
			$mes = 'Agosto';
			break;
		case '09':
			$mes = 'Septiembre';
			break;
		case '10':
			$mes = 'Octubre';
			break;
		case '11':
			$mes = 'Noviembre';
			break;
		case '12':
			$mes = 'Diciembre';
			break;
	}

	return $dia . ' de ' . $mes . ' de ' .$anio;

}

function qty_cotizacion(){
	$carro = json_decode($_COOKIE['carro_cotizacion'], true);
	$cantidad = 0;
	foreach ($carro as $item) {
		$cantidad += $item['qty'];
	}

	return $cantidad;
}

function getCarroCotizacion(){
	$carro = json_decode($_COOKIE['carro_cotizacion'], true);

	$i = 0;

	if ($carro) {
		foreach ($carro as $item) {
			$id = $item['id'];
			$qty = $item['qty'];

			$producto = consulta_bd('p.thumbs, p.nombre, p.cadena_medidas, pd.cadena_ancho, pd.cadena_perfil, pd.cadena_aro, pd.sku, pd.id', 'productos p JOIN productos_detalles pd ON p.id = pd.producto_id', "pd.id = $id", '');

			$out[$i]['id'] = $id;
			$out[$i]['nombre'] = $producto[0][1];
			$out[$i]['imagen'] = 'imagenes/productos/'.$producto[0][0];
			$out[$i]['cantidad'] = $qty;
			$out[$i]['cadena'] = $producto[0][2];
			$out[$i]['ancho'] = $producto[0][3];
			$out[$i]['perfil'] = $producto[0][4];
			$out[$i]['aro'] = $producto[0][5];
			$out[$i]['sku'] = $producto[0][6];

			$i++;
		}

		return $out;

	}else{
		return false;
	}
}

function clienteLista($cliente_id){
	$query = consulta_bd('lista_de_precio_id', 'clientes', "id = $cliente_id and es_empresa = 1 and lista_de_precio_id is not null and lista_de_precio_id != 0", '');
	if (is_array($query)) {
		$validarLista = consulta_bd('id', 'lista_de_precio_productos', "lista_de_precio_id = {$query[0][0]}", '');

		return (is_array($validarLista)) ? $query[0][0] : false;

	}
	return false;
}

function get_products($filtro){

	if (isset($filtro['max']) AND $filtro['max'] != false) {
		$campos = 'MAX(IF(pd.descuento > 0, pd.descuento, pd.precio))';
	}else{
		$campos = "DISTINCT(p.id), p.nombre, p.thumbs, l.nombre, pd.stock, pd.id, p.descripcion, m.nombre, pd.precio_cantidad, pd.precio_cyber, l.id, p.video, pd.sku, p.ficha_tecnica, IFNULL(pp.posicion, 999999) as posicion, p.descripcion_grilla, p.solo_cotizar";
	}
	
	$tablas = 'productos p 
	LEFT JOIN lineas_productos lp ON p.id = lp.producto_id 
	JOIN productos_detalles pd ON pd.producto_id = p.id 
	LEFT JOIN lineas l ON l.id = lp.linea_id 
	LEFT JOIN categorias c ON c.id = lp.categoria_id
	LEFT JOIN subcategorias sb ON sb.id = lp.subcategoria_id
	LEFT JOIN posicion_productos pp ON p.id = pp.producto_id 
	JOIN marcas m ON m.id = p.marca_id';
	$where = 'p.publicado = 1';

	if (isset($_COOKIE['usuario_id'])) {
		$clienteValidado = clienteLista($_COOKIE['usuario_id']);
		if ($clienteValidado) {
			$campos .= ", lpp.precio, lpp.descuento, min(IF(lpp.descuento > 0, lpp.descuento, lpp.precio)) as valorMenor, (case when lpp.descuento > '0' then lpp.descuento else lpp.precio end) as precio_calculado";
			$tablas .= " JOIN lista_de_precio_productos lpp ON lpp.productos_detalle_id = pd.id";
		}else{
			$campos .= ", pd.precio, pd.descuento, min(IF(pd.descuento > 0, pd.descuento, pd.precio)) as valorMenor, (case when pd.descuento > '0' then pd.descuento else pd.precio end) as precio_calculado";
		}
	}else{
		$campos .= ", pd.precio, pd.descuento, min(IF(pd.descuento > 0, pd.descuento, pd.precio)) as valorMenor, (case when pd.descuento > '0' then pd.descuento else pd.precio end) as precio_calculado";
	}

	if (isset($filtro['orden']) AND $filtro['orden'] != null AND $filtro['orden'] != '') {
		$last .= $filtro['orden'];
	}else if (isset($filtro['random']) AND $filtro['random'] != false) {
		$last .= '';
	}else{
		$last .= 'p.id desc';
	}

	if (isset($filtro['ficha']) AND $filtro['ficha'] != null AND $filtro['ficha'] != '') {
		$where .= ' AND p.id = ' . $filtro['ficha'];
	}

	if (isset($filtro['random']) AND $filtro['random'] != false AND $filtro['random'] != '') {
		$last .= ' RAND()';
	}

	// Limite normal
	if (isset($filtro['limite']) AND $filtro['limite'] != null AND $filtro['limite'] != '') {
		$last .= ' LIMIT '.$filtro['limite'];
	}

	// Límite para paginado
	if (isset($filtro['limit']) AND $filtro['limit'] != null AND $filtro['limit'] != '') {
		$last .= ' '.$filtro['limit'];
	}

	if (isset($filtro['linea']) AND $filtro['linea'] != null AND $filtro['linea'] != '') {
		$where .= ' AND pp.linea_id = ' . $filtro['linea'];
	}

	if (isset($filtro['categoria']) AND $filtro['categoria'] != null AND $filtro['categoria'] != '') {
		$where .= ' AND pp.categoria_id = ' . $filtro['categoria'];
	}

	if (isset($filtro['subcategoria']) AND $filtro['subcategoria'] != null AND $filtro['subcategoria'] != '') {
		$where .= " AND pp.subcategoria_id = {$filtro['subcategoria']}";
	}

	if (isset($filtro['destacados']) AND $filtro['destacados'] != null AND $filtro['destacados'] != false) {
		$where .= " AND p.recomendado = 1";
	}

	if (isset($filtro['recom_sup']) AND $filtro['recom_sup'] != null AND $filtro['recom_sup'] != false) {
		$where .= " AND p.recomendado_superior = 1";
	}

	if (isset($filtro['recom_inf']) AND $filtro['recom_inf'] != null AND $filtro['recom_inf'] != false) {
		$where .= " AND p.recomendado_inferior = 1";
	}

	if (isset($filtro['home']) AND $filtro['home'] != null AND $filtro['home'] != false) {
		$where .= " AND p.home = 1";
	}

	if (isset($filtro['id_ficha']) AND $filtro['id_ficha'] != null AND $filtro['id_ficha'] != '') {
		$where .= " AND p.id != {$filtro['id_ficha']}";
	}

	if (isset($filtro['rel']) AND $filtro['rel'] != null AND $filtro['rel'] != '') {
		$where .= " AND p.id != " . $filtro['rel'];
	}

	if ( (isset($filtro['ofertas']) AND $filtro['ofertas'] != false AND $filtro['ofertas'] != '' )) {
		$where .= " AND ( pd.descuento > 0 OR (pd.oferta_tiempo_activa = 1 AND pd.oferta_tiempo_hasta > NOW() ) )";
	}

	if (isset($filtro['marca']) AND $filtro['marca'] != NULL AND $filtro['marca'] != '') {
		$where .= " AND p.marca_id = $filtro[marca]";
	}

	if (isset($filtro['cantidad']) AND $filtro['cantidad'] != false AND $filtro['cantidad'] != '') {
		$where .= " AND pd.precio_cantidad = 1";
	}

	if (isset($filtro['ancho']) AND $filtro['ancho'] != false AND $filtro['ancho'] != '') {
		$where .= " AND pd.cadena_ancho = $filtro[ancho]";
	}

	if (isset($filtro['perfil']) AND $filtro['perfil'] != false AND $filtro['perfil'] != '') {
		$where .= " AND pd.cadena_perfil = $filtro[perfil]";
	}

	if (isset($filtro['aro']) AND $filtro['aro'] != false AND $filtro['aro'] != '') {
		$where .= " AND pd.cadena_aro = $filtro[aro]";
	}

	if (isset($filtro['solo_cotizar']) AND $filtro['solo_cotizar'] != false AND $filtro['solo_cotizar'] != '') {
		$where .= " AND p.solo_cotizar = 1";
	}

	$whereMarcas = '';
	if(isset($filtro['marcas']) AND $filtro['marcas'] != NULL AND $filtro['marcas'] != ''){
		$marcas_seleccionadas = $filtro['marcas'];
		$explode_marcas = explode('-', $marcas_seleccionadas);
		$contador_marcas = 0;

		$whereMarcas = ' AND (';
		foreach ($explode_marcas as $item_brand) {
			if ($contador_marcas == 0) {
				$whereMarcas .= "m.id = $item_brand";
			}else{
				$whereMarcas .= " OR m.id = $item_brand";
			}
			$contador_marcas++;
		}
		$whereMarcas .= ')';

	}

	$whereLineas = '';
	if (isset($filtro['lineas']) AND $filtro['lineas'] != NULL AND $filtro['lineas'] != '') {
		$lineas_seleccionadas = $filtro['lineas'];
		$explode_lineas = explode('-', $lineas_seleccionadas);
		$contador_lineas = 0;

		$whereLineas = ' AND (';
		foreach ($explode_lineas as $item_line) {
			if ($contador_lineas == 0) {
				$whereLineas .= "lp.linea_id = $item_line";
			}else{
				$whereLineas .= " OR lp.linea_id = $item_line";
			}
			$contador_lineas++;
		}
		$whereLineas .= ')';
	}

	$whereCategorias = '';
	if (isset($filtro['categorias']) AND $filtro['categorias'] != NULL AND $filtro['categorias'] != '') {
		$categorias_seleccionadas = $filtro['categorias'];
		$explode_categorias = explode('-', $categorias_seleccionadas);

		$contador_categorias = 0;

		$whereCategorias = ' AND (';
		foreach ($explode_categorias as $item_cat) {
			if ($contador_categorias == 0) {
				$whereCategorias .= "lp.categoria_id = $item_cat";
			}else{
				$whereCategorias .= " OR lp.categoria_id = $item_cat";
			}
			$contador_categorias++;
		}
		$whereCategorias .= ')';
	}

	$whereSubcategorias = '';
	if (isset($filtro['subcategorias']) AND $filtro['subcategorias'] != NULL AND $filtro['subcategorias'] != '') {
		$subcategorias_seleccionadas = $filtro['subcategorias'];
		$explode_subcategorias = explode('-', $subcategorias_seleccionadas);

		$contador_subcategorias = 0;

		$whereSubcategorias = ' AND (';
		foreach ($explode_subcategorias as $item_cat) {
			if ($contador_subcategorias == 0) {
				$whereSubcategorias .= "lp.subcategoria_id = $item_cat";
			}else{
				$whereSubcategorias .= " OR lp.subcategoria_id = $item_cat";
			}
			$contador_subcategorias++;
		}
		$whereSubcategorias .= ')';
	}

	$whereFiltros = '';
	if (isset($filtro['filtros']) AND $filtro['filtros'] != NULL AND $filtro['filtros'] != '') {

		$tablas .= ' JOIN filtro_productos fp ON fp.producto_id = p.id';

		$filtros_seleccionados = $filtro['filtros'];
		$explode_filtros = explode('-', $filtros_seleccionados);

		$contador_filtros = 0;

		$whereFiltros = ' AND (';
		foreach ($explode_filtros as $item_fil) {
			if ($contador_filtros == 0) {
				$whereFiltros .= "fp.filtros_subcategoria_id = $item_fil";
			}else{
				$whereFiltros .= " OR fp.filtros_subcategoria_id = $item_fil";
			}
			$contador_filtros++;
		}
		$whereFiltros .= ')';
	}

	if (isset($filtro['desde']) AND $filtro['desde'] != NULL AND $filtro['desde'] != '') {
		$having .= 'AND precio_calculado >= ' . $filtro['desde'];
	}

	if (isset($filtro['hasta']) AND $filtro['hasta'] != NULL AND $filtro['hasta'] != '') {
		$having .= ' AND precio_calculado <= ' . $filtro['hasta'];
	}

	if (isset($filtro['nuevos']) AND $filtro['nuevos'] != NULL AND $filtro['nuevos'] != false) {
		$where .= ' AND p.nuevos_productos = 1';
	}

	if (isset($filtro['busqueda']) AND $filtro['busqueda'] != null AND trim($filtro['busqueda']) != '') {
		$busqueda = $filtro['busqueda'];
		$where .= " AND (p.nombre LIKE '%$busqueda%' OR pd.sku LIKE '%$busqueda%' OR m.nombre LIKE '%$busqueda%' OR c.nombre LIKE '%$busqueda%' OR l.nombre LIKE '%$busqueda%')";
	}

	// return "SELECT $campos FROM $tablas WHERE $where $whereMarcas $whereLineas $whereCategorias $whereFiltros GROUP BY p.id HAVING MIN(IF(pd.descuento > 0, pd.descuento, pd.precio)) $having ORDER BY $last";

	/* FIN FILTROS */

	if ($having != null and $having != '') {
		$having = "HAVING " . $having;
	}
	if (isset($filtro['max']) AND $filtro['max'] != false) {
		$productos = consulta_bd($campos,$tablas,$where.$whereMarcas.$whereLineas.$whereCategorias.$whereFiltros.$whereSubcategorias." AND pd.principal = 1 GROUP BY p.id $having",$last);
	}else{
		$productos = consulta_bd($campos,$tablas,$where.$where_posicion.$whereMarcas.$whereLineas.$whereCategorias.$whereFiltros.$whereSubcategorias." GROUP BY p.id $having",$last);
	}

	// return "SELECT $campos FROM $tablas WHERE $where$where_posicion$whereMarcas$whereLineas$whereCategorias$whereFiltros$whereSubcategorias GROUP BY p.id $having ORDER BY $last";
	
	$index = 0;
	$count = 0;

	if (is_array($productos)) {
		if (isset($filtro['max']) AND $filtro['max'] != false) {
			$out['valor_max'] = $productos[0][0];
		}else{
			foreach ($productos as $aux) {
				$out['productos']['producto'][$index]['id_producto'] = $aux[0]; // Madre
				$out['productos']['producto'][$index]['id_hijo'] = $aux[5]; // Hijo
				$out['productos']['producto'][$index]['nombre'] = $aux[1];
				$out['productos']['producto'][$index]['imagen_grilla'] = ($aux[2] != NULL OR $aux[2] != '') ? 'imagenes/productos/'.$aux[2] : 'img/not-image.jpg';
				$out['productos']['producto'][$index]['precio'] = $aux[17];
				$out['productos']['producto'][$index]['descuento'] = $aux[18];
				$out['productos']['producto'][$index]['precio_cyber'] = $aux[9];
				// $out['productos']['producto'][$index]['porcentaje'] = (($aux[3] - $aux[4]) / $aux[3]) * 100;
				$out['productos']['producto'][$index]['stock'] = $aux[4];
				$out['productos']['producto'][$index]['descripcion'] = $aux[6];
				$out['productos']['producto'][$index]['nombre_seteado'] = url_amigables($aux[1]);
				$out['productos']['producto'][$index]['marca'] = $aux[7];
				$out['productos']['producto'][$index]['linea'] = $aux[3];
				$out['productos']['producto'][$index]['linea_id'] = $aux[10];
				$out['productos']['producto'][$index]['precio_cantidad'] = $aux[8];
				$out['productos']['producto'][$index]['video'] = $aux[11];
				$out['productos']['producto'][$index]['sku'] = $aux[12];
				$out['productos']['producto'][$index]['ficha_tecnica'] = $aux[13];
				$out['productos']['producto'][$index]['descripcion_grilla'] = $aux[15];
				$out['productos']['producto'][$index]['solo_cotizar'] = $aux[16];

				if ($out['productos']['producto'][$index]['imagen_grilla'] != 'img/sin-foto.jpg') {
					if (!file_exists('imagenes/productos/'.$aux[2])) {
						$out['productos']['producto'][$index]['imagen_grilla'] = 'img/sin-foto.jpg';

						$out['productos']['producto'][$index]['ruta_imagen'] = 'img/'; 
						$out['productos']['producto'][$index]['nombre_imagen'] = 'sin-foto.jpg';
					}else{
						$out['productos']['producto'][$index]['ruta_imagen'] = 'imagenes/productos/'; 
						$out['productos']['producto'][$index]['nombre_imagen'] = $aux[2];
					}
				}

				$index++;
				$count++;
			}
			$out['productos']['total'][] = $count;
		}
	}else{
		return false;
	}

	return $out;
}

function tiene_posicion($columna, $id, $producto_id){
	// return "SELECT posicion FROM posicion_productos WHERE $columna = $id AND producto_id = $producto_id AND posicion IS NOT NULL";
	$query = consulta_bd('posicion', "posicion_productos", "$columna = $id AND producto_id = $producto_id AND posicion IS NOT NULL", '');
	// Si no es arreglo lo elimino en el actions.php
	return is_array($query);
}

function getCategoriesWithSub($str){
	$explode_sub = explode('-', $str);
	$whereStr = '';

	foreach ($explode_sub as $item) {
		if ($whereStr == '') {
			$whereStr .= 'id = '. $item;
		}else{
			$whereStr .= ' OR id = ' . $item;
		}
	}

	$query = consulta_bd('categoria_id', 'subcategorias', "$whereStr", '');
	$i = 0;
	foreach ($query as $aux) {
		$out[$i] = $aux[0];
		$i++;
	}

	return $out;

}

function getSubcategoriesWithFilter($str){
	$explode_sub = explode('-', $str);
	$whereStr = '';

	foreach ($explode_sub as $item) {
		if ($whereStr == '') {
			$whereStr .= 'id = '. $item;
		}else{
			$whereStr .= ' OR id = ' . $item;
		}
	}

	$query = consulta_bd('subcategoria_id', 'filtros_subcategorias', "$whereStr", '');
	$i = 0;
	foreach ($query as $aux) {
		$out[$i] = $aux[0];
		$i++;
	}

	return $out;
}

function filtros_aplicados($subs, $filtro_desde, $filtro_hasta, $filtros){
	
	if ($subs != 0) {
		$explode_sub = explode('-', $subs);

		$index = 0;
		foreach ($explode_sub as $item) {
			$marca = consulta_bd('id, nombre', 'subcategorias', "id = $item", '');

			if (is_array($marca)) {
				$out['filtro_subcategorias'][$index]['id'] = $marca[0][0];
				$out['filtro_subcategorias'][$index]['nombre'] = $marca[0][1];

				$index++;
			}
		}
	}

	if ($filtros != 0) {
		$explode_filtros = explode('-', $filtros);
		$index = 0;
		foreach ($explode_filtros as $item) {
			
			$data_filtro = consulta_bd('id, nombre', 'filtros_subcategorias', "id = $item", '');

			if (is_array($data_filtro)) {
				$out['filtro_filtros'][$index]['id'] = $data_filtro[0][0];
				$out['filtro_filtros'][$index]['nombre'] = $data_filtro[0][1];

				$index++;
			}

		}
	}
	

	if ($filtro_hasta != 0) {
		$out['filtro_precios'][0] = 'Desde ' . $filtro_desde . ' hasta ' . $filtro_hasta;
	}

	return $out;
}

function check_carrier(){
	global $db;
	if(!isset($_COOKIE[cart_alfa_cm])){
		setcookie("cart_alfa_cm", "", time() + (365 * 24 * 60 * 60), "/");
		} 
	$cart = $_COOKIE[cart_alfa_cm];
	if ($cart) {
		$items = explode(',',$cart);
		$contents = array();
		foreach ($items as $item) {
			$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
		}
		
		$envio = 0;
		$i = 1;
		$peso = 0;
		
		foreach ($contents as $prd_id=>$qty) {
			$productos = consulta_bd("peso, ancho, alto, largo","productos_detalles","id = $prd_id","");

			$ancho = $productos[0][1];
			$alto = $productos[0][2];
			$largo = $productos[0][3];

			$volumen += round(((($ancho * $largo) * $alto) / 4000)) * $qty;

			$peso += $productos[0][0] * $qty;
			$i++;
		}
			
	} else {
		//$envio = 0;
	}

	if ($volumen < 100 AND $peso < 100) {
		$peso = ($peso > $volumen) ? $peso : $volumen;
	}elseif($volumen > 100 AND $peso < 100){
		$peso = $peso;
	}elseif($volumen < 100 AND $peso > 100){
		$peso = $peso;
	}elseif($volumen > 100 AND $peso > 100){
		$peso = $volumen;
	}

	if ($peso < 20) {
		return 'CHX';
	}else{
		return 'BLX';
	}
}

?>