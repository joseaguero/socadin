<?php

/********************************************************\
|  Moldeable CMS - generador de fecha.		             |
|  Fecha Modificación: 25/06/2012		                 |
|  Todos los derechos reservados © Moldeable S.A. 2012   |
|  Prohibida su copia parcial o total  			         |
|  http://www.moldeable.com/                             |
\********************************************************/

date_default_timezone_set('America/Santiago');

//Devuelve fecha
function fecha()
{
			if (date("M")=='Aug') 
			{
				$mes= Agosto;
			}else if (date("M")=='Sep') {
				$mes = Septiembre;
			}else if (date("M")=='Oct') {
				$mes = Octubre;
			}else if (date("M")=='Nov') {
				$mes = Noviembre;
			}else if (date("M")=='Dec') {
				$mes = Diciembre;
			}else if (date("M")=='Jan') {
				$mes = Enero;
			}else if (date("M")=='Feb') {
				$mes = Febrero;
			}else if (date("M")=='Mar') {
				$mes = Marzo;
			}else if (date("M")=='Apr') {
				$mes = Abril;
			}else if (date("M")=='May') {
				$mes = Mayo;
			}else if (date("M")=='Jun') {
				$mes = Junio;
			}else if (date("M")=='Jul') {
				$mes = Julio;
			}
			if (date("D")=='Thu')
			{
			  	$dia = Jueves;
			}else if (date("D")=='Mon') {
				$dia = Lunes;
			}else if (date("D")=='Tue') {
				$dia = Martes;
			}else if (date("D")=='Wed') {
				$dia = "Mi&eacute;rcoles";
			}else if (date("D")=='Fri') {
				$dia = Viernes;
			}else if (date("D")=='Sat') {
				$dia = "S&aacute;bado";
			}else if (date("D")=='Sun') {
				$dia = Domingo;
			}
			$dia_num= date( "d" ); 
			$ano=date("Y");
			$fecha = "$dia $dia_num de $mes de $ano - "; 
			$fecha .= date ( "H:i" ); 
			return $fecha;
}

?>
