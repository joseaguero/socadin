<div class="popupCarro">
    <div class="head">
        <h3>Carro de compras <img class="closeCart" onclick="closeCart()" src="img/icons/closeNegro.svg"></h3>
    </div>
    
    <div class="ajxLoad-carro">
        <div class="content-popcarro">
            <?php 

            $carro = getCarro(); 
            if ($carro) :
                foreach($carro as $item):
                $url = 'ficha/' . $item['producto_id'] . '/' . url_amigables($item['nombre']); ?>
                    <div class="row_pr">
                        <div class="eliminar" onclick="eliminaItemCarroOpen(<?= $item['id_hijo'] ?>)"><img src="img/icons/closeGris.svg"></div>
                        <a href="<?= $url ?>">
                            <img src="imagenes/productos/<?= $item['imagen'] ?>">
                        </a>

                        <div class="info">
                            <a href="<?= $url ?>" class="nombre"><?= $item['nombre'] ?></a>

                            <span class="unidad"><?= $item['cantidad'] ?> Unidad</span>

                            <div class="precios">
                                <?php if ($item['descuento'] > 0): ?>
                                    <div class="descuento">Antes $<?= number_format($item['precio'], 0, ",", ".") ?> + IVA</div>
                                    <div class="precio">$<?= number_format($item['descuento'], 0, ",", ".") ?> + IVA</div>
                                <?php else: ?>
                                    <div class="precio">$<?= number_format($item['precio'], 0, ",", ".") ?> + IVA</div>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
            <?php endforeach;
            else:?>
                <p class="carro_vaciopop">Su carro está vacío</p>
            <?php endif;

            ?>
            
            <?php if ($carro): ?>
                 <a href="envio-y-pago" class="buttonPop">Finalizar compra: $<?= number_format(totalCart(), 0, ",", '.') ?></a>
            <?php else: ?>
                 <a href="javascript:void(0)" class="buttonPop disabledBtn">Finalizar compra: $<?= number_format(totalCart(), 0, ",", '.') ?></a>
            <?php endif ?>
           
        </div>
    </div>
    
    <span class="msj_despacho">Los costos de envío se calcularán previo pago</span>
</div>