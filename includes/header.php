<div class="parcheHeader"></div>
<div class="top">
    <div class="container">
        <a href="#" class="a-left">Salvador Gutiérrez 4539, Quinta Normal, Santiago de Chile.</a>
        <a href="mailto:ventas@socadin.cl" class="a-right">ventas@socadin.cl</a>
        <a href="tel:+56227733726" class="a-right">+56 2 2773 3726</a>
    </div>
</div>
<header>
    <div class="container gr-header">
        <div class="col colnav-res">
            <a href="javascript:void(0)" class="btnRes">
                <img src="img/navres.png">
            </a>
        </div>
        <h1 class="logo">
            <a href="home">
                <img src="img/logo.svg">
            </a>
        </h1>

        <div class="col col-buscador">
            <form action="busquedas" method="get" class="principal-search">
                <input type="text" name="buscar" placeholder="¿Qué estás buscando?">
                <button><img src="img/icons/search.svg"></button>
            </form>
        </div>

        <div class="col col-distribuidor">
            <a href="hazte-distribuidor" class="hazte_distribuidor">Hazte distribuidor</a>
        </div>
        <div class="col gr-buttonHeader">
            <a href="javascript:void(0)" class="search_xs">
                <img src="img/search_res.svg">
            </a>

            <a href="carro-cotizacion" class="box_action">
                <img src="img/icons/list.svg">
                <span>Cotizador</span>
                <small class="qty_cot"><?= qty_cotizacion() ?></small>
            </a>

            <a href="javascript:void(0)" class="box_action open-cart">
                <img src="img/icons/cart.svg">
                <span>Carro</span>
                <small class="qty_cart"><?= qty_pro() ?></small>
            </a>
            
            <?php if (!isset($_COOKIE['usuario_id'])): ?>
                <a href="javascript:void(0)" class="box_action openLogin">
                    <img src="img/icons/account.svg">
                    <span>Mi cuenta</span>
                </a>
            <?php else: ?>
                <div class="box_action openDash relative">
                    <div class="nombre-header"><?= $_COOKIE['letrasNombre'] ?></div>
                    <span>Mi cuenta</span>

                    <div class="menu-dash-header">
                        <a href="cuenta/datos">Mis datos</a>
                        <a href="cuenta/direcciones">Mis direcciones</a>
                        <a href="cuenta/pedidos">Mis pedidos</a>
                        <a href="cuenta/certificados">Certificados</a>
                        <form action="logout" method="post">
                            <button class="btnLogoutHeader">Salir</button>
                        </form>
                    </div>
                </div>
            <?php endif; ?>
            
        </div>
    </div>
    <div class="responsive_search">
        <form action="busquedas" method="get">
            <input type="text" name="buscar" placeholder="¿Qué estás buscando?">
        </form>
        <a href="javascript:void(0)" class="btnCloseSearch">
            <img src="img/icons/closeNegro.svg">
        </a>
    </div>
    <div class="bg_search"></div>
</header>
<nav>
    <div class="categories">
        <img src="img/icons/menu.svg">
        <span>Más categorías</span>
        <?php $lineasAll = consulta_bd('id, nombre', 'lineas', "publicado = 1", 'posicion asc'); ?>
        <div class="menu_categorias">
            <a href="catalogos">Descargar catálogos</a>
            <?php foreach ($lineasAll as $l): ?>
                <a href="lineas/<?= $l[0] ?>/<?= url_amigables($l[1]) ?>"><?= $l[1] ?></a>
            <?php endforeach ?>
        </div>
    </div>
    <?php $lineas_header = consulta_bd('id, nombre', 'lineas', 'publicado = 1 and recomendado = 1', 'posicion asc LIMIT 4') ?>
    
    <ul id="mainMenu">
        <?php foreach ($lineas_header as $item_h): 
            $categorias = consulta_bd('id, nombre', 'categorias', "linea_id = $item_h[0]", 'posicion asc');
            $trueCat = is_array($categorias); ?>
            <li <?= ($trueCat) ? 'class="opensub"' : '' ?>><a href="javascript:void(0)" class="amenu"><span><?= $item_h[1] ?></span> <?= ($trueCat) ? '<i class="fas fa-chevron-down"></i>' : '' ?></a>
                <?php if ($trueCat): ?>
                    <div class="content_submenu">
                        <ul class="submenu container">
                            <?php foreach ($categorias as $cat_h):
                                $subcategorias = consulta_bd('id, nombre', 'subcategorias', "categoria_id = $cat_h[0]", 'nombre asc');
                                $trueSub = is_array($subcategorias); ?>
                                <li><a href="categorias/<?= $cat_h[0] ?>/<?= url_amigables($cat_h[1]) ?>" class="tituloCatMenu"><?= $cat_h[1] ?></a>
                                    <?php if ($trueSub): ?>
                                        <ul class="subsubmenu">
                                            <?php foreach ($subcategorias as $sub_h): ?>
                                                <li><a href="subcategorias/<?= $sub_h[0] ?>/<?= url_amigables($sub_h[1]) ?>"><?= $sub_h[1] ?></a></li>
                                            <?php endforeach ?>
                                        </ul>
                                    <?php endif ?>
                                </li>
                            <?php endforeach ?>
                        </ul>
                    </div>  
                <?php endif ?>
            </li>
        <?php endforeach ?>
        <li><a href="somos"><span>Socadin 40 años</span></a></li>
        <li><a href="contacto"><span>Contáctanos</span></a></li>
        <li><a href="ofertas" class="ofertasnav"><span>Ofertas</span></a></li>
    </ul>
</nav>
<div class="nav_resp">
    <div class="content_login_res">
        <div class="title">Bienvenido</div>
        <div class="subtitle">Tu cuenta para todo lo relacionado con Socadin</div>

        <div class="buttons">
            <a href="javascript:void(0)" class="btn btn-login-xs open-login-xs">Iniciar sesión</a>
            <a href="javascript:void(0)" class="btn btn-registro-xs open-registro-xs">Regístrate</a>
        </div>
    </div>

    <div class="list-menu">
        <a href="hazte-distribuidor" class="hazte-res">hazte distribuidor</a>
        <?php foreach ($lineasAll as $l_res):
            $categorias_res = consulta_bd('id, nombre', 'categorias', "linea_id = $l_res[0]", 'posicion asc');
            $trueCat = is_array($categorias_res); ?>
            <li>
                <a href="lineas/<?= $l_res[0] ?>/<?= url_amigables($l_res[1]) ?>" class="line_res">
                    <?= $l_res[1] ?> 
                    <?php if ($trueCat): ?>
                        <span class="btn-open-cats"><img src="img/icons/arrownav.svg"></span>
                    <?php endif ?>
                </a>
                
                <?php if ($trueCat): ?>
                    <div class="subres">
                        <div class="content_action">
                            <div class="volver"><span class="material-icons">keyboard_arrow_left</span> <span class="spnot">Volver</span></div>
                            <div class="closenavres"><span class="spnot">Cerrar</span> <span class="material-icons">close</span></div>
                        </div>
                        
                        <div>
                            <?php foreach ($categorias_res as $c_res):
                                $subcategorias_res = consulta_bd('id, nombre', 'subcategorias', "categoria_id = $c_res[0]", 'nombre asc');
                                $trueSub = (is_array($subcategorias_res)); ?>
                                <a href="categorias/<?= $c_res[0] ?>/<?= url_amigables($c_res[1]) ?>" class="cat_res">
                                    <?= $c_res[1] ?> 
                                    <?php if ($trueSub): ?>
                                        <span class="material-icons btn-open-sub">keyboard_arrow_right</span>
                                    <?php endif ?>
                                </a>

                                <div class="subsubres">
                                    <div class="content_action">
                                        <div class="volver"><span class="material-icons">keyboard_arrow_left</span> <span class="spnot">Volver</span></div>
                                        <div class="closenavres"><span class="spnot">Cerrar</span> <span class="material-icons">close</span></div>
                                    </div>
                                    
                                    <?php foreach ($subcategorias_res as $s_res): ?>
                                        <a href="subcategorias/<?= $s_res[0] ?>/<?= url_amigables($s_res[1]) ?>" class="cat_res">
                                            <?= $s_res[1] ?> 
                                        </a>
                                    <?php endforeach ?>
                                    
                                </div>
                            <?php endforeach ?>

                        </div>
                    </div>
                <?php endif ?>
                
            </li>
            
        <?php endforeach ?>
        <a href="ofertas" class="ofertasnavres">Ofertas</a>
        <a href="somos" class="line_res">Socadin 40 años</a>
        <a href="contacto" class="line_res">Contáctanos</a>
    </div>
</div>