<div class="bg-pop"></div>
<div class="popLogin">
	<div class="titulo">Bienvenido</div>
	<div class="subtitulo">Tu cuenta para todo lo relacionado con Socadin</div>
    
    <div class="main-input mbxs-20">
        <div class="input-form">
            <label class="label_form">Correo electrónico</label>
            <input type="text" name="email" class="input_text" id="emailLogin">
        </div>
    </div>
	
    <div class="main-input mbxs-20">
        <div class="input-form">
            <label class="label_form">Contraseña</label>
            <input type="password" name="password" class="input_text" id="passLogin">
        </div>
    </div>
    

    <a href="javascript:void(0)" class="btn-login">Iniciar sesión</a>

    <a href="javascript:void(0)" class="recPasswordOpen">¿Olvidaste la contraseña?</a>

    <a href="javascript:void(0)" class="btn-registro-open">Regístrate</a>

    <div class="terminos">Al iniciar sesión, aceptas la <a href="politicas-de-privacidad">Politica de Privacidad</a><br>
y los <a href="terminos-y-condiciones">Términos y Condiciones</a> de Socadin</div>
</div>

<div class="popRegistro">
	<div class="titulo">Regístrate</div>
	<div class="subtitulo">Tu cuenta para todo lo relacionado con Socadin</div>

    <div class="main-input mbxs-20">
        <div class="input-form">
            <label class="label_form">Nombre y apellido</label>
            <input type="text" name="nombre" class="input_text" id="nombreRegistro">
        </div>
    </div>
    
    <div class="main-input mbxs-20">
        <div class="input-form">
            <label class="label_form">Correo electrónico</label>
            <input type="text" name="email" class="input_text" id="emailRegistro">
        </div>
    </div>

    <div class="main-input mbxs-20">
        <div class="input-form">
            <label class="label_form">Contraseña</label>
            <input type="password" name="password" class="input_text" id="passRegistro">
        </div>
    </div>

    <div class="main-input mbxs-20">
        <div class="input-form">
            <label class="label_form">Repetir Contraseña</label>
            <input type="password" name="repassword" class="input_text" id="rePassRegistro">
        </div>
    </div>

    <a href="javascript:void(0)" class="btn-registro">Regístrate</a>

    <div class="terminos">Al registrarte, aceptas la <a href="politicas-de-privacidad">Politica de Privacidad</a><br>
y los <a href="terminos-y-condiciones">Términos y Condiciones</a> de Socadin</div>

	<div class="terminos">¿Ya estas registrado? <a href="javascript:void(0)" class="loginOpenReg">inicia sesión</a></div>
</div>

<div class="popRecuperar">
	<div class="titulo">¿Has olvidado tu contraseña?</div>
	<div class="subtitulo">Introduce tu dirección de correo electrónico y recibirás un enlace para restablecer la contraseña.</div>
    
    <div class="main-input mbxs-20">
        <div class="input-form">
            <label class="label_form">Correo electrónico</label>
            <input type="text" name="email" id="emailRecuperar" class="input_text">
        </div>
    </div>

    <a href="javascript:void(0)" class="btn-recuperar">Restablecer contraseña</a>

    <div class="terminos"><a href="javascript:void(0)" class="login-rec">Iniciar Sesión</a> - <a href="javascript:void(0)" class="reg-rec">Regístrate</a></div>
</div>