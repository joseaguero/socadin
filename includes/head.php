<!DOCTYPE html>
<html lang="es">
<head>
    <?php include("includes/metatags.php"); ?>
    <?php include("includes/titulos.php"); ?>
    
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
    
    <!--<script src="//code.jquery.com/jquery-3.2.1.min.js"></script>-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

	<!-- Sweet alert -->
    <script type="text/javascript" src="js/sweetalert.js"></script>
    <link href="css/sweetalert2.min.css" rel="stylesheet">
<?php 
	
	$base = file_get_contents('tienda/base_tienda.css');
	echo '<style type="text/css">';
	echo minify_css($base);
	echo '</style>';
		
	$uniform = file_get_contents('css/agent.css');
	echo '<style type="text/css">';
	echo minify_css($uniform);
	echo '</style>';
	
	$socadin = file_get_contents('css/style.css');
	echo '<style type="text/css">';
	echo minify_css($socadin);
	echo '</style>';

	$socadin_res = file_get_contents('css/responsive.css');
	echo '<style type="text/css">';
	echo minify_css($socadin_res);
	echo '</style>';

	$ui = file_get_contents('css/jquery-ui.css');
	echo '<style type="text/css">';
	echo minify_css($ui);
	echo '</style>';
?>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/slick.css">
	
	<link rel="stylesheet" href="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.css">
    <script src="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.js"></script>

    <script src="js/jquery.cycle2.js"></script>
    <script src="js/jquery.mousewheel.js"></script>
    <script src="js/jquery.ui.core.min.js"></script>
    <script src="js/jquery.ui.widget.min.js"></script>
    <script src="js/jquery.ui.button.min.js"></script>
    <script src="js/jquery.ui.spinner.min.js"></script>
    <script src="js/slick.min.js"></script>

    <!-- Recaptcha en su v3 -->
    <script src="https://www.google.com/recaptcha/api.js?render=6LdgYlYcAAAAAED7ARuvOg0qEHQRpA-65TL7ZjvT"></script>

    <!-- <script src="js/material.js"></script> -->
    
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
	ga('create', 'UA-69086179-3', 'auto');
	ga('require', 'ec');
	ga('send', 'pageview');
	
	function productClick(sku, nombre_producto,categoria, variante, posicion, lista, marca ) {
	  ga('ec:addProduct', {
	    'id': sku,
	    'name': nombre_producto,
	    'category': categoria,
	    'brand': marca,
	    'variant': variante,
	    'position': posicion
	  });
	  ga('ec:setAction', 'click', {list: lista});
	  ga('send', 'event', 'UX', 'click', 'Results', {
	      hitCallback: function() {
	      }
	  });
	};
  
	function addToCart(sku, nombre_producto,categoria, variante, precio, cantidad, marca) {
	 ga('ec:addProduct', {
	    'id': sku,
	    'name': nombre_producto,
	    'category': categoria,
	    'brand': marca,
	    'variant': variante,
	    'price': precio,
	    'quantity': cantidad
	  });
	  ga('ec:setAction', 'add');
	  ga('send', 'event', 'UX', 'click', 'add to cart');     // Send data using an event.
	}
</script>

<!--agrego el chat en el caso que el cliente lo cargo en su pagina de configuracion -->
<?= opciones("chat"); ?>
</head>