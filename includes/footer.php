<footer>
    <div class="container gr-footer">
        <div class="col">
            <div class="rowImg"><img src="img/logofooter.png"></div>
            <div class="rowImg"><img src="img/ccs.png"></div>
            <div class="rowImg"><img src="img/sicep.png"> <img src="img/iso.png" class="ml-20"></div>
        </div>
        <div class="col">
            <div class="titleSec open-footer">Contacto <span class="material-icons">keyboard_arrow_down</span></div>
            <ul class="listFooter">
                <li><a href="#">Salvador Gutiérrez 4539, Quinta Normal, Santiago de Chile.</a></li>
                <li><a href="mailto:ventas@socadin.cl">ventas@socadin.cl</a></li>
                <li><a href="tel:+56227733726">+56 2 2773 3726</a></li>
                <li><a href="tel:+56227738820">+56 2 2773 8820</a></li>
                <li><a href="contacto">contacto</a></li>
            </ul>
        </div>
        <div class="col">
            <div class="titleSec open-footer">Horarios <span class="material-icons">keyboard_arrow_down</span></div>
            <ul class="listFooter">
                <li>Lunes a Viernes: <br>09:00 a 18:00 Hrs.</li>
                <!-- <li>Sábados: 08:30 a 18:30 Hrs.</li> -->
                <!-- <li>Domingos y Festivos: <br>09:30 a 14:30 Hrs.</li> -->
            </ul>
        </div>
        <div class="col">
            <?php $lineas_footer = consulta_bd('id, nombre', 'lineas', "publicado = 1 and recomendado = 1", 'posicion asc LIMIT 4'); ?>
            <div class="titleSec open-footer">Categorías <span class="material-icons">keyboard_arrow_down</span></div>
            <ul class="listFooter">
                <?php foreach ($lineas_footer as $item): ?>
                    <li><a href="lineas/<?= $item[0] ?>/<?= url_amigables($item[1]) ?>"><?= $item[1] ?></a></li>
                <?php endforeach ?>
                <li><a href="somos">40 Años Socadin</a></li>
                <li><a href="catalogos">Descargas Catálogos</a></li>
                <li><a href="https://www.socadin.cl/wsboleta/boleta.php" target="_blank">Revisa tu boleta electrónica <span class="material-icons">save_alt</span></a></li>
            </ul>
        </div>
        <div class="col">
            <div class="titleSec notopen_footer">Suscríbete</div>

            <div class="content_news">
                <input type="text" name="emailNewsletter" class="emailNewsletter" placeholder="Ingresa tu correo">
                <a href="javascript:void(0)" class="btnNewsletter">Suscríbete</a>
            </div>

            <div class="titleSec mt-30 notopen_footer">Medios de pago</div>

            <div class="content_metodosFooter">
                <img src="img/webpayfooter.png">
                <img src="img/mpagofooter.png">
                <img src="img/transferenciafooter.png">
            </div>
            <div class="mt-20 mb-20" style="margin-left: 20px;">
                <img src="img/webpay2-6cuotas.png">
            </div>
        </div>
    </div>

    <div class="linksFooter-xs">
        <a href="como-comprar">¿Cómo comprar?</a>
        <a href="politicas-de-privacidad">Políticas de privacidad</a>
        <a href="terminos-y-condiciones">Términos y condiciones</a>
        <a href="cambios-y-devoluciones">Cambios y devoluciones</a>
    </div>

    <div class="img-resp_footer">
        <div class="rowImg">
            <img src="img/ccs.png">
            <img src="img/sicep.png" class="ml-20"> 
            <img src="img/iso.png" class="ml-20">
        </div>
    </div>

    <div class="btFooter">
        <div class="container">
            <div class="copy">&copy; todos los derechos reservados</div>

            <div class="linksFooter">
                <a href="como-comprar">¿Cómo comprar?</a>
                <a href="politicas-de-privacidad">Políticas de privacidad</a>
                <a href="terminos-y-condiciones">Términos y condiciones</a>
                <a href="cambios-y-devoluciones">Cambios y devoluciones</a>
            </div>

            <div class="clearfix"></div>
        </div>
    </div>
</footer>
<div class="content_success_news">
    <div class="title">¡Gracias por suscribirte a Socadin!</div>
    <div class="sub">Enviaremos un correo de confirmación a <br>
<span class="correo_news"></span></div>

    <a href="javascript:void(0)" class="btn_news">aceptar</a>
</div>