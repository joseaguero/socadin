<?php 
header('Content-type: text/html; charset=utf-8');
include("admin/conf.php");
/********************************************************\
|  Alfastudio Ltda - Mail sender V0.5			 		 |
|  Fecha Modificacion: 16/06/2011		                 |
|  Todos los derechos reservados © Alfastudio Ltda 2011  |
|  Prohibida su copia parcial o total, 					 |
|  venta, comercializacion o distribucion				 |
|  http://www.moldeable.com/                             |
\********************************************************/

//******************Cambiar sólo estos valores*************************//

require 'PHPMailer/PHPMailerAutoload.php';

$nombre_sitio = opciones("nombre_cliente");
$nombre_corto = opciones("dominio");
$noreply = "no-reply@".$nombre_corto;
$url_sitio = "https://".$nombre_corto;
$correo_venta = opciones("correo_venta");
$color_logo = opciones("color_logo");
	
$url_logo = opciones("logo_mail");
$campo_oculto = 'validacion';
$para = 'htorres@moldeable.com';
$asunto = "Contacto desde sitio web $nombre_sitio";
$ruta_retorno = $url_base.'contacto';
$ruta_exito = $url_base.'contacto';
$campo_mensaje = 'mensaje';
$obligatorios = "nombre, email, $campo_mensaje";
$lang = (isset($_GET[lang])) ? "ing": "esp";



$file_field = ""; //Nombre del campo del archivo adjunto, dejar vacÃ­o si no se va a usar

$from = "noreply@spazziomuebles.cl";
$save_in_db = false;

//Campos del formulario correspondiente a nombre e email.
$nombre_cl = $_POST['nombre'];
$email_cl = $_POST['email'];

//******************Comienzo del código, no modificar*********************//

 
if ($_POST[$campo_oculto] == ''){
	$vars = get_post('');
	$obligatorios = explode(',', $obligatorios);
	foreach($obligatorios as $o)
	{
		$campo = trim($o);
		if ($_POST[$campo] == '')
		{
			$error = ($lang == 'esp') ? "Los campos marcados con * son necesarios." : "* indicates requiered fields.";
			die(header("location:$ruta_retorno?msje=$error"));
			//die('1');
		}
	}

	$comp_mail = comprobar_mail($email_cl);
	if ($comp_mail == 1)
	{
		$mail = $email_cl;
	}
	else
	{
		$error = ($lang == 'esp') ? "Correo Inválido $email." : "Invalid email.";
		die(header("location:$ruta_retorno?msje=$error"));
		//die('2');
	}
	
	$msg2 = '
		<html>
			<head>
			<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel="stylesheet">
			<title>'.$nombre_sitio.'</title>
			<style type="text/css">
					p, ul, a { 
						color:#666; 
						font-family: "Open Sans", sans-serif;
						background-color: #ffffff; 
						font-weight:300;
					}
					strong{
						font-weight:600;
					}
					a {
						color:#666;
					}
				</style>
			</head>
			<body style="background:#f9f9f9;">
				<div style="background:#fff; width:86%; margin-left:5%; padding-left:2%; padding-right:2%; border-top:solid 4px '.$color_logo.'; border-bottom:solid 4px '.$color_logo.'; font-family: Open Sans, sans-serif; padding-bottom: 20px;">
				
						<table cellspacing="0" cellpadding="0" border="0" width="100%" style="margin-top:20px;margin-bottom:10px;">
							<tr>
								<th align="left" width="50%">
									<p>
										<a href="'.$url_sitio.'" style="color:#8CC63F;">
											<img src="'.$logo.'" alt="'.$logo.'" border="0" width="214"/>
										</a>
									</p>
								</th>
							</tr>
						</table>
						<br/><br/>
							
							<table cellspacing="0" cellpadding="0" border="0" width="100%">
								<tr>
									<td valign="top">
										<p><strong>Estimado:</strong></p>
										<p>Se ha enviado una consulta a trav&eacute;s de la p&aacute;gina web de '.$nombre_sitio.'. <br />Los datos de la persona que contacta son:<br /></p>
										<ul>
											';
												foreach($_POST as $key=>$val)
												{
													if ($key != 'enviar' AND $key != $campo_mensaje AND $key != '')
													{
														$nombre = ucwords($key);
														$msg2 .= "<li style='height:30px;'>".$nombre.": ".htmlentities($val,ENT_QUOTES,"UTF-8")."</li>";
													}
												}
										$msg2 .= '</ul>
										<p>El cliente ha dejado el siguiente mensaje: <br /><br />
											<em>'.htmlentities($_POST[$campo_mensaje],ENT_QUOTES,"UTF-8").'</em>
										</p>
										<p>Rogamos contactarse con el cliente lo antes posible.</p>
										<p>Muchas gracias<br /> Atte,</p>
										<p><strong>Equipo de '.$nombre_sitio.'</strong></p>
									</td>
								</tr>
							</table>
				</div>
			</body>
		</html>';
	
			
if ($lang == 'esp'){
		$asuntoRespuesta = "Gracias por visitar $nombre_sitio";
		$msg3 = '
		<html>
					<head>
					<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel="stylesheet">
					<title>'.$nombre_sitio.'</title>
					<style type="text/css">
							p, ul, a { 
								color:#666; 
								font-family: "Open Sans", sans-serif;
								background-color: #ffffff; 
								font-weight:300;
							}
							strong{
								font-weight:600;
							}
							a {
								color:#666;
							}
						</style>
					</head>
					<body style="background:#f9f9f9;">
						<div style="background:#fff; width:86%; margin-left:5%; padding-left:2%; padding-right:2%; border-top:solid 4px '.$color_logo.'; border-bottom:solid 4px '.$color_logo.'; font-family: Open Sans, sans-serif; padding-bottom: 20px;">
						
							<table cellspacing="0" cellpadding="0" border="0" width="100%" style="margin-top:20px;margin-bottom:10px;">
								<tr>
									<th align="left" width="50%">
										<p>
											<a href="'.$url_sitio.'" style="color:#8CC63F;">
												<img src="'.$logo.'" alt="'.$logo.'" border="0" width="214"/>
											</a>
										</p>
									</th>
								</tr>
							</table>
							<br/><br/>
								
							<table cellspacing="0" cellpadding="0" border="0" width="100%">
								<tr>
									<td valign="top">
										<p><strong>Estimado '.htmlentities($nombre_cl,ENT_QUOTES,"UTF-8").':</strong></p>
										<p>Muchas gracias por contactarse con '.$nombre_sitio.'!, <br />le enviaremos una respuesta lo antes posible.<br /></p>
										<p>Saludos,</p>
										<p><strong>Equipo de '.$nombre_sitio.'.</strong></p>
									</td>
								</tr>
							</table>
						</div>
					</body>
				</html>';
	}
	else
	{
		$asuntoRespuesta = "Thanks for visiting $nombre_sitio";
		$msg3 = '<html>
					<head>
					<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel="stylesheet">
					<title>'.$nombre_sitio.'</title>
					<style type="text/css">
							p, ul, a { 
								color:#666; 
								font-family: "Open Sans", sans-serif;
								background-color: #ffffff; 
								font-weight:300;
							}
							strong{
								font-weight:600;
							}
							a {
								color:#666;
							}
						</style>
					</head>
					<body style="background:#f9f9f9;">
						<div style="background:#fff; width:86%; margin-left:5%; padding-left:2%; padding-right:2%; border-top:solid 4px '.$color_logo.'; border-bottom:solid 4px '.$color_logo.'; font-family: Open Sans, sans-serif; padding-bottom: 20px;">
						
							<table cellspacing="0" cellpadding="0" border="0" width="100%" style="margin-top:20px;margin-bottom:10px;">
								<tr>
									<th align="left" width="50%">
										<p>
											<a href="'.$url_sitio.'" style="color:#8CC63F;">
												<img src="'.$logo.'" alt="'.$logo.'" border="0" width="214"/>
											</a>
										</p>
									</th>
								</tr>
							</table>
							<br/><br/>
								
							<table cellspacing="0" cellpadding="0" border="0" width="100%">
								<tr>
									<td valign="top">
										<p><strong>Dear '.htmlentities($nombre_cl,ENT_QUOTES,"UTF-8").':</strong></p>
										<p>Thanks you for reaching '.$nombre_sitio.'!, <br />we will contact you as soon as posible<br /></p>
										<p>Best regards,</p>
										<p><strong>Team of '.$nombre_sitio.'.</strong></p>
									</td>
								</tr>
							</table>
						</div>
					</body>
				</html>';
		
	}
		
		
		$mailRespuesta = new PHPMailer;
		$mailRespuesta->isSMTP();
		$mailRespuesta->SMTPDebug = 0;
		$mailRespuesta->Debugoutput = 'html';
		$mailRespuesta->Host = opciones("Host");
		$mailRespuesta->Port = opciones("Port");
		$mailRespuesta->SMTPSecure = opciones("SMTPSecure");
		$mailRespuesta->SMTPAuth = true;
		$mailRespuesta->Username = opciones("Username");
		$mailRespuesta->Password = opciones("Password");
		
		$mailRespuesta->setFrom(opciones("setFrom_mail"), opciones("nombre_setFrom_mail"));
		//Set an alternative reply-to address
	
		$mailRespuesta->addAddress($email_cl, $nombre_cl);
		$mailRespuesta->Subject = $asuntoRespuesta;
		$mailRespuesta->msgHTML($msg3);
		$mailRespuesta->AltBody = $msg3;
		$mailRespuesta->CharSet = 'UTF-8';
		$mailRespuesta->send();
		
		
		
		$mail = new PHPMailer;
		$mail->isSMTP();
		$mail->SMTPDebug = 0;
		$mail->Debugoutput = 'html';
		$mail->Host = opciones("Host");
		$mail->Port = opciones("Port");
		$mail->SMTPSecure = opciones("SMTPSecure");
		$mail->SMTPAuth = true;
		$mail->Username = opciones("Username");
		$mail->Password = opciones("Password");
		
		$mail->setFrom(opciones("setFrom_mail"), opciones("nombre_setFrom_mail"));
		//Set an alternative reply-to address
	
		$mail->addAddress("htorres@moldeable.com", "Herna Torres");
		$mail->Subject = $asunto;
		$mail->msgHTML($msg2);
		$mail->AltBody = $msg2;
		$mail->CharSet = 'UTF-8';
			
	
		
			//send the message, check for errors
			if (!$mail->send()) {
			    //echo "Mailer Error: " . $mail->ErrorInfo;
				$error = ($lang == 'esp') ? "Error enviando el correo, por favor inténtelo nuevamente." : "We had some dificulties sending the email, please try again later.";
				header("Location: $ruta_retorno?msje=$error");
				die("2");
			} else {
			    //echo "Message sent!";
				$error = ($lang == 'esp') ?  "Su mensaje fue enviado. Muchas gracias por contactarnos." : "Your message has been sent, we'll answer you asap. Thank You.";
				header("Location: $ruta_retorno?msje=$error&a=1");
				die("1");
			}
			//header("Location: ../login?msje=te enviamos una nueva clave a tu email: $correo.");
			
	} else {
		//si no existe retorno e indico que no esta en los registros
		$error = ($lang == 'esp') ? "Error al enviar el mensaje" : "Error sending the email.";
		header("Location: $ruta_retorno?msje=$error");
		die("3");
	}

?>