<?php 
include('../admin/conf.php');
include("../admin/includes/tienda/cart/inc/functions.inc.php");
require '../vendor/autoload.php';
include("../tienda/mailchimp.class.php");

$newsletter_list = consulta_bd('id_lista', 'mailchimp', "short_name = 'newsletter'", '');
$todos_list = consulta_bd('id_lista', 'mailchimp', "short_name = 'todos_mail'", '');

$apikey = opciones('key_mailchimp');

$mcc = new MailChimpClient($apikey);

$nombre = mysqli_real_escape_string($conexion, $_POST['nombre']);
$email = mysqli_real_escape_string($conexion, $_POST['email']);

$existe_email = consulta_bd('email', 'popup', "email = '$email'",'');

if (!is_array($existe_email)) {
	$insert = insert_bd('popup', 'nombre, email, fecha_creacion', "'$nombre', '$email', NOW()");
	insert_bd("newsletter","email, fecha_creacion","'$email', now()");

	$data['email'] = $email;
	$data['nombre'] = $nombre;
	$data['listId'] = $newsletter_list[0][0];

	$data_all['email'] = $email;
	$data_all['nombre'] = $nombre;
	$data_all['listId'] = $todos_list[0][0];

	echo json_encode($mcc->subscribe_sin_apellido($data));
	echo json_encode($mcc->subscribe_sin_apellido($data_all));
}else{
	echo 'nada';
}


?>