<?php 
header('Content-type: text/html; charset=utf-8');
include('../admin/conf.php');
include '../Mailin.php';
// require '../PHPMailer/PHPMailerAutoload.php';

/* Defino las variables a utilizar */
define('clave', '6LdgYlYcAAAAAKDLfmeQNxDApCXEmAfad8wIewB2');
$nombre = mysqli_real_escape_string($conexion, $_POST['nombre']);
$email = mysqli_real_escape_string($conexion, $_POST['email']);
$telefono = mysqli_real_escape_string($conexion, $_POST['telefono']);
$mensaje = mysqli_real_escape_string($conexion, $_POST['mensaje']);
$token = mysqli_real_escape_string($conexion, $_POST['token']);
$action = mysqli_real_escape_string($conexion, $_POST['action']);


/* Realizo la conexion a la api de recaptcha de google para verificar si es un robot */
$cu = curl_init();
curl_setopt($cu, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
curl_setopt($cu, CURLOPT_POST, 1);
curl_setopt($cu, CURLOPT_POSTFIELDS, http_build_query(array('secret' => clave, 'response' => $token)));
curl_setopt($cu, CURLOPT_RETURNTRANSFER, true);
$response = curl_exec($cu);
curl_close($cu);

$datos = json_decode($response, true);


/* verifico que me dice el algoritmo de recaptcha si success es 1 y si score es mayor o igual a 5 pasa si no es un robot */
if($datos['success'] == 1 && $datos['score'] >= 0.5){
	$nombre_sitio = opciones('nombre_cliente');
	$nombre_corto = strtolower($nombre_sitio);
	$noreply = "no-reply@".$nombre_corto.".cl";
	$url_sitio = opciones('url_sitio');
	$logo = opciones('logo_mail');
	$color_logo = opciones('color_logo');

	$msje = 
	'
	<html>
	<head><link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet"></head>
	<body style="font-family: \'Open Sans\', sans-serif; color: #333;">
	<div style="background: #f9f9f9; width: 100%;">
		<div style="background: #fff; border-top: 2px solid #db4437; border-bottom: 2px solid #db4437; width: 90%;margin: auto; padding: 30px; box-sizing: border-box;">
			<img src="'.$logo.'" alt="'.$nombre_sitio.'">
			<p style="font-size: 14px;margin-bottom:5px;color:'.$color_logo.';"><b>Estimado/a:</b></p>
			<p style="font-size: 13px; margin-top: 0;">Se ha enviado un nuevo mensaje de contacto a través de la página web de '.$nombre_sitio.'. <br>
			Los datos de la persona que contacta son:</p>
			<blockquote style="font-size: 13px;">
			<strong>Nombre:</strong> '.$nombre.'<br>
			<strong>E-mail:</strong> '.$email.' <br>
			<strong>Teléfono:</strong> '.$telefono.' <br>
			<strong>Mensaje:</strong> '.$mensaje.' <br>
			</blockquote>

			<p style="font-size: 13px;">Rogamos contactarse lo antes posible con el cliente.<br><br>
			Muchas gracias. <br>
			Atte,<br>
			<b style="color: '.$color_logo.';">Equipo de '.$nombre_sitio.'</b></p>
		</div>
	</div>
	</body>
	</html>';
	$asunto = 'Mensaje desde la página de contacto.';

	$msje_cliente = 
	'
	<html>
	<head><link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet"></head>
	<body style="font-family: \'Open Sans\', sans-serif; color: #333;">
	<div style="background: #f9f9f9; width: 100%;">
		<div style="background: #fff; border-top: 2px solid #db4437; border-bottom: 2px solid #db4437; width: 90%;margin: auto; padding: 30px; box-sizing: border-box;">
			<img src="'.$logo.'" alt="'.$nombre_sitio.'">
			<p style="font-size: 14px;margin-bottom:5px;color:'.$color_logo.';"><b>Estimado/a:</b></p>
			<p style="font-size: 13px; margin-top: 0;">Gracias por contactarse con '.$nombre_sitio.'<br>
			Nuestro equipo se pondrá en contacto con usted a la brevedad.</p>
			

			<p style="font-size: 13px;">
			Muchas gracias. <br>
			Atte,<br>
			<b style="color: '.$color_logo.';">Equipo de '.$nombre_sitio.'</b></p>
		</div>
	</div>
	</body>
	</html>';
	$asunto_cliente = 'Gracias por contactarse con '. $nombre_sitio;

	$mailin = new Mailin('grupoatlasholding@gmail.com', 'VSU8IBE6jn21kNYJ');
    if(opciones("correo_admin1") != ""){
		$mailin-> addTo(opciones("correo_admin1"),opciones("nombre_correo_admin1"));
	}
	if(opciones("correo_admin2") != ""){
		$mailin-> addTo(opciones("correo_admin2"),opciones("nombre_correo_admin2"));
	}
	if(opciones("correo_admin3") != ""){
		$mailin-> addTo(opciones("correo_admin3"),opciones("nombre_correo_admin3"));
	}

	$mailin->
        setFrom(opciones("setFrom_mail"), opciones("nombre_setFrom_mail"))->
        setSubject("$asunto")->
        setText("$msje")->
            setHtml("$msje");
    $res = $mailin->send();
    $res2 = json_decode($res);

    if ($res2->{'result'} == true) {
    	$insert = insert_bd('contacto', 'nombre, email, telefono, mensaje, fecha_creacion', "'$nombre', '$email', '$telefono', '$mensaje', NOW()");
	    if ($insert) {		
	        //envio copia al cliente
			$mailinCliente = new Mailin('grupoatlasholding@gmail.com', 'VSU8IBE6jn21kNYJ');
	        $mailinCliente->
	            addTo("$email", "$nombre")->
	            setFrom(opciones("setFrom_mail"), opciones("nombre_setFrom_mail"))->
	            setSubject("$asunto_cliente")->
	            setText("$msje_cliente")->
	            setHtml("$msje_cliente");
	        $resCliente = $mailinCliente->send();
	        $res2Cliente = json_decode($resCliente);
	        //fin envio copia al cliente
		        if ($res2Cliente->{'result'} == true) {
			        $status = 'success';
				    $message = 'Coctacto enviado correctamente';
				}else{
					$status = 'error';
		   			$message = 'Error al enviar formulario de contacto, intentar nuevamente';
				}
		}
    } else {

       $status = 'error';
	   $message = 'Error al enviar formulario de contacto, intentar nuevamente';
    }

}else{
    $status = 'error';
	$message = 'Error al enviar formulario de contacto';
}

$_SESSION['flash']['status'] = $status;
$_SESSION['flash']['message'] = $message;
header("Location: contacto?status=$status");

?>