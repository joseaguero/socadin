<?php 

include('../../admin/conf.php');

$id = (int)mysqli_real_escape_string($conexion, $_POST['id']);
$qty = (int)mysqli_real_escape_string($conexion, $_POST['qty']);

$carro = json_decode($_COOKIE['carro_cotizacion'], true);

$action = mysqli_real_escape_string($conexion, $_POST['action']);

$itemNuevo = array('id' => $id, 'qty' => $qty);

switch ($action) {
	case 'add':

		if ($carro == null) {
			
			$carro[] = $itemNuevo;
		}else{
			$valida = false;
			$indexValidado = 0;
			// Si el carro no está vacío se validará si ya hay del mismo producto en el carro y se verifica el Stock.
			foreach ($carro as $index => $item) {
				if ($item['id'] == $id) {
					$indexValidado = $index;
					$valida = true;
					break;
				}
			}

			if ($valida) {
				$carro[$indexValidado]['qty'] += $qty;
			}else{
				$carro[] = $itemNuevo;
			}

			
		}
	break;

	case 'update':
		// Si el carro no está vacío se validará si ya hay del mismo producto en el carro y se verifica el Stock.
		foreach ($carro as $index => $item) {
			if ($item['id'] == $id) {
				$indexValidado = $index;
				break;
			}
		}

		$carro[$indexValidado]['qty'] = $qty;
	
	break;

	case 'delete':
		foreach ($carro as $index => $item) {
			if ($item['id'] == $id) {
				$indexValidado = $index;
				break;
			}
		}

		unset($carro[$indexValidado]);
	break;
}

$arr = json_encode($carro);

setcookie("carro_cotizacion", "$arr", time() + (365 * 24 * 60 * 60), "/");

echo $arr;

?>