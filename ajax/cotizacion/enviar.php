<?php 
header('Content-type: text/html; charset=utf-8');
include '../../Mailin.php';
include('../../admin/conf.php');
include('../../admin/includes/tienda/cart/inc/functions.inc.php');
// include('../../PHPMailer/PHPMailerAutoload.php');

$nombre = mysqli_real_escape_string($conexion, $_POST['nombre']);
$email = mysqli_real_escape_string($conexion, $_POST['email']);
$telefono = mysqli_real_escape_string($conexion, $_POST['telefono']);
$mensaje = mysqli_real_escape_string($conexion, $_POST['mensaje']);

$carro = json_decode($_COOKIE['carro_cotizacion'], true);

$nombre_sitio = opciones("nombre_cliente");
$nombre_corto = opciones("dominio");
$noreply = "no-reply@".$nombre_corto;
$url_sitio="http://".$nombre_corto;
$logo = opciones("logo_mail");
$correo_venta = opciones("correo_venta");
$color_logo = opciones("color_logo");

$date = date('ymdhi', time());

$oc = "COT_".$date;

$asunto = "Nueva cotización ".$oc."";
$asuntoCliente = "Cotización realizada ".$oc."";

$fecha_hoy = date('Y-m-d H:i:s', time());

$productos = getCarroCotizacion();

$tabla_compra = '<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">';
       
for ($i=0; $i <sizeof($productos) ; $i++) {

    $tabla_compra .= '<tr>';
    $tabla_compra .= '  <td valign="top" align="left" width="20%" style="border-bottom: 1px solid #f0f0f0;">
							<p style="float:left; width:85px; height:85px; margin:20px 0 20px 0; border: 1px solid #f0f0f0;">
								<img src="'.$url_sitio."/".getIMG($productos[$i]["id"]).' " width="100%"/>
							</p>
							
						</td>';
    $tabla_compra .= '  <td valign="top" align="left" width="80%" style="border-bottom: 1px solid #f0f0f0;color:#797979;">';

    $tabla_compra .= '<p style="float:left; width:100%; margin:10px 0 0 0; font-size: 16px;"">'.$productos[$i]['nombre'].'</p>';
							
    if ($productos[$i]['cadena'] == 1) {
    	$tabla_compra .= '<p style="float:left; width:100%; margin:0 0 5px 0; font-size: 14px;"">Medidas: Ancho '.$productos[$i]['ancho'].' | Perfil '. $productos[$i]['perfil'] .' | Aro '. $productos[$i]['aro'] .'</p>';
    }

	$tabla_compra .= '<p style="float:left; width:100%; margin:0 0 5px 0;"><small><strong>SKU: '.$productos[$i]["sku"].'</strong></small></p>
					<p style="float:left; width:100%; margin:0 0 20px 0;"><small>Cantidad: '.$productos[$i]["cantidad"].'</small></p>
					</td>'; //nombre producto
    
    $tabla_compra .= '</tr>';

}			

             

$tabla_compra .= '</table>';

$msgCliente = '
<html>
    <head>
	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <title>'.$nombre_sitio.'</title>
    </head>
    <body style="background:#f9f9f9;">
		<div style="background:#fff; width:86%; max-width: 600px; margin: 0 auto; padding-left:2%; padding-right:2%; border-top:solid 4px '.$color_logo.'; border-bottom:solid 4px '.$color_logo.'; font-family: Open Sans, sans-serif;">
        
                    <table cellspacing="0" cellpadding="0" border="0" width="100%" style="margin-top:10px;margin-bottom:10px;">
                        <tr>
                            <th align="center" width="100%">
								<p style="margin:20px 0 30px 0;">
									<a href="'.$url_sitio.'" style="color:#8CC63F;">
										<img src="'.$logo.'" alt="'.$logo.'" border="0"/>
									</a>
								</p>
                            </th>
                        </tr>
						<tr>
							<th align="center" width="100%" style="color:#797979;"> 
                                <p style="text-transform: uppercase;float:right;width:100%;margin: 0px; font-size:20px;">Estimado '.$nombre.'</p>
                                <p style="float:right;width:100%;margin: 0px;">Gracias por su cotización</p>
                                <p style="float:right;width:100%;margin: 0px;"><strong style="color:'.$color_logo.';">'.$oc.'</strong></p>
                            </th>
						</tr>
                    </table>
                    <br/><br/>
                    <p style="color:#666;">Uno de nuestros vendedores se contactará con usted a la brevedad para continuar con el proceso de cotización.</p>';
                    $$msgCliente .= '<br/><br/>
                    <table cellspacing="0" cellpadding="0" border="0" width="100%">
                        <tr>
                            <td valign="top">
                               ';
                                
                        $$msgCliente.='<p style="color: #333;">PRODUCTOS COMPRADOS</p>

                                <p>'.$tabla_compra.'</p>';
                                
                        $$msgCliente.='<p align="center" style="margin:0;color:#000;">'.$msje_despacho.'</p> 
                               
                                <p align="center" style="margin:0;color:#999;">Gracias,</p>
                                <p align="center" style="margin:0 0 20px 0;color:#999;">Saludos cordiales, <strong>Equipo '.$nombre_sitio.'</strong></p>
                            </td>
                        </tr>
                    </table>
        </div>
    </body>
</html>';

$msgAdmin = '
<html>
    <head>
	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <title>'.$nombre_sitio.'</title>
    </head>
    <body style="background:#f9f9f9;">
		<div style="background:#fff; width:86%; max-width: 600px; margin: 0 auto; padding-left:2%; padding-right:2%; border-top:solid 4px '.$color_logo.'; border-bottom:solid 4px '.$color_logo.'; font-family: Open Sans, sans-serif;">
        
                    <table cellspacing="0" cellpadding="0" border="0" width="100%" style="margin-top:10px;margin-bottom:10px;">
                        <tr>
                            <th align="center" width="100%">
								<p style="margin:20px 0 30px 0;">
									<a href="'.$url_sitio.'" style="color:#8CC63F;">
										<img src="'.$logo.'" alt="'.$logo.'" border="0"/>
									</a>
								</p>
                            </th>
                        </tr>
						<tr>
							<th align="center" width="100%" style="color:#797979;"> 
                                <p style="text-transform: uppercase;float:right;width:100%;margin: 0px; font-size:20px;">Estimado</p>
                                <p style="float:right;width:100%;margin: 0px;">Han realizado una nueva cotización</p>
                                <p style="float:right;width:100%;margin: 0px;"><strong style="color:'.$color_logo.';">'.$oc.'</strong></p>
                            </th>
						</tr>
                    </table>
                    <br/><br/>
                    <h3 style="color: #333;">DATOS</h3>
                    <p style="color:#666;">
						<span style="display:block;">Nombre: '.$nombre.'</span>
						<span style="display:block;">Email: '.$email.'</span>
						<span style="display:block;">Teléfono: '.$telefono.'</span>
						<span style="display:block;">Mensaje: '.$mensaje.'</span>
                    </p>';
                                
                        $msgAdmin.='<p style="color: #333;">PRODUCTOS COMPRADOS</p>'.

                        $tabla_compra;

                    $msgAdmin .= '<br/><br/>
                    <table cellspacing="0" cellpadding="0" border="0" width="100%">
                        <tr>
                            <td valign="top">
                               ';            
                        $msgAdmin.='<p align="center" style="margin:0;color:#000;">'.$msje_despacho.'</p> 
                               
                                <p align="center" style="margin:0;color:#999;">Gracias,</p>
                                <p align="center" style="margin:0 0 20px 0;color:#999;">Saludos cordiales, <strong>Equipo '.$nombre_sitio.'</strong></p>
                            </td>
                        </tr>
                    </table>
        </div>
    </body>
</html>';

$hoy = date('Y-m-d H:i:s');

$mailin = new Mailin('grupoatlasholding@gmail.com', 'VSU8IBE6jn21kNYJ');
    if(opciones("correo_admin1") != ""){
        $mailin-> addTo(opciones("correo_admin1"),opciones("nombre_correo_admin1"));
    }
    if(opciones("correo_admin2") != ""){
        $mailin-> addTo(opciones("correo_admin2"),opciones("nombre_correo_admin2"));
    }
    if(opciones("correo_admin3") != ""){
        $mailin-> addTo(opciones("correo_admin3"),opciones("nombre_correo_admin3"));
    }

    $mailin->
        setFrom(opciones("setFrom_mail"), opciones("nombre_setFrom_mail"))->
        setSubject("$asunto")->
        setText("$msgAdmin")->
            setHtml("$msgAdmin");
    $res = $mailin->send();
    $res2 = json_decode($res);

    if ($res2->{'result'} == true) {
        // Insertamos la cotización
        $insert = insert_bd('cotizaciones', 'oc, nombre, email, telefono, comentarios, fecha_creacion', "'$oc', '$nombre', '$email', '$telefono', '$mensaje', '$fecha_hoy'");
        $id_pedido = mysqli_insert_id($conexion);

        foreach ($productos as $prd) {
            $insert_productos = insert_bd('productos_cotizaciones', 'cotizacion_id,productos_detalle_id, cantidad, codigo, fecha_creacion', "$id_pedido, {$prd['id']}, {$prd['cantidad']}, '{$prd['sku']}', '$fecha_hoy'");
        }
        if ($insert) {      
            //envio copia al cliente
            $mailinCliente = new Mailin('grupoatlasholding@gmail.com', 'VSU8IBE6jn21kNYJ');
            $mailinCliente->
                addTo("$email", "$nombre")->
                setFrom(opciones("setFrom_mail"), opciones("nombre_setFrom_mail"))->
                setSubject("$asuntoCliente")->
                setText("$msgCliente")->
                setHtml("$msgCliente");
            $resCliente = $mailinCliente->send();
            $res2Cliente = json_decode($resCliente);
            //fin envio copia al cliente
                if ($res2Cliente->{'result'} == true) {
                    $_SESSION['flash'] = 'success';
                    header("Location: carro-cotizacion?status=1");
                }else{
                    $_SESSION['flash'] = 'error';
                    header("Location: carro-cotizacion?status=2");
                }
        }
    } else {

       $_SESSION['flash'] = 'error';
        header("Location: carro-cotizacion?status=2");
    }
?>