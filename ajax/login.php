<?php 
include("../admin/conf.php");

$email = (isset($_POST['email'])) ? mysqli_real_escape_string($conexion, $_POST['email']) : 0;
$password = (isset($_POST['password'])) ? mysqli_real_escape_string($conexion, $_POST['password']) : 0;

$pass_hash =  md5($password);
$pass_encrypted = consulta_bd("password_salt", "clientes", "email = '$email'", "");

if (!is_array($pass_encrypted[0][0])) {
	$password_final = hash('md5',$pass_hash.$pass_encrypted[0][0]);
	$enter  = consulta_bd("id, nombre", "clientes", "clave ='$password_final' and email ='$email'", "");

	if (is_array($enter)) {
		setcookie("usuario_id", $enter[0][0], time() + (365 * 24 * 60 * 60), "/");

		// Letras para mostrar en el header y no hacer la misma consulta cada vez que se recarga la pagina
		$temp = explode(' ', strtoupper($enter[0][1]));
		$letras = '';
		$contador = 0;
		foreach($temp as $t):
			if ($contador < 2) {
				$letras .= $t[0];
			}
			$contador++;
		endforeach;

		setcookie("letrasNombre", $letras, time() + (365 * 24 * 60 * 60), "/");

		$status = 'success';
		$message = 'Login ok';
	}else{
		$status = 'error';
		$message = 'Datos ingresados incorrectos';
	}

}else{
	$status = 'error';
	$message = 'Email ingresado no se encuentra registrado';
}

$out['status'] = $status;
$out['message'] = $message;

echo json_encode($out);

?>
