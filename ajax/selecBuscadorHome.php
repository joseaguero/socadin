<?php 

include('../admin/conf.php');
include('../admin/includes/tienda/cart/inc/functions.inc.php');

$cadenaAncho 	= (int)mysqli_real_escape_string($conexion, $_POST['cadenaAncho']);
$cadenaPerfil 	= (int)mysqli_real_escape_string($conexion, $_POST['cadenaPerfil']);
$cadenaAro 		= (int)mysqli_real_escape_string($conexion, $_POST['cadenaAro']);

$tipoSelect		= mysqli_real_escape_string($conexion, $_POST['requestType']);

$out['id_hijo']	= 0;
$out['final']	= false;

switch ($tipoSelect) {
	case 'ancho':

		$index = 0;

		$queryPerfil 	= consulta_bd("cadena_perfil", "productos_detalles","cadena_ancho = $cadenaAncho and publicado = 1 GROUP BY cadena_perfil", '');

		foreach ($queryPerfil as $item) {
			$out['selectPerfil'][$index] = $item[0];
			$index++;
		}

		$out['selectAncho'] = 'no_action';
		$out['selectAro'] 	= 'no_action';

	break;
	case 'perfil':

		$index = 0;

		$queryAro 		= consulta_bd("cadena_aro", "productos_detalles","cadena_ancho = $cadenaAncho AND cadena_perfil = $cadenaPerfil and publicado = 1 GROUP BY cadena_aro", '');

		foreach ($queryAro as $item) {
			$out['selectAro'][$index] = $item[0];
			$index++;
		}

		$out['selectPerfil'] 	= "no_action";
		$out['selectAncho'] 	= "no_action";

	break;
	case 'aro':
		$campos = 'pd.id, p.solo_cotizar';
		$from = 'productos p JOIN productos_detalles pd ON p.id = pd.producto_id';

		if (isset($_COOKIE['usuario_id']) AND clienteLista($_COOKIE['usuario_id'])) {
			$campos .= ',lpp.precio, lpp.descuento';
			$from .= ' JOIN lista_de_precio_productos lpp ON lpp.productos_detalle_id = pd.id';
		}else{
			$campos .= ',pd.precio, pd.descuento';
		}

		$query = consulta_bd($campos, $from, "pd.cadena_ancho = $cadenaAncho and pd.cadena_perfil = $cadenaPerfil and pd.cadena_aro = $cadenaAro", '');
		if (is_array($query)) {
			$out['id_hijo'] 		= $query[0][0];
			$out['precio'] 			= $query[0][2];
			$out['descuento'] 		= $query[0][3];
			$out['solo_cotizar'] 	= $query[0][1];
		}

		$out['final'] = true;

		$out['selectAncho'] 	= "no_action";
		$out['selectAro'] 		= "no_action";
		$out['selectPerfil']  	= "no_action";

	break;
}


echo json_encode($out);

?>