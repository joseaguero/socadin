<?php 

include('../admin/conf.php');

$region_id = (int)mysqli_real_escape_string($conexion, $_POST['region_id']);

$query = consulta_bd('id, nombre', 'comunas', "region_id = $region_id", 'nombre asc');

$i = 0;
foreach ($query as $item) {
	$out[$i]['id'] 		= $item[0];
	$out[$i]['nombre'] 	= ucwords(strtolower($item[1]));
	$i++;
}

echo json_encode($out);

?>