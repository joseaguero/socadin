<?php 

include('../../admin/conf.php');

$uid 			= (int)$_COOKIE['usuario_id'];

$nombre 		= mysqli_real_escape_string($conexion, $_POST['nombre']);
$dia 			= mysqli_real_escape_string($conexion, $_POST['dia']);
$mes 			= mysqli_real_escape_string($conexion, $_POST['mes']);
$anio 			= mysqli_real_escape_string($conexion, $_POST['anio']);
$email 			= mysqli_real_escape_string($conexion, $_POST['email']);
$pass_actual 	= mysqli_real_escape_string($conexion, $_POST['actual']);
$pass_nueva 	= mysqli_real_escape_string($conexion, $_POST['nueva']);
$pass_nueva_re 	= mysqli_real_escape_string($conexion, $_POST['renueva']);

$email_actual = consulta_bd('email', 'clientes', "id = $uid", '');

$fecha_nacimiento = ($dia != '' AND $mes != '' AND $anio != '') ? "'{$anio}-{$mes}-{$dia}'" : NULL;
$fecha_hoy = date('Y-m-d H:i:s', time());

if ($email != $email_actual[0][0]) {
	// Consultamos si ya existe el correo registrado
	$queryEmail = consulta_bd('email', 'clientes', "email = '$email'", '');

	if (is_array($queryEmail)) {
		$_SESSION['message_dashboard']['status'] = 'error';
		$_SESSION['message_dashboard']['message'] = 'Email ingresado ya está registrado';
		
		header('Location: cuenta/datos');
	}
}

if ($pass_actual != '') {

	$pass_hash =  md5($pass_actual);
	$pass_encrypted = consulta_bd("password_salt", "clientes", "id = '$uid'", "");

	$password_final = hash('md5',$pass_hash.$pass_encrypted[0][0]);
	$enter  = consulta_bd("id, nombre", "clientes", "id = $uid AND clave ='$password_final'", "");

	if (is_array($enter)) {

		if ($pass_nueva == $pass_nueva_re) {
			$pass_encrypted = md5($pass_nueva);
			$rand = rand(1000, 9999);
		    $password_salt = md5($rand);
		    $password_final = hash('md5',$pass_encrypted.$password_salt);

		    $update = update_bd('clientes',
				"nombre 			= '$nombre',
				email 				= '$email',
				fecha_nacimiento 	= $fecha_nacimiento,
				fecha_modificacion  = '$fecha_hoy',
				clave 				= '$password_final',
				password_salt 		= '$password_salt'",
			"id = $uid");

			if ($update) {
				$_SESSION['message_dashboard']['status'] = 'success';
				$_SESSION['message_dashboard']['message'] = 'Datos actualizados correctamente';
				
				header('Location: cuenta/datos');
			}else{
				$_SESSION['message_dashboard']['status'] = 'error';
				$_SESSION['message_dashboard']['message'] = 'Error al actualizar los datos, favor intentar más tarde';
				
				header('Location: cuenta/datos');
			}

		}else{
			$_SESSION['message_dashboard']['status'] = 'error';
			$_SESSION['message_dashboard']['message'] = 'Las contraseñas no coinciden';
			
			header('Location: cuenta/datos');
		}
		
	}else{
		$_SESSION['message_dashboard']['status'] = 'error';
		$_SESSION['message_dashboard']['message'] = 'La contraseña actual no es correcta';
		
		header('Location: cuenta/datos');
	}
	
}else{
	$update = update_bd('clientes',
				"nombre 			= '$nombre',
				email 				= '$email',
				fecha_nacimiento 	= $fecha_nacimiento,
				fecha_modificacion  = '$fecha_hoy'",
			"id = $uid");
	if ($update) {
		$_SESSION['message_dashboard']['status'] = 'success';
		$_SESSION['message_dashboard']['message'] = 'Datos actualizados correctamente';
		
		header('Location: cuenta/datos');
	}else{
		$_SESSION['message_dashboard']['status'] = 'error';
		$_SESSION['message_dashboard']['message'] = 'Error al actualizar los datos, favor intentar más tarde';
		
		header('Location: cuenta/datos');
	}
}

?>