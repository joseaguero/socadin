<?php 

include('../../admin/conf.php');

$uid = (int)$_COOKIE['usuario_id'];

$region = (int)mysqli_real_escape_string($conexion, $_POST['region_despacho']);
$comuna = (int)mysqli_real_escape_string($conexion, $_POST['comuna_despacho']);
$direccion = mysqli_real_escape_string($conexion, $_POST['direccion']);
$numero = mysqli_real_escape_string($conexion, $_POST['numero']);
$numero_extra = mysqli_real_escape_string($conexion, $_POST['numero_extra']);

$insert = insert_bd('clientes_direcciones', 'cliente_id, region_id, comuna_id, calle, numero, numero_extra', "$uid, $region, $comuna, '$direccion', '$numero', '$numero_extra'");

if ($insert) {
	$_SESSION['message_dashboard']['status'] = 'success';
	$_SESSION['message_dashboard']['message'] = 'Dirección agregada correctamente';
}else{
	$_SESSION['message_dashboard']['status'] = 'error';
	$_SESSION['message_dashboard']['message'] = 'Error al agregar la dirección';
}

header("Location: cuenta/direcciones");

?>