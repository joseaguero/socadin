<?php 

include('../../admin/conf.php');

$id = (int)mysqli_real_escape_string($conexion, $_POST['id']);
$region_id = (int)mysqli_real_escape_string($conexion, $_POST['region_despacho']);
$comuna_id = (int)mysqli_real_escape_string($conexion, $_POST['comuna_despacho']);
$direccion = mysqli_real_escape_string($conexion, $_POST['direccion']);
$numero = mysqli_real_escape_string($conexion, $_POST['numero']);
$numero_extra = mysqli_real_escape_string($conexion, $_POST['numero_extra']);

if ($region_id > 0 AND $comuna_id > 0 AND $direccion != '' AND $numero != '') {
	$update_bd = update_bd('clientes_direcciones',
					"region_id  = $region_id,
					comuna_id	= $comuna_id,
					calle 		= '$direccion',
					numero 		= '$numero',
					numero_extra = '$numero_extra'",
				"id = $id");
	if ($update_bd) {
		$_SESSION['message_dashboard']['status'] = 'success';
		$_SESSION['message_dashboard']['message'] = 'Dirección actualizada correctamente';

		header("Location: cuenta/direcciones");
	}else{
		$_SESSION['message_dashboard']['status'] = 'error';
		$_SESSION['message_dashboard']['message'] = 'Error al actualizar la dirección';

		header("Location: cuenta/direcciones/editar/{$id}");
	}

}else{
	$_SESSION['message_dashboard']['status'] = 'error';
	$_SESSION['message_dashboard']['message'] = 'Debes llenar todos los campos obligatorios';

	header("Location: cuenta/direcciones/editar/{$id}");
}

?>