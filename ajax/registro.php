<?php 
include("../admin/conf.php");
date_default_timezone_set('America/Santiago');

$nombre = (isset($_POST["nombre"])) ? ucwords(mysqli_real_escape_string($conexion, $_POST["nombre"])) : '';
$email = (isset($_POST["email"])) ? strtolower(mysqli_real_escape_string($conexion, $_POST["email"])) : '';
$password = (isset($_POST["password"])) ? mysqli_real_escape_string($conexion, $_POST["password"]) : '';
$repassword = (isset($_POST["repassword"])) ? mysqli_real_escape_string($conexion, $_POST["repassword"]) : '';

$fecha_hoy = date("Y-m-d H:i:s", time());

$existe_cliente = consulta_bd('id', 'clientes', "email = '$email'", '');

if (!is_array($existe_cliente)) {
	if ($nombre != '' AND $email != '' AND $password != '' AND $repassword != '') {
		if ($password == $repassword) {
			$pass_encrypted = md5($password);
			$rand = rand(1000, 9999);
		    $password_salt = md5($rand);
		    $password_final = hash('md5',$pass_encrypted.$password_salt);

		    $insert = insert_bd("clientes", "nombre, email, clave, password_salt, fecha_creacion, activo", "'{$nombre}', '{$email}', '{$password_final}', '{$password_salt}', '{$fecha_hoy}', 1");

		    if ($insert) {
		    	$status = 'success';
		    	$message = 'Cuenta creada correctamente';
		    }else{
		    	$status = 'error';
		    	$message = 'Error al crear la cuenta';
		    }

		}else{
			$status = 'error';
			$message = 'Contraseñas no coinciden';
		}
	}else{
		$status = 'error';
		$message = 'Debes completar todos los campos';
	}
}else{
	$status = 'error';
	$message = 'Correo ingresado ya se encuentra en nuestros registros';
}

$out['status'] = $status;
$out['message'] = $message;

echo json_encode($out);
	
?>