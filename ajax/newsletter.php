<?php 
header('Content-type: text/html; charset=utf-8');
include('../admin/conf.php');
include '../Mailin.php';
include("../admin/includes/tienda/cart/inc/functions.inc.php");
// require '../vendor/autoload.php';
// include("../tienda/mailchimp.class.php");

$correo = (isset($_POST["correo"])) ? mysqli_real_escape_string($conexion, $_POST["correo"]) : 0;

$existe_correo = consulta_bd("email","newsletter","email = '$correo'","");

if(is_array($existe_correo)){
	$out['status'] = 'error';
	$out['message'] = 'Correo ingresado ya se encuentra en nuestros registros';
}else{

	$fecha_hoy = date('Y-m-d H:i:s', time());
	$insert = insert_bd("newsletter","email, fecha_creacion","'$correo', '$fecha_hoy'");

	if ($insert) {

	$nombre_sitio = opciones('nombre_cliente');
	$nombre_corto = strtolower($nombre_sitio);
	$noreply = "no-reply@".$nombre_corto.".cl";
	$url_sitio = opciones('url_sitio');
	$logo = opciones('logo_mail');
	$color_logo = opciones('color_logo');

	$msje = 
	'
	<html>
	<head><link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet"></head>
	<body style="font-family: \'Open Sans\', sans-serif; color: #333;">
	<div style="background: #f9f9f9; width: 100%;">
		<div style="background: #fff; border-top: 2px solid #db4437; border-bottom: 2px solid #db4437; width: 90%;margin: auto; padding: 30px; box-sizing: border-box;">
			<img src="'.$logo.'" alt="'.$nombre_sitio.'">
			<p style="font-size: 14px;margin-bottom:5px;color:'.$color_logo.';"><b>Estimado/a:</b></p>
			<p style="font-size: 13px; margin-top: 0;">Se ha inscrito un nuevo cliente en el newsletter a través de la página web de '.$nombre_sitio.'. <br>
			El correo que fue inscrito es el siguiente:</p>
			<blockquote style="font-size: 13px;">
			<strong>E-mail:</strong> '.$correo.' <br>
			</blockquote>

			<p style="font-size: 13px;">Esperamos tenga un excelente día.<br><br>
			Muchas gracias. <br>
			Atte,<br>
			<b style="color: '.$color_logo.';">Equipo de '.$nombre_sitio.'</b></p>
		</div>
	</div>
	</body>
	</html>';
	$asunto = 'Nuevo suscriptor en Newsletter.';

	$msje_cliente = 
	'
	<html>
	<head><link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet"></head>
	<body style="font-family: \'Open Sans\', sans-serif; color: #333;">
	<div style="background: #f9f9f9; width: 100%;">
		<div style="background: #fff; border-top: 2px solid #db4437; border-bottom: 2px solid #db4437; width: 90%;margin: auto; padding: 30px; box-sizing: border-box;">
			<img src="'.$logo.'" alt="'.$nombre_sitio.'">
			<p style="font-size: 14px;margin-bottom:5px;color:'.$color_logo.';"><b>Estimado/a:</b></p>
			<p style="font-size: 13px; margin-top: 0;">Gracias por inscribirte en el newsletter de '.$nombre_sitio.'<br>
			Nuestro equipo te mantendrá al tanto de las novedades.</p>
			

			<p style="font-size: 13px;">
			Muchas gracias. <br>
			Atte,<br>
			<b style="color: '.$color_logo.';">Equipo de '.$nombre_sitio.'</b></p>
		</div>
	</div>
	</body>
	</html>';
	$asunto_cliente = 'Gracias por suscribirte al newsletter de '. $nombre_sitio;

	$mailin = new Mailin('grupoatlasholding@gmail.com', 'VSU8IBE6jn21kNYJ');
    if(opciones("correo_admin1") != ""){
		$mailin-> addTo(opciones("correo_admin1"),opciones("nombre_correo_admin1"));
	}
	if(opciones("correo_admin2") != ""){
		$mailin-> addTo(opciones("correo_admin2"),opciones("nombre_correo_admin2"));
	}
	if(opciones("correo_admin3") != ""){
		$mailin-> addTo(opciones("correo_admin3"),opciones("nombre_correo_admin3"));
	}

	$mailin->
        setFrom(opciones("setFrom_mail"), opciones("nombre_setFrom_mail"))->
        setSubject("$asunto")->
        setText("$msje")->
            setHtml("$msje");
    $res = $mailin->send();
    $res2 = json_decode($res);

    if ($res2->{'result'} == true) {	
        //envio copia al cliente
		$mailinCliente = new Mailin('grupoatlasholding@gmail.com', 'VSU8IBE6jn21kNYJ');
        $mailinCliente->
            addTo("$correo", "Estimado cliente")->
            setFrom(opciones("setFrom_mail"), opciones("nombre_setFrom_mail"))->
            setSubject("$asunto_cliente")->
            setText("$msje_cliente")->
            setHtml("$msje_cliente");
        $resCliente = $mailinCliente->send();
        $res2Cliente = json_decode($resCliente);
        //fin envio copia al cliente
	        if ($res2Cliente->{'result'} == true) {
	        	$out['status'] = 'success';
				$out['message'] = $correo;
			}else{
				$out['status'] = 'error';
				$out['message'] = 'Error al enviar el correo, pero ya estas suscrito.';
			}
	}
	// $newsletter_list = consulta_bd('id_lista', 'mailchimp', "short_name = 'newsletter'", '');
	// $todos_list = consulta_bd('id_lista', 'mailchimp', "short_name = 'todos_mail'", '');

	// $apikey = opciones('key_mailchimp');

	// if ($apikey) {
	// 	$data['email'] = $correo;
	// 	$data['listId'] = $newsletter_list[0][0];

	// 	$data_all['email'] = $correo;
	// 	$data_all['listId'] = $todos_list[0][0];

	// 	$mcc = new MailChimpClient($apikey);

	// 	$mcc->subscribeNewsletter($data);
	// 	$mcc->subscribeNewsletter($data_all);
	// }		
	}else{
		$out['status'] = 'error';
		$out['message'] = 'Error al ingresar el correo, por favor intentar nuevamente.';
	}
	
}

echo json_encode($out);

?>
