<?php
	include_once("../../admin/conf.php");
	require_once('../../admin/includes/tienda/cart/inc/functions.inc.php');

	$datos 			= $_SESSION['datos'];
	$email 			= mysqli_real_escape_string($conexion, $datos['email']);
	$nombre 		= mysqli_real_escape_string($conexion, $datos['nombre']);
	$telefono 		= mysqli_real_escape_string($conexion, $datos['telefono']);
	$direccion 		= mysqli_real_escape_string($conexion, $datos['direccion']);
	$comentarios 		= mysqli_real_escape_string($conexion, $datos['comentarios']);
	$tipo_despacho 	= mysqli_real_escape_string($conexion, $datos['tipo_despacho']);
	$tienda_id 		= mysqli_real_escape_string($conexion, $datos['tienda']);
	$nombre_retiro 	= mysqli_real_escape_string($conexion, $datos['nombre_retiro']);
	$rut_retiro 	= mysqli_real_escape_string($conexion, $datos['rut_retiro']);
	$region_id 		= mysqli_real_escape_string($conexion, $datos['region']);
	$comuna_id 		= mysqli_real_escape_string($conexion, $datos['comuna']);
	$numero_direccion = mysqli_real_escape_string($conexion, $datos['numero_direccion']);

	$tienda 		= consulta_bd("nombre, direccion, horario","sucursales","id = $tienda_id","");
	$retiro_tienda 	= $tienda[0][0].' '.$tienda[0][1].' '.$tienda[0][2];

	$region = consulta_bd("nombre","regiones","id = $region_id","");
	$comuna = consulta_bd("nombre","comunas","id = $comuna_id","");

	$direccion .= " #".$numero_direccion.", ".ucwords($comuna[0][0]).", ".$region[0][0];

	$factura 			= mysqli_real_escape_string($conexion, $datos['factura']);
	$razon_social 		= mysqli_real_escape_string($conexion, $datos['razon_social']);
	$rut_empresa 		= mysqli_real_escape_string($conexion, $datos['rut_empresa']);
	$giro 				= mysqli_real_escape_string($conexion, $datos['giro']);
	$direccion_empresa 	= mysqli_real_escape_string($conexion, $datos['direccion_empresa']);

	$quien_retira 		= mysqli_real_escape_string($conexion, $datos['quien_retira']);

?>



<h4>Información de compra</h4>
<div class="datos-line">
	<div class="line">
		<div>Email</div> <span> <?= $email; ?></span>
		<a href="#" class="editar-datos">Editar</a>
	</div>
	<div class="line">
		<div>Contacto</div> <span> <?= $nombre; ?></span>
		<a href="#" class="editar-datos">Editar</a>
	</div>
	<div class="line">
		<div>Teléfono</div> <span> <?= $telefono; ?></span>
		<a href="#" class="editar-datos">Editar</a>
	</div>
	<?php if($tipo_despacho == 'domicilio'){ ?>
		<div class="line">
			<div>Dirección envío</div> <span> <?= $direccion; ?></span>
			<a href="#" class="editar-datos">Editar</a>
		</div>
	<?php }else{ ?>
		<div class="line">
			<div>Retiro en tienda</div> <span> <?= $retiro_tienda; ?></span>
			<a href="#" class="editar-datos">Editar</a>
		</div>
		<div class="line">
			<?php if (!$quien_retira): ?>
				<div>Persona para retiro</div> <span> <?= $nombre_retiro; ?></span>
			<?php else: ?>
				<div>Persona para retiro</div> <span> Yo retiro</span>
			<?php endif ?>
			
			<a href="#" class="editar-datos">Editar</a>
		</div>
		<?php if (!$quien_retira): ?>
			<div class="line">
				<div>Rut para retiro</div> <span> <?= $rut_retiro; ?></span>
				<a href="#" class="editar-datos">Editar</a>
			</div>
		<?php endif ?>
	<?php } ?>

	<?php if($factura == 'true'){ ?>
		<div class="line">
			<div>Razón Social</div> <span> <?= $razon_social; ?></span>
			<a href="#" class="editar-datos">Editar</a>
		</div>
		<div class="line">
			<div>Rut Empresa</div> <span> <?= $rut_empresa; ?></span>
			<a href="#" class="editar-datos">Editar</a>
		</div>
		<div class="line">
			<div>Giro</div> <span> <?= $giro; ?></span>
			<a href="#" class="editar-datos">Editar</a>
		</div>
		<div class="line">
			<div>Dirección Facturación</div> <span> <?= $direccion_empresa; ?></span>
			<a href="#" class="editar-datos">Editar</a>
		</div>
	<?php } ?>
</div>
<div class="clearfix"></div>
<h4>Medios de pago</h4>
<form name="formMedios" id="formMedios" method="POST" action="tienda/procesar_compra.php">
	<div class="medios">
		
		<div class="col">
			<div class="radiobtn">
		        <input type="radio" name="metodoPago" id="webpay" value="webpay" checked="checked">
		        <div class="check"></div>
		    </div>
		    <label for="webpay"><img src="img/webpay.png"></label>			
		</div>

		<div class="col">
			<div class="radiobtn">
		        <input type="radio" name="metodoPago" id="mercadopago" value="mercadopago">
		        <div class="check"></div>
		    </div>
		    <label for="mercadopago"><img src="img/mpago.png"></small></label>
		</div>

		<div class="col">
			<div class="radiobtn">
		        <input type="radio" name="metodoPago" id="transferencia" value="transferencia">
		        <div class="check"></div>
		    </div>
		    <label for="transferencia"><img src="img/transferencia.png"></small></label>
		</div>

	</div>
	
	<img src="img/bannermpago.php" width="100%" class="mt-20 mb-20">

	<div class="terminos">
		<div class="campo checkbox chkTerminos">
			<div class="checkbtn">
				<input type="checkbox" name="terminos" id="terminos">
				<span class="checkmark"></span>
			</div>	
			<label for="terminos">Al comprar acepto los Términos y condiciones</label>
		</div>
	</div>
</form>
<div class="clearfix"></div>
<div class="btn pagar">Pagar</div>
<!--<a href="mi-carro" class="ver-carro">Ver mi carro</a>-->


<script type="text/javascript">
	$(document).ready(function(){
		$('.editar-datos').on('click', function(e){
			e.preventDefault();
			$('.cont_loading').fadeIn(100);
			var url 	= $(location).attr('href');
			var url_ 	= url.replace("resumen-compra","envio-y-pago");

			if(url.indexOf("?") != '-1'){
				var url_e 	= url_ + "&e=1";	
			}else{
				var url_e 	= url_ + "?e=1";
			}

			$(location).attr('href', url_e);
			
			/*$('.datos-cargados').slideUp(300, function(){
				$('.datos-cargados').empty();
				$('.datos-compra').slideDown(300, function(){
					loading();
				});
				$('.finalizar-compra').slideUp(300, function(){
					$('.finalizar-compra').empty();
                	$('.resumen-compra').slideDown(300);
                });
			});*/
		});
	});
</script>