<?php
	include_once("../../admin/conf.php");

	$email 	= mysqli_real_escape_string($conexion, $_POST['email']);
	$row 	= consulta_bd("nombre, telefono, razon_social, rut_empresa, giro, direccion_empresa, id","clientes","email = '$email'","");

	if($row){
		$datos['email'] 			= $email;
		$datos['factura'] 			= "boleta";
		$datos['newsletter'] 		= "";
		$datos['nombre'] 			= $row[0][0];
		$datos['telefono'] 			= $row[0][1];
		$datos['tipo_despacho'] 	= "domicilio";
		$datos['region'] 			= "";
		$datos['comuna'] 			= "";
		$datos['direccion'] 		= "";
		$datos['tienda'] 			= "";
		$datos['nombre_retiro'] 	= "";
		$datos['rut_retiro'] 		= "";
		$datos['razon_social'] 		= ""; //$row[0][2];
		$datos['rut_empresa'] 		= ""; //$row[0][3];
		$datos['giro'] 				= ""; //$row[0][4];
		$datos['direccion_empresa'] = ""; //$row[0][5];
		$datos['guardar_info'] 		= "";
		$datos['cliente_id'] 		= $row[0][6];
		$datos['mis_datos'] 		= 1;

		$_SESSION['datos'] = $datos;
		die("SUCCESS");
	}else{

		$datos['email'] 			= $email;
		$datos['factura'] 			= "boleta";
		$datos['newsletter'] 		= "";
		$datos['nombre'] 			= "";
		$datos['telefono'] 			= "";
		$datos['tipo_despacho'] 	= "domicilio";
		$datos['region'] 			= "";
		$datos['comuna'] 			= "";
		$datos['direccion'] 		= "";
		$datos['tienda'] 			= "";
		$datos['nombre_retiro'] 	= "";
		$datos['rut_retiro'] 		= "";
		$datos['razon_social'] 		= "";
		$datos['rut_empresa'] 		= "";
		$datos['giro'] 				= "";
		$datos['direccion_empresa'] = "";
		$datos['guardar_info'] 		= "";
		$datos['cliente_id'] 		= 0;
		$datos['mis_datos'] 		= 1;

		$_SESSION['datos'] = $datos;
		die("ERROR");
	}
?>