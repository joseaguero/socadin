<?php
	include("../../admin/conf.php");
	include("../../funciones.php");
	require_once('../../admin/includes/tienda/cart/inc/functions.inc.php');

	$datos 			= $_SESSION['datos'];
	$tipo_despacho 	= mysqli_real_escape_string($conexion, $datos['tipo_despacho']);
	$metodoDespacho = mysqli_real_escape_string($conexion, $datos['metodoDespacho']);
	$metodoDespacho = ($metodoDespacho == 'estandar') ? 1 : 0;
	if($tipo_despacho == 'domicilio'){
		$comuna_id 		= mysqli_real_escape_string($conexion, $datos['comuna']);
	}else{
		$comuna_id 		= 0;
	}
?>
<div class="resumen-compra resumen-desk">
    <div class="cartResumen">
        <?= ShowResumenCart($comuna_id, $datos['email'], 1, $metodoDespacho, (int)$_GET['resumen']); ?>
    </div>
    <!--<div class="btn ir-al-pago">Siguiente</div>-->
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$('.pagar').on('click', function(e){
			e.preventDefault();
			var formMedios = document.forms['formMedios'];

			if(!$('#terminos').is(':checked')){
				swal('','Debes aceptar los terminos y condiciones','');
				return false;
			}else{
				$('#formMedios').submit();
			}
		});
	});
</script>