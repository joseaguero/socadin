<?php
	include("../../admin/conf.php");
	include("../../funciones.php");
	require_once('../../admin/includes/tienda/cart/inc/functions.inc.php');

	$comuna = ($_GET['comuna']) ? mysqli_real_escape_string($conexion, $_GET['comuna']) : 0;
	$email 	= ($_GET['email']) ? mysqli_real_escape_string($conexion, $_GET['email']) : 0;
	$e 		= ($_GET['e']) ? mysqli_real_escape_string($conexion, $_GET['e']) : 0;
	$tp 	= ($_GET['tp']) ? mysqli_real_escape_string($conexion, $_GET['tp']) : 0;

	echo ShowResumenCart($comuna, $email, $e, $tp);
?>