<?php
	include("../../admin/conf.php");
	include("../../funciones.php");
	require_once('../../admin/includes/tienda/cart/inc/functions.inc.php');

	$datos 			= $_SESSION['datos'];
	
	$tipo_despacho 	= mysqli_real_escape_string($conexion, $datos['tipo_despacho']);

	if($tipo_despacho == 'domicilio'){
		$comuna_id 		= mysqli_real_escape_string($conexion, $datos['comuna']);
	}else{
		$comuna_id 		= 0;
	}

	echo ShowTotalesCart($comuna_id);
?>